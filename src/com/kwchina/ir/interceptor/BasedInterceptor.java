package com.kwchina.ir.interceptor;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.kwchina.ir.dao.MemberInforDao;
import com.kwchina.ir.entity.MemberInfor;

public class BasedInterceptor extends HandlerInterceptorAdapter {

	private static final Logger logger = Logger.getLogger(BasedInterceptor.class);

	@Resource
	private MemberInforDao memberInforDao;

	private boolean requireLogin(String path) {
		if (path.startsWith("/myaccount/") || path.startsWith("/reservation/") || 
			path.startsWith("/restaurant/addfavorite") || path.startsWith("/restaurant/review/add") || 
			path.startsWith("/api/myaccount/") || path.startsWith("/api/reservation/") ) {
			return true;
		} else {
			return false;
		}
	}

	private String fullPath(HttpServletRequest request) {
		String uri = request.getRequestURI();
		String query = request.getQueryString();
		return uri + (query == null ? "" : ("?" + query));
	}

	public String getIpAddr(HttpServletRequest request) {
		String ip = request.getHeader("x-forwarded-for");
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}
		return ip;
	}

	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

		logger.info(">>>>>>> " + getIpAddr(request) + " | " + fullPath(request) + " >>>>>>>");

		MemberInfor member = this.memberInforDao.getLoginMember(request);
		request.setAttribute("_LoginMember", member);
		if (member != null) {
			logger.info("User: " + member.getMemberId());
		}
		if (requireLogin(request.getRequestURI())) {
			if (member == null || member.getPersonalInfor() == null) {
				logger.info("Require login, redirect to /member/loginfirst.htm");
				response.sendRedirect("/member/loginfirst.htm");
				return false;
			} else {
				return true;
			}
		} else {
			return true;
		}
	}

	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object obj,
			ModelAndView modelandview) throws Exception {
		logger.info("<<<<<<< " + fullPath(request) + " <<<<<<<");
	}

	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object obj,
			Exception exception) throws Exception {
		logger.info("======= " + fullPath(request) + " =======");
	}

}
