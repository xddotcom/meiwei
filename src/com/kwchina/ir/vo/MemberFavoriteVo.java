package com.kwchina.ir.vo;

import com.kwchina.ir.vo.BaseVo;

public class MemberFavoriteVo extends BaseVo {

	private Integer favoriteId; // 数据Id

	private Integer memberId; // 会员
	
	private String loginName; // 会员名

	private Integer restaurantId; // 餐厅
	
	private String fullName; // 餐厅名

	private String favoriteTime; // 收藏时间

	public Integer getFavoriteId() {
		return favoriteId;
	}

	public void setFavoriteId(Integer favoriteId) {
		this.favoriteId = favoriteId;
	}

	public Integer getMemberId() {
		return memberId;
	}

	public void setMemberId(Integer memberId) {
		this.memberId = memberId;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public Integer getRestaurantId() {
		return restaurantId;
	}

	public void setRestaurantId(Integer restaurantId) {
		this.restaurantId = restaurantId;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getFavoriteTime() {
		return favoriteTime;
	}

	public void setFavoriteTime(String favoriteTime) {
		this.favoriteTime = favoriteTime;
	}

}
