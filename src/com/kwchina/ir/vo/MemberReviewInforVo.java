package com.kwchina.ir.vo;


public class MemberReviewInforVo extends BaseVo {

	private Integer reviewId; // 数据Id

	private Integer memberId; // 会员

	private String loginName; // 会员名

	private Integer restaurantId; // 餐厅

	private String fullName; // 餐厅名

	private Integer score; // 评价 1- 1星 2- 2星 3- 3星 4- 4星 5- 5星

	private String comments; // 评价内容

	private String reviewTime; // 评价时间

	private Integer isChecked; // 是否审核0- 默认 1- 已通过 2- 未通过

	private Integer environment; // 服务 1星 2星3星4星5- 5星 update 12/25
	private Integer taste; // 服务 1星 2星3星4星5- 5星 update 12/25
	private Integer service; // 服务 1星 2星3星4星5- 5星 update 12/25
	private Integer startPrice; // 起始均价 update 12/25
	private Integer endPrice; // 结束均价 update 12/25

	public Integer getReviewId() {
		return reviewId;
	}

	public void setReviewId(Integer reviewId) {
		this.reviewId = reviewId;
	}

	public Integer getMemberId() {
		return memberId;
	}

	public void setMemberId(Integer memberId) {
		this.memberId = memberId;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public Integer getRestaurantId() {
		return restaurantId;
	}

	public void setRestaurantId(Integer restaurantId) {
		this.restaurantId = restaurantId;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getReviewTime() {
		return reviewTime;
	}

	public void setReviewTime(String reviewTime) {
		this.reviewTime = reviewTime;
	}

	public Integer getIsChecked() {
		return isChecked;
	}

	public void setIsChecked(Integer isChecked) {
		this.isChecked = isChecked;
	}

	public Integer getEnvironment() {
		return environment;
	}

	public void setEnvironment(Integer environment) {
		this.environment = environment;
	}

	public Integer getTaste() {
		return taste;
	}

	public void setTaste(Integer taste) {
		this.taste = taste;
	}

	public Integer getService() {
		return service;
	}

	public void setService(Integer service) {
		this.service = service;
	}

	public Integer getStartPrice() {
		return startPrice;
	}

	public void setStartPrice(Integer startPrice) {
		this.startPrice = startPrice;
	}

	public Integer getEndPrice() {
		return endPrice;
	}

	public void setEndPrice(Integer endPrice) {
		this.endPrice = endPrice;
	}

}
