package com.kwchina.ir.vo;

import com.kwchina.core.sys.CoreConstant;

public class RestaurantInforVo extends BaseVo {
	
	private String address="";
	private String circle ="";
	private Double commissionRate = 0.0;
	private String cuisine="";
	private String description="";
	private String discount="";
	private String discountPic = "";
	private String frontPic = "";
	private String fullName="";
	private Integer isDeleted = CoreConstant.FIELD_FALSE;
	private String mapPic = "";
	private String parking="";
	private Double perBegin = 0.0; // 人均消费起
	private Double perEnd = 0.0; // 人均消费止
	private Integer restaurantId;
	private String restaurantNo="";
	private Double score = 3.0;
	private String shortName="";
	private String telphone = "";
	private String transport="";
	private Integer memberadmin;
	private String fullNameEN = "";
	private String descriptionEN = "";
	private String addressEN = "";
	private String parkingEN="";
	private String discountEN = "";
	private String workinghour="";
	private String workinghourEN = "";
	
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		if (address != null) {
			this.address = address;
		}
	}
	public String getCircle() {
		return circle;
	}
	public void setCircle(String circle) {
		if (circle != null) {
			this.circle = circle;
		}
	}
	public Double getCommissionRate() {
		return commissionRate;
	}
	public void setCommissionRate(Double commissionRate) {
		if (commissionRate != null) {
			this.commissionRate = commissionRate;
		}
	}
	public String getCuisine() {
		return cuisine;
	}
	public void setCuisine(String cuisine) {
		if (cuisine != null) {
			this.cuisine = cuisine;
		}
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		if (description != null) {
			this.description = description;
		}
	}
	public String getDiscount() {
		return discount;
	}
	public void setDiscount(String discount) {
		if (discount != null) {
			this.discount = discount;
		}
	}
	public String getDiscountPic() {
		return discountPic;
	}
	public void setDiscountPic(String discountPic) {
		if (discountPic != null) {
			this.discountPic = discountPic;
		}
	}
	public String getFrontPic() {
		return frontPic;
	}
	public void setFrontPic(String frontPic) {
		if (frontPic != null) {
			this.frontPic = frontPic;
		}
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		if (fullName != null) {
			this.fullName = fullName;
		}
	}
	public Integer getIsDeleted() {
		return isDeleted;
	}
	public void setIsDeleted(Integer isDeleted) {
		if (isDeleted != null) {
			this.isDeleted = isDeleted;
		}
	}
	public String getMapPic() {
		return mapPic;
	}
	public void setMapPic(String mapPic) {
		if (mapPic != null) {
			this.mapPic = mapPic;
		}
	}
	public String getParking() {
		return parking;
	}
	public void setParking(String parking) {
		if (parking != null) {
			this.parking = parking;
		}
	}
	public Double getPerBegin() {
		return perBegin;
	}
	public void setPerBegin(Double perBegin) {
		if (perBegin != null) {
			this.perBegin = perBegin;
		}
	}
	public Double getPerEnd() {
		return perEnd;
	}
	public void setPerEnd(Double perEnd) {
		if (perEnd != null) {
			this.perEnd = perEnd;
		}
	}
	public Integer getRestaurantId() {
		return restaurantId;
	}
	public void setRestaurantId(Integer restaurantId) {
		if (restaurantId != null) {
			this.restaurantId = restaurantId;
		}
	}
	public String getRestaurantNo() {
		return restaurantNo;
	}
	public void setRestaurantNo(String restaurantNo) {
		if (restaurantNo != null) {
			this.restaurantNo = restaurantNo;
		}
	}
	public Double getScore() {
		return score;
	}
	public void setScore(Double score) {
		if (score != null) {
			this.score = score;
		}
	}
	public String getShortName() {
		return shortName;
	}
	public void setShortName(String shortName) {
		if (shortName != null) {
			this.shortName = shortName;
		}
	}
	public String getTelphone() {
		return telphone;
	}
	public void setTelphone(String telphone) {
		if (telphone != null) {
			this.telphone = telphone;
		}
	}
	public String getTransport() {
		return transport;
	}
	public void setTransport(String transport) {
		if (transport != null) {
			this.transport = transport;
		}
	}
	public Integer getMemberadmin() {
		return memberadmin;
	}
	public void setMemberadmin(Integer memberadmin) {
		if (memberadmin != null) {
			this.memberadmin = memberadmin;
		}
	}
	public String getFullNameEN() {
		return fullNameEN;
	}
	public void setFullNameEN(String fullNameEN) {
		if (fullNameEN != null) {
			this.fullNameEN = fullNameEN;
		}
	}
	public String getDescriptionEN() {
		return descriptionEN;
	}
	public void setDescriptionEN(String descriptionEN) {
		if (descriptionEN != null) {
			this.descriptionEN = descriptionEN;
		}
	}
	public String getAddressEN() {
		return addressEN;
	}
	public void setAddressEN(String addressEN) {
		if (addressEN != null) {
			this.addressEN = addressEN;
		}
	}
	public String getParkingEN() {
		return parkingEN;
	}
	public void setParkingEN(String parkingEN) {
		if (parkingEN != null) {
			this.parkingEN = parkingEN;
		}
	}
	public String getDiscountEN() {
		return discountEN;
	}
	public void setDiscountEN(String discountEN) {
		if (discountEN != null) {
			this.discountEN = discountEN;
		}
	}
	public String getWorkinghour() {
		return workinghour;
	}
	public void setWorkinghour(String workinghour) {
		if (workinghour != null) {
			this.workinghour = workinghour;
		}
	}
	public String getWorkinghourEN() {
		return workinghourEN;
	}
	public void setWorkinghourEN(String workinghourEN) {
		if (workinghourEN != null) {
			this.workinghourEN = workinghourEN;
		}
	}
	

}
