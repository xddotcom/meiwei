package com.kwchina.ir.vo.api;

public class ApiBookTimeVo {
	
	private String optionTitle;
	
	private String optionValue;

	public String getOptionTitle() {
		return optionTitle;
	}

	public void setOptionTitle(String optionTitle) {
		if (optionTitle != null) {
			this.optionTitle = optionTitle;
		}
	}

	public String getOptionValue() {
		return optionValue;
	}

	public void setOptionValue(String optionValue) {
		if (optionValue != null) {
			this.optionValue = optionValue;
		}
	}

	public ApiBookTimeVo() {
		super();
	}

	public ApiBookTimeVo(String optionTitle, String optionValue) {
		super();
		this.optionTitle = optionTitle;
		this.optionValue = optionValue;
	}
	
	
	
	
}
