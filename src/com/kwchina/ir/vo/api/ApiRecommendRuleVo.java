package com.kwchina.ir.vo.api;

import java.util.ArrayList;
import java.util.List;

import com.kwchina.ir.entity.RestaurantInfor;

public class ApiRecommendRuleVo {
	private Integer ruleId;
	private String ruleName;
	private List<RestaurantInfor> restaurants = new ArrayList<RestaurantInfor>();

	public Integer getRuleId() {
		return ruleId;
	}

	public void setRuleId(Integer ruleId) {
		if (ruleId != null) {
			this.ruleId = ruleId;
		}
	}

	public String getRuleName() {
		return ruleName;
	}

	public void setRuleName(String ruleName) {
		if (ruleName != null) {
			this.ruleName = ruleName;
		}
	}

	public List<RestaurantInfor> getRestaurants() {
		return restaurants;
	}

	public void setRestaurants(List<RestaurantInfor> restaurants) {
		if (restaurants != null) {
			this.restaurants = restaurants;
		}
	}

	public ApiRecommendRuleVo() {
	}

	public ApiRecommendRuleVo(Integer ruleId, String ruleName, List<RestaurantInfor> restaurants) {
		super();
		this.ruleId = ruleId;
		this.ruleName = ruleName;
		this.restaurants = restaurants;
	}
}
