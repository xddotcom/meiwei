package com.kwchina.ir.vo.api;

public class ApiRestaurantVo {
	private String retaurantNo = "";
	private String name = "";
	private String shortName = "";
	private String fullName = "";
	private String picture = "";
	private Integer restaurantId;

	public String getRetaurantNo() {
		return retaurantNo;
	}

	public void setRetaurantNo(String retaurantNo) {
		if (retaurantNo != null) {
			this.retaurantNo = retaurantNo;
		}
	}

	public String getName() {

		if (getShortName() != null) {
			return getShortName().length() < 20 ? getShortName() : getShortName().substring(0, 19);
		} else if (getFullName() != null) {
			return getFullName().length() < 20 ? getFullName() : getFullName().substring(0, 19);
		}

		return name;
	}

	public void setName(String name) {
		if (name != null) {
			this.name = name;
		}
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		if (shortName != null) {
			this.shortName = shortName;
		}
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		if (fullName != null) {
			this.fullName = fullName;
		}
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		if (picture != null) {
			this.picture = picture;
		}
	}

	
	
	public Integer getRestaurantId() {
		return restaurantId;
	}

	public void setRestaurantId(Integer restaurantId) {
		if (restaurantId != null) {
			this.restaurantId = restaurantId;
		}
	}

	public ApiRestaurantVo() {
	}

	
	
	public ApiRestaurantVo(Integer restaurantId , String fullName) {
		super();
		this.fullName = fullName;
		this.restaurantId = restaurantId;
	}

	public ApiRestaurantVo(String retaurantNo,String fullName,  String shortName, String picture) {
		super();
		this.retaurantNo = retaurantNo;
		this.shortName = shortName;
		this.fullName = fullName;
		this.picture = picture;
	}
}
