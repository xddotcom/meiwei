package com.kwchina.ir.vo;

import java.util.Date;

public class MemberCreditInforVo extends BaseVo {
	private Integer creditId;
	private Integer amount = 0;
	private String reason = "";
	private Integer memberId = null;
	private Date happendTime = new Date(System.currentTimeMillis());
	 //1(new user) 2(order) 3(order on reg day) 4(2 consumes in a week) 5(5 consumes in a month)
	//6 积分兑换
	private Integer creditType = 0;
	public Integer getCreditId() {
		return creditId;
	}
	public void setCreditId(Integer creditId) {
		if (creditId != null) {
			this.creditId = creditId;
		}
	}
	public Integer getAmount() {
		return amount;
	}
	public void setAmount(Integer amount) {
		if (amount != null) {
			this.amount = amount;
		}
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		if (reason != null) {
			this.reason = reason;
		}
	}
	public Integer getMemberId() {
		return memberId;
	}
	public void setMemberId(Integer memberId) {
		if (memberId != null) {
			this.memberId = memberId;
		}
	}
	public Date getHappendTime() {
		return happendTime;
	}
	public void setHappendTime(Date happendTime) {
		if (happendTime != null) {
			this.happendTime = happendTime;
		}
	}
	public Integer getCreditType() {
		return creditType;
	}
	public void setCreditType(Integer creditType) {
		if (creditType != null) {
			this.creditType = creditType;
		}
	}
	
	
}
