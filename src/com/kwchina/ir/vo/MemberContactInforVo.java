package com.kwchina.ir.vo;

import com.kwchina.ir.vo.BaseVo;

public class MemberContactInforVo extends BaseVo {

	private Integer contactId;// 联络人id

	private Integer memberId;

	private String name;

	private String telphone;

	private String email;

	private Integer sexe;
	
	public Integer getContactId() {
		return contactId;
	}

	public void setContactId(Integer contactId) {
		this.contactId = contactId;
	}

	public Integer getMemberId() {
		return memberId;
	}

	public void setMemberId(Integer memberId) {
		this.memberId = memberId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTelphone() {
		return telphone;
	}

	public void setTelphone(String telphone) {
		this.telphone = telphone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getSexe() {
		return sexe;
	}

	public void setSexe(Integer sexe) {
		this.sexe = sexe;
	}
	
}
