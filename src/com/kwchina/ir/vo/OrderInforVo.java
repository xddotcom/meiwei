package com.kwchina.ir.vo;

import com.kwchina.core.sys.CoreConstant;

public class OrderInforVo extends RestaurantInforVo {

	private Integer orderId; // 订单Id

	private String orderNo; // 订单号

	private Integer memberId; // 会员

	private String loginName; // 会员名

	private Integer restaurantId; // 餐厅

	private String fullName;

	private String orderSubmitTime; // 订单时间

	private String orderDate; // 就餐时间（天）

	private String orderTime; // 就餐时间（天）

	private Integer personNum; // 就餐人数

	private String other; // 其它要求

	private Integer status; // 状态 0- 新订单 1- 订单取消 2- 已确认桌位的订单 3- 已输入消费金额的订单 4-
						// 确认消费金额的订 5- 已完成的订单

	private Double consumeAmount; // 消费价格

	private Double commission; // 佣金

	private Double payedCommission; // 支付佣金

	private Integer payedStatus;

	private String cityName;// 餐厅地区

	private String cuisine;// 菜系

	private Integer orderStatus;// 订单状态查询

	private String startDate;// 订单查询起始时间

	private String endDate;// 订单查询结束时间

	private String tables;

	private Integer contactId;

	private String adminother;

	private String inviteFriends = "";//邀请就餐 id1|id2...
	
	private String productIds = "";
	
	
	private String contactName = "";
	private Integer contactSexe = CoreConstant.SEX_MALE;
	private String contactTelphone = "";
	
	public Integer getContactId() {
		return contactId;
	}

	public void setContactId(Integer contactId) {
		this.contactId = contactId;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public Integer getOrderId() {
		return orderId;
	}

	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public Integer getMemberId() {
		return memberId;
	}

	public void setMemberId(Integer memberId) {
		this.memberId = memberId;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public Integer getRestaurantId() {
		return restaurantId;
	}

	public void setRestaurantId(Integer restaurantId) {
		this.restaurantId = restaurantId;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getSubmitTime() {
		return orderSubmitTime;
	}

	public void setSubmitTime(String orderSubmitTime) {
		this.orderSubmitTime = orderSubmitTime;
	}

	public String getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}

	public Integer getPersonNum() {
		return personNum;
	}

	public void setPersonNum(Integer personNum) {
		this.personNum = personNum;
	}

	public String getOther() {
		return other;
	}

	public void setOther(String other) {
		this.other = other;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Double getConsumeAmount() {
		return consumeAmount;
	}

	public void setConsumeAmount(Double consumeAmount) {
		this.consumeAmount = consumeAmount;
	}

	public Double getCommission() {
		return commission;
	}

	public void setCommission(Double commission) {
		this.commission = commission;
	}

	public Double getPayedCommission() {
		return payedCommission;
	}

	public void setPayedCommission(Double payedCommission) {
		this.payedCommission = payedCommission;
	}

	public Integer getPayedStatus() {
		return payedStatus;
	}

	public void setPayedStatus(Integer payedStatus) {
		this.payedStatus = payedStatus;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getCuisine() {
		return cuisine;
	}

	public void setCuisine(String cuisine) {
		this.cuisine = cuisine;
	}

	public Integer getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(Integer orderStatus) {
		this.orderStatus = orderStatus;
	}

	public String getTables() {
		return tables;
	}

	public void setTables(String tables) {
		this.tables = tables;
	}

	public String getOrderTime() {
		return orderTime;
	}

	public void setOrderTime(String orderTime) {
		this.orderTime = orderTime;
	}

	public String getAdminother() {
		return adminother;
	}

	public void setAdminother(String adminother) {
		this.adminother = adminother;
	}

	public String getInviteFriends() {
		return inviteFriends;
	}

	public void setInviteFriends(String inviteFriends) {
		if (inviteFriends != null) {
			this.inviteFriends = inviteFriends;
		}
	}

	public String getProductIds() {
		return productIds;
	}

	public void setProductIds(String productIds) {
		if (productIds != null) {
			this.productIds = productIds;
		}
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		if (contactName != null) {
			this.contactName = contactName;
		}
	}

	public Integer getContactSexe() {
		return contactSexe;
	}

	public void setContactSexe(Integer contactSexe) {
		if (contactSexe != null) {
			this.contactSexe = contactSexe;
		}
	}
	
	public String getContactTelphone() {
		return contactTelphone;
	}

	public void setContactTelphone(String contactTelphone) {
		if (contactTelphone != null) {
			this.contactTelphone = contactTelphone;
		}
	}

	
	
}
