package com.kwchina.ir.vo;

import com.kwchina.core.sys.CoreConstant;

public class MemberInforVo extends BaseVo {

	private Integer memberId; // 会员Id
	private Integer personalId; // 详细Id
	private String loginName = null; // 登录名
	private String loginPassword = null; // 密码
	private Integer memberType = CoreConstant.MEMBERTYPE_PERSONAL;
	private String registerTime; // 注册时间
	private String lastTime; // 最后登录时间
	private Integer isDeleted = CoreConstant.VALID; // 是否有效（0-无，1-有）
	private String nickName; // 昵称
	private String invitationCode; // 邀请码
	private String email = null; // 电子邮箱
	private String mobile = null; // 手机号码
	private String birthday; // 出生日期
	private String memberNo; // 会员编号
	private String anniversary; // 重要纪念日
	private Float point; // 会员积分
	private String registerMonth; // 注册月份
	private String startDate;// 订单查询起始时间
	private String endDate;// 订单查询结束时间
	private Integer sexe;//0:先生1：小姐
	private String newPassword;
	
	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public Integer getMemberId() {
		return memberId;
	}

	public void setMemberId(Integer memberId) {
		this.memberId = memberId;
	}

	public Integer getPersonalId() {
		return personalId;
	}

	public void setPersonalId(Integer personalId) {
		this.personalId = personalId;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		if (loginName != null && loginName.trim()!="") {
			this.loginName = loginName;
		}
	}

	public String getLoginPassword() {
		return loginPassword;
	}

	public void setLoginPassword(String loginPassword) {
		if (loginPassword != null && loginPassword.trim()!="") {
			this.loginPassword = loginPassword;
		}
	}

	public Integer getMemberType() {
		return memberType;
	}

	public void setMemberType(Integer memberType) {
		this.memberType = memberType;
	}

	public String getRegisterTime() {
		return registerTime;
	}

	public void setRegisterTime(String registerTime) {
		this.registerTime = registerTime;
	}

	public String getLastTime() {
		return lastTime;
	}

	public void setLastTime(String lastTime) {
		this.lastTime = lastTime;
	}

	public Integer getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Integer isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getInvitationCode() {
		return invitationCode;
	}

	public void setInvitationCode(String invitationCode) {
		this.invitationCode = invitationCode;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		if (email != null && email.trim()!="") {
			this.email = email;
		}
	}
	
	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		if (mobile != null && mobile.trim()!="") {
			this.mobile = mobile;
		}
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getMemberNo() {
		return memberNo;
	}

	public void setMemberNo(String memberNo) {
		this.memberNo = memberNo;
	}

	public String getAnniversary() {
		return anniversary;
	}

	public void setAnniversary(String anniversary) {
		this.anniversary = anniversary;
	}

	public float getPoint() {
		return point;
	}

	public void setPoint(float point) {
		this.point = point;
	}

	public String getRegisterMonth() {
		return registerMonth;
	}

	public void setRegisterMonth(String registerMonth) {
		this.registerMonth = registerMonth;
	}

	public Integer getSexe() {
		return sexe;
	}

	public void setSexe(Integer sexe) {
		this.sexe = sexe;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}
}
