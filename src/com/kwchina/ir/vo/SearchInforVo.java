package com.kwchina.ir.vo;

import java.sql.Date;

import com.kwchina.core.util.DateConverter;
import com.kwchina.ir.util.StringHelper;
import com.kwchina.ir.vo.BaseVo;

public class SearchInforVo extends BaseVo {

	private String searchKey;

	private Integer cuisineId;

	private Integer circleId;

	private String date;

	private String time;

	private Integer ruleId;

	public String getSearchKey() {
		return searchKey;
	}

	public void setSearchKey(String searchKey) {
		this.searchKey = searchKey;
	}

	public Integer getCuisineId() {
		return cuisineId;
	}

	public void setCuisineId(Integer cuisineId) {
		this.cuisineId = cuisineId;
	}

	public Integer getCircleId() {
		return circleId;
	}

	public void setCircleId(Integer circleId) {
		this.circleId = circleId;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getQueryDate() {

		if (StringHelper.isEmpty(date)) {
			date = DateConverter.getDateString(new Date(System
					.currentTimeMillis()));
			return null;
		}
		if (StringHelper.isEmpty(time)) {
			return date + " 00:00:00";
		} else {
			return date + " " + time + ":00";
		}
		// SimpleDateFormat format= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		// try {
		// return new Date(format.parse( date +" "+time ).getTime());
		// } catch (ParseException e) {
		// }
		// return new Date(System.currentTimeMillis());
	}


	public Integer getRuleId() {
		return ruleId;
	}

	public void setRuleId(Integer ruleId) {
		this.ruleId = ruleId;
	}

	public static void main(String[] args) {
		System.out.println(new SearchInforVo().getQueryDate());
	}
}
