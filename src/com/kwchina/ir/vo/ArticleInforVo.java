package com.kwchina.ir.vo;


import com.kwchina.ir.vo.BaseVo;

public class ArticleInforVo  extends BaseVo{

	private Integer articleId; // 文章Id

	private Integer isTop; // 是否置顶 0- 不置顶1- 置顶

	private Integer isPublish; // 是否发布 0- 不发布1- 发布

	private String title; // 标题

	private String subtitle; // 副标题

	private String author; // 作者

	private String content; // 内容
	
	private String cssStyle;

	private String submitTime; // 提交时间

	private Integer categoryId; // 栏目ID
	
	private String categoryName; // 栏目name

	private Integer visitCount; // 访问次数

	private String pictureFilePath; // 首图

	private String accessoryName; // 附件名称

	private String accessoryFilePath; // 附件文件路径

	private Integer isChecked; // 是否审核 0- 未审核 1- 通过审核 2- 未通过审核
	
	private String pictureFile;//图片附件
	
	private String accesslryFile;//文件附件

	public Integer getArticleId() {
		return articleId;
	}

	public void setArticleId(Integer articleId) {
		this.articleId = articleId;
	}

	public Integer getIsTop() {
		return isTop;
	}

	public void setIsTop(Integer isTop) {
		this.isTop = isTop;
	}

	public Integer getIsPublish() {
		return isPublish;
	}

	public void setIsPublish(Integer isPublish) {
		this.isPublish = isPublish;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSubtitle() {
		return subtitle;
	}

	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getSubmitTime() {
		return submitTime;
	}

	public void setSubmitTime(String submitTime) {
		this.submitTime = submitTime;
	}

	public Integer getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public Integer getVisitCount() {
		return visitCount;
	}

	public void setVisitCount(Integer visitCount) {
		this.visitCount = visitCount;
	}

	public String getPictureFilePath() {
		return pictureFilePath;
	}

	public void setPictureFilePath(String pictureFilePath) {
		this.pictureFilePath = pictureFilePath;
	}

	public String getAccessoryName() {
		return accessoryName;
	}

	public void setAccessoryName(String accessoryName) {
		this.accessoryName = accessoryName;
	}

	public String getAccessoryFilePath() {
		return accessoryFilePath;
	}

	public void setAccessoryFilePath(String accessoryFilePath) {
		this.accessoryFilePath = accessoryFilePath;
	}

	public Integer getIsChecked() {
		return isChecked;
	}

	public void setIsChecked(Integer isChecked) {
		this.isChecked = isChecked;
	}

	public String getPictureFile() {
		return pictureFile;
	}

	public void setPictureFile(String pictureFile) {
		this.pictureFile = pictureFile;
	}

	public String getAccesslryFile() {
		return accesslryFile;
	}

	public void setAccesslryFile(String accesslryFile) {
		this.accesslryFile = accesslryFile;
	}

	
	public String getCssStyle() {
		return cssStyle;
	}

	public void setCssStyle(String cssStyle) {
		this.cssStyle = cssStyle;
	}
}
