package com.kwchina.ir.vo;

import java.util.Date;

import com.kwchina.core.sys.CoreConstant;

public class BaseContactInforVo extends BaseVo {

	private Integer contactId;
	private String contactName = "";
	private Integer contactType = CoreConstant.CONTACT_TYPE_MEMBER;
	private String email = "";
	private Date messageDate = new Date(System.currentTimeMillis());
	private String mobile = "";
	private String remark = "";
	
	public Integer getContactId() {
		return contactId;
	}
	public void setContactId(Integer contactId) {
		this.contactId = contactId;
	}
	public String getContactName() {
		return contactName;
	}
	public void setContactName(String contactName) {
		this.contactName = contactName;
	}
	public Integer getContactType() {
		return contactType;
	}
	public void setContactType(Integer contactType) {
		this.contactType = contactType;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Date getMessageDate() {
		return messageDate;
	}
	public void setMessageDate(Date messageDate) {
		this.messageDate = messageDate;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	 
	
}
