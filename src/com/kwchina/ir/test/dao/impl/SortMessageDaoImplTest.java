package com.kwchina.ir.test.dao.impl;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kwchina.core.common.BasicDaoImpl;
import com.kwchina.ir.entity.SortMessagesEntity;
import com.kwchina.ir.test.dao.SortMessageDaoTest;

@SuppressWarnings("unchecked")
@Repository
@Transactional(rollbackFor=Exception.class)
public class SortMessageDaoImplTest extends BasicDaoImpl<SortMessagesEntity> implements SortMessageDaoTest {

	public void e() {
		try {
			this.save(new SortMessagesEntity());
			SortMessagesEntity s = new SortMessagesEntity();
			s.setStatus( Integer.parseInt( "a"));
			this.save(s);
		} catch (Exception e) {
			throw new RuntimeException( "ERROR");
		}
		 
	}

}
