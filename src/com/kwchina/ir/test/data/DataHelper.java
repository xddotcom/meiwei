package com.kwchina.ir.test.data;

import java.io.File;
import java.io.FileInputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import jxl.Sheet;
import jxl.Workbook;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.collections.CollectionUtils;
import org.hibernate.Query;
import org.junit.Test;

import com.kwchina.ir.entity.ArticleInfor;
import com.kwchina.ir.entity.BaseCircleInfor;
import com.kwchina.ir.entity.BaseCityInfor;
import com.kwchina.ir.entity.BaseCuisineInfor;
import com.kwchina.ir.entity.BaseDistrictInfor;
import com.kwchina.ir.entity.MemberInfor;
import com.kwchina.ir.entity.MemberPersonalInfor;
import com.kwchina.ir.entity.RecommendInfor;
import com.kwchina.ir.entity.RecommendRule;
import com.kwchina.ir.entity.RestaurantInfor;
import com.kwchina.ir.entity.RestaurantMenuInfor;
import com.kwchina.ir.entity.RestaurantPicInfor;
import com.kwchina.ir.entity.RestaurantTablePicInfor;
import com.kwchina.ir.entity.RestaurantTimeInfor;
import com.kwchina.ir.entity.TextInfor;
import com.kwchina.ir.test.AbstractTest;
import com.kwchina.ir.util.PrefixHelper;

@SuppressWarnings({"unused", "unchecked", "rawtypes"})
public class DataHelper extends AbstractTest {

	// private static String WEB_ROOT = "D:\\HOME\\Workspace\\meiwei\\web\\";
	// private static String DEFAULT_PATH = "D:\\Dropbox\\meiwei\\餐厅物料录入\\";
	// private static String PATH = "D:\\Dropbox\\meiwei\\餐厅数据.xls";

	private static String WEB_ROOT = "F:\\services\\apache-tomcat-6.0.36\\webapps\\ROOT\\";
	private static String DEFAULT_PATH = "F:\\programe\\Dropbox\\meiwei\\餐厅物料录入\\";
	private static String PATH = "F:\\programe\\Dropbox\\meiwei\\餐厅数据.xls";

	private static String FONT_MAP = "餐厅地图";
	private static String FONT_TABLE = "餐厅桌位图";
	private static String FONT_PICTURE = "餐厅内外景";
	private static String FONT_SMALL = "餐厅小图";
	private static String FONT_ITEM = "特色菜";

	/**
	 * 文件读取模式 ORDER/MAP
	 */
	private static String GRAP_MODE = "MAP";

	private static Map PICTURE_MAP = new HashMap();

	@Test
	public void init() throws Exception {

		initMap();

		article();

		city();

		cuisine();

		modifyValue();

		recommend();

		time();

		defaultUser();

	}

	private void initMap() {

		List<RestaurantMenuInfor> menus = getSession().createQuery("FROM RestaurantMenuInfor")
				.list();
		List<RestaurantPicInfor> pics = getSession().createQuery("FROM RestaurantPicInfor").list();
		List<RestaurantTablePicInfor> tables = getSession().createQuery(
				"FROM RestaurantTablePicInfor").list();

		for (RestaurantPicInfor e : pics) {
			PICTURE_MAP.put(e.getBigPath(), e.getPicId());
		}
		for (RestaurantMenuInfor e : menus) {
			PICTURE_MAP.put(e.getMenuPic(), e.getMenuName());
		}
		for (RestaurantTablePicInfor e : tables) {
			PICTURE_MAP.put(e.getTablePicPath(), e.getPicName());
		}

	}

	private void defaultUser() {

		MemberInfor m = (MemberInfor) getUniqueResult("from MemberInfor where loginName=?",
				new Object[] { "test007" });
		if (m == null) {
			MemberPersonalInfor mp = new MemberPersonalInfor();
			mp.setBirthday(new java.sql.Date(System.currentTimeMillis() - 1000 * 60));
			mp.setCredit(0);
			mp.setEmail("default@default.default");
			mp.setMemberNo("521444444");
			mp.setMobile("11111111111");
			mp.setNickName("无效目标");
			mp.setSexe(0);

			m = new MemberInfor();
			m.setLastTime(new Date(System.currentTimeMillis()));
			m.setIsDeleted(0);
			m.setLoginName("test007");
			m.setLoginPassword("111111");
			m.setMemberType(0);
			m.setRegisterTime(new Date(System.currentTimeMillis() - 1000 * 50));
			m.setPersonalInfor(mp);
			mp.setMember(m);
			getSession().save(mp);
		}
	}

	public void time() throws Exception {
		Workbook book = Workbook.getWorkbook(new FileInputStream(new File(PATH)));
		Sheet sheet = book.getSheet(5);
		int rows = sheet.getRows();
		for (int i = 1; i < rows; i++) {
			try {
				RestaurantInfor restaurant = (RestaurantInfor) getUniqueResult(
						"from RestaurantInfor where fullName.chinese=?",
						new Object[] { valueOf(sheet.getCell(0, i).getContents()) });
				Pattern p = Pattern.compile("^(1[0-9]|0?[1-9]|2[0123]){1}:([0-5][0-9]){1}$");
				String sd = valueOf(sheet.getCell(3, i).getContents());
				String ed = valueOf(sheet.getCell(4, i).getContents());

				Matcher m1 = p.matcher(sd);
				Matcher m2 = p.matcher(ed);

				if (restaurant != null && m1.find() && m2.find()) {
					RestaurantTimeInfor time = new RestaurantTimeInfor();
					time.setTimeName(new TextInfor(valueOf(sheet.getCell(1, i).getContents()),
							valueOf(sheet.getCell(2, i).getContents())));
//					time.setStartTime(sd);
//					time.setEndTime(ed);
					time.setRestaurant(restaurant);
					getSession().save(time);
				}

			} catch (Exception e) {
				logger.debug("时间段录入：行：" + i + "ERROR:(");
				e.printStackTrace();
			}

		}
	}

	public void article() throws Exception {
		Workbook book = Workbook.getWorkbook(new FileInputStream(new File(PATH)));
		Sheet sheet = book.getSheet(4);
		int rows = sheet.getRows();

		if (CollectionUtils.isEmpty(getSession().createQuery(" FROM ArticleInfor ").list())) {
			for (int i = 1; i < rows; i++) {
				try {
					ArticleInfor article = new ArticleInfor();
					article.setContent(valueOf(sheet.getCell(0, i).getContents()));
					article.setSubmitTime(new Date(System.currentTimeMillis()));
					getSession().save(article);
				} catch (Exception e) {
					logger.debug("公告信息录入：行：" + i + "ERROR:(");
					e.printStackTrace();
				}
			}
		}
	}

	public void recommend() throws Exception {
		Workbook book = Workbook.getWorkbook(new FileInputStream(new File(PATH)));
		Sheet sheet = book.getSheet(3);
		int rows = sheet.getRows();
		for (int i = 1; i < rows; i++) {

			try {
				RestaurantInfor restaurant = (RestaurantInfor) getUniqueResult(
						"from RestaurantInfor where fullName.chinese=?",
						new Object[] { valueOf(sheet.getCell(0, i).getContents()) });

				if (restaurant != null) {
					RecommendRule rule = (RecommendRule) getUniqueResult(
							"from RecommendRule where ruleName.chinese=?",
							new Object[] { valueOf(sheet.getCell(1, i).getContents()) });

					if (rule == null) {
						rule = new RecommendRule();
						rule.setRuleName(new TextInfor(valueOf(sheet.getCell(1, i).getContents()),
								valueOf(sheet.getCell(2, i).getContents())));
						rule.setIsRecommended(1);
						rule.setRuleOrder(1000);
						getSession().save(rule);
					}

					RecommendInfor recommend = (RecommendInfor) getUniqueResult(
							"from RecommendInfor where rankRule.ruleId=? and restaurant.restaurantId=?",
							new Object[] { rule.getRuleId(), restaurant.getRestaurantId() });
					if (recommend == null) {
						recommend = new RecommendInfor();
						recommend.setRankRule(rule);
						recommend.setRestaurant(restaurant);
						recommend.setRank(1000);
						getSession().save(recommend);
					}
				}
			} catch (Exception e) {
				logger.debug("推荐录入：行：" + i + "ERROR:(");
				e.printStackTrace();
			}
		}
	}

	public void cuisine() throws Exception {
		Workbook book = Workbook.getWorkbook(new FileInputStream(new File(PATH)));
		Sheet sheet = book.getSheet(1);
		int rows = sheet.getRows();
		for (int i = 1; i < rows; i++) {
			BaseCuisineInfor cuisine = (BaseCuisineInfor) getUniqueResult(
					"from BaseCuisineInfor where cuisineName.chinese=?",
					new Object[] { valueOf(sheet.getCell(1, i).getContents()) });
			if (cuisine == null) {
				cuisine = new BaseCuisineInfor();
				cuisine.setCuisineName(new TextInfor(valueOf(sheet.getCell(1, i).getContents()),
						valueOf(sheet.getCell(0, i).getContents())));
				cuisine.setCuisineType(Integer.parseInt(valueOf(sheet.getCell(2, i).getContents())));
				cuisine.setIsRecommended(Integer
						.parseInt(valueOf(sheet.getCell(4, i).getContents())));
				cuisine.setRank(Integer.parseInt(valueOf(sheet.getCell(3, i).getContents())));
				getSession().save(cuisine);
			}
		}
	}

	public void city() throws Exception {
		Workbook book = Workbook.getWorkbook(new FileInputStream(new File(PATH)));
		Sheet sheet = book.getSheet(2);
		int rows = sheet.getRows();
		for (int i = 1; i < rows; i++) {
			BaseCityInfor city = (BaseCityInfor) getUniqueResult(
					"from BaseCityInfor where cityName.chinese=?", new Object[] { valueOf(sheet
							.getCell(2, i).getContents()) });

			if (city == null) {
				city = new BaseCityInfor();
				TextInfor cityName = new TextInfor(valueOf(sheet.getCell(2, i).getContents()),
						valueOf(sheet.getCell(1, i).getContents()));
				city.setCityName(cityName);
				city.setCountry(new TextInfor("中国", "China"));
				getSession().save(city);
			}
			BaseDistrictInfor district = (BaseDistrictInfor) getUniqueResult(
					"from BaseDistrictInfor where districtName.chinese=?",
					new Object[] { valueOf(sheet.getCell(5, i).getContents()) });

			if (district == null) {
				district = new BaseDistrictInfor();
				TextInfor districtName = new TextInfor(valueOf(sheet.getCell(5, i).getContents()),
						valueOf(sheet.getCell(4, i).getContents()));
				district.setDistrictName(districtName);
				district.setCity(city);
				getSession().save(district);
			}
			BaseCircleInfor circle = (BaseCircleInfor) getUniqueResult(
					"from BaseCircleInfor where circleName.chinese=?", new Object[] { valueOf(sheet
							.getCell(7, i).getContents()) });

			if (circle == null) {
				circle = new BaseCircleInfor();
				TextInfor circleName = new TextInfor(valueOf(sheet.getCell(7, i).getContents()),
						valueOf(sheet.getCell(6, i).getContents()));
				circle.setCircleName(circleName);
				circle.setDistrict(district);
				circle.setIsRecommended(Integer
						.parseInt(valueOf(sheet.getCell(9, i).getContents())));
				circle.setRank(Integer.parseInt(valueOf(sheet.getCell(8, i).getContents())));
				getSession().save(circle);
			}
		}
	}

	public void modifyValue() throws Exception {
		Workbook book = Workbook.getWorkbook(new FileInputStream(new File(PATH)));
		Sheet sheet = book.getSheet(0);
		int rows = sheet.getRows();
		for (int i = 1; i < rows; i++) {
			try {

				String name = valueOf(sheet.getCell(grapModel(Key.NAME_CN), i).getContents());
				RestaurantInfor restaurant = (RestaurantInfor) getUniqueResult(
						"from RestaurantInfor where fullName.chinese=?", new Object[] { name });

				if (restaurant == null) {
					restaurant = new RestaurantInfor();
				}

				BaseCityInfor city = (BaseCityInfor) getUniqueResult(
						"from BaseCityInfor where cityName.chinese=?", new Object[] { valueOf(sheet
								.getCell(grapModel(Key.CITY_CN), i).getContents()) });

				if (city == null) {
					city = new BaseCityInfor();
					TextInfor cityName = new TextInfor(valueOf(sheet.getCell(
							grapModel(Key.CITY_CN), i).getContents()), valueOf(sheet.getCell(
							grapModel(Key.CITY_EN), i).getContents()));
					city.setCityName(cityName);
					city.setCountry(new TextInfor("中国", "China"));
					getSession().save(city);
				}
				BaseDistrictInfor district = (BaseDistrictInfor) getUniqueResult(
						"from BaseDistrictInfor where districtName.chinese=?",
						new Object[] { valueOf(sheet.getCell(grapModel(Key.DISTRICT_CN), i)
								.getContents()) });

				if (district == null) {
					district = new BaseDistrictInfor();
					TextInfor districtName = new TextInfor(valueOf(sheet.getCell(
							grapModel(Key.DISTRICT_CN), i).getContents()), valueOf(sheet.getCell(
							grapModel(Key.DISTRICT_EN), i).getContents()));
					district.setDistrictName(districtName);
					district.setCity(city);
					getSession().save(district);
				}
				BaseCircleInfor circle = (BaseCircleInfor) getUniqueResult(
						"from BaseCircleInfor where circleName.chinese=?",
						new Object[] { valueOf(sheet.getCell(grapModel(Key.CIRCLE_CN), i)
								.getContents()) });

				if (circle == null) {
					circle = new BaseCircleInfor();
					TextInfor circleName = new TextInfor(valueOf(sheet.getCell(
							grapModel(Key.CIRCLE_CN), i).getContents()), valueOf(sheet.getCell(
							grapModel(Key.CIRCLE_EN), i).getContents()));
					circle.setCircleName(circleName);
					circle.setDistrict(district);
					circle.setIsRecommended(0);
					circle.setRank(1000);
					getSession().save(circle);
				}

				TextInfor fullName = new TextInfor(valueOf(sheet.getCell(grapModel(Key.NAME_CN), i)
						.getContents()), valueOf(sheet.getCell(grapModel(Key.NAME_EN), i)
						.getContents()));
				BaseCuisineInfor cuisine = (BaseCuisineInfor) getUniqueResult(
						"from BaseCuisineInfor where cuisineName.chinese=?",
						new Object[] { valueOf(sheet.getCell(grapModel(Key.CUISINE_CN), i)
								.getContents()) });

				if (cuisine == null) {
					cuisine = new BaseCuisineInfor();
					cuisine.setCuisineName(new TextInfor(valueOf(sheet.getCell(
							grapModel(Key.CUISINE_CN), i).getContents()), valueOf(sheet.getCell(
							grapModel(Key.CUISINE_EN), i).getContents())));
					cuisine.setCuisineType(1);
					cuisine.setIsRecommended(0);
					cuisine.setRank(1000);
				}

				TextInfor discount = new TextInfor(valueOf(sheet.getCell(
						grapModel(Key.DISCOUNT_CN), i).getContents()), valueOf(sheet.getCell(
						grapModel(Key.DISCOUNT_EN), i).getContents()));
				TextInfor address = new TextInfor(valueOf(sheet.getCell(grapModel(Key.ADDRESS_CN),
						i).getContents()), valueOf(sheet.getCell(grapModel(Key.ADDRESS_EN), i)
						.getContents()));
				TextInfor introduce = new TextInfor(valueOf(sheet.getCell(
						grapModel(Key.DESCRIPTION_CN), i).getContents()), valueOf(sheet.getCell(
						grapModel(Key.DESCRIPTION_EN), i).getContents()));
				TextInfor park = new TextInfor(valueOf(sheet.getCell(grapModel(Key.PARKING_CN), i)
						.getContents()), valueOf(sheet.getCell(grapModel(Key.PARKING_EN), i)
						.getContents()));
				TextInfor shortName = new TextInfor(valueOf(sheet.getCell(
						grapModel(Key.SHORTNAME_CN), i).getContents()), valueOf(sheet.getCell(
						grapModel(Key.SHORTNAME_EN), i).getContents()));

				restaurant.setFullName(fullName);
				restaurant.setCommissionRate(Double.parseDouble(valueOf(
						sheet.getCell(grapModel(Key.COM_RATE), i).getContents(), Value.FLOAT)
						.toString()));
				restaurant.setCuisine(cuisine);
				restaurant.setDiscount(discount);
				restaurant.setAddress(address);
				restaurant.setDescription(introduce);
				restaurant.setParking(park);
				restaurant.setPerBegin(Double.parseDouble(valueOf(
						sheet.getCell(grapModel(Key.PER_BEGIN), i).getContents(), Value.FLOAT)
						.toString()));
				restaurant.setShortName(shortName);
				restaurant.setTransport(new TextInfor("", ""));
				restaurant.setDiscountPic(valueOf(
						sheet.getCell(grapModel(Key.DIS_PIC), i).getContents(), Value.FLOAT)
						.toString());
				restaurant.setCircle(circle);

				setRestaurantPicture(restaurant);
				if (restaurant.getRestaurantId() != null) {
					getSession().update(restaurant);
				} else {
					restaurant.setIsDeleted(0);
					restaurant.setScore(3.0);
					restaurant.setRestaurantNo(PrefixHelper.getPrefix());
					getSession().save(restaurant);
				}

				insertProfile(restaurant);

			} catch (Exception e) {
				logger.debug("餐厅录入：行：" + i + "ERROR:(");
				e.printStackTrace();
			}
		}
	}

	private void setRestaurantPicture(RestaurantInfor restaurant) {
		File single = new File(DEFAULT_PATH + restaurant.getFullName().getChinese() + "\\"
				+ FONT_MAP);
		if (single.exists()) {
			File[] pictures = single.listFiles();
			if (pictures != null && pictures.length > 0) {
				restaurant.setMapPic(getFileNameOfMD5(restaurant, pictures[0],
						restaurant.getMapPic()));
			}
		}

		single = new File(DEFAULT_PATH + restaurant.getFullName().getChinese() + "\\" + FONT_SMALL);
		if (single.exists()) {
			File[] pictures = single.listFiles();
			if (pictures != null && pictures.length > 0) {
				restaurant.setFrontPic(getFileNameOfMD5(restaurant, pictures[0],
						restaurant.getFrontPic()));
			}
		}

	}

	private Object[] insertProfile(RestaurantInfor restaurant) {
		File single = new File(DEFAULT_PATH + restaurant.getFullName().getChinese());
		if (!single.exists()) {
			single.mkdir();
		}
		File[] singles = single.listFiles();

		for (int i = 0; i < singles.length; i++) {
			File[] pictures = singles[i].listFiles();
			if (singles[i].getName().equals(FONT_PICTURE)) {
				for (int k = 0; k < pictures.length; k++) {
					RestaurantPicInfor picture = new RestaurantPicInfor();
					picture.setDisplayOrder(200);
					picture.setRestaurant(restaurant);
					picture.setBigPath(getFileNameOfMD5(restaurant, pictures[k], null));
					getSession().save(picture);
				}
			} else if (singles[i].getName().equals(FONT_TABLE)) {
				for (int k = 0; k < pictures.length; k++) {
					if (pictures[k].getName().endsWith(".svg")) {
						RestaurantTablePicInfor table = new RestaurantTablePicInfor();
						table.setPicName(new TextInfor(pictures[k].getName().substring(0,
								pictures[k].getName().lastIndexOf(".")), ""));
						table.setTablePicPath(getFileNameOfMD5(restaurant, pictures[k], null));
						table.setRestaurant(restaurant);
						getSession().save(table);
					}
				}
			} else if (singles[i].getName().equals(FONT_ITEM)) {
				for (int k = 0; k < pictures.length; k++) {
					RestaurantMenuInfor menu = new RestaurantMenuInfor();
					menu.setMenuName(new TextInfor("", ""));
					menu.setDescription(new TextInfor("", ""));
					menu.setPrice(0.0);
					//menu.setMenuType(new TextInfor("", ""));
					menu.setMenuCategory(0);
					menu.setIsRecommended(0);
					menu.setMenuPic(getFileNameOfMD5(restaurant, pictures[k], null));
					menu.setRestaurant(restaurant);
					getSession().save(menu);
				}
			}
		}

		return null;
	}

	private String getFileNameOfMD5(RestaurantInfor restaurant, File file, Object compare) {
		String fileName = DigestUtils
				.md5Hex(restaurant.getFullName().getChinese() + file.getName());

		if (compare instanceof String) {
			if (((String) compare).indexOf(fileName) > 0) {
				fileName = DigestUtils.md5Hex(restaurant.getFullName().getChinese()
						+ file.getName() + PrefixHelper.getPrefix());
			}
			return copyFile(file, fileName);
		} else if (compare == null || compare instanceof Map) {
			Map m = PICTURE_MAP;
			if (compare instanceof Map) {
				m = (Map) compare;
			}
			if (m.containsKey((fileName + file.getName().substring(file.getName().lastIndexOf(".")))
					.toLowerCase())) {
				fileName = DigestUtils.md5Hex(restaurant.getFullName().getChinese()
						+ file.getName() + PrefixHelper.getPrefix());
			}
			return copyFile(file, fileName);
		}
		return "";
	}

	private String copyFile(File picture, String fileName) {
		if (picture != null && picture.exists()) {
			File toFile = new File(WEB_ROOT + "upload\\restaurant\\");
			if (!toFile.exists()) {
				toFile.mkdirs();
			}
			String end = picture.getName().substring(picture.getName().lastIndexOf("."));
			fileName = (fileName + end).toLowerCase();
			if (picture.getName().endsWith(".svg")) {
				InsertSvg.changeContent(picture, toFile.getAbsolutePath() + "\\" + fileName);
			} else {
				com.kwchina.core.util.File.copy(picture.getAbsolutePath(), toFile.getAbsolutePath()
						+ "\\" + fileName);
			}
			return fileName;
		}
		return "";
	}

	private String valueOf(String content) {
		return content.trim();
	}

	private int grapModel(Key key) {
		return key.ordinal();
	}

	private Object valueOf(String content, DataHelper.Value value) {
		if (value == Value.STRING) {
			return content.trim();
		} else if (value == Value.INTEGER || value == Value.FLOAT) {
			if (content == null || content.equals("")) {
				return 0;
			}
			return content;
		}
		return "";
	}

	private Object getUniqueResult(String hql, Object[] params) {
		Query query = getSession().createQuery(hql);
		for (int i = 0; i < params.length; i++) {
			query.setParameter(i, params[i]);
		}
		List results = query.list();
		if (CollectionUtils.isNotEmpty(results)) {
			return results.get(0);
		}
		return null;
	}

	enum Value {
		INTEGER, FLOAT, STRING
	}

	/**
	 * 对应Excel列顺序
	 */
	enum Key {
		NAME_CN, NAME_EN, COM_RATE, CUISINE_CN, CUISINE_EN, DISCOUNT_CN, DISCOUNT_EN, ADDRESS_CN, ADDRESS_EN, DESCRIPTION_CN, DESCRIPTION_EN, PARKING_CN, PARKING_EN, PER_BEGIN, SHORTNAME_CN, SHORTNAME_EN, DIS_PIC, CITY_CN, CITY_EN, DISTRICT_CN, DISTRICT_EN, CIRCLE_CN, CIRCLE_EN
	}

	public static void main(String[] args) {

	}

}
