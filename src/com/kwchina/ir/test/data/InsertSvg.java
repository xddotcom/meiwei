package com.kwchina.ir.test.data;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class InsertSvg {

	private static String DEFAULT_PATH = "D:\\svg";
	private static String COLOR_BASE = "#8C7D73";
	private static String COLOR_OLD = "#8C7D73";
	private static Long SYSTEM_TIME=System.currentTimeMillis();

	public static void excute( ) {
		File single = new File(DEFAULT_PATH);
		File[] singles = single.listFiles();
		try {
			for (int k = 0; k < singles.length; k++) {
				if (singles[k].getName().contains(".svg")) {
					changeContent(singles[k], getNewFile(singles[k]) );
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static String getNewFile(File file) {
		File floder = new File(file.getParentFile().getAbsolutePath() +"\\"+SYSTEM_TIME  );
		if(!floder.exists()){
			floder.mkdirs();
		}
		return floder.getAbsolutePath()+"\\"+file.getName();
	}

	private static boolean similarColor(String s1, String s2) {
		int size = 15;
		Color c1 = new Color(Integer.parseInt(s1.replace("#", ""), 16));
		Color c2 = new Color(Integer.parseInt(s2.replace("#", ""), 16));
		if (Math.abs(c1.getRed() - c2.getRed()) < size && Math.abs(c1.getGreen() - c2.getGreen()) < size
				&& Math.abs(c1.getBlue() - c2.getBlue()) < size) {
			return true;
		}
		return false;
	}
 
	public static void changeContent(File picture, String toWhere) {
		changeContent(picture, toWhere,  "fill=\"#" );
	}
	
	public static void changeContent(File picture, String toWhere  , String regex) {

		try {
			FileInputStream in = new FileInputStream(picture.getAbsoluteFile());
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			StringBuffer sb = new StringBuffer();
			String s = null;
			Pattern p = Pattern.compile(regex);
			Matcher m;
			int id = 1;
			while ((s = br.readLine()) != null) {
				if( !s.matches( "tableid|class|status|number")  ){
					m = p.matcher(s.toString());
					if (m.find() && similarColor(s.substring(m.end(), m.end() + 6), COLOR_OLD)) {
						s=s.replaceFirst( m.group() + s.substring(m.end(), m.end() + 7) , " fill=\""
								+ COLOR_BASE
								+ "\" tableid=\""
								+ id
								+ "\" class=\"table\" status=\"available\" number=\""+id+"\" style=\"cursor: pointer;\" " );
						id++;
 					}
					sb.append(s);
					sb.append( "\n");
				}
			}

			br.close();

			FileOutputStream out = new FileOutputStream( toWhere );
			PrintWriter pw = new PrintWriter(out);
			pw.write(sb.toString().toCharArray());
			pw.flush();
			pw.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	public static void main(String[] args) throws FileNotFoundException {
		excute();
	}

}
