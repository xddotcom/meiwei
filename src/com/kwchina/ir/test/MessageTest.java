package com.kwchina.ir.test;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Properties;

import org.junit.Test;
import org.springframework.core.io.Resource;

public class MessageTest extends AbstractTest {

	@Test
	public void excute() {
		
		
		Resource cn=  applicationContext.getResource( "messages_zh_CN.properties");
		Resource en=  applicationContext.getResource( "messages_en.properties");
		
		
		String output ="var MD={";
		
		Properties p = new Properties();
		Properties p2 = new Properties();
		try {
			p.load( cn.getInputStream() );
			p2.load( en.getInputStream() );
		} catch (IOException e) {
			e.printStackTrace();
		}
		Enumeration<Object> k = p.keys();
		int i=1;
		while(k.hasMoreElements()){
			String key = k.nextElement().toString();
			System.out.println(i++ + "\t" + key);
			output += "\""+ key +"\"";
			output += ":{ cn:";
			output +="\""+ p.getProperty( key ) +"\"";
			output += ",en:";
			output += "\""+ p2.getProperty( key ) +"\" } ,";
			if(i%10==0){
				output += "\n";
			}
		}
		
		System.out.println(output.substring(0 , output.length()-1) +"}");
 	}
	
}
