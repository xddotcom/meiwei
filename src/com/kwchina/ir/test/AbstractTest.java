package com.kwchina.ir.test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:applicationContext.xml")
public abstract class AbstractTest extends AbstractJUnit4SpringContextTests {
	
	protected static final Logger logger = Logger.getLogger(AbstractTest.class);
	
	protected Session session;
	
	protected EntityManager entityManager;
	
	
	public EntityManager getEntityManager() {
		if(entityManager == null){
			return ((EntityManagerFactory)applicationContext.getBean( "entityManagerFactory")).createEntityManager();
		}
		return entityManager;
	}
	
	public Session getSession() {
		if(session==null){
			return (Session)getEntityManager().getDelegate();
		}
		return session;
	}

}
