package com.kwchina.ir.test;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

/**
<filter>
		<filter-name>filterTest</filter-name>
		<filter-class>com.kwchina.ir.test.FilterTest</filter-class>
	</filter>
	<filter-mapping>
		<filter-name>filterTest</filter-name>
		<url-pattern>/*</url-pattern>
	</filter-mapping>
 */

public class FilterTest implements Filter {

	@Override
	public void destroy() {
		
	}

	@Override
	public void doFilter(ServletRequest arg0, ServletResponse arg1, FilterChain arg2) throws IOException,
			ServletException {
		System.out.println( ((HttpServletRequest)arg0).getRequestURI());
		arg2.doFilter(arg0, arg1);
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		
	}

}
