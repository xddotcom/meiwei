//package com.kwchina.ir.exception;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
//import org.springframework.web.servlet.HandlerExceptionResolver;
//import org.springframework.web.servlet.ModelAndView;
//import org.springframework.web.servlet.view.RedirectView;
//
//public class CustomExceptionHandler implements HandlerExceptionResolver {
//	public ModelAndView resolveException(HttpServletRequest request,
//			HttpServletResponse response, Object object, Exception ex) {
//		String targetUrl = request.getContextPath() + "/error.htm";
//		return new ModelAndView(new RedirectView(targetUrl));
//	}
//}
