package com.kwchina.ir.util.mail;

import java.util.Date;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.apache.log4j.Logger;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

import com.kwchina.core.sys.CoreConstant;

public class MailHelper {

	private static JavaMailSender mailSender;
	
	protected static Logger logger = Logger.getLogger(MailHelper.class);
	
 	private String from;
	private String to;
	private Date sentDate;
	private String subject;
	private String text;

	public boolean send() {
		MimeMessage message = mailSender.createMimeMessage();
		try {
			MimeMessageHelper helper = new MimeMessageHelper(message, true);

			helper.setFrom("systemadmin@clubmeiwei.com");
			helper.setTo(getTo());
			helper.setSubject(getSubject());
			helper.setText(getText());
			if(CoreConstant.JAVA_MAIL_POSITION){
				mailSender.send(message);
			}
			return true;
		} catch (MessagingException e) {
			logger.error("send email failed", e);
			return false;
		}
	}
	
	
	
	public String getTo() {
		return to;
	}

	public MailHelper setTo(String to) {
		this.to = to;
		return this;
	}

	public Date getSentDate() {
		return sentDate;
	}

	public MailHelper setSentDate(Date sentDate) {
		this.sentDate = sentDate;
		return this;
	}

	public String getSubject() {
		return subject;
	}

	public MailHelper setSubject(String subject) {
		this.subject = subject;
		return this;
	}

	public String getText() {
		return text;
	}

	public MailHelper setText(String text) {
		this.text = text;
		return this;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public MailHelper() {
	}
	
	public static void setMailSender(JavaMailSender mailSender) {
		MailHelper.mailSender = mailSender;
	}

}
