package com.kwchina.ir.util;

import org.apache.log4j.Logger;

import com.kwchina.core.sys.CoreConstant;
import com.kwchina.ir.entity.TextInfor;

public class TextHelper {

	private static final Logger logger = Logger.getLogger(TextHelper.class);

	public static String getText(TextInfor textInfor) {
		try {
			int languageType = LanguageHelper.launage();
			if (languageType == CoreConstant.LANGUAGETYPE_ENGLISH) {
				return textInfor.getEnglish();
			} else if (languageType == CoreConstant.LANGUAGETYPE_CHINESE) {
				return textInfor.getChinese();
			} else {
				return textInfor.getChinese();
			}
		} catch (Exception e) {
			logger.error("Null Text!", e);
			return "";
		}
	}

	public static String getSafeText(String text) {
		return text.replace("'", "''").replace("\"", "\"\"").replace('\n', ' ').replace('\r', ' ').replace('\\', ' ');
	}

	public enum Templete {
		PASSWORD_MAIL("尊敬的%s，您好:\n"
				+ "您在美位网（www.clubmeiwei.com）点击了“忘记密码”按钮，故系统自动为您发送了这封邮件。您可以点击以下链接修改您的密码：\n"
				+ "http://www.clubmeiwei.com/member/resetpassword.htm?key=%s&name=%s\n"
				+ "如果您不需要修改密码，或者您从未点击过“忘记密码”按钮，请忽略本邮件。\n" 
				+ "如有任何疑问，请联系美位网客服，客服热线：021-60515617"),
		BEFORE_HAVING_MESSAGE("%s，您预订的%s%s 在%s(%s) %s人用餐订单，已确认成功。感谢您使用美位网订餐，祝您用餐愉快!(www.clubmeiwei.com)【美位网】"),
		AFTER_HAVING_MESSAGE("%s，感谢您使用美位网订餐，您于%s %s在%s用餐的消费金额是%s元(美位积分+%s)。欢迎再次使用美位网订餐，谢谢!(www.clubmeiwei.com）【美位网】")
		,NEW_ORDER_CONTENT("--\n新预定 \n下单时间:%s\n订单编号:%s\n用户名:%s\n餐厅名称:%s\n餐厅电话:%s\n备注:%s\n增值服务:%s"),
		NEW_ORDER_TITLE("新预定 %s %s %s %s位 %s %s "),
		ORDER_CANCEL_CONTENT("--\n订单取消\n下单时间:%s\n订单编号:%s\n用户名:%s\n餐厅名称:%s\n餐厅电话:%s\n备注:%s"),
		ORDER_CANCEL_TITLE("订单取消 %s %s %s %s位 %s %s "),
		INVITE_MESSAGE("%s诚邀您于%s %s莅临%s(%s)共享佳宴!(www.clubmeiwei.com)【美位网】");
		private String templete;

		private Templete(String templete) {
			this.templete = templete;
		}

		public String getTemplete() {
			return templete;
		}
	}
}
