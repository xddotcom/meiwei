//package com.kwchina.ir.util;
//
//import java.io.FileInputStream;
//import java.io.FileOutputStream;
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.OutputStream;
//import java.util.Iterator;
//import java.util.Map;
//import java.util.Properties;
//
//public class PropertiesHelper {
//
//	public static void write(String filePath, Map<String, String> values) {
//		Properties prop = new Properties();
//		try {
//			InputStream fis = new FileInputStream(filePath);
//			prop.load(fis);
//			OutputStream fos = new FileOutputStream(filePath);
//			for (Iterator<String> iterator = values.keySet().iterator(); iterator.hasNext();) {
//				String key = iterator.next();
//				String value = values.get(key);
//				prop.setProperty(key, value);
//				prop.store(fos, key);
//			}
//			fis.close();
//			fos.close();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//	}
//
//	public static void write(String filePath, String name, String value) {
//		Properties prop = new Properties();
//		try {
//			InputStream fis = new FileInputStream(filePath);
//			prop.load(fis);
//			OutputStream fos = new FileOutputStream(filePath);
//			prop.setProperty(name, value);
//			prop.store(fos, name);
//			fis.close();
//			fos.close();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//	}
//
//}
