package com.kwchina.ir.util;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringHelper {

	public static boolean isEmpty(String str) {
		return str == null || str.length() == 0;
	}

	public static boolean isNotEmpty(String str) {
		return !isEmpty(str);
	}

	public static boolean isTrimNotEmpty(String str) {
		return isNotEmpty(str) && isNotEmpty(str.trim());
	}

	public static String trim(String str) {
		return str != null ? str.trim() : null;
	}

	public static String trimToNull(String str) {
		String ts = trim(str);
		return isEmpty(ts) ? null : ts;
	}

	public static String trimToEmpty(String str) {
		return str != null ? str.trim() : "";
	}

	public static Integer regex(String content, String regex, String replacement) {
		if (isNotEmpty(content)) {
			return Integer.parseInt(content.replaceAll(regex, ""));
		}
		return 0;
	}

	public static Integer regex(String content, String regex) {
		return regex(content, regex, "");
	}

	public static String parameterConvert(String param, String key) {

		String[] pa = param.split("&");
		Map<String, String> m = new HashMap<String, String>();
		if (pa != null && pa.length > 0) {
			for (int i = 0; i < pa.length; i++) {
				String[] pae = pa[i].split("=");
				if (pae != null && pae.length == 2) {
					m.put(pae[0], pae[1]);
				} else {
					m.put(pae[0], "");
				}

			}
		}
		String result = m.get(key);
		return result == null ? "" : result;
	}

	public static boolean toBoolean(Object value) {
		try {
			return Boolean.valueOf(value == null ? "false" : value.toString());
		} catch (Exception e) {
			return false;
		}
	}

	public static String getName(String name, int sexe) {

		if (name != null) {
			Pattern p = Pattern.compile("先生|女士|小姐|老师|主任|经理");
			Matcher m  = p.matcher(name);
			if (m.find()) {
				return name;
			} else {
				if (sexe == 0) {
					return name + " 先生";
				} else {
					return name + " 小姐";
				}
			}
		}
		return "";
	}

}
