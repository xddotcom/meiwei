package com.kwchina.ir.util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;

import com.kwchina.ir.vo.api.ApiBookTimeVo;

public class CommonHelper {

	private int split = 30;

	public List<String> getData(String[] input) {

		String begin = parse(input[0]), over = parse(input[1]);
		if (begin == null | over == null ) {
			return new ArrayList<String>();
		}

		List<String> data = new LinkedList<String>();
		data.add(begin);
		while (Math.abs(Integer.parseInt(begin.replace(":", "")) - Integer.parseInt(over.replace(":", ""))) > split ) {
			if (data.size() > 100 ) {
				break;
			}
			data.add(begin = parse(begin.substring(0, begin.length() - 2)
					+ (Integer.parseInt(begin.substring(begin.length() - 2)) + split)));
		}
		data.add(over);
		return data;
	}

	public List<ApiBookTimeVo> getData(List<String[]> inputs) {

		if (inputs != null && inputs.size() > 0) {
			Map<String, ApiBookTimeVo> m = new HashMap<String, ApiBookTimeVo>();
			for (int i = 0; i < inputs.size(); i++) {
				boolean b = (inputs.get(i).length > 2 && StringHelper.isNotEmpty(inputs.get(i)[2]));
				String[] r = inputs.get(i);
				List<String> rs = getData(new String[] { r[0],  r[1] });
				if (CollectionUtils.isNotEmpty(rs)) {
					for (String me : rs) {
						if (b) {
							m.put(me, new ApiBookTimeVo(me, "" + me + " " + r[2]));
						} else {
							m.put(me, new ApiBookTimeVo(me, "" + me));
						}
					}
				}
			}
			return sort( new ArrayList<ApiBookTimeVo>(m.values()));
		}
		return new ArrayList<ApiBookTimeVo>();

	}

	public List<ApiBookTimeVo> sort(List<ApiBookTimeVo> targets) {
		Collections.sort(targets, new Comparator<ApiBookTimeVo>() {
			@Override
			public int compare(ApiBookTimeVo o1, ApiBookTimeVo o2) {
				return o1.getOptionTitle().compareTo( o2.getOptionTitle() );
			}
		});
		return targets;
	}

	private String parse(String regex) {
		try {
			SimpleDateFormat s = new SimpleDateFormat("HH:mm");
			Date e = s.parse(regex.replaceAll("[,.：();]", ":"));
			return s.format(e);
		} catch (Exception e) {
			return null;
		}
	}

	public CommonHelper() {
	}

	public CommonHelper(int split) {
		this.split = split;
	}
}
