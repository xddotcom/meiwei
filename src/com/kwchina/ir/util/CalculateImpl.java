package com.kwchina.ir.util;

import org.apache.log4j.Logger;

import com.kwchina.core.common.BasicDao;
import com.kwchina.ir.entity.OrderInfor;

public class CalculateImpl {
	protected static final Logger logger = Logger
			.getLogger(CalculateImpl.class);
	private Double result;
	private Double quotiety;
	private Calculate calculate;
	private OrderInfor order;
	private BasicDao<?> basicDao;

	public static final Calculate RULE_SIMPLE = new Calculate() {
		@Override
		public void calculate(CalculateImpl arguments) {
			// do nothing..
		}
	};

	public static final Calculate RULE_NORMAL = new Calculate() {
		@Override
		public void calculate(CalculateImpl arguments) {
			Double result = arguments.getOrder().getConsumeAmount()
					* arguments.getQuotiety();
			if (arguments.getOrder().getCreditGain() < 0) {
				result *= -arguments.getOrder().getCreditGain();
			}
			arguments.setResult(result);
		}
	};

	public static final Calculate RULE_DOUBLE = new Calculate() {
		@Override
		public void calculate(CalculateImpl arguments) {

			Double result = arguments.getOrder().getConsumeAmount()
					* arguments.getQuotiety() * 2;
			if (arguments.getOrder().getCreditGain() < 0) {
				result *= -arguments.getOrder().getCreditGain();
			}
			arguments.setResult(result);
		}
	};

	public static final Calculate RULE_AWEEK = new Calculate() {
		@Override
		public void calculate(CalculateImpl arguments) {
			Integer amount = arguments
					.getBasicDao()
					.getResultNumBySQLQuery(
							"SELECT IF(COUNT(*) >= 2, 100, 0) FROM order_infor O "
									+ " INNER JOIN member_infor M ON O.member = M.memberId "
									+ " WHERE M.memberId="
									+ arguments.getOrder().getMember()
											.getMemberId()
									+ " AND DATEDIFF(O.ordersubmittime, M.registertime) < 7 AND O.status >= 30 AND O.status < 90");
			arguments.setResult(amount.doubleValue());
		}
	};

	public static final Calculate RULE_AMONTH = new Calculate() {
		@Override
		public void calculate(CalculateImpl arguments) {
			Integer amount = arguments
					.getBasicDao()
					.getResultNumBySQLQuery(
							"SELECT IF(COUNT(*) >= 5, 100, 0) FROM order_infor O "
									+ " INNER JOIN member_infor M ON O.member = M.memberId "
									+ " WHERE M.memberId="
									+ arguments.getOrder().getMember()
											.getMemberId()
									+ " AND DATEDIFF(O.ordersubmittime, M.registertime) < 31 AND O.status >= 30 AND O.status < 90");
			arguments.setResult(amount.doubleValue());
		}
	};

	public CalculateImpl() {
		this.result = 100d;
		this.quotiety = 1d;
	}

	public CalculateImpl(OrderInfor order, BasicDao<?> basicDao) {
		this();
		this.order = order;
		this.basicDao = basicDao;
	}

	public Double get(Calculate calculate) {
		return calculate(calculate).get();
	}

	public CalculateImpl calculate(Calculate calculate) {
		this.calculate = calculate;
		return this;
	}

	public Double get() {
		try {
			if (this.calculate != null)
				this.calculate.calculate(this);
			if (getResult() > 888) {
				logger.info("  a large amount . : " + getResult());
			}
			return getResult();
		} catch (Exception e) {
			logger.error(" error while calculate money " + getResult());
			return 0.0;
		}
	}

	public Double getResult() {
		return result;
	}

	public void setResult(Double result) {
		this.result = result;
	}

	public Double getQuotiety() {
		return quotiety;
	}

	public void setQuotiety(Double quotiety) {
		this.quotiety = quotiety;
	}

	public OrderInfor getOrder() {
		return order;
	}

	public BasicDao<?> getBasicDao() {
		return basicDao;
	}

}
