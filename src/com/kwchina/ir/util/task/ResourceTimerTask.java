//package com.kwchina.ir.util.task;
//
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//import java.util.TimerTask;
//
//import javax.annotation.Resource;
//
//import org.apache.commons.collections.CollectionUtils;
//import org.apache.log4j.Logger;
//import org.springframework.stereotype.Component;
//
//import com.kwchina.ir.dao.ArticleInforDao;
//import com.kwchina.ir.entity.LabelDetailInfor;
//import com.kwchina.ir.front.controller.AbstractController;
//import com.kwchina.ir.util.PropertiesHelper;
//
//@Component
//public class ResourceTimerTask extends TimerTask {
//
//	protected static final Logger logger = Logger.getLogger(ResourceTimerTask.class);
//	
//	@Resource
//	private ArticleInforDao articleInforDao;
//	private PropertiesHelper propertiesHelper = new PropertiesHelper();
//
//	@Override
//	public void run() {
//		String zh_CN = ResourceTimerTask.class.getResource("/").getPath() + "messages_zh_CN.properties";
//		String en = ResourceTimerTask.class.getResource("/").getPath() + "messages_en.properties";
//		set(zh_CN, "FROM LabelDetailInfor T WHERE labelOption = 'OPTION_MESSAGE_RESOURCE_CN' ");
//		set(en, "FROM LabelDetailInfor T WHERE labelOption = 'OPTION_MESSAGE_RESOURCE_EN' ");
//	}
//
//	@SuppressWarnings({ "static-access", "rawtypes" })
//	void set(String path, String sql) {
//		try {
//			List results = this.articleInforDao.getResultByQueryString(sql);
//			if (CollectionUtils.isNotEmpty(results)) {
//				Map<String, String> values = new HashMap<String, String>();
//				for (Object o : results) {
//					LabelDetailInfor entity = ((LabelDetailInfor) (o));
//					values.put(entity.getLabelName(), entity.getLabelValue());
//				}
//				propertiesHelper.write(path, values);
//			}
//		} catch (Exception e) {
//			logger.error("Message Resource Update Error" , e);
//		}
//	}
//
//}
