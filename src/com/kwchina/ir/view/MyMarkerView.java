package com.kwchina.ir.view;

import org.springframework.web.servlet.view.freemarker.FreeMarkerView;

import com.kwchina.ir.support.MyRequestContext;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * Created by IntelliJ IDEA. User: Administrator Date: 2009-5-25 Time: 10:52:30
 * To change this template use File | Settings | File Templates.
 */
public class MyMarkerView extends FreeMarkerView {

	public static final String SPRING_SECURITY_MACRO_REQUEST_CONTEXT_ATTRIBUTE = "springSecurityMacroRequestContext";

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	protected void exposeHelpers(Map map, HttpServletRequest httpServletRequest) throws Exception {
		try {
			super.exposeHelpers(map, httpServletRequest);
			map.put(SPRING_SECURITY_MACRO_REQUEST_CONTEXT_ATTRIBUTE, new MyRequestContext(
					httpServletRequest));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
