package com.kwchina.ir.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.kwchina.core.sys.CoreConstant;

@Table(name = "Restaurant_Infor")
@Entity
public class RestaurantInfor {

	private TextInfor address = new TextInfor();
	private BaseCircleInfor circle = new BaseCircleInfor();
	private Double commissionRate = 0.0;
	private BaseCuisineInfor cuisine = new BaseCuisineInfor();
	private TextInfor description = new TextInfor();
	private TextInfor discount = new TextInfor();
	private String discountPic = "";
	private String frontPic = "";
	private TextInfor fullName = new TextInfor();
	private Integer isDeleted = CoreConstant.FIELD_FALSE;
	private String mapPic = "";
	private TextInfor parking = new TextInfor();
	private Double perBegin = 0.0; // 人均消费起
	private Double perEnd = 0.0; // 人均消费止
	private Integer restaurantId;
	private String restaurantNo;
	private Double score = 3.0;
	private TextInfor shortName = new TextInfor();
	private String telphone = "";
	private TextInfor transport = new TextInfor();
	private TextInfor workinghour = new TextInfor();
	private MemberInfor memberadmin = null;
	private RestaurantSearchInfor restaurantSearch = null;

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "address")
	public TextInfor getAddress() {
		return address;
	}

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "circle")
	public BaseCircleInfor getCircle() {
		return circle;
	}

	@Column(name = "commissionRate")
	public Double getCommissionRate() {
		return commissionRate;
	}

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "cuisine")
	public BaseCuisineInfor getCuisine() {
		return cuisine;
	}

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "description")
	public TextInfor getDescription() {
		return description;
	}

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "discount")
	public TextInfor getDiscount() {
		return discount;
	}

	@Column(name = "discountPic", length = 300)
	public String getDiscountPic() {
		return discountPic;
	}

	@Column(name = "frontPic", length = 300)
	public String getFrontPic() {
		return frontPic;
	}

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "fullName")
	public TextInfor getFullName() {
		return fullName;
	}

	@Column(name = "isDeleted")
	public Integer getIsDeleted() {
		return isDeleted;
	}

	@Column(name = "mapPic", length = 300)
	public String getMapPic() {
		return mapPic;
	}

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "parking")
	public TextInfor getParking() {
		return parking;
	}

	@Column(name = "perBegin")
	public Double getPerBegin() {
		return perBegin;
	}

	@Column(name = "perEnd")
	public Double getPerEnd() {
		return perEnd;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "restaurantId")
	public Integer getRestaurantId() {
		return restaurantId;
	}

	@Column(name = "restaurantNo", length = 100, unique = true, nullable = false)
	public String getRestaurantNo() {
		return restaurantNo;
	}

	@Column(name = "score")
	public Double getScore() {
		return score;
	}

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "shortName")
	public TextInfor getShortName() {
		return shortName;
	}

	@Column(name = "telphone", length = 20)
	public String getTelphone() {
		return telphone;
	}

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "transport")
	public TextInfor getTransport() {
		return transport;
	}
	
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "workinghour")
	public TextInfor getWorkinghour() {
		return workinghour;
	}

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "memberadmin")
	public MemberInfor getMemberadmin() {
		return memberadmin;
	}

	@OneToOne(cascade = CascadeType.ALL, mappedBy = "restaurant", fetch = FetchType.EAGER)
	public RestaurantSearchInfor getRestaurantSearch() {
		return restaurantSearch;
	}

	public void setAddress(TextInfor address) {
		if (address != null) {
			this.address = address;
		}
	}

	public void setCircle(BaseCircleInfor circle) {
		if (circle != null) {
			this.circle = circle;
		}
	}

	public void setCommissionRate(Double commissionRate) {
		if (commissionRate != null) {
			this.commissionRate = commissionRate;
		}
	}

	public void setCuisine(BaseCuisineInfor cuisine) {
		if (cuisine != null) {
			this.cuisine = cuisine;
		}
	}

	public void setDescription(TextInfor description) {
		if (description != null) {
			this.description = description;
		}
	}

	public void setDiscount(TextInfor discount) {
		if (discount != null) {
			this.discount = discount;
		}
	}

	public void setDiscountPic(String discountPic) {
		if (discountPic != null) {
			this.discountPic = discountPic;
		}
	}

	public void setFrontPic(String frontPic) {
		if (frontPic != null) {
			this.frontPic = frontPic;
		}
	}

	public void setFullName(TextInfor fullName) {
		if (fullName != null) {
			this.fullName = fullName;
		}
	}

	public void setIsDeleted(Integer isDeleted) {
		if (isDeleted != null) {
			this.isDeleted = isDeleted;
		}
	}

	public void setMapPic(String mapPic) {
		if (mapPic != null) {
			this.mapPic = mapPic;
		}
	}

	public void setParking(TextInfor parking) {
		if (parking != null) {
			this.parking = parking;
		}
	}

	public void setPerBegin(Double perBegin) {
		if (perBegin != null) {
			this.perBegin = perBegin;
		}
	}

	public void setPerEnd(Double perEnd) {
		if (perEnd != null) {
			this.perEnd = perEnd;
		}
	}

	public void setRestaurantId(Integer restaurantId) {
		if (restaurantId != null) {
			this.restaurantId = restaurantId;
		}
	}

	public void setRestaurantNo(String restaurantNo) {
		if (restaurantNo != null) {
			this.restaurantNo = restaurantNo;
		}
	}

	public void setScore(Double score) {
		if (score != null) {
			this.score = score;
		}
	}

	public void setShortName(TextInfor shortName) {
		if (shortName != null) {
			this.shortName = shortName;
		}
	}

	public void setTelphone(String telphone) {
		if (telphone != null) {
			this.telphone = telphone;
		}
	}

	public void setTransport(TextInfor transport) {
		if (transport != null) {
			this.transport = transport;
		}
	}

	public void setWorkinghour(TextInfor workinghour) {
		if (workinghour != null) {
			this.workinghour = workinghour;
		}
	}
	
	public void setMemberadmin(MemberInfor memberadmin) {
		if (memberadmin != null) {
			this.memberadmin = memberadmin;
		}
	}

	public void setRestaurantSearch(RestaurantSearchInfor restaurantSearch) {
		if (restaurantSearch != null) {
			this.restaurantSearch = restaurantSearch;
		}
	}
	
	public RestaurantInfor() {
 	}

	public RestaurantInfor(TextInfor fullName) {
		super();
		this.fullName = fullName;
	}

	public RestaurantInfor(TextInfor address, TextInfor description, TextInfor discount, String frontPic,
			TextInfor fullName, Double perBegin, Integer restaurantId, String restaurantNo, Double score) {
		super();
		this.address = address;
		this.description = description;
		this.discount = discount;
		this.frontPic = frontPic;
		this.fullName = fullName;
		this.perBegin = perBegin;
		this.restaurantId = restaurantId;
		this.restaurantNo = restaurantNo;
		this.score = score;
	}
	
	
}
