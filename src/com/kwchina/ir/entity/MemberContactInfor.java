package com.kwchina.ir.entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.kwchina.core.sys.CoreConstant;

@Table(name = "Member_Contact_Infor")
@Entity
public class MemberContactInfor {

	private Integer contactId;
	private String email = "";
	private MemberInfor member = null;
	private String name = "";
	private Integer sexe = CoreConstant.SEX_MALE;
	private String telphone = "";
	private Date createTime = new Date(System.currentTimeMillis()); 

	public void delete() {
		this.member = null;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "contactId")
	public Integer getContactId() {
		return contactId;
	}

	@Column(name = "email", length = 50)
	public String getEmail() {
		return email;
	}

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "member")
	public MemberInfor getMember() {
		return member;
	}

	@Column(name = "name", length = 80)
	public String getName() {
		return name;
	}

	@Column(name = "sexe")
	public Integer getSexe() {
		return sexe;
	}

	@Column(name = "telphone", length = 20)
	public String getTelphone() {
		return telphone;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "createTime")
	public Date getCreateTime() {
		return createTime;
	}

	public void setContactId(Integer contactId) {
		if (contactId != null) {
			this.contactId = contactId;
		}
	}

	public void setEmail(String email) {
		if (email != null) {
			this.email = email;
		}
	}

	public void setMember(MemberInfor member) {
		if (member != null) {
			this.member = member;
		}
	}

	public void setName(String name) {
		if (name != null) {
			this.name = name;
		}
	}

	public void setSexe(Integer sexe) {
		if (sexe != null) {
			this.sexe = sexe;
		}
	}

	public void setTelphone(String telphone) {
		if (telphone != null) {
			this.telphone = telphone;
		}
	}

	public void setCreateTime(Date createTime) {
		if (createTime != null) {
			this.createTime = createTime;
		}
	}
	
}
