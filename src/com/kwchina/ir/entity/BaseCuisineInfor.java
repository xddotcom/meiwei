package com.kwchina.ir.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.kwchina.core.sys.CoreConstant;

@Entity
@Table(name = "Base_Cuisine_Infor")
public class BaseCuisineInfor {

	private Integer cuisineId;
	private TextInfor cuisineName = new TextInfor();
	private Integer cuisineType = CoreConstant.CUISINETYPE_COOKING;
	private Integer isRecommended = CoreConstant.FIELD_TRUE;
	private Integer rank = Integer.MAX_VALUE;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "cuisineId")
	public Integer getCuisineId() {
		return cuisineId;
	}

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "cuisineName")
	public TextInfor getCuisineName() {
		return cuisineName;
	}

	@Column(name = "cuisineType")
	public Integer getCuisineType() {
		return cuisineType;
	}

	@Column(name = "isRecommended")
	public Integer getIsRecommended() {
		return isRecommended;
	}

	@Column(name = "rank")
	public Integer getRank() {
		return rank;
	}

	public void setCuisineId(Integer cuisineId) {
		this.cuisineId = cuisineId;
	}

	public void setCuisineName(TextInfor cuisineName) {
		this.cuisineName = cuisineName;
	}

	public void setCuisineType(Integer cuisineType) {
		this.cuisineType = cuisineType;
	}

	public void setIsRecommended(Integer isRecommended) {
		this.isRecommended = isRecommended;
	}

	public void setRank(Integer rank) {
		this.rank = rank;
	}

}
