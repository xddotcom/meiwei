package com.kwchina.ir.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "Article_Article_Infor")
public class ArticleInfor {

	private Integer articleId;
	private String content = "";
	private Date submitTime = new Date(System.currentTimeMillis());

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "articleId")
	public Integer getArticleId() {
		return articleId;
	}

	@Column(name = "content", columnDefinition = "text")
	public String getContent() {
		return content;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "submitTime")
	public Date getSubmitTime() {
		return submitTime;
	}

	public void setArticleId(Integer articleId) {
		if (articleId != null) {
			this.articleId = articleId;
		}
	}

	public void setContent(String content) {
		if (content != null) {
			this.content = content;
		}
	}

	public void setSubmitTime(Date submitTime) {
		if (submitTime != null) {
			this.submitTime = submitTime;
		}
	}

}
