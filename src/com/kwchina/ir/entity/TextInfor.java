package com.kwchina.ir.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.kwchina.ir.util.TextHelper;

@Entity
@Table(name = "Text_Infor")
public class TextInfor {

	private Integer textId;
	private String chinese = "";
	private String english = "";

	public TextInfor() {
		super();
	}

	public TextInfor(String chinese, String english) {
		super();
		setEnglish(english);
		setChinese(chinese);
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "textId")
	public Integer getTextId() {
		return textId;
	}

	@Column(name = "chinese", length = 1000)
	public String getChinese() {
		return chinese;
	}

	@Column(name = "english", length = 1000)
	public String getEnglish() {
		return english;
	}

	@Transient
	public String getText() {
		try {
			return TextHelper.getText(this);
		} catch (Exception e) {
			return "";
		}
	}

	public void setChinese(String chinese) {
		if (chinese != null) {
			this.chinese = chinese;
		}
	}

	public void setEnglish(String english) {
		if (english != null) {
			this.english = english;
		}
	}

	public void setTextId(Integer textId) {
		if (textId != null) {
			this.textId = textId;
		}
	}

}
