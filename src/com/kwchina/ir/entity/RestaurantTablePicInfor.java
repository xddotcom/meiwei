package com.kwchina.ir.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Table(name = "Restaurant_TablePic_Infor")
@Entity
public class RestaurantTablePicInfor {

	private Integer picHeight = 100;
	private TextInfor picName = new TextInfor();
	private Integer picWidth = 100;
	private RestaurantInfor restaurant = null;
	private Integer tablePicId;
	private String tablePicPath = "";

	@Column(name = "picHeight")
	public Integer getPicHeight() {
		return picHeight;
	}

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "picName")
	public TextInfor getPicName() {
		return picName;
	}

	@Column(name = "picWidth")
	public Integer getPicWidth() {
		return picWidth;
	}

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "restaurant")
	public RestaurantInfor getRestaurant() {
		return restaurant;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "tablePicId")
	public Integer getTablePicId() {
		return tablePicId;
	}

	@Column(name = "tablePicPath", columnDefinition = "nvarchar(300) default ''")
	public String getTablePicPath() {
		return tablePicPath;
	}

	public void setPicHeight(Integer picHeight) {
		if (picHeight != null) {
			this.picHeight = picHeight;
		}
	}

	public void setPicName(TextInfor picName) {
		if (picName != null) {
			this.picName = picName;
		}
	}

	public void setPicWidth(Integer picWidth) {
		if (picWidth != null) {
			this.picWidth = picWidth;
		}
	}

	public void setRestaurant(RestaurantInfor restaurant) {
		if (restaurant != null) {
			this.restaurant = restaurant;
		}
	}

	public void setTablePicId(Integer tablePicId) {
		if (tablePicId != null) {
			this.tablePicId = tablePicId;
		}
	}

	public void setTablePicPath(String tablePicPath) {
		if (tablePicPath != null) {
			this.tablePicPath = tablePicPath;
		}
	}

}
