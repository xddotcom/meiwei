package com.kwchina.ir.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Base_District_Infor")
public class BaseDistrictInfor {

	private Integer districtId;
	private BaseCityInfor city = new BaseCityInfor();
	private TextInfor districtName = new TextInfor();
	List<BaseCircleInfor> circles = new ArrayList<BaseCircleInfor>();

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "districtId")
	public Integer getDistrictId() {
		return districtId;
	}

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "city")
	public BaseCityInfor getCity() {
		return city;
	}

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "districtName")
	public TextInfor getDistrictName() {
		return districtName;
	}
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy="district")
	public List<BaseCircleInfor> getCircles() {
		return circles;
	}

	public void setCity(BaseCityInfor city) {
		if (city != null) {
			this.city = city;
		}
	}

	public void setDistrictId(Integer districtId) {
		if (districtId != null) {
			this.districtId = districtId;
		}
	}

	public void setDistrictName(TextInfor districtName) {
		if (districtName != null) {
			this.districtName = districtName;
		}
	}
	
	public void setCircles(List<BaseCircleInfor> circles) {
		if (circles != null) {
			this.circles = circles;
		}
	}
}
