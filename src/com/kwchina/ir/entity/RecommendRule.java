package com.kwchina.ir.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.kwchina.core.sys.CoreConstant;

@Table(name = "Recommend_Rule")
@Entity
public class RecommendRule {

	private Integer isRecommended = CoreConstant.FIELD_TRUE;
	private Integer ruleId;
	private TextInfor ruleName = new TextInfor();
	private Integer ruleOrder = Integer.MAX_VALUE;

	@Column(name = "isRecommended")
	public Integer getIsRecommended() {
		return isRecommended;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ruleId")
	public Integer getRuleId() {
		return ruleId;
	}

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "ruleName")
	public TextInfor getRuleName() {
		return ruleName;
	}

	@Column(name = "ruleOrder")
	public Integer getRuleOrder() {
		return ruleOrder;
	}

	public void setIsRecommended(Integer isRecommended) {
		if (isRecommended != null) {
			this.isRecommended = isRecommended;
		}
	}

	public void setRuleId(Integer ruleId) {
		if (ruleId != null) {
			this.ruleId = ruleId;
		}
	}

	public void setRuleName(TextInfor ruleName) {
		if (ruleName != null) {
			this.ruleName = ruleName;
		}
	}

	public void setRuleOrder(Integer ruleOrder) {
		if (ruleOrder != null) {
			this.ruleOrder = ruleOrder;
		}
	}

}
