package com.kwchina.ir.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Table(name = "Restaurant_Search")
@Entity
public class RestaurantSearchInfor {

	private Integer id;
	private RestaurantInfor restaurant;
	private String address="";
	private Integer latitude=0;
	private Integer longitude=0;
	private String tile = "";
	private String keywords="";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		if (id != null) {
			this.id = id;
		}
	}

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "restaurant")
	public RestaurantInfor getRestaurant() {
		return restaurant;
	}

	public void setRestaurant(RestaurantInfor restaurant) {
		if (restaurant != null) {
			this.restaurant = restaurant;
		}
	}

	@Column(name = "address", length = 200)
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		if (address != null) {
			this.address = address;
		}
	}

	@Column(name = "latitude")
	public Integer getLatitude() {
		return latitude;
	}

	public void setLatitude(Integer latitude) {
		if (latitude != null) {
			this.latitude = latitude;
		}
	}

	@Column(name = "longitude")
	public Integer getLongitude() {
		return longitude;
	}

	public void setLongitude(Integer longitude) {
		if (longitude != null) {
			this.longitude = longitude;
		}
	}

	@Column(name = "tile", length = 200)
	public String getTile() {
		return tile;
	}

	public void setTile(String tile) {
		if (tile != null) {
			this.tile = tile;
		}
	}

	@Column(name = "keywords", length = 1000)
	public String getKeywords() {
		return keywords;
	}

	public void setKeywords(String keywords) {
		if (keywords != null) {
			this.keywords = keywords;
		}
	}

}
