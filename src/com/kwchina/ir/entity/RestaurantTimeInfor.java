package com.kwchina.ir.entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Table(name = "Restaurant_Time_Infor")
@Entity
public class RestaurantTimeInfor {

	private Integer timeId;
//	private String endDate = "";
	private RestaurantInfor restaurant = null;
//	private String startDate = "";
	
	private TextInfor timeName = new TextInfor();
	private TextInfor discount = new TextInfor();
//	private Integer weeks = 127;

	private Date startTime = new Date(System.currentTimeMillis());
	
	private Date endTime = new Date(System.currentTimeMillis());
		
	private Integer days = 127;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "timeId")
	public Integer getTimeId() {
		return timeId;
	}

	@Temporal(TemporalType.TIME)
	@Column(name = "startTime")
	public Date getStartTime() {
		return startTime;
	}
	
	@Temporal(TemporalType.TIME)
	@Column(name = "endTime")
	public Date getEndTime() {
		return endTime;
	}
	
	@Column(name = "days" )
	public Integer getDays() {
		return days;
	}

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "restaurant")
	public RestaurantInfor getRestaurant() {
		return restaurant;
	}

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "timeName")
	public TextInfor getTimeName() {
		return timeName;
	}

	public void setRestaurant(RestaurantInfor restaurant) {
		if (restaurant != null) {
			this.restaurant = restaurant;
		}
	}
	
	public void setTimeId(Integer timeId) {
		if (timeId != null) {
			this.timeId = timeId;
		}
	}

	public void setTimeName(TextInfor timeName) {
		if (timeName != null) {
			this.timeName = timeName;
		}
	}

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "discount")
	public TextInfor getDiscount() {
		return discount;
	}

	public void setDiscount(TextInfor discount) {
		if (discount != null) {
			this.discount = discount;
		}
	}
	
	public void setDays(Integer days) {
		if (days != null) {
			this.days = days;
		}
	}
	
	public void setEndTime(Date endTime) {
		if (endTime != null) {
			this.endTime = endTime;
		}
	}

	public void setStartTime(Date startTime) {
		if (startTime != null) {
			this.startTime = startTime;
		}
	}
}
