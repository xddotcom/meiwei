package com.kwchina.ir.entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Table(name = "Member_Credit_Infor")
@Entity
public class MemberCreditInfor {

	private Integer creditId;
	private Integer amount = 0;
	private String reason = "";
	private MemberInfor member = null;
	private Date happendTime = new Date(System.currentTimeMillis());
	 //1(new user) 2(order) 3(order on reg day) 4(2 consumes in a week) 5(5 consumes in a month)
	//6 积分兑换
	private Integer creditType = 0;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "creditId")
	public Integer getCreditId() {
		return creditId;
	}

	@Column(name = "amount")
	public Integer getAmount() {
		return amount;
	}

	@Column(name = "reason" ,length=3000)
	public String getReason() {
		return reason;
	}

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "member")
	public MemberInfor getMember() {
		return member;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "happendTime")
	public Date getHappendTime() {
		return happendTime;
	}

	@Column(name = "creditType")
	public Integer getCreditType() {
		return creditType;
	}

	public void setCreditId(Integer creditId) {
		if (creditId != null) {
			this.creditId = creditId;
		}
	}

	public void setAmount(Integer amount) {
		if (amount != null) {
			this.amount = amount;
		}
	}

	public void setReason(String reason) {
		if (reason != null) {
			this.reason = reason;
		}
	}

	public void setMember(MemberInfor member) {
		if (member != null) {
			this.member = member;
		}
	}

	public void setHappendTime(Date happendTime) {
		if (happendTime != null) {
			this.happendTime = happendTime;
		}
	}

	public void setCreditType(Integer creditType) {
		if (creditType != null) {
			this.creditType = creditType;
		}
	}

	public MemberCreditInfor() {
	}

	public MemberCreditInfor(Integer amount, String reason, MemberInfor member,
			Date happendTime, Integer creditType) {
		super();
		this.amount = amount;
		this.reason = reason;
		this.member = member;
		this.happendTime = happendTime;
		this.creditType = creditType;
	}

}
