package com.kwchina.ir.entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.kwchina.core.sys.CoreConstant;

@Table(name = "Member_Review_Infor")
@Entity
public class MemberReviewInfor {

	private String comments = "";
	private Integer endPrice = 0;
	private Integer environmentScore = 3;
	private Integer isChecked = CoreConstant.COMMENT_CHECKED;
	private MemberInfor member = null;
	private RestaurantInfor restaurant = null;
	private Integer reviewId;
	private Date reviewTime = new Date(System.currentTimeMillis());
	private Integer score = 3;
	private Integer serviceScore = 3;
	private Integer startPrice = 0;
	private Integer tasteScore = 3;

	@Column(name = "comments", length = 1000)
	public String getComments() {
		return comments;
	}

	@Column(name = "priceHigh")
	public Integer getEndPrice() {
		return endPrice;
	}

	@Column(name = "environmentScore")
	public Integer getEnvironmentScore() {
		return environmentScore;
	}

	@Column(name = "isChecked")
	public Integer getIsChecked() {
		return isChecked;
	}

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "member")
	public MemberInfor getMember() {
		return member;
	}

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "restaurant")
	public RestaurantInfor getRestaurant() {
		return restaurant;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "reviewId")
	public Integer getReviewId() {
		return reviewId;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "reviewTime")
	public Date getReviewTime() {
		return reviewTime;
	}

	@Column(name = "score")
	public Integer getScore() {
		return score;
	}

	@Column(name = "serviceScore")
	public Integer getServiceScore() {
		return serviceScore;
	}

	@Column(name = "priceLow")
	public Integer getStartPrice() {
		return startPrice;
	}

	@Column(name = "tasteScore")
	public Integer getTasteScore() {
		return tasteScore;
	}

	public void setComments(String comments) {
		if (comments != null) {
			this.comments = comments;
		}
	}

	public void setEndPrice(Integer endPrice) {
		if (endPrice != null) {
			this.endPrice = endPrice;
		}
	}

	public void setEnvironmentScore(Integer environmentScore) {
		if (environmentScore != null) {
			this.environmentScore = environmentScore;
		}
	}

	public void setIsChecked(Integer isChecked) {
		if (isChecked != null) {
			this.isChecked = isChecked;
		}
	}

	public void setMember(MemberInfor member) {
		if (member != null) {
			this.member = member;
		}
	}

	public void setRestaurant(RestaurantInfor restaurant) {
		if (restaurant != null) {
			this.restaurant = restaurant;
		}
	}

	public void setReviewId(Integer reviewId) {
		if (reviewId != null) {
			this.reviewId = reviewId;
		}
	}

	public void setReviewTime(Date reviewTime) {
		if (reviewTime != null) {
			this.reviewTime = reviewTime;
		}
	}

	public void setScore(Integer score) {
		if (score != null) {
			this.score = score;
		}
	}

	public void setServiceScore(Integer serviceScore) {
		if (serviceScore != null) {
			this.serviceScore = serviceScore;
		}
	}

	public void setStartPrice(Integer startPrice) {
		if (startPrice != null) {
			this.startPrice = startPrice;
		}
	}

	public void setTasteScore(Integer tasteScore) {
		if (tasteScore != null) {
			this.tasteScore = tasteScore;
		}
	}

	public void delete() {
		this.member = null;
		this.restaurant = null;
		
	}

}
