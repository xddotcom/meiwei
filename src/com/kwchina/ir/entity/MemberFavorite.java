package com.kwchina.ir.entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Table(name = "Member_Favorite")
@Entity
public class MemberFavorite {

	private Integer favoriteId;
	private Date favoriteTime = new Date(System.currentTimeMillis());
	private MemberInfor member = null;
	private RestaurantInfor restaurant = null;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "favoriteId")
	public Integer getFavoriteId() {
		return favoriteId;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "favoriteTime")
	public Date getFavoriteTime() {
		return favoriteTime;
	}

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "member")
	public MemberInfor getMember() {
		return member;
	}

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "restaurant")
	public RestaurantInfor getRestaurant() {
		return restaurant;
	}

	public void setFavoriteId(Integer favoriteId) {
		if (favoriteId != null) {
			this.favoriteId = favoriteId;
		}
	}

	public void setFavoriteTime(Date favoriteTime) {
		if (favoriteTime != null) {
			this.favoriteTime = favoriteTime;
		}
	}

	public void setMember(MemberInfor member) {
		if (member != null) {
			this.member = member;
		}
	}

	public void setRestaurant(RestaurantInfor restaurant) {
		if (restaurant != null) {
			this.restaurant = restaurant;
		}
	}

	public void delete() {
		this.member = null;
		this.restaurant = null;
	}
	
}
