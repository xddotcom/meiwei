package com.kwchina.ir.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "Member_Reference_Infor")
@Entity
public class MemberReferenceInfor {

	private Integer referenceId;	//For each member with memberid = ###, pick a number with referenceId = ### for him 
	private String number = "";
	private String refnumber = "";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "referenceId")
	public Integer getReferenceId() {
		return referenceId;
	}

	@Column(name = "number", length = 100, unique = true, nullable = false)
	public String getNumber() {
		return number;
	}

	@Column(name = "refnumber", length = 100)
	public String getRefnumber() {
		return refnumber;
	}

	public void setReferenceId(Integer referenceId) {
		if (referenceId != null) {
			this.referenceId = referenceId;
		}
	}

	public void setNumber(String number) {
		if (number != null) {
			this.number = number;
		}
	}

	public void setRefnumber(String refnumber) {
		if (refnumber != null) {
			this.refnumber = refnumber;
		}
	}

}
