package com.kwchina.ir.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Table(name = "Restaurant_Pic_Infor")
@Entity
public class RestaurantPicInfor {

	private String bigPath = "";
	private Integer displayOrder = Integer.MAX_VALUE;
	private Integer picId;
	private String picTitle = "";
	private RestaurantInfor restaurant = null;
	private String smallPath = "";

	@Column(name = "bigPath", length = 300)
	public String getBigPath() {
		return bigPath;
	}

	@Column(name = "displayOrder")
	public Integer getDisplayOrder() {
		return displayOrder;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "picId")
	public Integer getPicId() {
		return picId;
	}

	@Column(name = "picTitle", length = 100)
	public String getPicTitle() {
		return picTitle;
	}

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "restaurant")
	public RestaurantInfor getRestaurant() {
		return restaurant;
	}

	@Column(name = "smallPath", length = 300)
	public String getSmallPath() {
		return smallPath;
	}

	public void setBigPath(String bigPath) {
		if (bigPath != null) {
			this.bigPath = bigPath;
		}
	}

	public void setDisplayOrder(Integer displayOrder) {
		if (displayOrder != null) {
			this.displayOrder = displayOrder;
		}
	}

	public void setPicId(Integer picId) {
		if (picId != null) {
			this.picId = picId;
		}
	}

	public void setPicTitle(String picTitle) {
		if (picTitle != null) {
			this.picTitle = picTitle;
		}
	}

	public void setRestaurant(RestaurantInfor restaurant) {
		if (restaurant != null) {
			this.restaurant = restaurant;
		}
	}

	public void setSmallPath(String smallPath) {
		if (smallPath != null) {
			this.smallPath = smallPath;
		}
	}

}
