package com.kwchina.ir.entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Table(name = "History_View_Infor")
@Entity
public class HistoryViewInfor {

	private Date historyDate = new Date(System.currentTimeMillis());
	private Integer historyId;
	private MemberInfor member = null;
	private RestaurantInfor restaurant = null;

	public HistoryViewInfor() {
		super();
	}

	public HistoryViewInfor(MemberInfor member, RestaurantInfor restaurant) {
		super();
		this.setMember(member);
		this.setRestaurant(restaurant);
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "historyDate")
	public Date getHistoryDate() {
		return historyDate;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "historyId")
	public Integer getHistoryId() {
		return historyId;
	}

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "member")
	public MemberInfor getMember() {
		return member;
	}

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "restaurant")
	public RestaurantInfor getRestaurant() {
		return restaurant;
	}

	public void setHistoryDate(Date historyDate) {
		if (historyDate != null) {
			this.historyDate = historyDate;
		}
	}

	public void setHistoryId(Integer historyId) {
		if (historyId != null) {
			this.historyId = historyId;
		}
	}

	public void setMember(MemberInfor member) {
		if (member != null) {
			this.member = member;
		}
	}

	public void setRestaurant(RestaurantInfor restaurant) {
		if (restaurant != null) {
			this.restaurant = restaurant;
		}
	}

}
