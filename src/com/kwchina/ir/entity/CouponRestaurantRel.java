package com.kwchina.ir.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Table(name = "Coupon_Restaurant_Rel")
@Entity
public class CouponRestaurantRel {

	private CouponInfor coupon = new CouponInfor();
	private Integer id;
	private RestaurantInfor restaurant = null;

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "coupon")
	public CouponInfor getCoupon() {
		return coupon;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	public Integer getId() {
		return id;
	}

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "restaurant")
	public RestaurantInfor getRestaurant() {
		return restaurant;
	}

	public void setCoupon(CouponInfor coupon) {
		if (coupon != null) {
			this.coupon = coupon;
		}
	}

	public void setId(Integer id) {
		if (id != null) {
			this.id = id;
		}
	}

	public void setRestaurant(RestaurantInfor restaurant) {
		if (restaurant != null) {
			this.restaurant = restaurant;
		}
	}

}
