package com.kwchina.ir.entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.kwchina.core.sys.CoreConstant;

@Table(name = "Member_Infor")
@JsonIgnoreProperties(value = { "personalInfor" })
@Entity
public class MemberInfor {

	private String hashCode;
	private Integer isDeleted = CoreConstant.FIELD_FALSE;
	private Date lastTime = new Date(System.currentTimeMillis());
	private String loginName;
	private String loginPassword;
	private Integer memberId;
	private Integer memberType = CoreConstant.MEMBERTYPE_PERSONAL;
	private MemberPersonalInfor personalInfor = null;
	private Date registerTime = new Date(System.currentTimeMillis());

	@Column(name = "hashCode", length = 100)
	public String getHashCode() {
		return hashCode;
	}

	@Column(name = "isDeleted")
	public Integer getIsDeleted() {
		return isDeleted;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "lastTime")
	public Date getLastTime() {
		return lastTime;
	}

	@Column(name = "loginName", length = 120, unique = true, nullable = false)
	public String getLoginName() {
		return loginName;
	}

	@Column(name = "loginPassword", length = 128, nullable = false)
	public String getLoginPassword() {
		return loginPassword;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "memberId")
	public Integer getMemberId() {
		return memberId;
	}

	@Column(name = "memberType")
	public Integer getMemberType() {
		return memberType;
	}

	@OneToOne(cascade = CascadeType.ALL, mappedBy = "member", fetch = FetchType.EAGER)
	@JoinColumn(name = "personalInfor")
	public MemberPersonalInfor getPersonalInfor() {
		return personalInfor;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "registerTime")
	public Date getRegisterTime() {
		return registerTime;
	}

	public void setHashCode(String hashCode) {
		if (hashCode != null) {
			this.hashCode = hashCode;
		}
	}

	public void setIsDeleted(Integer isDeleted) {
		if (isDeleted != null) {
			this.isDeleted = isDeleted;
		}
	}

	public void setLastTime(Date lastTime) {
		if (lastTime != null) {
			this.lastTime = lastTime;
		}
	}

	public void setLoginName(String loginName) {
		if (loginName != null) {
			this.loginName = loginName;
		}
	}

	public void setLoginPassword(String loginPassword) {
		if (loginPassword != null) {
			this.loginPassword = loginPassword;
		}
	}

	public void setMemberId(Integer memberId) {
		if (memberId != null) {
			this.memberId = memberId;
		}
	}

	public void setMemberType(Integer memberType) {
		if (memberType != null) {
			this.memberType = memberType;
		}
	}

	public void setPersonalInfor(MemberPersonalInfor personalInfor) {
		if (personalInfor != null) {
			this.personalInfor = personalInfor;
		}
	}

	public void setRegisterTime(Date registerTime) {
		if (registerTime != null) {
			this.registerTime = registerTime;
		}
	}

}
