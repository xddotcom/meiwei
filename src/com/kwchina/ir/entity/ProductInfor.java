package com.kwchina.ir.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Table(name = "product_Infor")
@Entity
public class ProductInfor {

	private Integer productId;

	private ProductTypeInfor productType;

	private TextInfor productName = new TextInfor();

	private TextInfor detail = new TextInfor();

	private String price = "";

	private Integer count = 0;

	private TextInfor model = new TextInfor();// 型号

	private Integer credit = 0;

	private String picturePath = "";

	private TextInfor productRemark = new TextInfor();

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "productId")
	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		if (productId != null) {
			this.productId = productId;
		}
	}

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "detail")
	public TextInfor getDetail() {
		return detail;
	}

	public void setDetail(TextInfor detail) {
		if (detail != null) {
			this.detail = detail;
		}
	}

	@Column(name = "price", length = 40)
	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		if (price != null) {
			this.price = price;
		}
	}

	@Column(name = "count")
	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		if (count != null) {
			this.count = count;
		}
	}

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "model")
	public TextInfor getModel() {
		return model;
	}

	public void setModel(TextInfor model) {
		if (model != null) {
			this.model = model;
		}
	}

	@Column(name = "credit")
	public Integer getCredit() {
		return credit;
	}

	public void setCredit(Integer credit) {
		if (credit != null) {
			this.credit = credit;
		}
	}

	@Column(name = "picturePath", length = 100)
	public String getPicturePath() {
		return picturePath;
	}

	public void setPicturePath(String picturePath) {
		if (picturePath != null) {
			this.picturePath = picturePath;
		}
	}

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "productType")
	public ProductTypeInfor getProductType() {
		return productType;
	}

	public void setProductType(ProductTypeInfor productType) {
		if (productType != null) {
			this.productType = productType;
		}
	}

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "productName")
	public TextInfor getProductName() {
		return productName;
	}

	public void setProductName(TextInfor productName) {
		if (productName != null) {
			this.productName = productName;
		}
	}

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "productRemark")
	public TextInfor getProductRemark() {
		return productRemark;
	}

	public void setProductRemark(TextInfor productRemark) {
		if (productRemark != null) {
			this.productRemark = productRemark;
		}
	}

}
