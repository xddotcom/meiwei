package com.kwchina.ir.entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.kwchina.core.sys.CoreConstant;

@Table(name = "Coupon_Infor")
@Entity
public class CouponInfor {

	private Integer available = CoreConstant.FIELD_TRUE;
	private Integer couponId;
	private TextInfor couponName = new TextInfor();
	private TextInfor description = new TextInfor();
	private Double discount = 0.0;
	private Date endTime = new Date(System.currentTimeMillis());
	private Date stateTime = new Date(System.currentTimeMillis());

	@Column(name = "available")
	public Integer getAvailable() {
		return available;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "couponId")
	public Integer getCouponId() {
		return couponId;
	}

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "couponName")
	public TextInfor getCouponName() {
		return couponName;
	}

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "description")
	public TextInfor getDescription() {
		return description;
	}

	@Column(name = "discount")
	public Double getDiscount() {
		return discount;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "endTime")
	public Date getEndTime() {
		return endTime;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "stateTime")
	public Date getStateTime() {
		return stateTime;
	}

	public void setAvailable(Integer available) {
		if (available != null) {
			this.available = available;
		}
	}

	public void setCouponId(Integer couponId) {
		if (couponId != null) {
			this.couponId = couponId;
		}
	}

	public void setCouponName(TextInfor couponName) {
		if (couponName != null) {
			this.couponName = couponName;
		}
	}

	public void setDescription(TextInfor description) {
		if (description != null) {
			this.description = description;
		}
	}

	public void setDiscount(Double discount) {
		if (discount != null) {
			this.discount = discount;
		}
	}

	public void setEndTime(Date endTime) {
		if (endTime != null) {
			this.endTime = endTime;
		}
	}

	public void setStateTime(Date stateTime) {
		if (stateTime != null) {
			this.stateTime = stateTime;
		}
	}

}
