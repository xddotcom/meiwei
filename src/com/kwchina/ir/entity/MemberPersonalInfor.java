package com.kwchina.ir.entity;

import java.util.Date;
import java.util.GregorianCalendar;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.kwchina.core.sys.CoreConstant;

@Table(name = "Member_Personal_Infor")
@Entity
public class MemberPersonalInfor {

	private String anniversary = "";
	private Date birthday = new GregorianCalendar(1900, 0, 1).getTime();
	private Integer credit = 0;
	private String email = null;
	private Integer Id;
	private MemberInfor member = null;
	private String memberNo;
	private String mobile = null;
	private String nickName = "";
	private Integer sexe = CoreConstant.SEX_MALE;

	@Column(name = "anniversary", length = 100)
	public String getAnniversary() {
		return anniversary;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "birthday")
	public Date getBirthday() {
		return birthday;
	}

	@Column(name = "credit")
	public Integer getCredit() {
		return credit;
	}

	@Column(name = "email", length = 120, unique = true)
	public String getEmail() {
		return email;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "Id")
	public Integer getId() {
		return Id;
	}

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "member", nullable = false)
	public MemberInfor getMember() {
		return member;
	}

	@Column(name = "memberNo", length = 100, unique = true, nullable = false)
	public String getMemberNo() {
		return memberNo;
	}

	@Column(name = "mobile", length = 20, unique = true)
	public String getMobile() {
		return mobile;
	}

	@Column(name = "nickName", length = 20)
	public String getNickName() {
		return nickName;
	}

	@Column(name = "sexe")
	public Integer getSexe() {
		return sexe;
	}

	public void setAnniversary(String anniversary) {
		if (anniversary != null) {
			this.anniversary = anniversary;
		}
	}

	public void setBirthday(Date birthday) {
		if (birthday != null) {
			this.birthday = birthday;
		}
	}

	public void setCredit(Integer credit) {
		if (credit != null) {
			this.credit = credit;
		}
	}

	public void setEmail(String email) {
		if (email != null) {
			this.email = email;
		}
	}

	public void setId(Integer id) {
		if (id != null) {
			Id = id;
		}
	}

	public void setMember(MemberInfor member) {
		if (member != null) {
			this.member = member;
		}
	}

	public void setMemberNo(String memberNo) {
		if (memberNo != null) {
			this.memberNo = memberNo;
		}
	}

	public void setMobile(String mobile) {
		if (mobile != null) {
			this.mobile = mobile;
		}
	}

	public void setNickName(String nickName) {
		if (nickName != null) {
			this.nickName = nickName;
		}
	}

	public void setSexe(Integer sexe) {
		if (sexe != null) {
			this.sexe = sexe;
		}
	}

}
