package com.kwchina.ir.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Table(name = "Recommend_Infor")
@Entity
public class RecommendInfor {

	private Integer rank = Integer.MAX_VALUE;
	private RecommendRule rankRule = new RecommendRule();
	private Integer recommendId;
	private RestaurantInfor restaurant = null;

	@Column(name = "rank")
	public Integer getRank() {
		return rank;
	}

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "rankRule")
	public RecommendRule getRankRule() {
		return rankRule;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "recommendId")
	public Integer getRecommendId() {
		return recommendId;
	}

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "restaurant")
	public RestaurantInfor getRestaurant() {
		return restaurant;
	}

	public void setRank(Integer rank) {
		if (rank != null) {
			this.rank = rank;
		}
	}

	public void setRankRule(RecommendRule rankRule) {
		if (rankRule != null) {
			this.rankRule = rankRule;
		}
	}

	public void setRecommendId(Integer recommendId) {
		if (recommendId != null) {
			this.recommendId = recommendId;
		}
	}

	public void setRestaurant(RestaurantInfor restaurant) {
		if (restaurant != null) {
			this.restaurant = restaurant;
		}
	}

}
