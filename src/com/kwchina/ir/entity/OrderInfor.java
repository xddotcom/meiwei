package com.kwchina.ir.entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.kwchina.core.sys.CoreConstant;

@Table(name = "Order_Infor")
@Entity
public class OrderInfor {

	private Double commission = 0.0;
	private Double consumeAmount = 0.0;	
	private String contactName = "";
	private String contactTelphone = "";
	private MemberInfor member = null;
	private Date orderDate = new Date(System.currentTimeMillis()); // 就餐时间
	private Integer orderId;
	private String orderNo = "";
	private Date orderSubmitTime = new Date(System.currentTimeMillis()); // 订单时间
	private Date orderTime = new Date(System.currentTimeMillis()); // 就餐时间
	private String other = "";
	private Integer creditGain = 0;
	private Integer personNum = 1; // 就餐人数
	private RestaurantInfor restaurant = null;
	private Integer status = CoreConstant.ORDER_STATUS_NEW;
	private String tables = "";
	private String adminother = "";
	private String inviteFriends = "";//邀请就餐 id1|id2...
	private String productIds = "";
	
	@Column(name = "commission")
	public Double getCommission() {
		return commission;
	}

	@Column(name = "consumeAmount")
	public Double getConsumeAmount() {
		return consumeAmount;
	}

	@Column(name = "contactName" , length= 20 )
	public String getContactName() {
		return contactName;
	}
	
	@Column(name = "contactTelphone", length = 20)
	public String getContactTelphone() {
		return contactTelphone;
	}
	
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "member")
	public MemberInfor getMember() {
		return member;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "orderDate")
	public Date getOrderDate() {
		return orderDate;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "orderId")
	public Integer getOrderId() {
		return orderId;
	}

	@Column(name = "orderNo", length = 100, unique = true, nullable = false)
	public String getOrderNo() {
		return orderNo;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "orderSubmitTime")
	public Date getOrderSubmitTime() {
		return orderSubmitTime;
	}

	@Temporal(TemporalType.TIME)
	@Column(name = "orderTime")
	public Date getOrderTime() {
		return orderTime;
	}

	@Column(name = "other", length = 1000)
	public String getOther() {
		return other;
	}

	@Column(name = "personNum")
	public Integer getPersonNum() {
		return personNum;
	}

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "restaurant")
	public RestaurantInfor getRestaurant() {
		return restaurant;
	}

	@Column(name = "status")
	public Integer getStatus() {
		return status;
	}

	@Column(name = "tables", length = 200)
	public String getTables() {
		return tables;
	}

	@Column(name = "creditGain")
	public Integer getCreditGain() {
		return creditGain;
	}
	
	@Column(name = "adminother", length = 1000)
	public String getAdminother() {
		return adminother;
	}
	
	@Column(name = "inviteFriends", length = 50)
	public String getInviteFriends() {
		return inviteFriends;
	}
	
	@Column(name = "productIds", length = 100)
	public String getProductIds() {
		return productIds;
	}
	
	public void setCommission(Double commission) {
		if (commission != null) {
			this.commission = commission;
		}
	}

	public void setConsumeAmount(Double consumeAmount) {
		if (consumeAmount != null) {
			this.consumeAmount = consumeAmount;
		}
	}
	
	public void setContactName(String contactName) {
		if (contactName != null) {
			this.contactName = contactName;
		}
	}
	
	public void setContactTelphone(String contactTelphone) {
		if (contactTelphone != null) {
			this.contactTelphone = contactTelphone;
		}
	}
	
	public void setMember(MemberInfor member) {
		if (member != null) {
			this.member = member;
		}
	}

	public void setOrderDate(Date orderDate) {
		if (orderDate != null) {
			this.orderDate = orderDate;
		}
	}

	public void setOrderId(Integer orderId) {
		if (orderId != null) {
			this.orderId = orderId;
		}
	}

	public void setOrderNo(String orderNo) {
		if (orderNo != null) {
			this.orderNo = orderNo;
		}
	}

	public void setOrderSubmitTime(Date orderSubmitTime) {
		if (orderSubmitTime != null) {
			this.orderSubmitTime = orderSubmitTime;
		}
	}

	public void setOrderTime(Date orderTime) {
		if (orderTime != null) {
			this.orderTime = orderTime;
		}
	}

	public void setOther(String other) {
		if (other != null) {
			this.other = other;
		}
	}

	public void setPersonNum(Integer personNum) {
		if (personNum != null) {
			this.personNum = personNum;
		}
	}

	public void setRestaurant(RestaurantInfor restaurant) {
		if (restaurant != null) {
			this.restaurant = restaurant;
		}
	}

	public void setStatus(Integer status) {
		if (status != null) {
			this.status = status;
		}
	}

	public void setTables(String tables) {
		if (tables != null) {
			this.tables = tables;
		}
	}

	public void setCreditGain(Integer creditGain) {
		if(creditGain !=null){
			this.creditGain = creditGain;
		}
	}

	public void setAdminother(String adminother) {
		if(adminother !=null){
			this.adminother = adminother;
		}
	}
	
	public void setInviteFriends(String inviteFriends) {
		if (inviteFriends != null) {
			this.inviteFriends = inviteFriends;
		}
	}
	
	public void setProductIds(String productIds) {
		if (productIds != null) {
			this.productIds = productIds;
		}
	}
	
}
