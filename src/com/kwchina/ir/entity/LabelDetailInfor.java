//package com.kwchina.ir.entity;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.Table;
//
//@Table(name = "Label_Detail_Infor")
//@Entity
//public class LabelDetailInfor {
//
//	private Integer id;
//	
//	private String labelOption = "";
//	
//	private String labelName = "";
//		
//	private String labelValue = "";
//
//	@Id
//	@GeneratedValue(strategy = GenerationType.IDENTITY)
//	@Column(name = "id")
//	public Integer getId() {
//		return id;
//	}
//
//	public void setId(Integer id) {
//		if (id != null) {
//			this.id = id;
//		}
//	}
//
//	@Column(name = "labelOption" , length = 50)
//	public String getLabelOption() {
//		return labelOption;
//	}
//
//	public void setLabelOption(String labelOption) {
//		if (labelOption != null) {
//			this.labelOption = labelOption;
//		}
//	}
//
//	@Column(name = "labelName" , length = 100)
//	public String getLabelName() {
//		return labelName;
//	}
//
//	public void setLabelName(String labelName) {
//		if (labelName != null) {
//			this.labelName = labelName;
//		}
//	}
//
//	@Column(name = "labelValue" , length = 300)
//	public String getLabelValue() {
//		return labelValue;
//	}
//
//	public void setLabelValue(String labelValue) {
//		if (labelValue != null) {
//			this.labelValue = labelValue;
//		}
//	}
//	
//}
