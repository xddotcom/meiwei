package com.kwchina.ir.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.kwchina.core.sys.CoreConstant;

@Table(name = "Restaurant_Menu_Infor")
@Entity
public class RestaurantMenuInfor {

	private TextInfor description = new TextInfor();
	private Integer isRecommended = CoreConstant.FIELD_TRUE;
	private Integer menuCategory = CoreConstant.MENUCATEGORY_MEAL;
	private Integer menuId;
	private TextInfor menuName = new TextInfor();
	private String menuPic = "";
	private Double price = 0.0;
	private RestaurantInfor restaurant = null;

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "description")
	public TextInfor getDescription() {
		return description;
	}

	@Column(name = "isRecommended")
	public Integer getIsRecommended() {
		return isRecommended;
	}

	@Column(name = "menuCategory")
	public Integer getMenuCategory() {
		return menuCategory;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "menuId")
	public Integer getMenuId() {
		return menuId;
	}

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "menuName")
	public TextInfor getMenuName() {
		return menuName;
	}

	@Column(name = "menuPic", length = 300)
	public String getMenuPic() {
		return menuPic;
	}

	@Column(name = "price")
	public Double getPrice() {
		return price;
	}

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "restaurant")
	public RestaurantInfor getRestaurant() {
		return restaurant;
	}

	public void setDescription(TextInfor description) {
		if (description != null) {
			this.description = description;
		}
	}

	public void setIsRecommended(Integer isRecommended) {
		if (isRecommended != null) {
			this.isRecommended = isRecommended;
		}
	}

	public void setMenuCategory(Integer menuCategory) {
		if (menuCategory != null) {
			this.menuCategory = menuCategory;
		}
	}

	public void setMenuId(Integer menuId) {
		if (menuId != null) {
			this.menuId = menuId;
		}
	}

	public void setMenuName(TextInfor menuName) {
		if (menuName != null) {
			this.menuName = menuName;
		}
	}

	public void setMenuPic(String menuPic) {
		if (menuPic != null) {
			this.menuPic = menuPic;
		}
	}

	public void setPrice(Double price) {
		if (price != null) {
			this.price = price;
		}
	}

	public void setRestaurant(RestaurantInfor restaurant) {
		if (restaurant != null) {
			this.restaurant = restaurant;
		}
	}

}
