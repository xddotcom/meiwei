package com.kwchina.ir.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.kwchina.core.sys.CoreConstant;

@Entity
@Table(name = "Base_Ad_Infor")
public class BaseAdInfor {

	private Integer adId;
	private TextInfor adIntro = new TextInfor();
	private String adLink = "";
	private Integer adType = CoreConstant.ADTYPE_PICTUER;
	private String filePath = "";
	private Integer linkType = CoreConstant.TARGET_FORMER;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "adId")
	public Integer getAdId() {
		return adId;
	}

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "adIntro")
	public TextInfor getAdIntro() {
		return adIntro;
	}

	@Column(name = "adLink", length = 200)
	public String getAdLink() {
		return adLink;
	}

	@Column(name = "adType")
	public Integer getAdType() {
		return adType;
	}

	@Column(name = "filePath", length = 200)
	public String getFilePath() {
		return filePath;
	}

	@Column(name = "linkType")
	public Integer getLinkType() {
		return linkType;
	}

	public void setAdId(Integer adId) {
		if (adId != null) {
			this.adId = adId;
		}
	}

	public void setAdIntro(TextInfor adIntro) {
		if (adIntro != null) {
			this.adIntro = adIntro;
		}
	}

	public void setAdLink(String adLink) {
		if (adLink != null) {
			this.adLink = adLink;
		}
	}

	public void setAdType(Integer adType) {
		if (adType != null) {
			this.adType = adType;
		}
	}

	public void setFilePath(String filePath) {
		if (filePath != null) {
			this.filePath = filePath;
		}
	}

	public void setLinkType(Integer linkType) {
		if (linkType != null) {
			this.linkType = linkType;
		}
	}

}
