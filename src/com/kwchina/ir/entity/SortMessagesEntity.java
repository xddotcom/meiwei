package com.kwchina.ir.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "Sort_Messages")
public class SortMessagesEntity {

	private Integer messageId;
	private String content = "";
	private String receiverMobile = "";
	private String receiverName = "";
	private Date sendTime = new Date(System.currentTimeMillis());
	private Integer status = 0; // 0-succeed 1-failed

	public SortMessagesEntity() {
		super();
	}

	public SortMessagesEntity(String receiverName, String receiverMobile, String content,
			Date sendTime, Integer status) {
		super();
		setReceiverName(receiverName);
		setReceiverMobile(receiverMobile);
		setContent(content);
		setSendTime(sendTime);
		setStatus(status);
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "messageId")
	public Integer getMessageId() {
		return messageId;
	}

	@Column(name = "content", length = 500)
	public String getContent() {
		return content;
	}

	@Column(name = "receiverMobile", length = 20)
	public String getReceiverMobile() {
		return receiverMobile;
	}

	@Column(name = "receiverName", length = 80)
	public String getReceiverName() {
		return receiverName;
	}

	@Temporal(TemporalType.TIMESTAMP)
	public Date getSendTime() {
		return sendTime;
	}

	@Column(name = "status")
	public Integer getStatus() {
		return status;
	}

	public void setContent(String content) {
		if (content != null) {
			this.content = content;
		}
	}

	public void setMessageId(Integer messageId) {
		if (messageId != null) {
			this.messageId = messageId;
		}
	}

	public void setReceiverMobile(String receiverMobile) {
		if (receiverMobile != null) {
			this.receiverMobile = receiverMobile;
		}
	}

	public void setReceiverName(String receiverName) {
		if (receiverName != null) {
			this.receiverName = receiverName;
		}
	}

	public void setSendTime(Date sendTime) {
		if (sendTime != null) {
			this.sendTime = sendTime;
		}
	}

	public void setStatus(Integer status) {
		if (status != null) {
			this.status = status;
		}
	}

}
