package com.kwchina.ir.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Base_City_Infor")
public class BaseCityInfor {

	private Integer cityId;
	private TextInfor cityName = new TextInfor();
	private TextInfor country = new TextInfor();

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "cityId")
	public Integer getCityId() {
		return cityId;
	}

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "cityName")
	public TextInfor getCityName() {
		return cityName;
	}

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "country")
	public TextInfor getCountry() {
		return country;
	}

	public void setCityId(Integer cityId) {
		if (cityId != null) {
			this.cityId = cityId;
		}
	}

	public void setCityName(TextInfor cityName) {
		if (cityName != null) {
			this.cityName = cityName;
		}
	}

	public void setCountry(TextInfor country) {
		if (country != null) {
			this.country = country;
		}
	}

}
