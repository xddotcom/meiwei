package com.kwchina.ir.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Table(name = "productType_Infor")
@Entity
public class ProductTypeInfor {

	private Integer productTypeId;
	
	// 1 ： 增值服务  2：积分兑换
	private Integer category = 0;
	
	private TextInfor typeName = new TextInfor();
	
	private TextInfor typeRemark = new TextInfor();
	
	private Integer typeOrder = Integer.MAX_VALUE;
	
	private List<ProductInfor> products = new ArrayList<ProductInfor>();
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "productTypeId")
	public Integer getProductTypeId() {
		return productTypeId;
	}

	public void setProductTypeId(Integer productTypeId) {
		if (productTypeId != null) {
			this.productTypeId = productTypeId;
		}
	}

	@Column(name = "category" )
	public Integer getCategory() {
		return category;
	}

	public void setCategory(Integer category) {
		if (category != null) {
			this.category = category;
		}
	}
	
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "typeName")
	public TextInfor getTypeName() {
		return typeName;
	}

	public void setTypeName(TextInfor typeName) {
		if (typeName != null) {
			this.typeName = typeName;
		}
	}

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "typeRemark")
	public TextInfor getTypeRemark() {
		return typeRemark;
	}

	public void setTypeRemark(TextInfor typeRemark) {
		if (typeRemark != null) {
			this.typeRemark = typeRemark;
		}
	}

	@Column(name="typeOrder")
	public Integer getTypeOrder() {
		return typeOrder;
	}

	public void setTypeOrder(Integer typeOrder) {
		if (typeOrder != null) {
			this.typeOrder = typeOrder;
		}
	}

	@OneToMany(cascade = CascadeType.ALL, mappedBy="productType", fetch = FetchType.EAGER)
	public List<ProductInfor> getProducts() {
		return products;
	}

	public void setProducts(List<ProductInfor> products) {
		if (products != null) {
			this.products = products;
		}
	}
	
	
	
}
