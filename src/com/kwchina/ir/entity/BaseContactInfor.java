package com.kwchina.ir.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.kwchina.core.sys.CoreConstant;

@Table(name = "Base_Contact_Infor")
@Entity
public class BaseContactInfor {

	private Integer contactId;
	private String contactName = "";
	private Integer contactType = CoreConstant.CONTACT_TYPE_MEMBER;
	private String email = "";
	private Date messageDate = new Date(System.currentTimeMillis());
	private String mobile = "";
	private String remark = "";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "contactId")
	public Integer getContactId() {
		return contactId;
	}

	@Column(name = "contactName", length = 100)
	public String getContactName() {
		return contactName;
	}

	@Column(name = "contactType")
	public Integer getContactType() {
		return contactType;
	}

	@Column(name = "email", length = 80)
	public String getEmail() {
		return email;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "messageDate")
	public Date getMessageDate() {
		return messageDate;
	}

	@Column(name = "mobile", length = 20)
	public String getMobile() {
		return mobile;
	}

	@Column(name = "remark", length = 1000)
	public String getRemark() {
		return remark;
	}

	public void setContactId(Integer contactId) {
		if (contactId != null) {
			this.contactId = contactId;
		}
	}

	public void setContactName(String contactName) {
		if (contactName != null) {
			this.contactName = contactName;
		}
	}

	public void setContactType(Integer contactType) {
		if (contactType != null) {
			this.contactType = contactType;
		}
	}

	public void setEmail(String email) {
		if (email != null) {
			this.email = email;
		}
	}

	public void setMessageDate(Date messageDate) {
		if (messageDate != null) {
			this.messageDate = messageDate;
		}
	}

	public void setMobile(String mobile) {
		if (mobile != null) {
			this.mobile = mobile;
		}
	}

	public void setRemark(String remark) {
		if (remark != null) {
			this.remark = remark;
		}
	}

}
