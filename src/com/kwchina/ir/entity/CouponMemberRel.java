package com.kwchina.ir.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Table(name = "Coupon_Member_Rel")
@Entity
public class CouponMemberRel {

	private CouponInfor coupon = new CouponInfor();
	private Integer id;
	private MemberInfor member = null;
	private Integer quantity = 0;

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "coupon")
	public CouponInfor getCoupon() {
		return coupon;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	public Integer getId() {
		return id;
	}

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "member")
	public MemberInfor getMember() {
		return member;
	}

	@Column(name = "quantity")
	public Integer getQuantity() {
		return quantity;
	}

	public void setCoupon(CouponInfor coupon) {
		if (coupon != null) {
			this.coupon = coupon;
		}
	}

	public void setId(Integer id) {
		if (id != null) {
			this.id = id;
		}
	}

	public void setMember(MemberInfor member) {
		if (member != null) {
			this.member = member;
		}
	}

	public void setQuantity(Integer quantity) {
		if (quantity != null) {
			this.quantity = quantity;
		}
	}

}
