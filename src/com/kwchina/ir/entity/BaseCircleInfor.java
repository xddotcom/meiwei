package com.kwchina.ir.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.kwchina.core.sys.CoreConstant;

@Entity
@Table(name = "Base_Circle_Infor")
public class BaseCircleInfor {

	private Integer circleId;
	private TextInfor circleName = new TextInfor();
	private BaseDistrictInfor district = new BaseDistrictInfor();
	private Integer isRecommended = CoreConstant.FIELD_TRUE;
	private Integer rank = Integer.MAX_VALUE;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "circleId")
	public Integer getCircleId() {
		return circleId;
	}

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "circleName")
	public TextInfor getCircleName() {
		return circleName;
	}

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "district")
	public BaseDistrictInfor getDistrict() {
		return district;
	}

	@Column(name = "isRecommended")
	public Integer getIsRecommended() {
		return isRecommended;
	}

	@Column(name = "rank")
	public Integer getRank() {
		return rank;
	}

	public void setCircleId(Integer circleId) {
		if (circleId != null) {
			this.circleId = circleId;
		}
	}

	public void setCircleName(TextInfor circleName) {
		if (circleName != null) {
			this.circleName = circleName;
		}
	}

	public void setDistrict(BaseDistrictInfor district) {
		if (district != null) {
			this.district = district;
		}
	}

	public void setIsRecommended(Integer isRecommended) {
		if (isRecommended != null) {
			this.isRecommended = isRecommended;
		}
	}

	public void setRank(Integer rank) {
		if (rank != null) {
			this.rank = rank;
		}
	}

}
