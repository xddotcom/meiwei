package com.kwchina.ir.front.message;

import org.apache.log4j.Logger;

import com.jianzhou.sdk.BusinessService;
import com.kwchina.core.sys.CoreConstant;

public class MessageHelper {

	private static final Logger logger = Logger.getLogger(MessageHelper.class);
	private static final String LOGIN_NAME = "sdk_meiwei";
	private static final String LOGIN_PASSWORD = "jianzhou";
	private static final String LOGIN_URL = "http://www.jianzhou.sh.cn/JianzhouSMSWSServer/services/BusinessService";

	public synchronized static boolean sendMessage(String telphone, String content) {

		if (telphone == null || telphone.length() != 11) {
			return false;
		}

		BusinessService bs = new BusinessService();
		bs.setWebService(LOGIN_URL);
		int result = -9999;
		if (CoreConstant.SORT_MESSAGE_POSITION) {
			result = bs.sendBatchMessage(LOGIN_NAME, LOGIN_PASSWORD, telphone, content);
			if (result > 0) {
				logger.info("SUCCESS:" + result + " CONTENT: " + content);
				return true;
			} else {
				logger.error("FAILED:" + result + " CONTENT: " + content);
				return false;
			}
		} else {
			logger.error("Send message control OFF.");
			return false;
		}
	}
}
