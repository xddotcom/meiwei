package com.kwchina.ir.front.controller;

import java.util.ArrayList;
import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ArrayUtils;

import com.kwchina.ir.entity.RecommendInfor;
import com.kwchina.ir.entity.RecommendRule;
import com.kwchina.ir.front.controller.api.json.JsonConfigFactory;
import com.kwchina.ir.vo.api.ApiRecommendRuleVo;

@SuppressWarnings({ "rawtypes", "unchecked" })
public abstract class AbstractAPIController extends AbstractController {

	protected static String[] MEMBER_SIMPLE_EXCLUDES = new String[] { "hashCode",
		"isDeleted", "lastTime", "loginPassword", "member" };
	
	protected static String[] RESTAURANT_SIMPLE_EXCLUDES =  new String[] { "circle", "cuisine", "commissionRate", "isDeleted",
		"memberadmin", "perEnd", "telphone" , "restaurant" } ;

	protected static String[] RESTAURANT_MOSTER_EXCLUDES =  new String[] { "circle", "cuisine", "commissionRate", "isDeleted",
		"memberadmin", "perEnd", "telphone" , "restaurantSearch" , "address" ,"description","discount" ,"parking" ,"transport" } ;
		
	
	protected List getRecommends(){
		List results = new ArrayList();
		List<RecommendRule> rules = recommendRuleDao.getRecommendedRules();
		for (RecommendRule rule : rules) {
			List<RecommendInfor> recommends = recommendInforDao.getRecommendedRestaurants(rule.getRuleId());
			if (CollectionUtils.isNotEmpty(recommends)) {
				List restaurants = new ArrayList();
				for (RecommendInfor R : recommends) {
					restaurants.add(R.getRestaurant());
				}
				results.add(new ApiRecommendRuleVo(rule.getRuleId(), rule.getRuleName().getText(), restaurants));
			}
		}
		return results;
	}
	
	protected JSONArray convertList( List results , JsonConfig config  ) {
		if(CollectionUtils.isNotEmpty( results)){
			JSONArray array = new JSONArray();
			for (Object ele : results) {
				array.add(JSONObject.fromObject( ele , config  ));
			}
			return array;
		}
		return new JSONArray() ;
	}
	
	protected JSONArray convertList( List results  ) {
		return convertList( results ,JsonConfigFactory.create( ArrayUtils.EMPTY_STRING_ARRAY ) );
	}
	
	protected JSONArray convertList( List results , String[] excludes ) {
		return convertList( results ,JsonConfigFactory.create( excludes ) );
	}
	
}
