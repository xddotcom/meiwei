package com.kwchina.ir.front.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.kwchina.ir.entity.MemberInfor;
import com.kwchina.ir.vo.MemberInforVo;
import com.kwchina.ir.vo.OrderInforVo;
import com.kwchina.ir.vo.SearchInforVo;

@Scope("prototype")
@Controller
public class TemplateController  extends AbstractController {

	@RequestMapping("/index.html")
	public String html(HttpServletRequest request, HttpServletResponse response) {
		return index(request, response);
	}
	
	@RequestMapping("/index.htm")
	public String htm(HttpServletRequest request, HttpServletResponse response) {
		return index( request, response);
	}
	
	@RequestMapping("/home.htm")
	public String index(HttpServletRequest request, HttpServletResponse response) {
		return "/front/index";
	}

	@RequestMapping("/error.htm")
	public String error(HttpServletRequest request, HttpServletResponse response) {
		return "/front/error";
	}
	
	@RequestMapping("/restaurant/search/{key}")
	public String quickQuery(@PathVariable("key") String key,
			@ModelAttribute("searchInforVo") SearchInforVo searchInforVo, Model model,
			HttpServletRequest request, HttpServletResponse response) {
		request.setAttribute( "key", key);
		model.addAttribute( "searchInforVo", searchInforVo);
		return "/front/restaurant/list";
	}

	@RequestMapping("/restaurant/view/{id}")
	public String viewRestaurant(@PathVariable("id") String id,
			HttpServletRequest request, HttpServletResponse response) {
		request.setAttribute( "id", id);
		return "/front/restaurant/view";
	}

	@RequestMapping("/restaurant/book/{id}")
	public String beforeReservation(@PathVariable("id") String id, 
			@ModelAttribute("orderInforVo") OrderInforVo orderInforVo,
			HttpServletRequest request,HttpServletResponse response) {
		request.setAttribute( "id", id);	
		request.setAttribute( "searchInforVo", request.getSession().getAttribute("_FRONT_SEARCH_FILTER"));
		request.getSession().setAttribute( "_FRONT_SEARCH_FILTER", null );
		return "/front/order/view";
	}
	
	@RequestMapping("/restaurant/viewtable/{key}")
	public String viewtable(@PathVariable("key") String key, HttpServletRequest request,
			HttpServletResponse response) {
		request.setAttribute( "key", key);	
		return "/front/member/tableview";
	}
	
	@RequestMapping("/member/findpassword.htm")
	public String findpassword(  HttpServletRequest request, HttpServletResponse response) {
		return "/front/member/findpassword";
	}
	
	@RequestMapping("/member/resetpassword.htm")
	public String resetpassword(  HttpServletRequest request, HttpServletResponse response) {
		return "/front/member/resetpassword";
	}
	
	@RequestMapping("/member/register.htm")
	public String beforeRegister( HttpServletRequest request , HttpServletResponse response) {
		return "/front/member/register";
	}
	
	@RequestMapping("/member/loginfirst.htm")
	public String loginfirst( HttpServletRequest request,HttpServletResponse response) {
		return "/front/member/loginfirst";
	}
	
	
	@RequestMapping("/member/loginout.htm")
	public String loginout(HttpServletRequest request) {
		try {
			keepUserBySession(request, null);
		} catch (Exception e) {
			logger.error("Logout failed.", e);
		}
		return "redirect:/";
	}
	 
	@RequestMapping("/myaccount/user/usercenter.htm")
	public String usercenter(HttpServletRequest request , HttpServletResponse response) {
		MemberInfor member = this.memberInforDao.getLoginMember(request);
		memberCreditInforDao.calculateTotalCredits(member.getPersonalInfor());
		return "/front/member/user/usercenter";
	}
	
	@RequestMapping("/myaccount/user/editprofile.htm")
	public String editProfile(@ModelAttribute("memberInforVo") MemberInforVo memberInforVo,HttpServletRequest request,
			HttpServletResponse response) {
		MemberInfor memberInfor = memberInforDao.getLoginMember(request);
		request.setAttribute("_LoginMember", memberInfor);
		return "/front/member/user/useredit";
	}
	
	@RequestMapping("/myaccount/user/handlepassword.htm")
	public String handlepassword( HttpServletRequest request,HttpServletResponse response) {
		return "/front/member/user/updatepassword";
	}
	
	@RequestMapping("/myaccount/user/integral.htm")
	public String myintegraluse(HttpServletRequest request, HttpServletResponse response) {
		return "/front/member/user/integral";
	}
	
	@RequestMapping("/myaccount/user/contactedit.htm")
	public String contactedit( HttpServletRequest request,HttpServletResponse response) {
		return "/front/member/user/contactedit";
	}
	
	@RequestMapping("/myaccount/restaurant/detail.htm")
	public String restaurantDetail( HttpServletRequest request, HttpServletResponse response) {
		return "/front/member/restaurant/edit";
	}
	
	@RequestMapping("/myaccount/*/{search}.htm")
	public String handleUserListing( @PathVariable("search") String search,HttpServletRequest request,
			HttpServletResponse response) {
		search = request.getRequestURI();
		if (search.contains("user/favorites")) {
			return "front/member/user/favorites";
		} else if (search.contains("restaurant/favorites")) {
			return "front/member/restaurant/favorites";
		} else if (search.contains("user/orders")) {
			return "front/member/user/orders";
		} else if (search.contains("user/credits")) {
			return "front/member/user/credits";
		} else if (search.contains("user/reviews")) {
			return "front/member/user/reviews";
		} else if (search.contains("restaurant/reviews")) {
			return "front/member/restaurant/reviews";
		} else if (search.contains("user/contacts")) {
			return "front/member/user/contacts";
		} else if (search.contains("restaurant/newOrders")) {
			return "front/member/restaurant/newOrders";
		} else if (search.contains("restaurant/doOrders")) {
			return "front/member/restaurant/doOrders";
		}
		return "redirect:/";
	}
	
}