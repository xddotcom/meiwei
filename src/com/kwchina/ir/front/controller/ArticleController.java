package com.kwchina.ir.front.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.kwchina.core.util.HttpHelper;
import com.kwchina.ir.entity.ArticleInfor;
import com.kwchina.ir.entity.BaseContactInfor;
import com.kwchina.ir.vo.ArticleInforVo;
import com.kwchina.ir.vo.BaseContactInforVo;
import com.kwchina.ir.vo.SearchInforVo;

@Scope("prototype")
@Controller
public class ArticleController extends AbstractController {

	@RequestMapping("/article/information.htm")
	public String information(@ModelAttribute("articleInforVo") ArticleInforVo articleInforVo,
			@ModelAttribute("searchInforVo") SearchInforVo searchInforVo,
			HttpServletRequest request, HttpServletResponse response) {
		try {
			if (articleInforVo.getArticleId() != null && articleInforVo.getArticleId() != 0) {
				ArticleInfor article = this.articleInfoDao.get(articleInforVo.getArticleId());
				request.setAttribute("_ArticleInfor", article.getContent());
			}
		} catch (Exception e) {
			request.setAttribute("_ArticleInfor", "");
			logger.error("Get articles failed.", e);
		}
		return "/front/common/information";
	}

	@RequestMapping("/article/{staticPage}.htm")
	public String staticPage(@PathVariable("staticPage") String staticPage,
			@ModelAttribute("searchInforVo") SearchInforVo searchInforVo,
			HttpServletRequest request, HttpServletResponse response) {
		return "/front/statics/" + staticPage;
	}

	@RequestMapping("/article/about/{staticPage}.htm")
	public String staticAboutPage(@PathVariable("staticPage") String staticPage,
			@ModelAttribute("searchInforVo") SearchInforVo searchInforVo,
			HttpServletRequest request, HttpServletResponse response) {

		try {
			String lang = "";

			ArticleInfor articleInfor = this.articleInfoDao.get(Integer.parseInt(lang));
			if (articleInfor != null) {
				request.setAttribute("content", articleInfor);
			}
			String path = request.getContextPath();
			String basePath = request.getScheme() + "://" + request.getServerName() + ":"
					+ request.getServerPort() + path + "/";
			request.setAttribute("basePath", basePath);
		} catch (Exception e) {
			request.setAttribute("basePath", "");
			request.setAttribute("content", "");
			logger.error("Error get" + staticPage, e);
		}

		return "/front/common/information";
	}

	@RequestMapping("/article/saveConatct.htm")
	public void saveConatct(
			@ModelAttribute("baseContactInforVo") BaseContactInforVo baseContactInforVo,
			HttpServletRequest request, HttpServletResponse response) {

		try {
			BaseContactInfor contactInfor = new BaseContactInfor();
			contactInfor.setContactName(baseContactInforVo.getContactName());
			contactInfor.setContactType(baseContactInforVo.getContactType());
			contactInfor.setEmail(baseContactInforVo.getEmail());
			contactInfor.setMessageDate(new Date());
			contactInfor.setMobile(baseContactInforVo.getMobile());
			contactInfor.setRemark(baseContactInforVo.getRemark());
			baseContactInforDao.save(contactInfor);
			HttpHelper.DEFAULT.output(response, "{\"info\":\"ok\",\"status\":\"y\"}");
		} catch (Exception e) {
			e.printStackTrace();
			HttpHelper.DEFAULT.output(response, "{\"info\":\"ok\",\"status\":\"n\"}");
		}
	}

}
