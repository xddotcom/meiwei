package com.kwchina.ir.front.controller;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.DelegatingMessageSource;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.mail.javamail.JavaMailSender;

import com.kwchina.core.sys.CoreConstant;
import com.kwchina.ir.dao.ArticleInforDao;
import com.kwchina.ir.dao.BaseAdInforDao;
import com.kwchina.ir.dao.BaseCircleInforDao;
import com.kwchina.ir.dao.BaseContactInforDao;
import com.kwchina.ir.dao.BaseCuisineInforDao;
import com.kwchina.ir.dao.BaseDistrictInforDao;
import com.kwchina.ir.dao.HistoryViewInforDao;
import com.kwchina.ir.dao.MemberContactInforDao;
import com.kwchina.ir.dao.MemberCreditInforDao;
import com.kwchina.ir.dao.MemberFavoriteDao;
import com.kwchina.ir.dao.MemberInforDao;
import com.kwchina.ir.dao.MemberPersonalInforDao;
import com.kwchina.ir.dao.MemberReviewInforDao;
import com.kwchina.ir.dao.OrderInforDao;
import com.kwchina.ir.dao.ProductInforDao;
import com.kwchina.ir.dao.ProductTypeInforDao;
import com.kwchina.ir.dao.RecommendInforDao;
import com.kwchina.ir.dao.RecommendRuleDao;
import com.kwchina.ir.dao.RestaurantInforDao;
import com.kwchina.ir.dao.RestaurantMenuInforDao;
import com.kwchina.ir.dao.RestaurantPicInforDao;
import com.kwchina.ir.dao.RestaurantTablePicInforDao;
import com.kwchina.ir.dao.RestaurantTimeInforDao;
import com.kwchina.ir.entity.MemberInfor;
import com.kwchina.ir.util.StringHelper;
import com.kwchina.ir.util.mail.MailHelper;

public abstract class AbstractController {

	protected static final Logger logger = Logger.getLogger(AbstractController.class);

	protected ReloadableResourceBundleMessageSource messageSource;
 	
 	public void setMessageSource(DelegatingMessageSource messageSource) {
 		this.messageSource = (ReloadableResourceBundleMessageSource) messageSource.getParentMessageSource();
	}
 	
	protected Map<?, ?> systemProperties;

	public void setSystemProperties(Map<?, ?> systemProperties) {
		this.systemProperties = systemProperties;
		CoreConstant.SORT_MESSAGE_POSITION = StringHelper.toBoolean(systemProperties.get("sort_message_position"));
		CoreConstant.JAVA_MAIL_POSITION = StringHelper.toBoolean(systemProperties.get("java_mail_position"));
		CoreConstant.JM_NEWORDER_POSITION = StringHelper.toBoolean(systemProperties.get("jm_neworder_position"));
		CoreConstant.JM_PASSWORD_POSITION = StringHelper.toBoolean(systemProperties.get("jm_password_position"));
	}

	protected JavaMailSender mailSender;

	public void setMailSender(JavaMailSender mailSender) {
		if (mailSender != null) {
			this.mailSender = mailSender;
			MailHelper.setMailSender(mailSender);
		}
	}

	@Resource
	protected ArticleInforDao articleInfoDao;

	@Resource
	protected BaseAdInforDao baseAdInforDao;

	@Resource
	protected BaseDistrictInforDao baseDistrictInforDao;
	
	@Resource
	protected BaseCircleInforDao baseCircleInforDao;

	@Resource
	protected BaseCuisineInforDao baseCuisineInforDao;

	@Resource
	protected HistoryViewInforDao historyViewInforDao;

	@Resource
	protected MemberContactInforDao memberContactInforDao;

	@Resource
	protected MemberFavoriteDao memberFavoriteDao;

	@Resource
	protected MemberInforDao memberInforDao;

	@Resource
	protected MemberPersonalInforDao memberPersonalInforDao;

	@Resource
	protected MemberReviewInforDao memberReviewInforDao;

	@Resource
	protected OrderInforDao orderInforDao;

	@Resource
	protected RestaurantInforDao restaurantInforDao;

	@Resource
	protected RestaurantMenuInforDao restaurantMenuInforDao;

	@Resource
	protected RestaurantPicInforDao restaurantPicInforDao;

	@Resource
	protected RestaurantTablePicInforDao restaurantTablePicInforDao;

	@Resource
	protected RecommendRuleDao recommendRuleDao;

	@Resource
	protected RecommendInforDao recommendInforDao;

	@Resource
	protected BaseContactInforDao baseContactInforDao;

	@Resource
	protected RestaurantTimeInforDao restaurantTimeInforDao;

	@Resource
	protected MemberCreditInforDao memberCreditInforDao;
	
	@Resource
	protected ProductInforDao productInforDao;
	
	@Resource
	protected ProductTypeInforDao productTypeInforDao;

	protected int languageType(HttpServletRequest request) {
		try {
			String LANGUAGE_PARAM = request.getSession().getAttribute("LANGUAGE").toString();
			int language = Integer.parseInt(LANGUAGE_PARAM);
			if (language == CoreConstant.LANGUAGETYPE_ENGLISH)
				return CoreConstant.LANGUAGETYPE_CHINESE;
			else
				return CoreConstant.LANGUAGETYPE_CHINESE;
		} catch (Exception e) {
			return CoreConstant.LANGUAGETYPE_CHINESE;
		}
	}

	protected String getMessage(String key) {
		return messageSource.getMessage(key, null, LocaleContextHolder.getLocale());
	}
	
	protected void keepUserBySession(HttpServletRequest request, MemberInfor member) {
		HttpSession session = request.getSession();
		if (member != null) {
			session.setAttribute(CoreConstant.SESSION_ATTR_LOGIN_NAME, member.getLoginName());
			session.setAttribute(CoreConstant.SESSION_ATTR_LOGIN_PASSWORD, member.getLoginPassword());
		} else {
			session.setAttribute(CoreConstant.SESSION_ATTR_LOGIN_NAME, null);
			session.setAttribute(CoreConstant.SESSION_ATTR_LOGIN_PASSWORD, null);
		}
	}
}
