package com.kwchina.ir.front.controller;

import java.util.Locale;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.LocaleResolver;

@Controller
public class LocaleController {

	@Resource
	private LocaleResolver localeResolver;

	@RequestMapping("/changelanguage.htm")
	public void changeLocal(String locale, HttpServletRequest request,
			HttpServletResponse response) {
		String languageType = "1";
		if ("zh_CN".equals(locale)) {
			localeResolver.setLocale(request, response, Locale.CHINA);
			languageType = "1";
		} else if ("en".equals(locale)) {
			localeResolver.setLocale(request, response, Locale.ENGLISH);
			languageType = "2";
		}
		request.getSession().setAttribute("LANGUAGE", languageType);
	}
}
