package com.kwchina.ir.front.controller.api;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jianzhou.sdk.BusinessService;
import com.kwchina.ir.front.controller.AbstractAPIController;

@Scope("prototype")
@Controller
public class APIMessageController  extends AbstractAPIController {
	
	private static final String KEY = "meiwei-api-sms";//5b4131b2e8cfa11b835ac4a80911804d
	private static final String LOGIN_NAME = "sdk_meiwei";
	private static final String LOGIN_PASSWORD = "jianzhou";
	private static final String LOGIN_URL = "http://www.jianzhou.sh.cn/JianzhouSMSWSServer/services/BusinessService";
	
	@RequestMapping(value = "/api/sms/send-message.htm")
	public int sendMessage(HttpServletRequest request, HttpServletResponse response) {
//		BusinessService bs = new BusinessService();
//		bs.setWebService(LOGIN_URL);
//		String key = request.getParameter( "key");
//		if(DigestUtils.md5Hex( KEY ).equals( key )){
//			String telphone= request.getParameter( "telphone");
//			String content= request.getParameter( "content" );
//			bs.sendMessage(LOGIN_NAME, LOGIN_PASSWORD, telphone, content);
//		}
		return sendBatchMessage(request, response);
	}

	@RequestMapping(value = "/api/sms/send-batch-message.htm")
	public int sendBatchMessage(HttpServletRequest request, HttpServletResponse response) {
		BusinessService bs = new BusinessService();
		bs.setWebService(LOGIN_URL);
		String key = request.getParameter( "key");
		if(DigestUtils.md5Hex( KEY ).equals( key )){
			String telphone= request.getParameter( "mobile");
			String content= request.getParameter( "content" );
			return bs.sendBatchMessage(LOGIN_NAME, LOGIN_PASSWORD, telphone, content);
		}
		return -9999;
	}
}
