package com.kwchina.ir.front.controller.api;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kwchina.core.sys.CoreConstant;
import com.kwchina.core.util.HttpHelper;
import com.kwchina.ir.entity.MemberInfor;
import com.kwchina.ir.front.controller.AbstractAPIController;
import com.kwchina.ir.front.controller.api.json.JsonConfigFactory;
import com.kwchina.ir.util.CookieHelper;
import com.kwchina.ir.util.StringHelper;
import com.kwchina.ir.util.TextHelper.Templete;
import com.kwchina.ir.util.mail.MailHelper;
import com.kwchina.ir.vo.MemberInforVo;

@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
@Scope("prototype")
@Controller
public class APIMemberController extends AbstractAPIController {
	@RequestMapping(value = "/api/member/user/detail.htm")
	public void getMemberDetail(HttpServletRequest request, HttpServletResponse response) {
		MemberInfor member = memberInforDao.getLoginMember(request);
		JSONObject json = new JSONObject();
		JSONObject single = json.fromObject(member, JsonConfigFactory.create(false, MEMBER_SIMPLE_EXCLUDES));
		if (member != null) {
			Map m = new HashMap();
			m.put("contacts", memberContactInforDao.getMemberContacts(member.getMemberId()));
			single.putAll(m, JsonConfigFactory.create(false, new String[] { "member" }));
		}
		json.put("member", single);
		response.setContentType("application/json;charset=UTF-8");
		try {
			response.getWriter().print(json);
		} catch (IOException e) {
			logger.error(" API Catch Error : Member Detail  ", e);
		}
	}

	@RequestMapping("/api/member/login")
	public void login(@ModelAttribute("memberInforVo") MemberInforVo memberInforVo, HttpServletRequest request,
			HttpServletResponse response) {
		try {
			String password = DigestUtils.md5Hex(memberInforVo.getLoginPassword());
			memberInforVo.setLoginName(memberInforVo.getLoginName().replaceAll(" ", ""));
			MemberInfor member = memberInforDao.validateLogin(memberInforVo.getLoginName(), password);
			keepUserBySession(request, member);
			if (member != null) {
				CookieHelper.addCookie(response, "JSESSIONID", request.getSession().getId(), 65 * 7 * 24 * 3600);
				member.setLastTime(new Date(System.currentTimeMillis()));
				memberInforDao.saveOrUpdate(member, member.getMemberId());
				new HttpHelper().output(response, "{\"info\":\"ok\",\"status\":\"y\"}");
			}else{
				new HttpHelper().output(response, "{\"info\":\"ok\",\"status\":\"n\"}");
			}
		} catch (Exception e) {
			new HttpHelper().output(response, "{\"info\":\"ok\",\"status\":\"n\"}");
			logger.error("Login failed.", e);
		}
	}

	@RequestMapping("/api/member/register")
	public void regedit(@ModelAttribute("memberInforVo") MemberInforVo memberInforVo, HttpServletRequest request,
			HttpServletResponse response) {
		try {
			logger.info("Registration request submit.");
			memberInforVo.setLoginName(memberInforVo.getEmail() == null ? memberInforVo.getMobile() : memberInforVo
					.getEmail());
			List<MemberInfor> ms = memberInforDao.validateMemberExist(memberInforVo.getLoginName());
			if (ms == null || ms.size() == 0) {
				logger.info("Email " + memberInforVo.getEmail() + " doesnt exist.");
				MemberInfor member = memberInforDao.createMember(memberInforVo);
				keepUserBySession(request, member);
			}
			new HttpHelper().output(response, "{\"info\":\"ok\",\"status\":\"y\"}");

		} catch (Exception e) {
			logger.error("Register failed.", e);
			new HttpHelper().output(response, "{\"info\":\"ok\",\"status\":\"n\"}");
		}
	}

	@RequestMapping("/api/member/resetpassword")
	public void resetpassword(
			@RequestParam(required=false) String loginPassword, 
			@RequestParam(required=false) String name, @RequestParam(required=false) String key,
			HttpServletRequest request, HttpServletResponse response) {
		if (StringHelper.isNotEmpty(key) && StringHelper.isNotEmpty(name)) {
			String loginName = "";
			try {
				loginName = URLDecoder.decode(name, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				logger.error("LoginName decode failed", e);
			}
			MemberInfor m = memberInforDao.getInforByLoginName(loginName);
			if (m != null && StringHelper.isNotEmpty(m.getHashCode()) && StringHelper.isNotEmpty(loginPassword)
					&& m.getHashCode().equals(key)) {
				m.setLoginPassword(DigestUtils.md5Hex(loginPassword));
				m.setHashCode("");
				memberInforDao.getSession().update(m);
			}
		}
	}

	@RequestMapping("/api/member/findpassword")
	public void findpassword(@RequestParam(required=false) String loginName , @RequestParam(required=false) String verifyCode ,
			HttpServletRequest request, HttpServletResponse response ) {
		String status = "n";
		if (StringHelper.isNotEmpty(loginName)) {
			MemberInfor m = memberInforDao.getInforByLoginName(loginName);
			if (m != null && m.getPersonalInfor() != null) {
				Object sc = request.getSession().getAttribute(CoreConstant.SESSION_ATTR_VERIFYCODE);
				if (StringHelper.isNotEmpty(verifyCode) && sc != null
						&& sc.toString().toLowerCase().equals(verifyCode.toLowerCase())) {
					try {
						MailHelper mailHelper = new MailHelper();
						String key = DigestUtils.md5Hex(m.getLoginName() + m.getLoginPassword()
								+ System.currentTimeMillis());
						String n = URLEncoder.encode(m.getLoginName(), "UTF-8");
						String templete = Templete.PASSWORD_MAIL.getTemplete();
						String title = "忘记密码提示(www.clubmeiwei.com)";
						String nickname = m.getPersonalInfor().getNickName();
						String email = m.getPersonalInfor().getEmail();

						mailHelper.setTo(email).setSubject(title).setText(String.format(templete, nickname, key, n));
						if (CoreConstant.JM_PASSWORD_POSITION) {
							mailHelper.send();
						}
						m.setHashCode(key);
						memberInforDao.getSession().update(m);
						status = "y";
					} catch (Exception e) {
						logger.error("Send Email failed. loginName : " + loginName, e);
					}
 				}
			} 
		}
		request.getSession().setAttribute(CoreConstant.SESSION_ATTR_VERIFYCODE, null);
		new HttpHelper().json(response, "{\"info\":\"ok\",\"status\":\""+status+"\"}");
 	}

	// ======== Validate =====
	@RequestMapping(value = "/api/member/validate/judgexist/member")
	public @ResponseBody
	void judgeExistMember(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String status = CoreConstant.NO;
		String infor = getMessage("basic.title226");
		try {
			String loginName = request.getParameter("param");
			String repeat = request.getParameter("repeat");

			if ((repeat != null && loginName.equals(repeat))
					|| CollectionUtils.isEmpty(memberInforDao.validateMemberExist(loginName))) {
				status = CoreConstant.YES;
				infor = getMessage("basic.title225");
			}
		} catch (Exception e) {
			infor = "Error";
			logger.error("Judge existing member failed.", e);
		}
		response.setContentType("application/json;charset=UTF-8");
		response.getWriter().print("{\"info\":\"" + infor + "\",\"status\":\"" + status + "\"}");
	}

	@RequestMapping(value = "/api/member/validate/judgexist/email")
	public @ResponseBody
	void judgeExistEmail(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String status = CoreConstant.NO;
		String infor = getMessage("basic.title227");
		try {
			String email = request.getParameter("param");
			String repeat = request.getParameter("repeat");

			if ((repeat != null && email.equals(repeat))
					|| CollectionUtils.isEmpty(memberInforDao.validateEmailExist(email, 0))) {
				status = CoreConstant.YES;
				infor = getMessage("basic.title225");
			}

		} catch (Exception e) {
			infor = "Error";
			logger.error("Judge existing email failed.", e);
		}
		response.setContentType("application/json;charset=UTF-8");
		response.getWriter().print("{\"info\":\"" + infor + "\",\"status\":\"" + status + "\"}");
	}

	@RequestMapping(value = "/api/member/validate/judgexist/mobile")
	public @ResponseBody
	void judgeExistMobile(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String status = CoreConstant.NO;
		String infor = "手机号码已经存在";
		try {
			String mobile = request.getParameter("param");
			String repeat = request.getParameter("repeat");

			if ((repeat != null && mobile.equals(repeat))
					|| CollectionUtils.isEmpty(memberInforDao.validateMobileExist(mobile, 0))) {
				status = CoreConstant.YES;
				infor = getMessage("basic.title225");
			}

		} catch (Exception e) {
			infor = "Error";
			logger.error("Judge existing mobile failed.", e);
		}
		response.setContentType("application/json;charset=UTF-8");
		response.getWriter().print("{\"info\":\"" + infor + "\",\"status\":\"" + status + "\"}");
	}

	@RequestMapping(value = "/api/member/validate/judgexist/invitation")
	public @ResponseBody
	void judgeInvitation(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String status = CoreConstant.NO;
		String infor = getMessage("basic.title228");
		try {
			String code = request.getParameter("param");
			List<MemberInfor> list = this.memberInforDao.validateInvitation(code, 0);
			if (list != null && list.size() > 0) {
				status = CoreConstant.YES;
				infor = getMessage("basic.title225");
			}
		} catch (Exception e) {
			infor = "Error";
			logger.error("Judge invitation failed.", e);
		}
		response.setContentType("text/html;charset=UTF-8");
		response.getWriter().print("{\"info\":\"" + infor + "\",\"status\":\"" + status + "\"}");
	}

	@RequestMapping(value = "/api/member/validate/judgexist/code")
	public @ResponseBody
	void judgeCode(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String status = CoreConstant.NO;
		String infor = getMessage("basic.title229");
		try {
			String code = request.getParameter("param");
			Object sc = request.getSession().getAttribute(CoreConstant.SESSION_ATTR_VERIFYCODE);
			if (StringHelper.isNotEmpty(code) && sc != null && sc.toString().toLowerCase().equals(code.toLowerCase())) {
				status = CoreConstant.YES;
				infor = getMessage("basic.title225");
			}
		} catch (Exception e) {
			infor = "Error";
		}
		response.setContentType("text/html;charset=UTF-8");
		response.getWriter().print("{\"info\":\"" + infor + "\",\"status\":\"" + status + "\"}");
	}
	// ===================

}
