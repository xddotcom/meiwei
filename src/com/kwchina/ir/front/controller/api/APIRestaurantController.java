package com.kwchina.ir.front.controller.api;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.kwchina.core.sys.CoreConstant;
import com.kwchina.core.util.HttpHelper;
import com.kwchina.ir.entity.MemberFavorite;
import com.kwchina.ir.entity.MemberInfor;
import com.kwchina.ir.entity.RestaurantInfor;
import com.kwchina.ir.front.controller.AbstractAPIController;
import com.kwchina.ir.front.controller.api.json.JsonConfigFactory;
import com.kwchina.ir.vo.MemberFavoriteVo;
import com.kwchina.ir.vo.MemberReviewInforVo;
import com.kwchina.ir.vo.RestaurantInforVo;
import com.kwchina.ir.vo.SearchInforVo;

@SuppressWarnings({ "static-access", "unchecked", "rawtypes" })
@Scope("prototype")
@Controller
public class APIRestaurantController extends AbstractAPIController {

	@RequestMapping(value = "/api/restaurant/view/{id}")
	public void restaurantSingle(@PathVariable String id, HttpServletRequest request, HttpServletResponse response) {

		RestaurantInfor restaurant = restaurantInforDao.get(Integer.parseInt(id));
		JSONObject json = new JSONObject();
		JSONObject single = json.fromObject(
				restaurant,
				JsonConfigFactory.create( ArrayUtils.EMPTY_STRING_ARRAY ));
		Map m = new HashMap();
		m.put("tags", recommendRuleDao.getRestaurantTags(restaurant.getRestaurantId()));
		m.put("pictures", restaurantPicInforDao.getRestaurantPics(restaurant.getRestaurantId()));
		m.put("menus", restaurantMenuInforDao.getRecommendedMealMenus(restaurant.getRestaurantId()));
 		m.put("reviews", memberReviewInforDao.getReviewInforsByRestaurantId(restaurant.getRestaurantId(), 0));
		m.put("hours", restaurantTimeInforDao.getRestaurantHours(restaurant.getRestaurantId()));
		m.put("valueAddedProducts",productTypeInforDao.getValueAddedProducts());
		m.put("_FavoriteStatus", "0");
		MemberInfor member = memberInforDao.getLoginMember(request);
		if (member != null) {
			MemberFavorite mf = memberFavoriteDao.getFavoriteByMAR(member.getMemberId(), restaurant.getRestaurantId());
			if (mf == null) {
				m.put("_FavoriteStatus", "1");
			}
		}
		m.put("tablePics", restaurantTablePicInforDao.getRestaurantTablePics(restaurant.getRestaurantId()));
		single.putAll(m, JsonConfigFactory.create( ArrayUtils.EMPTY_STRING_ARRAY ));
 		json.put("restaurant", single);
 		new HttpHelper().json(response, json.toString());

	}
	
	
	@RequestMapping(value = "/api/restaurant/list/all.htm")
	public void all( HttpServletRequest request, HttpServletResponse response) {

 		JSONObject json = new JSONObject();
 		JSONObject single = new JSONObject();
		Map m = new HashMap();
		m.put( "list", restaurantInforDao.getResultByQueryString( "FROM RestaurantInfor WHERE isDeleted=0"));
		single.putAll(m,  JsonConfigFactory.create(false, RESTAURANT_SIMPLE_EXCLUDES ) );
		json.put("RESTAURANTS",  single );

		response.setContentType("application/json;charset=UTF-8");
		try {
			response.getWriter().print(json);
		} catch (IOException e) {
			logger.error("API restaurant view ERROR", e);
		}

	}
	

	@RequestMapping(value = "/api/restaurant/search/{key}" )
	public void restaurantList(@PathVariable("key") String key,
			@ModelAttribute("searchInforVo") SearchInforVo searchVo, HttpServletRequest request,
			HttpServletResponse response) {

		if (key.contains("-")) {
			convertQuery(key, searchVo);
		} else {
			logger.info("Quick Search");
		}

		request.getSession().setAttribute("_FRONT_SEARCH_FILTER", searchVo);
 		List<RestaurantInfor> restaurants = restaurantInforDao.searchRestaurants(searchVo);
 		if(CollectionUtils.isEmpty( restaurants )){
 			restaurants = restaurantInforDao.getResultByQueryString( "SELECT R.restaurant FROM RecommendInfor R WHERE R.restaurant.isDeleted = 0"
				+ " AND R.rank < 4 GROUP BY R.restaurant.restaurantId  ORDER BY R.rank ASC");
 		}
  		String content = convertList( restaurants , JsonConfigFactory.includes(
				new String[]{ "address","description","discount","frontPic","fullName","perBegin","restaurantId","restaurantNo","score" } , 
				new String[]{"chinese","english"}) ).toString();
 		new HttpHelper().json(response, content);
	}

	@RequestMapping(value = "/api/restaurant/list/detail.htm" )
	public void restaurantListDetail( @ModelAttribute("searchInforVo") SearchInforVo searchVo,
			@ModelAttribute("restaurantInforVo") RestaurantInforVo restaurantInforVo,HttpServletRequest request,
			HttpServletResponse response) {

		MemberInfor member = this.memberInforDao.getLoginMember(request);
		if (member != null) {
			String sql = "FROM RestaurantInfor T WHERE T.memberadmin.memberId=" + member.getMemberId();
			if (restaurantInforVo.getRestaurantId() != null) {
				sql = "FROM RestaurantInfor T WHERE T.memberadmin.memberId=" + member.getMemberId()
						+ " T.restaurantId=" + restaurantInforVo.getRestaurantId();
			}

			if (member.getMemberType() == CoreConstant.MEMBERTYPE_ADMIN) {
				sql = "FROM RestaurantInfor T";
				if (restaurantInforVo.getRestaurantId() != null) {
					sql = " FROM RestaurantInfor T WHERE T.restaurantId=" + restaurantInforVo.getRestaurantId();
				}
			}

			List<RestaurantInfor> results = restaurantInforDao.getResultByQueryString(sql);
			if (CollectionUtils.isNotEmpty(results)) {
				try {
					JSONArray array = new JSONArray();
					String[] excludes = RESTAURANT_SIMPLE_EXCLUDES;
					for (RestaurantInfor restaurant : results) {
						array.add(JSONObject.fromObject(restaurant, JsonConfigFactory.create(false, excludes)));
					}
			 
					response.setContentType("application/json;charset=UTF-8");
					response.getWriter().print(array);
				} catch (Exception e) {
					logger.error( "API restaurant list ERROR", e);
				}
			}
		}
		
	}

	private void convertQuery(String queryContent, SearchInforVo searchInforVo) {
		if (queryContent != null && queryContent.contains("-")) {
			String[] params = queryContent.split("-");
			for (int i = 0; i < params.length; i++) {
				if (params[i].equals("0"))
					params[i] = "";
			}
			searchInforVo.setSearchKey(params[0]);
			searchInforVo.setCircleId(params[1].isEmpty() ? null : Integer.parseInt(params[1]));
			searchInforVo.setCuisineId(params[2].isEmpty() ? null : Integer.parseInt(params[2]));
			searchInforVo.setRuleId(params[3].isEmpty() ? null : Integer.parseInt(params[3]));
		}
	}

	@RequestMapping("/api/restaurant/favorite/add.htm")
	public void addFavorite(@ModelAttribute("memberFavoriteVo") MemberFavoriteVo memberFavoriteVo,
			HttpServletRequest request, HttpServletResponse response) {
		try {
			MemberInfor member = memberInforDao.getLoginMember(request);
			if (member != null) {
				memberFavoriteVo.setMemberId(member.getMemberId());
				memberFavoriteDao.saveFavorite(memberFavoriteVo);
			}
			HttpHelper.DEFAULT.output(response, "{\"info\":\"" + getMessage("basic.title230") + "\",\"status\":\"y\"}");
		} catch (Exception e) {
			logger.error("Save favorite failed.", e);
			HttpHelper.DEFAULT.output(response, "{\"info\":\"" + getMessage("system.error") + "\",\"status\":\"y\"}");
		}
	}

	@RequestMapping("/api/restaurant/review/add.htm")
	public void addReview(@ModelAttribute("memberReviewInforVo") MemberReviewInforVo memberReviewInforVo,
			HttpServletRequest request, HttpServletResponse response) {

		MemberInfor member = this.memberInforDao.getLoginMember(request);

		if (member != null) {
			List results = memberReviewInforDao
					.getSession()
					.createSQLQuery(
							" SELECT * FROM member_review_infor T where T.reviewTime > DATE_SUB(  NOW() ,INTERVAL 20 SECOND ) AND T.member="
									+ member.getMemberId()).list();
			if (CollectionUtils.isNotEmpty(results)) {
				logger.info("save review in 20 second.");
				return;
			}

			memberReviewInforVo.setMemberId(member.getMemberId());
			memberReviewInforDao.saveReview(memberReviewInforVo);
		} 

	}

}
