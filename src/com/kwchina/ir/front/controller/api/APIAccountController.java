package com.kwchina.ir.front.controller.api;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kwchina.core.sys.CoreConstant;
import com.kwchina.core.util.HttpHelper;
import com.kwchina.core.util.PageList;
import com.kwchina.core.util.Pages;
import com.kwchina.ir.entity.MemberContactInfor;
import com.kwchina.ir.entity.MemberInfor;
import com.kwchina.ir.entity.ProductInfor;
import com.kwchina.ir.entity.RestaurantInfor;
import com.kwchina.ir.front.controller.AbstractAPIController;
import com.kwchina.ir.front.controller.api.json.JsonConfigFactory;
import com.kwchina.ir.util.StringHelper;
import com.kwchina.ir.vo.MemberContactInforVo;
import com.kwchina.ir.vo.MemberCreditInforVo;
import com.kwchina.ir.vo.MemberFavoriteVo;
import com.kwchina.ir.vo.MemberInforVo;
import com.kwchina.ir.vo.MemberReviewInforVo;
import com.kwchina.ir.vo.RestaurantInforVo;

@SuppressWarnings({ "rawtypes", "unchecked" })
@Scope("prototype")
@Controller
public class APIAccountController extends AbstractAPIController {
	
	@RequestMapping(value = "/api/myaccount/restaurant/neworder/count.htm" )
	public @ResponseBody
	String newOrderCount(HttpServletRequest request, HttpServletResponse response) {
		String status = "0";
		try {
			String sql = "";
			MemberInfor member = this.memberInforDao.getLoginMember(request);
			if (member.getMemberType() == CoreConstant.MEMBERTYPE_ADMIN) {
				sql = "SELECT COUNT(T.orderId) FROM OrderInfor T WHERE T.status = 0";
			} else {
				sql = "SELECT COUNT(T.orderId) FROM OrderInfor T "
						+ " WHERE T.status = 0 AND T.restaurant.restaurantId in "
						+ "(SELECT R.restaurantId  FROM RestaurantInfor R where R.memberadmin =" + member.getMemberId()
						+ ")";
			}
			int totalOrders = orderInforDao.getResultNumByQueryString(sql);
			status = String.valueOf(totalOrders);
		} catch (Exception e) {
			logger.error("Unsuccessfully get new order count", e);
		}
		return status;
	}


	@RequestMapping("/api/myaccount/user/profile/save.htm")
	public void saveProfile(@ModelAttribute("memberInforVo") MemberInforVo memberInforVo,  
			HttpServletRequest request, HttpServletResponse response) {
		try {

			if (StringHelper.isNotEmpty(memberInforVo.getMobile())) {
				memberInforVo.setLoginName(memberInforVo.getMobile());
			}
			if (StringHelper.isNotEmpty(memberInforVo.getEmail())) {
				memberInforVo.setLoginName(memberInforVo.getEmail());
			}

			MemberInfor member = this.memberInforDao.getLoginMember(request);
			if (member.getMemberType() == CoreConstant.MEMBERTYPE_STORE) {
				HttpHelper.DEFAULT.output(response, "{\"info\":\"ok\",\"status\":\"n\"}");
			}
			member = memberInforDao.ModifyMember(memberInforVo, member.getMemberId());

			if (member != null) {
				request.getSession().setAttribute(CoreConstant.SESSION_ATTR_LOGIN_NAME, member.getLoginName());
				request.getSession().setAttribute(CoreConstant.SESSION_ATTR_LOGIN_PASSWORD, member.getLoginPassword());
			}

			HttpHelper.DEFAULT.output(response, "{\"info\":\"ok\",\"status\":\"y\"}");
		} catch (Exception e) {
			logger.error("Saving user failed.", e);
			HttpHelper.DEFAULT.output(response, "{\"info\":\"ok\",\"status\":\"n\"}");
		}
	}



	@RequestMapping("/api/myaccount/*/{search}.htm")
	public void handleUserListing( @PathVariable("search") String search,HttpServletRequest request,
			HttpServletResponse response) {
		MemberInfor member = this.memberInforDao.getLoginMember(request);
		if (member != null) {
			Pages pages = new Pages( request );
			handle(request.getRequestURI(), pages, member, request);
			int page = 1;
			try { page = Integer.parseInt( request.getParameter( "pageNo")  ); } catch (Exception e) { page = 1; }
			pages.setPage(page);
			PageList pl = this.memberFavoriteDao.getResultByQueryString(pages.getSqls()[0], pages.getSqls()[1], true,
					pages);
			JSONObject json = new JSONObject();
			Map m = new HashMap();
			m.put("member", member);
	  		m.put("results", convertList( pl.getObjectList() ));
			m.put("page", pl.getPages());
			json.putAll( m , JsonConfigFactory.create());
 			new HttpHelper().json(response, json.toString() );
 		}

	}
	
	@RequestMapping("/api/myaccount/user/contact/view.htm")
	public void contactView(@ModelAttribute("memberContactInforVo") MemberContactInforVo memberContactInforVo, 
			HttpServletRequest request,HttpServletResponse response ) {
		MemberInfor memberInfor = this.memberInforDao.getLoginMember(request);
		if (memberInfor != null) {
 			if (memberContactInforVo.getContactId() != null && memberContactInforVo.getContactId() > 0) {
				MemberContactInfor contact = this.memberContactInforDao.get(memberContactInforVo.getContactId());
				if (contact != null
						&& (contact.getMember() == null || contact.getMember().getMemberId() != memberInfor
								.getMemberId())) {
					contact = new MemberContactInfor();
				}
				JSONObject json = new JSONObject();
				Map m = new HashMap();
				m.put("contact", contact );
				json.putAll( m , JsonConfigFactory.create(ArrayUtils.EMPTY_STRING_ARRAY));
	 			new HttpHelper().json(response, json.toString() );
 			}
		}
	}

	
	@RequestMapping(value = "/api/myaccount/user/contact/save.htm" )
	public @ResponseBody
	String saveContact(@ModelAttribute("memberContactInforVo") MemberContactInforVo memberContactInforVo, Model model,
			HttpServletRequest request, HttpServletResponse response) {
		String status = "n";
		try {
			MemberInfor memberInfor = this.memberInforDao.getLoginMember(request);
			if (memberInfor != null) {
				memberContactInforDao.saveOrUpdateByMember(memberInfor, memberContactInforVo);
				status = "y";
			}
		} catch (Exception e) {
			logger.error("Unsuccessfully add contact", e);
		}
		return status;
	}

	@RequestMapping("/api/myaccount/user/contact/delete.htm")
	public void deleteContact(@ModelAttribute("memberContactInforVo") MemberContactInforVo memberContactInforVo,
			HttpServletRequest request,HttpServletResponse response) {
		try {
			MemberInfor memberInfor = this.memberInforDao.getLoginMember(request);
			if (memberContactInforVo.getContactId() != null) {
				this.memberContactInforDao.deleteMemberContacts(memberInfor.getMemberId(),
						memberContactInforVo.getContactId());
				HttpHelper.DEFAULT.output(response, "y");
			} else {
				HttpHelper.DEFAULT.output(response, "n");
			}
		} catch (Exception e) {
			logger.error("Unsuccessfully delete contact", e);
			HttpHelper.DEFAULT.output(response, "n");
		}
	}

	@RequestMapping("/api/myaccount/user/favorite/delete.htm")
	public void deleteFavorite(@ModelAttribute("memberFavoriteVo") MemberFavoriteVo memberFavoriteVo,
			HttpServletRequest request, HttpServletResponse response) {
		try {
			MemberInfor memberInfor = this.memberInforDao.getLoginMember(request);
			if (memberInfor != null && memberFavoriteVo.getFavoriteId() != null) {
				this.memberFavoriteDao.deleteFavorite(memberInfor.getMemberId(), memberFavoriteVo.getFavoriteId());
				HttpHelper.DEFAULT.output(response, "y");
			} else {
				HttpHelper.DEFAULT.output(response, "n");
			}
		} catch (Exception e) {
			logger.error("Unsuccessfully delete favorite", e);
			HttpHelper.DEFAULT.output(response, "n");
		}
	}

	@RequestMapping("/api/myaccount/user/review/delete.htm")
	public void deleteReview(@ModelAttribute("memberReviewInforVo") MemberReviewInforVo memberReviewInforVo,
			HttpServletRequest request, HttpServletResponse response) {
		try {
			MemberInfor memberInfor = this.memberInforDao.getLoginMember(request);
			if (memberReviewInforVo.getReviewId() != null) {
				this.memberReviewInforDao.deleteReview(memberInfor.getMemberId(), memberReviewInforVo.getReviewId());
				HttpHelper.DEFAULT.output(response, "y");
			} else {
				HttpHelper.DEFAULT.output(response, "n");
			}
		} catch (Exception e) {
			logger.error("Unsuccessfully delete review", e);
			HttpHelper.DEFAULT.output(response, "n");
		}
	}

	@RequestMapping("/api/myaccount/user/updatepassword.htm")
	public void updatepassword(@ModelAttribute("memberInforVo") MemberInforVo memberInforVo,
			HttpServletRequest request,HttpServletResponse response) {
		if (StringHelper.isNotEmpty(memberInforVo.getNewPassword())
				&& StringHelper.isNotEmpty(memberInforVo.getLoginPassword())) {
			MemberInfor member = this.memberInforDao.getLoginMember(request);
			if (member != null) {
				String md5 = DigestUtils.md5Hex(memberInforVo.getLoginPassword());
				if (member.getLoginPassword().equals(md5)) {
					member.setLoginPassword(DigestUtils.md5Hex(memberInforVo.getNewPassword()));
					memberInforDao.update(member);
				}
			}
		}
 	}

	@RequestMapping("/api/myaccount/user/integral/view.htm")
	public void integraluseView(HttpServletRequest request, HttpServletResponse response) {
		MemberInfor member = this.memberInforDao.getLoginMember(request);
		if (member != null) {
			memberCreditInforDao.calculateTotalCredits(member.getPersonalInfor());
			List results = productInforDao
					.getResultByQueryString("FROM ProductInfor WHERE productType.category =2 ORDER BY productType.typeOrder");
			JSONObject json = new JSONObject();
			Map m = new HashMap();
			m.put("member", member);
			m.put("results", results );
 			json.putAll( m , JsonConfigFactory.create());
			new HttpHelper().json(response, json.toString() );
		}
	}

	@RequestMapping("/api/myaccount/user/integral/save.htm")
	public void integralSave(
			@ModelAttribute("memberContactInfor") MemberContactInforVo memberContactInfor,
			@RequestParam(value = "productIdArray", required = true) String[] productIdArray,
			@RequestParam(value = "countArray", required = true) String[] countArray, HttpServletRequest request,
			HttpServletResponse response) {
		MemberInfor member = this.memberInforDao.getLoginMember(request);
		if (member != null) {
			memberCreditInforDao.calculateTotalCredits(member.getPersonalInfor());
			Integer oldCredit = member.getPersonalInfor().getCredit();
			try {
				Integer credit = 0;
				String reason = "";
				if (productIdArray.length == countArray.length) {
					for (int i = 0; i < productIdArray.length; i++) {
						ProductInfor product = productInforDao.get(Integer.parseInt(productIdArray[i]));
						if (product.getProductType().getCategory() == 2 && product.getCredit() > 0) {
							credit += product.getCredit() * Integer.parseInt(countArray[i]);
							reason += product.getProductName().getText() + " X " + countArray[i] + "\n";
						}
					}
				}
				MemberCreditInforVo memberCreditInforVo = new MemberCreditInforVo();
				memberCreditInforVo.setAmount(-credit);
				memberCreditInforVo.setCreditType(6);
				memberCreditInforVo.setHappendTime(new Date());
				memberCreditInforVo.setMemberId(member.getMemberId());
				memberCreditInforVo.setReason(reason);
				if (oldCredit > credit && credit > 0) {
					memberContactInforDao.saveEntityByIntegral(member, memberContactInfor);
					memberCreditInforDao.saveEntityByIntegral(member, memberCreditInforVo);
				}
			} catch (Exception e) {
				logger.error("Add integral Entity Failed.", e);
			}
 
		}
	}

	@RequestMapping("/api/myaccount/restaurant/save.htm")
	public void saveRestaurant(
			@ModelAttribute("restaurantInforVo") RestaurantInforVo restaurantInforVo, 
			HttpServletRequest request, HttpServletResponse response) {
		MemberInfor member = this.memberInforDao.getLoginMember(request);
		if (member != null) {
			String sql =  "FROM RestaurantInfor T WHERE T.restaurantId="+restaurantInforVo.getRestaurantId()
					 +" AND T.memberadmin.memberId="+member.getMemberId() ;
			if (member.getMemberType() == CoreConstant.MEMBERTYPE_ADMIN) {
				sql =  "FROM RestaurantInfor T WHERE T.restaurantId="+restaurantInforVo.getRestaurantId();
			}
			List<RestaurantInfor> results = restaurantInforDao.getResultByQueryString( sql );
			if(CollectionUtils.isNotEmpty( results)){
				restaurantInforDao.updateRestaurant( results.get( 0 ) ,  restaurantInforVo );
			}
		}
	}

	private void handle(String search, Pages pages, MemberInfor member, HttpServletRequest request) {
		String[] queryString = new String[2];
		int pagenum = 4;
		if (search.contains("user/favorites")) {
			queryString[0] = "FROM MemberFavorite T WHERE T.member.memberId = " + member.getMemberId()
					+ " ORDER BY T.favoriteTime DESC";
			queryString[1] = "SELECT COUNT(T.favoriteId) FROM MemberFavorite T WHERE T.member.memberId = "
					+ member.getMemberId();
			pagenum = 10;
		} else if (search.contains("restaurant/favorites")) {
			if (member.getMemberType() == CoreConstant.MEMBERTYPE_ADMIN) {
				queryString[0] = "FROM MemberFavorite T WHERE T.restaurant is not null ORDER BY T.favoriteTime DESC";
				queryString[1] = "SELECT COUNT(T.favoriteId) FROM MemberFavorite T WHERE T.restaurant is not null   ";
			} else {
				queryString[0] = "FROM MemberFavorite T WHERE  T.restaurant.memberadmin.memberId ="
						+ member.getMemberId() + " ORDER BY T.favoriteTime DESC";
				queryString[1] = "SELECT COUNT(T.favoriteId) FROM MemberFavorite T WHERE T.restaurant.memberadmin.memberId = "
						+ member.getMemberId();
			}
			pagenum = 10;
		} else if (search.contains("user/orders")) {
			String orderSubmitTime = StringHelper.isNotEmpty( request.getParameter( "orderSubmitTime") )
					? request.getParameter( "orderSubmitTime") : new SimpleDateFormat("yyyy-MM").format( new Date() );
			String addsql = "T.orderDate like '%"+orderSubmitTime+"%'  AND ";
			request.setAttribute( "orderSubmitTime", orderSubmitTime);
			
			queryString[0] = "FROM OrderInfor T WHERE "+addsql+" T.member.memberId = " + member.getMemberId()
					+ " ORDER BY T.orderSubmitTime DESC";
			queryString[1] = "SELECT COUNT(T.orderId) FROM OrderInfor T WHERE  "+addsql+" T.member.memberId = "
					+ member.getMemberId();

			if (member != null) {
				List<MemberContactInfor> list = memberContactInforDao.getMemberContacts(member.getMemberId());
				request.setAttribute("contacts", list);
			}
			pagenum = 3;
			
		} else if (search.contains("user/credits")) {
			queryString[0] = "FROM MemberCreditInfor T WHERE T.member.memberId = " + member.getMemberId()
					+ " ORDER BY T.happendTime DESC";
			queryString[1] = "SELECT COUNT(T.creditId) FROM MemberCreditInfor T WHERE T.member.memberId = "
					+ member.getMemberId();
		} else if (search.contains("user/reviews")) {
			queryString[0] = "FROM MemberReviewInfor T WHERE T.member.memberId = " + member.getMemberId()
					+ " ORDER BY T.reviewTime DESC";
			queryString[1] = "SELECT COUNT(T.reviewId) FROM MemberReviewInfor T WHERE T.member.memberId = "
					+ member.getMemberId();
		} else if (search.contains("restaurant/reviews")) {
			if (member.getMemberType() == CoreConstant.MEMBERTYPE_ADMIN) {
				queryString[0] = "FROM MemberReviewInfor T  WHERE T.restaurant is not null   ORDER BY T.reviewTime DESC";
				queryString[1] = "SELECT COUNT(T.reviewId) FROM MemberReviewInfor T  WHERE T.restaurant is not null  ";
			} else {
				queryString[0] = "FROM MemberReviewInfor T WHERE T.restaurant.memberadmin.memberId = "
						+ member.getMemberId() + " ORDER BY T.reviewTime DESC";
				queryString[1] = "SELECT COUNT(T.reviewId) FROM MemberReviewInfor T WHERE T.restaurant.memberadmin.memberId = "
						+ member.getMemberId();
			}
		} else if (search.contains("user/contacts")) {
			queryString[0] = "FROM MemberContactInfor T WHERE T.member.memberId = " + member.getMemberId()
					+ " ORDER BY T.name ASC";
			queryString[1] = "SELECT COUNT(T.contactId) FROM MemberContactInfor T WHERE T.member.memberId = "
					+ member.getMemberId();
		} else if (search.contains("restaurant/newOrders")) {
			
//			String orderSubmitTime = StringHelper.isNotEmpty( request.getParameter( "orderSubmitTime") )
//					? request.getParameter( "orderSubmitTime") : new SimpleDateFormat("yyyy-MM-dd").format( new Date() );
//			String addsql = "T.orderSubmitTime like '%"+orderSubmitTime+"%'  AND ";
//			request.setAttribute( "orderSubmitTime", orderSubmitTime);
			String addsql = " T.member.memberId!=962 AND ";
			
			if (member.getMemberType() == CoreConstant.MEMBERTYPE_ADMIN) {
				queryString[0] = "FROM OrderInfor T WHERE "+addsql+" T.status = 0 ORDER BY T.orderSubmitTime DESC";
				queryString[1] = "SELECT COUNT(T.orderId) FROM OrderInfor T WHERE  "+addsql+" T.status = 0";
			} else {
				queryString[0] = "FROM OrderInfor T " + " WHERE  "+addsql+" T.status = 0 AND T.restaurant.restaurantId in "
						+ "(SELECT R.restaurantId  FROM RestaurantInfor R where R.memberadmin =" + member.getMemberId()
						+ ")" + " ORDER BY T.orderSubmitTime DESC";
				queryString[1] = "SELECT COUNT(T.orderId) FROM OrderInfor T "
						+ " WHERE  "+addsql+" T.status = 0 AND T.restaurant.restaurantId in "
						+ "(SELECT R.restaurantId  FROM RestaurantInfor R where R.memberadmin =" + member.getMemberId()
						+ ")";
			}
			pagenum = 3;

		} else if (search.contains("restaurant/doOrders")) {
			String orderSubmitTime = StringHelper.isNotEmpty( request.getParameter( "orderSubmitTime") )
					? request.getParameter( "orderSubmitTime") : new SimpleDateFormat("yyyy-MM").format( new Date() );
			String addsql = "T.orderDate like '%"+orderSubmitTime+"%'  AND  T.member.memberId!=962 AND ";
			request.setAttribute( "orderSubmitTime", orderSubmitTime);
			
			if (member.getMemberType() == CoreConstant.MEMBERTYPE_ADMIN) {
				queryString[0] = "FROM OrderInfor T WHERE  "+addsql+" T.status != 0 ORDER BY T.orderSubmitTime DESC";
				queryString[1] = "SELECT COUNT(T.orderId) FROM OrderInfor T WHERE  "+addsql+" T.status != 0";
			} else {
				queryString[0] = "FROM OrderInfor T " + " WHERE  "+addsql+" T.status != 0 and T.restaurant.restaurantId in "
						+ "(SELECT R.restaurantId  FROM RestaurantInfor R where R.memberadmin =" + member.getMemberId()
						+ ")" + " ORDER BY T.orderSubmitTime DESC";
				queryString[1] = "SELECT COUNT(T.orderId) FROM OrderInfor T "
						+ " WHERE  "+addsql+" T.status != 0 AND T.restaurant.restaurantId in "
						+ "(SELECT R.restaurantId  FROM RestaurantInfor R where R.memberadmin =" + member.getMemberId()
						+ ")";
			}
			pagenum = 10;
			// 订单已过就餐日期3天以上仍旧为餐厅确认状态 则设置为未出席状态
			orderInforDao
					.excuteBySQL("UPDATE  `order_infor` O SET O.status=96 WHERE O.status=10 AND DATEDIFF(NOW() , O.orderDate ) > 3 ");
			logger.info("excute sql ：Synchronization is not present status  ");

		}
		pages.setSqls(queryString);
		pages.setPerPageNum(pagenum);
 
	}
 
}
