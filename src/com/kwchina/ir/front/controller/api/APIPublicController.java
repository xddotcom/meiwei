package com.kwchina.ir.front.controller.api;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.kwchina.core.sys.CoreConstant;
import com.kwchina.core.util.DateConverter;
import com.kwchina.core.util.HttpHelper;
import com.kwchina.ir.entity.RestaurantTimeInfor;
import com.kwchina.ir.front.controller.AbstractAPIController;
import com.kwchina.ir.front.controller.api.json.AbstractCatch;
import com.kwchina.ir.front.controller.api.json.JsonConfigFactory;
import com.kwchina.ir.util.CommonHelper;
import com.kwchina.ir.util.StringHelper;
import com.kwchina.ir.util.TextHelper;
import com.kwchina.ir.util.VerifyCodeHelper;
import com.kwchina.ir.vo.api.ApiBookTimeVo;

@SuppressWarnings({ "rawtypes", "unchecked" })
@Scope("prototype")
@Controller
public class APIPublicController extends AbstractAPIController {

	private static String[] RESTAURANT_EXCLUDES = new String[]{ "description","restaurantSearch" ,"discount" ,"address","cuisine"};
	
	@RequestMapping(value = "/api/public/restaurant/nameList.htm")
	public void nameList(HttpServletRequest request, HttpServletResponse response) {

		new AbstractCatch( request , response , "restaurants" ) {
			@Override
			public String excute() {
				List results = restaurantInforDao.getResultByQueryString(
				"SELECT new RestaurantInfor(T.fullName) FROM RestaurantInfor T WHERE 1=1 AND T.isDeleted=0 ");
 				return convertList( results , JsonConfigFactory.includes(new String[]{ "fullName" } , new String[]{} ) ).toString();
			}
		}._do();
	}

	@RequestMapping(value = "/api/public/order/openingHours.htm")
	public void openingHours(HttpServletRequest request, HttpServletResponse response) {
		
		List<ApiBookTimeVo> timeVos = new ArrayList<ApiBookTimeVo>();
		String paramDay = request.getParameter("time");
		try {
			String regex = "[,.：:();]";
			Integer id = Integer.parseInt(request.getParameter("restaurantId"));
			String currentTime = new SimpleDateFormat("HH:mm").format(new Date());
			String currentDay = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
			if (StringHelper.isEmpty(paramDay)) {
				paramDay = currentDay;
			}
			List<RestaurantTimeInfor> results = restaurantTimeInforDao.getRestaurantHours(id);

			List<String[]> noEqualParts = new ArrayList<String[]>();

			SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
			Date date = s.parse(paramDay);
			Calendar c = Calendar.getInstance();
			c.setTime(date);
			int day = c.get(Calendar.DAY_OF_WEEK);
			int mask = 1 << (7 - day);
			if (CollectionUtils.isNotEmpty(results)) {
				SimpleDateFormat format = new SimpleDateFormat("HH:mm");
				for (RestaurantTimeInfor timeEntity : results) {
					if ((timeEntity.getDays() & mask) != 0) {
						noEqualParts.add(new String[] { format.format(timeEntity.getStartTime()), format.format(timeEntity.getEndTime()),
								TextHelper.getText(timeEntity.getDiscount()) });
					}
				}
				timeVos.addAll(new CommonHelper().getData(noEqualParts));
			}

			if (CollectionUtils.isNotEmpty(timeVos) && currentDay.equals(paramDay)) {
				Iterator<ApiBookTimeVo> iterator = timeVos.iterator();
				while (iterator.hasNext()) {
					ApiBookTimeVo vo = iterator.next();
					if (StringHelper.regex(currentTime, regex) > StringHelper.regex(vo.getOptionTitle(), regex)) {
						iterator.remove();
					}
				}
			}
			if (CollectionUtils.isEmpty(timeVos)) {
				timeVos.addAll(new CommonHelper().getData(noEqualParts));
				paramDay = DateConverter.getSpecifiedDayAfter(new Date());
				if (CollectionUtils.isEmpty(timeVos)) {
					List<String[]> empty = new ArrayList<String[]>();
					empty.add(new String[] { "18:30", "23:30" });
					timeVos.addAll(new CommonHelper().getData(empty));
				}
			}
		} catch (Exception e) {
			logger.error("Unsuccessfully get restaurant openning time", e);
		}
		
		JSONObject json = new JSONObject();
  		json.put("times", convertList( timeVos ));
		json.put("day", paramDay);
		new HttpHelper().json(response, json.toString() );
	}

	@RequestMapping("/api/public/base/code.htm")
	public void getVerifyCode(HttpServletRequest req, HttpServletResponse resp) {
		req.getSession().setAttribute(CoreConstant.SESSION_ATTR_VERIFYCODE, VerifyCodeHelper.getCode(req, resp));
	}

	
	@RequestMapping(value = "/api/public/base/cuisines.htm")
	public void cuisines(HttpServletRequest request, HttpServletResponse response) {
		new AbstractCatch( request , response , "cuisines" ) {
			@Override
			public String excute() {
				return convertList( baseCuisineInforDao.getSortedCuisines() ).toString();
			}
		}._do();
	}
	
	@RequestMapping(value = "/api/public/base/circles.htm")
	public void circles(HttpServletRequest request, HttpServletResponse response) {
		new AbstractCatch( request , response , "circles" ) {
			@Override
			public String excute() {
				return convertList(  baseCircleInforDao.getSortedCircles() ).toString();
			}
		}._do();
	}
	
	@RequestMapping(value = "/api/public/base/districts.htm")
	public void districts(HttpServletRequest request, HttpServletResponse response) {
		new AbstractCatch( request , response , "districts" ) {
			@Override
			public String excute() {
				return convertList(  baseDistrictInforDao.getDistricts() ).toString();
			}
		}._do();
	}
	
	@RequestMapping(value = "/api/public/base/recommends.htm")
	public void recommends(HttpServletRequest request, HttpServletResponse response) {
		new AbstractCatch( request , response , "recommends" ) {
			@Override
			public String excute() {
				return convertList( getRecommends() ,RESTAURANT_EXCLUDES).toString();
			}
		}._do();
	}
	
	@RequestMapping(value = "/api/public/base/baseData.htm")
	public void baseData(HttpServletRequest request, HttpServletResponse response) {
		new AbstractCatch( request , response , "base" ) {
			@Override
			public String excute() {
				JSONObject json = new JSONObject();
				JSONObject single = new JSONObject();
				Map m = new HashMap();
				m.put("cuisines", baseCuisineInforDao.getSortedCuisines());
				m.put("circles", baseCircleInforDao.getSortedCircles());
				m.put("recommends",  getRecommends() );
				m.put("districts", baseDistrictInforDao.getDistricts());
				single.putAll(m, JsonConfigFactory.create(new String[] {}));
				json.put("base", single);
				return json.toString();
			}
		}._do();
	}
}


