package com.kwchina.ir.front.controller.api;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.kwchina.core.sys.CoreConstant;
import com.kwchina.core.util.HttpHelper;
import com.kwchina.ir.entity.MemberInfor;
import com.kwchina.ir.entity.OrderInfor;
import com.kwchina.ir.entity.RestaurantTablePicInfor;
import com.kwchina.ir.front.controller.AbstractAPIController;
import com.kwchina.ir.util.StringHelper;
import com.kwchina.ir.vo.OrderInforVo;

@Scope("prototype")
@Controller
public class APIOrderController extends AbstractAPIController {
	@RequestMapping("/api/reservation/roll/{id}.htm")
	public void roll(@PathVariable Integer id, HttpServletRequest request,
			HttpServletResponse response) {
		try {
			OrderInfor order = orderInforDao.get(id);
			if (order.getStatus() == 0 && order.getCreditGain() == 0) {
				int roll = (int) (2 + Math.random() * 2.5);
				order.setCreditGain(-roll);
				orderInforDao.saveOrUpdate(order, order.getOrderId());
				HttpHelper.DEFAULT.output(response, String.valueOf(roll));
				return;
			}
		} catch (Exception e) {
			logger.error("Roll Failed", e);
		}
		HttpHelper.DEFAULT.output(response, "0");
	}
	
	
	private OrderInfor orderBelongsToRestaurant(Integer orderId, MemberInfor member) {
		OrderInfor order = null;
		if (member.getMemberType() != CoreConstant.MEMBERTYPE_STORE
				&& member.getMemberType() != CoreConstant.MEMBERTYPE_ADMIN) {
			logger.info("Login User Is Not Restaurant");
			return null;
		} else if (orderId == null || (order = orderInforDao.get(orderId)) == null) {
			logger.debug("OrderId Invalid");
			return null;
		} else if (member.getMemberType() != CoreConstant.MEMBERTYPE_ADMIN
				&& member.getMemberId() != order.getRestaurant().getMemberadmin().getMemberId()) {
			logger.info("Order Doesn't Belong To Current Restaurant User!!!");
			return null;
		} else {
			logger.debug("Order Belongs To Restaurant.");
			return order;
		}
	}

	private OrderInfor orderBelongsToMember(Integer orderId, MemberInfor member) {
		OrderInfor order = null;
		if (member.getMemberType() != CoreConstant.MEMBERTYPE_PERSONAL) {
			logger.info("Login User Is Not Restaurant");
			return null;
		} else if (orderId == null || (order = orderInforDao.get(orderId)) == null) {
			logger.debug("OrderId Invalid");
			return null;
		} else if (order.getMember().getMemberId() != member.getMemberId()) {
			logger.info("Order Doesn't Belong To Current User!!!");
			return null;
		} else {
			logger.debug("Order Belongs To Member.");
			return order;
		}
	}
	
	@RequestMapping("/api/reservation/do/inviteFriends.htm")
	public void inviteFriends(@ModelAttribute("orderInforVo") OrderInforVo orderInforVo, HttpServletRequest request,
			HttpServletResponse response) {
		try {
			MemberInfor member = memberInforDao.getLoginMember(request);
			OrderInfor order = orderBelongsToMember(orderInforVo.getOrderId(), member);
			if (order != null) {
				order.setInviteFriends(orderInforVo.getInviteFriends());
				orderInforDao.update(order);
			}
		} catch (Exception e) {
			logger.error("In inviteFriends Saving order failed.", e);
		}
	}
	
	

	@RequestMapping("/api/reservation/do/delete.htm")
	public void delete(@ModelAttribute("orderInforVo") OrderInforVo orderInforVo, Model model,
			HttpServletRequest request, HttpServletResponse response) {
		try {
			MemberInfor member = memberInforDao.getLoginMember(request);
			Integer orderId = orderInforVo.getOrderId();
			OrderInfor order = null;
			int status = CoreConstant.ORDER_STATUS_MEMBER_CANCEL;
			if (member != null && member.getMemberType() == CoreConstant.MEMBERTYPE_PERSONAL) {
				order = orderBelongsToMember(orderId, member);
			} else if (member != null
					&& (member.getMemberType() == CoreConstant.MEMBERTYPE_STORE || member.getMemberType() == CoreConstant.MEMBERTYPE_ADMIN)) {
				order = orderBelongsToRestaurant(orderId, member);
				status = CoreConstant.ORDER_STATUS_RESTAU_CANCEL;
			}
			if (order != null) {
				if (order.getStatus() != CoreConstant.ORDER_STATUS_NEW
						&& order.getStatus() != CoreConstant.ORDER_STATUS_RESTAU_CONFIRM) {
					logger.info("Cannot Delete. Not A New Order.");
					return;
				}
				order.setStatus(status);
				orderInforDao.update(order);
				
				orderInforDao.sendMessage(order.getContactName(), order.getContactTelphone(), order, 5);
				
				HttpHelper.DEFAULT.output(response, "1");
				return;
			}
		} catch (Exception e) {
			logger.error("Delete order failed.", e);
		}
		HttpHelper.DEFAULT.output(response, "0");
	}

	@RequestMapping("/api/reservation/restaurant/confirm.htm")
	public void restaurantConfirm(@ModelAttribute("orderInforVo") OrderInforVo orderInforVo, 
			HttpServletRequest request, HttpServletResponse response) {
		try {
			MemberInfor member = memberInforDao.getLoginMember(request);
			Integer orderId = orderInforVo.getOrderId();
			OrderInfor order = orderBelongsToRestaurant(orderId, member);
			if (order != null) {
				if (order.getStatus() != CoreConstant.ORDER_STATUS_NEW) {
					logger.info("Cannot Confirm. Not A New Order.");
					return;
				}
				order.setStatus(CoreConstant.ORDER_STATUS_RESTAU_CONFIRM);
				orderInforDao.saveOrUpdate(order, order.getOrderId());

				HttpHelper.DEFAULT.output(response, "1");
				return;
			}
		} catch (Exception e) {
			logger.error("Change order status failed.", e);
		}
		HttpHelper.DEFAULT.output(response, "0");
	}
	
	@RequestMapping("/api/reservation/save/amount.htm")
	public void saveAmount(@ModelAttribute("orderInforVo") OrderInforVo orderInforVo, HttpServletRequest request,
			HttpServletResponse response) {
		try {
			MemberInfor member = memberInforDao.getLoginMember(request);
			Integer orderId = orderInforVo.getOrderId();
			OrderInfor order = orderBelongsToRestaurant(orderId, member);
			if (order != null) {
				if (order.getStatus() != CoreConstant.ORDER_STATUS_RESTAU_CONFIRM
						&& order.getStatus() != CoreConstant.ORDER_STATUS_MEMBER_PRESENT) {
					logger.info("Cannot Save Amount. Order WAS NOT Confirmed OR USER WAS NOT PRESENT.");
					return ;
				}
				order.setConsumeAmount(orderInforVo.getConsumeAmount());
				double commissionRate = order.getRestaurant().getCommissionRate();
				double commission = Math.round(orderInforVo.getConsumeAmount() * commissionRate) / 100.0;
				order.setCommission(commission);
				order.setStatus(CoreConstant.ORDER_STATUS_RESTAU_PAYABLE);
				orderInforDao.saveOrUpdate(order, order.getOrderId());
				orderInforDao.sendMessage(order.getContactName(), order.getContactTelphone(), order, 2);
				orderInforDao.calculateCreditFromOrder(order);
			}
		} catch (Exception e) {
			logger.error("Cannot save amount.", e);
		}
 	}
	

	@RequestMapping("/api/reservation/do/confirmation.htm")
	public void saveReservation(@ModelAttribute("orderInforVo") OrderInforVo orderInforVo, HttpServletRequest request,
			HttpServletResponse response) {
		try {
			MemberInfor member = memberInforDao.getLoginMember(request);

			if (orderInforVo.getOrderId() != null) {
				OrderInfor oop = orderBelongsToMember(orderInforVo.getOrderId(), member);
				if (oop == null || oop.getStatus() != CoreConstant.ORDER_STATUS_NEW) {
					logger.info("Cannot update. But A New Order.");
					return ;
				}
			}
			
 			List<?> results =  orderInforDao.getSession().createSQLQuery
					( " SELECT * FROM order_infor O where O.orderSubmitTime > DATE_SUB(  NOW() ,INTERVAL 60 SECOND ) AND O.member="+member.getMemberId()   ).list() ;
			if(CollectionUtils.isNotEmpty( results )){
				logger.info("save order in 60 second.");
				return ;
			}
			
			OrderInfor order = orderInforDao.saveOrder(orderInforVo, member.getMemberId());
			orderInforDao.sendMessage(order.getContactName(), order.getContactTelphone(), order, 3);
			orderInforDao.sendMessage(order.getContactName(), order.getContactTelphone(), order, 6);
			orderInforDao.sendMessage(order.getContactName(), order.getContactTelphone(), order, 1);
			orderInforDao.sendMessage(order.getContactName(), order.getContactTelphone(), order, 4);
			orderInforDao.sendInviteMessage(order);
		} catch (DataAccessException e) {
			logger.error("Confirm reservation failed.", e);
		}
	}
	
	
	@RequestMapping("/api/reservation/viewtable/{key}")
	public void viewtable(@PathVariable("key") String key , HttpServletRequest request, HttpServletResponse response) {
		try {
			if (StringHelper.isNotEmpty(key)) {
				OrderInfor order = orderInforDao.getOrderByNo(key);
				if (order != null && StringHelper.isNotEmpty(order.getTables()) && order.getTables().contains("|")) {
					Integer id = Integer.parseInt(order.getTables().substring(0, order.getTables().indexOf("|")));
					RestaurantTablePicInfor table = restaurantTablePicInforDao.get(id);
				
					response.setContentType("application/json;charset=UTF-8");
					try {
						response.getWriter().print( " {\"imgPath\":\""+table.getTablePicPath()+"\" , \"tables\" : \""+order.getTables()+"\" }");
					} catch (IOException e) {
						logger.error("API restaurant view ERROR", e);
					}
				}
 			}
		} catch (Exception e) {
			logger.error("Show Table Pictrue Error", e);
		}
 	}
	
}
