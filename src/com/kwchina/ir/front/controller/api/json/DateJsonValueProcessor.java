package com.kwchina.ir.front.controller.api.json;

import java.text.SimpleDateFormat;
import java.util.Date;

import net.sf.json.JsonConfig;
import net.sf.json.processors.JsonValueProcessor;

public class DateJsonValueProcessor implements JsonValueProcessor {

	private String pattern;

	public DateJsonValueProcessor() {
		this.pattern = "yyyy-MM-dd HH:mm:ss";
	}
	
	public DateJsonValueProcessor(String pattern) {
		super();
		this.pattern = pattern;
	}

	@Override
	public Object processObjectValue(String key, Object value, JsonConfig config) {
		return process(value);
	}

	@Override
	public Object processArrayValue(Object value, JsonConfig config) {
		return process(value);
	}

	private Object process(Object value) {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(pattern);
			if (value instanceof Date) {
				return sdf.format((Date) value);
			} else if (value instanceof java.sql.Date) {
				return sdf.format((java.sql.Date) value);
			}
			return value == null ? "" : value.toString();
		} catch (Exception e) {
			return "";
		}
	}
}
