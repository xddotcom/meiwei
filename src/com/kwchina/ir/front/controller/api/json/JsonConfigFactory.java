package com.kwchina.ir.front.controller.api.json;

import java.util.Arrays;
import java.util.Date;

import net.sf.json.JsonConfig;
import net.sf.json.util.PropertyFilter;

import com.kwchina.core.util.Pages;
import com.kwchina.ir.entity.BaseCircleInfor;
import com.kwchina.ir.entity.MemberInfor;
import com.kwchina.ir.entity.MemberPersonalInfor;
import com.kwchina.ir.entity.ProductInfor;
import com.kwchina.ir.entity.RestaurantInfor;
import com.kwchina.ir.entity.RestaurantMenuInfor;
import com.kwchina.ir.entity.RestaurantPicInfor;
import com.kwchina.ir.entity.RestaurantSearchInfor;
import com.kwchina.ir.entity.RestaurantTablePicInfor;
import com.kwchina.ir.entity.RestaurantTimeInfor;

public class JsonConfigFactory {

	public static JsonConfig create( boolean ignoreDefaultExcludes ,  String[] excludes){
		JsonConfig config = new JsonConfig();
		config.setIgnoreDefaultExcludes( ignoreDefaultExcludes );
		config.setExcludes( excludes );
		config.registerJsonValueProcessor(Date.class, new DateJsonValueProcessor( ));
		return config;
	}
	
	public static JsonConfig create(  String[] excludes ){
		JsonConfig config = new JsonConfig();
		config.setIgnoreDefaultExcludes( false );
		config.setExcludes( excludes );
		config.registerJsonValueProcessor(Date.class, new DateJsonValueProcessor( ));
		config.setJsonPropertyFilter( new PropertyFilter() {
			@Override
			public boolean apply(Object source, String name, Object value) {
				if(source.getClass() == MemberPersonalInfor.class &&  !name.equals( "nickName") && !name.equals( "sexe")){
					return true;
				}else if( source.getClass() == MemberInfor.class  && !name.equals( "personalInfor")){
					return true;
				}else if(source.getClass() == RestaurantSearchInfor.class && name.equals( "restaurant")){
					return true;
				}else if(source.getClass() == RestaurantInfor.class
						&& "circle|memberadmin|commissionRate|isDeleted|telphone".contains(name)){
					return true;
				}else if(source.getClass() == BaseCircleInfor.class && "district".contains( name )){
					return true;
				}else if(source.getClass() == RestaurantPicInfor.class && "restaurant".contains( name )){
					return true;
				}else if(source.getClass() == RestaurantMenuInfor.class && "restaurant".contains( name )){
					return true;
				}else if(source.getClass() == RestaurantTimeInfor.class && "restaurant".contains( name )){
					return true;
				}else if(source.getClass() == ProductInfor.class && "productType".contains( name )){
					return true;
				}else if(source.getClass() == RestaurantTablePicInfor.class && "restaurant".contains( name )){
					return true;
				}
				return false;
			}
		});
		return config;
	}
	
	public static JsonConfig create(){
		JsonConfig config = new JsonConfig();
		config.setIgnoreDefaultExcludes( false );
 		config.registerJsonValueProcessor(Date.class, new DateJsonValueProcessor( ));
		config.setJsonPropertyFilter( new PropertyFilter() {
			@Override
			public boolean apply(Object source, String name, Object value) {
				if(source.getClass() == MemberPersonalInfor.class &&  "member|".contains( name ) ){
					return true;
				}else if( source.getClass() == MemberInfor.class  && "loginPassword|hashCode".contains( name ) ){
					return true;
				}else if( source.getClass() == Pages.class && "sqls|request".contains( name )){
					return true;
				}else if( source.getClass() == ProductInfor.class && "productType".contains( name )){
					return true;
				}
				return false;
			}
		});
		return config;
	}
	
	
	public static JsonConfig includes(final String[] includes , String[] excludes){
		JsonConfig config = new JsonConfig();
		config.setIgnoreDefaultExcludes( false );
		config.setExcludes( excludes );
 		config.registerJsonValueProcessor(Date.class, new DateJsonValueProcessor( ));
		config.setJsonPropertyFilter( new PropertyFilter() {
			@Override
			public boolean apply(Object source, String name, Object value) {
				if(source.getClass() == RestaurantInfor.class &&  !Arrays.toString(includes).contains( name ) ){
					return true;
				}else if(source.getClass() == MemberPersonalInfor.class && !Arrays.toString(includes).contains( name ) ){
					return true;
				}
				return false;
			}
		});
		return config;
	}
	
}
