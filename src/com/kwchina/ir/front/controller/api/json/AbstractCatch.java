package com.kwchina.ir.front.controller.api.json;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kwchina.core.sys.CoreConstant;
import com.kwchina.core.util.HttpHelper;
import com.kwchina.ir.util.LanguageHelper;
import com.kwchina.ir.util.StringHelper;

public abstract class AbstractCatch implements Catch {
	
	protected HttpServletRequest request;
	protected HttpServletResponse response ;
	protected String name;
	@Override
	public boolean isNeed(String name) {
		String _catch = request.getParameter( "catch");
 		if( "true".equals( _catch)){
			return true;
		}
		return false;
	}
	@Override
	public void _do() {
		String name = get( this.name );
		String content = CoreConstant.cacheMap.get( name );
		if(isNeed( name ) && StringHelper.isEmpty(content)){
			String json = excute();
			CoreConstant.cacheMap.put(name, (content = (json == null ? "" : json.toString())));
		}else{
			content = excute();
		}
		new HttpHelper().json(response, content);
 	}
	public abstract String excute();
	public AbstractCatch(HttpServletRequest request, HttpServletResponse response, String name){
		this.request = request;
		this.response = response;
		this.name = name;
	}

	private String get(String name) {
		int languageType = LanguageHelper.launage();
		if (languageType == CoreConstant.LANGUAGETYPE_ENGLISH) {
			return name + "_EN";
		} else if (languageType == CoreConstant.LANGUAGETYPE_CHINESE) {
			return name + "_CN";
		} else {
			return name + "_CN";
		}
	}
}