package com.kwchina.ir.front.freemarker;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;

import freemarker.template.TemplateMethodModel;
import freemarker.template.TemplateModelException;

public class EncodeURLMethod implements TemplateMethodModel {

	public Object exec(@SuppressWarnings("rawtypes") List args) throws TemplateModelException {
		try {
			return URLEncoder.encode(args.get(0).toString(), "UTF-8")
					.replaceAll("\\+", " ");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return new String("");
	}
}
