package com.kwchina.ir.dao;

import com.kwchina.core.common.BasicDao;
import com.kwchina.ir.entity.ArticleInfor;

public interface ArticleInforDao extends BasicDao<ArticleInfor> {

}
