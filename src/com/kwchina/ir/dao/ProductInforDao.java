package com.kwchina.ir.dao;

import java.util.List;

import com.kwchina.core.common.BasicDao;
import com.kwchina.ir.entity.ProductInfor;

public interface ProductInforDao  extends BasicDao<ProductInfor> {

	List<ProductInfor> getProductsByType(int i);

}
