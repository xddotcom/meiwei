package com.kwchina.ir.dao;

import com.kwchina.core.common.BasicDao;
import com.kwchina.ir.entity.CouponRestaurantRel;

public interface CouponRestaurantRelDao extends BasicDao<CouponRestaurantRel> {

}
