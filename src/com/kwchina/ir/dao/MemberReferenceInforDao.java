package com.kwchina.ir.dao;

import com.kwchina.core.common.BasicDao;
import com.kwchina.ir.entity.MemberReferenceInfor;

public interface MemberReferenceInforDao extends BasicDao<MemberReferenceInfor> {

}
