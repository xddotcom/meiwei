package com.kwchina.ir.dao;

import java.util.List;

import com.kwchina.core.common.BasicDao;
import com.kwchina.ir.entity.BaseCircleInfor;

public interface BaseCircleInforDao extends BasicDao<BaseCircleInfor> {

	List<BaseCircleInfor> getSortedCircles();

}
