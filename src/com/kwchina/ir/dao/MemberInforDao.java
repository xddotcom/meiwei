package com.kwchina.ir.dao;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.kwchina.core.common.BasicDao;
import com.kwchina.ir.entity.MemberInfor;
import com.kwchina.ir.vo.MemberInforVo;

public interface MemberInforDao extends BasicDao<MemberInfor> {

	MemberInfor createMember(MemberInforVo memberInforVo);

	MemberInfor ModifyMember(MemberInforVo memberInforVo, Integer memberId);

	List<MemberInfor> validateNameExist(String loginName, Integer memberId);

	List<MemberInfor> validateEmailExist(String email, Integer memberId);

	List<MemberInfor> validateInvitation(String code, Integer memberId);

	MemberInfor validateLogin(String loginName, String loginPassword);

	MemberInfor getInforByLoginName(String loginName);

	MemberInfor getLoginMember(HttpServletRequest request);

	List<MemberInfor> validateMemberExist(String loginName);

	List<MemberInfor> validateMobileExist(String mobile, Integer memberId);
}
