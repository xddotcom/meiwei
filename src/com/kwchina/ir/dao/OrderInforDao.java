package com.kwchina.ir.dao;

import java.util.List;

import com.kwchina.core.common.BasicDao;
import com.kwchina.ir.entity.OrderInfor;
import com.kwchina.ir.vo.OrderInforVo;

public interface OrderInforDao extends BasicDao<OrderInfor> {

	OrderInfor getOrderByNo(String searchName);

	List<OrderInfor> getOrderInforsByMemberId(int memberId, int top);

	OrderInfor saveOrder(OrderInforVo orderInforVo, Integer memberId);

	void sendMessage(String name, String telphone, OrderInfor order, int messageType);
	
	void sendInviteMessage(OrderInfor order );
	
	void calculateCreditFromOrder(OrderInfor order) ;
}
