package com.kwchina.ir.dao.impl;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kwchina.core.common.BasicDaoImpl;
import com.kwchina.ir.dao.MemberReferenceInforDao;
import com.kwchina.ir.entity.MemberReferenceInfor;

@SuppressWarnings("unchecked")
@Repository
@Transactional
public class MemberReferenceInforDaoImpl extends
		BasicDaoImpl<MemberReferenceInfor> implements MemberReferenceInforDao {

}
