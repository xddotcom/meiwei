package com.kwchina.ir.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kwchina.core.common.BasicDaoImpl;
import com.kwchina.ir.dao.ProductTypeInforDao;
import com.kwchina.ir.entity.ProductTypeInfor;

@SuppressWarnings("unchecked")
@Repository
@Transactional
public class ProductTypeInforDaoImpl extends BasicDaoImpl<ProductTypeInfor> implements ProductTypeInforDao {

	@Override
	public List<ProductTypeInfor> getValueAddedProducts() {
		return getResultByQueryString("FROM ProductTypeInfor WHERE category=1 ORDER BY typeOrder");
	}

}
