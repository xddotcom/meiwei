package com.kwchina.ir.dao.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kwchina.core.common.BasicDaoImpl;
import com.kwchina.ir.dao.RestaurantInforDao;
import com.kwchina.ir.entity.MemberInfor;
import com.kwchina.ir.entity.RestaurantInfor;
import com.kwchina.ir.entity.TextInfor;
import com.kwchina.ir.util.StringHelper;
import com.kwchina.ir.util.TextHelper;
import com.kwchina.ir.vo.RestaurantInforVo;
import com.kwchina.ir.vo.SearchInforVo;

@SuppressWarnings("unchecked")
@Repository
@Transactional
public class RestaurantInforDaoImpl extends BasicDaoImpl<RestaurantInfor> implements RestaurantInforDao {

	public RestaurantInfor getRestaurantByNo(String searchName) {
		logger.debug("Get restaurant by No: " + searchName);
		List<RestaurantInfor> results = getResultByQueryString(String.format(
				"FROM RestaurantInfor WHERE restaurantNo = '%s'", TextHelper.getSafeText(searchName)));
		if (CollectionUtils.isNotEmpty(results)) {
			return results.get(0);
		} else {
			return null;
		}
	}

	@Override
	public List<RestaurantInfor> searchRestaurants(SearchInforVo searchInforVo) {

		Integer ruleId = searchInforVo.getRuleId();
		if (ruleId != null) {
			String hql = "SELECT T.restaurant FROM RecommendInfor T " + " WHERE T.restaurant.isDeleted = 0 "
					+ " AND T.rankRule.ruleId = " + ruleId;
			return getResultByQueryString(hql);
		} else {

			String hql = "SELECT new RestaurantInfor(T.address,T.description,T.discount,T.frontPic,T.fullName,T.perBegin,T.restaurantId,T.restaurantNo,T.score) FROM RestaurantInfor T WHERE T.isDeleted = 0 ";

			String searchKey = searchInforVo.getSearchKey();
			if (searchKey != null && !searchKey.trim().isEmpty()) {
				String safeSearchKey = TextHelper.getSafeText(searchKey);
				hql += " AND (T.fullName.chinese LIKE '%" + safeSearchKey + "%' " + " OR T.fullName.english LIKE '%"
						+ safeSearchKey + "%')";
			}

			Integer circleId = searchInforVo.getCircleId();
			if (circleId != null) {
				hql += " AND T.circle = " + circleId;
			}

			Integer cuisineId = searchInforVo.getCuisineId();
			if (cuisineId != null) {
				hql += " AND T.cuisine = " + cuisineId;
			}

			String date = searchInforVo.getDate();
			if (date != null && !date.trim().isEmpty() && checkQueryDate(searchInforVo)) {

			}

			String orderBy = searchInforVo.getOrderBy();
			if (orderBy == null || orderBy.trim().isEmpty()) {
				hql += " ORDER BY T.score DESC ";
			} else {
				hql += " ORDER BY " + TextHelper.getSafeText(orderBy);
			}

			logger.info(hql);

			List<RestaurantInfor> restaurants = getResultByQueryString(hql);

			return restaurants;
		}
	}

	private boolean checkQueryDate(SearchInforVo searchInforVo) {
		try {
			String date = searchInforVo.getDate();
			String time = searchInforVo.getTime();
			long now = System.currentTimeMillis();
			long query = 0l;
			if (StringHelper.isTrimNotEmpty(date)) {
				searchInforVo.setDate(date);
				if (StringHelper.isTrimNotEmpty(time)) {
					DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
					Date dt = dateFormat.parse(date + " " + time);
					query = dt.getTime();
				} else {
					DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
					Date dt = dateFormat.parse(date);
					query = dt.getTime();
				}
				if (query > now) {
					return true;
				}
			}
		} catch (Exception e) {
			logger.error("Error checking query date & time.", e);
		}
		return false;
	}

	@Override
	public boolean isRestaurantMember(MemberInfor member, Integer restaurantId) {
		if (member != null && member.getMemberId()!=null && restaurantId != null) {
			@SuppressWarnings("rawtypes")
			List l = this.getResultByQueryString("from RestaurantInfor where memberadmin.memberId="
					+ member.getMemberId() + " and restaurantId=" + restaurantId);
			return CollectionUtils.isNotEmpty(l);
		}
		return false;
	}

	@Override
	public void updateRestaurant(RestaurantInfor restaurantInfor, RestaurantInforVo restaurantInforVo) {
		
		TextInfor fullName = insertTextInfor( restaurantInforVo.getFullName() , restaurantInforVo.getFullNameEN()  );
		TextInfor description = insertTextInfor( restaurantInforVo.getDescription(), restaurantInforVo.getDescriptionEN());
		TextInfor address = insertTextInfor( restaurantInforVo.getAddress(), restaurantInforVo.getAddressEN());
		TextInfor discount = insertTextInfor( restaurantInforVo.getDiscount(), restaurantInforVo.getDiscountEN());
		TextInfor parking = insertTextInfor( restaurantInforVo.getParking(), restaurantInforVo.getParkingEN());
		TextInfor workinghour = insertTextInfor( restaurantInforVo.getWorkinghour(), restaurantInforVo.getWorkinghourEN());
		
		restaurantInfor.setFullName(fullName);
		restaurantInfor.setDescription(description);
		restaurantInfor.setAddress(address);
		restaurantInfor.setDiscount(discount);
		restaurantInfor.setParking(parking);
		restaurantInfor.setWorkinghour(workinghour);
		update(restaurantInfor);
	}

	private TextInfor insertTextInfor( String cn, String en) {
		TextInfor text = new TextInfor();
		text.setChinese( cn  );
		text.setEnglish( en );
		save(text);
		return text;
	}
}
