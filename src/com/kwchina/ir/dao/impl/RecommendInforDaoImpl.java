package com.kwchina.ir.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kwchina.core.common.BasicDaoImpl;
import com.kwchina.ir.dao.RecommendInforDao;
import com.kwchina.ir.entity.RecommendInfor;

@SuppressWarnings("unchecked")
@Repository
@Transactional
public class RecommendInforDaoImpl extends
		BasicDaoImpl<RecommendInfor> implements
		RecommendInforDao {

	@Override
	public List<RecommendInfor> getRecommendedRestaurants(int ruleId) {
		return getResultByQueryString("FROM RecommendInfor "
				+ " WHERE restaurant.isDeleted = 0"
				+ " AND rankRule.ruleId = " + ruleId
				+ " ORDER BY rank ASC", true, 0, 8);
	}

}
