package com.kwchina.ir.dao.impl;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kwchina.core.common.BasicDaoImpl;
import com.kwchina.ir.dao.BaseCityInforDao;
import com.kwchina.ir.entity.BaseCityInfor;

@SuppressWarnings("unchecked")
@Repository
@Transactional
public class BaseCityInforDaoImpl extends BasicDaoImpl<BaseCityInfor> implements
		BaseCityInforDao {

}
