package com.kwchina.ir.dao.impl;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kwchina.core.common.BasicDaoImpl;
import com.kwchina.ir.dao.MemberInforDao;
import com.kwchina.ir.dao.MemberReviewInforDao;
import com.kwchina.ir.dao.RestaurantInforDao;
import com.kwchina.ir.entity.MemberInfor;
import com.kwchina.ir.entity.MemberReviewInfor;
import com.kwchina.ir.entity.RestaurantInfor;
import com.kwchina.ir.vo.MemberReviewInforVo;

@SuppressWarnings("unchecked")
@Repository
@Transactional
public class MemberReviewInforDaoImpl extends BasicDaoImpl<MemberReviewInfor> implements
		MemberReviewInforDao {

	@Resource
	private MemberInforDao memberInforDao;

	@Resource
	private RestaurantInforDao restaurantInforDao;

	public void saveReview(MemberReviewInforVo reviewInforVo) {
		MemberReviewInfor reviewInfor = new MemberReviewInfor();
		reviewInfor.setIsChecked(reviewInforVo.getIsChecked() == null ? 2 : reviewInforVo
				.getIsChecked());
		reviewInfor.setScore(reviewInforVo.getScore());
		reviewInfor.setComments(reviewInforVo.getComments());

		reviewInfor.setEnvironmentScore(reviewInforVo.getEnvironment());
		reviewInfor.setTasteScore(reviewInforVo.getTaste());
		reviewInfor.setServiceScore(reviewInforVo.getService());
		reviewInfor.setStartPrice(reviewInforVo.getStartPrice() == null ? 0 : reviewInforVo
				.getStartPrice());
		reviewInfor.setEndPrice(reviewInforVo.getEndPrice() == null ? 0 : reviewInforVo
				.getEndPrice());

		reviewInfor.setReviewTime(new Date(System.currentTimeMillis()));

		// Create
		if (reviewInforVo.getReviewId() != null) {
			reviewInfor.setReviewId(reviewInforVo.getReviewId());
		}

		if (reviewInforVo.getMemberId() != null && reviewInforVo.getMemberId() != 0) {
			MemberInfor memberInfor = memberInforDao.get(reviewInforVo.getMemberId());
			reviewInfor.setMember(memberInfor);
		}

		if (reviewInforVo.getRestaurantId() != null && reviewInforVo.getRestaurantId() != 0) {
			RestaurantInfor restaurant = restaurantInforDao.get(reviewInforVo.getRestaurantId());
			reviewInfor.setRestaurant(restaurant);
		}
		saveOrUpdate(reviewInfor, reviewInfor.getReviewId());
	}

	public List<MemberReviewInfor> getReviewInforsByMemberId(int memberId, int top) {
		String hql = "FROM MemberReviewInfor T WHERE T.member.memberId = " + memberId
				+ " ORDER BY T.reviewTime DESC";
		if (top > 0) {
			hql += " LIMIT " + top;
		}
		return getResultByQueryString(hql);
	}

	@Override
	public List<MemberReviewInfor> getReviewInforsByRestaurantId(int restaurantId, int top) {
		String hql = "FROM MemberReviewInfor T WHERE T.restaurant.restaurantId = " + restaurantId
				+ "ORDER BY T.reviewTime DESC";
		if (top > 0) {
			hql += " LIMIT " + top;
		}
		return getResultByQueryString(hql);
	}

	@Override
	public void deleteReview(Integer memberId, Integer reviewId) {
		MemberReviewInfor review = this.get(reviewId);
		if (review != null && review.getMember().getMemberId() == memberId) {
			review.delete();
			this.update(review);
		}
		
	}
}
