package com.kwchina.ir.dao.impl;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kwchina.core.common.BasicDaoImpl;
import com.kwchina.ir.dao.CouponRestaurantRelDao;
import com.kwchina.ir.entity.CouponRestaurantRel;

@SuppressWarnings("unchecked")
@Repository
@Transactional
public class CouponRestaurantRelDaoImpl extends
		BasicDaoImpl<CouponRestaurantRel> implements CouponRestaurantRelDao {

}
