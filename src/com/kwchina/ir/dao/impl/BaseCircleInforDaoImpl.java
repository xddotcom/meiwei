package com.kwchina.ir.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kwchina.core.common.BasicDaoImpl;
import com.kwchina.ir.dao.BaseCircleInforDao;
import com.kwchina.ir.entity.BaseCircleInfor;

@SuppressWarnings("unchecked")
@Repository
@Transactional
public class BaseCircleInforDaoImpl extends BasicDaoImpl<BaseCircleInfor> implements
		BaseCircleInforDao {

	@Override
	public List<BaseCircleInfor> getSortedCircles() {
		return getResultByQueryString("FROM BaseCircleInfor ORDER BY isRecommended DESC, rank");
	}

}
