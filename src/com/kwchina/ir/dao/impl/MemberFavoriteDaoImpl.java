package com.kwchina.ir.dao.impl;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kwchina.core.common.BasicDaoImpl;
import com.kwchina.ir.dao.MemberFavoriteDao;
import com.kwchina.ir.dao.MemberInforDao;
import com.kwchina.ir.dao.RestaurantInforDao;
import com.kwchina.ir.entity.MemberFavorite;
import com.kwchina.ir.entity.MemberInfor;
import com.kwchina.ir.entity.RestaurantInfor;
import com.kwchina.ir.vo.MemberFavoriteVo;

@SuppressWarnings("unchecked")
@Repository
@Transactional
public class MemberFavoriteDaoImpl extends BasicDaoImpl<MemberFavorite> implements
		MemberFavoriteDao {

	@Resource
	private MemberInforDao memberInforDao;

	@Resource
	private RestaurantInforDao restaurantInforDao;

	public void saveFavorite(MemberFavoriteVo memberFavoriteVo) {
		if (memberFavoriteVo.getMemberId() != null && memberFavoriteVo.getRestaurantId() != null) {
			MemberFavorite favorite = new MemberFavorite();
			favorite.setFavoriteTime(new Date(System.currentTimeMillis()));

			MemberInfor member = memberInforDao.get(memberFavoriteVo.getMemberId());
			favorite.setMember(member);
			RestaurantInfor restaurant = restaurantInforDao.get(memberFavoriteVo.getRestaurantId());
			favorite.setRestaurant(restaurant);
			
			MemberFavorite mf = getFavoriteByMAR(member.getMemberId() , restaurant.getRestaurantId() );
			
			if(mf ==null){
				save(favorite );
			}
		}
	}
	
	public MemberFavorite getFavoriteByMAR(Integer mid , Integer rid){
		if(mid!=null && rid !=null){
			List<MemberFavorite> list = getResultByQueryString("FROM MemberFavorite F WHERE "
					+ " F.member.memberId = " + mid
					+ " AND F.restaurant.restaurantId = " + rid);
			if (CollectionUtils.isNotEmpty(list)) {
				return list.get(0);
			}
		}
		return null;
	}
	

	public List<MemberFavorite> getFavoritesByMemberId(int memberId, int top) {
		String hql = "FROM MemberFavorite F WHERE F.member.memberId = " + memberId
				+ " ORDER BY F.favoriteTime DESC";
		if (top > 0) {
			hql += " LIMIT " + top;
		}
		return getResultByQueryString(hql);
	}

	@Override
	public void deleteFavorite(Integer memberId, Integer favoriteId) {
		MemberFavorite favorite = this.get(favoriteId);
		if (favorite != null && favorite.getMember().getMemberId() == memberId) {
			favorite.delete();
			this.update(favorite);
		}
		
	}
}
