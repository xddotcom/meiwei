package com.kwchina.ir.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kwchina.core.common.BasicDaoImpl;
import com.kwchina.ir.dao.RecommendRuleDao;
import com.kwchina.ir.entity.RecommendRule;

@SuppressWarnings("unchecked")
@Repository
@Transactional
public class RecommendRuleDaoImpl extends BasicDaoImpl<RecommendRule> implements
		RecommendRuleDao {

	@Override
	public List<RecommendRule> getRecommendedRules() {
		return getResultByQueryString("FROM RecommendRule WHERE isRecommended = 1 "
				+ " ORDER BY ruleOrder");
	}
	
	@Override
	public List<RecommendRule> getRestaurantTags(Integer restaurantId) {
		return getResultByQueryString("SELECT T.rankRule FROM RecommendInfor T WHERE " +
				" T.restaurant.restaurantId = " + restaurantId);
	}

}
