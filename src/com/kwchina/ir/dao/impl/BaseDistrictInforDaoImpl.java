package com.kwchina.ir.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kwchina.core.common.BasicDaoImpl;
import com.kwchina.ir.dao.BaseDistrictInforDao;
import com.kwchina.ir.entity.BaseDistrictInfor;

@SuppressWarnings("unchecked")
@Repository
@Transactional
public class BaseDistrictInforDaoImpl extends BasicDaoImpl<BaseDistrictInfor>
		implements BaseDistrictInforDao {

	@Override
	public List<BaseDistrictInfor> getDistricts() {
		return getResultByQueryString("FROM BaseDistrictInfor");
	}

}
