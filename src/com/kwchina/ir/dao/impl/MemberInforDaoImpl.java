package com.kwchina.ir.dao.impl;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kwchina.core.common.BasicDaoImpl;
import com.kwchina.core.sys.CoreConstant;
import com.kwchina.core.util.DateConverter;
import com.kwchina.ir.dao.MemberContactInforDao;
import com.kwchina.ir.dao.MemberInforDao;
import com.kwchina.ir.dao.MemberPersonalInforDao;
import com.kwchina.ir.dao.MemberReferenceInforDao;
import com.kwchina.ir.entity.MemberContactInfor;
import com.kwchina.ir.entity.MemberCreditInfor;
import com.kwchina.ir.entity.MemberInfor;
import com.kwchina.ir.entity.MemberPersonalInfor;
import com.kwchina.ir.entity.MemberReferenceInfor;
import com.kwchina.ir.util.CalculateImpl;
import com.kwchina.ir.util.StringHelper;
import com.kwchina.ir.util.TextHelper;
import com.kwchina.ir.vo.MemberInforVo;

@SuppressWarnings("unchecked")
@Repository
@Transactional
public class MemberInforDaoImpl extends BasicDaoImpl<MemberInfor> implements MemberInforDao {

	@Resource
	private MemberPersonalInforDao memberPersonalInforDao;

	@Resource
	private MemberReferenceInforDao memberReferenceInforDao;

	@Resource
	private MemberContactInforDao memberContactInforDao;

	@Override
	public List<MemberInfor> validateMemberExist(String loginName) {
		if (loginName != null && loginName.length() > 0) {
			String safeLoginName = loginName;
			String sql = String.format("FROM MemberInfor WHERE"
					+ " (personalInfor.mobile = '%s' OR personalInfor.email = '%s' OR personalInfor.memberNo = '%s')"
					+ " AND isDeleted = 0", safeLoginName, safeLoginName, safeLoginName);
			List<MemberInfor> list = this.getResultByQueryString(sql);
			if (CollectionUtils.isNotEmpty(list)) {
				return list;
			}
		}
		return null;
	}

	@Override
	public List<MemberInfor> validateNameExist(String loginName, Integer memberId) {
		String sql = String.format("FROM MemberInfor WHERE loginName = '%s'", TextHelper.getSafeText(loginName));
		if (memberId != null) {
			sql += String.format(" AND memberId != %d", memberId);
		}
		List<MemberInfor> list = this.getResultByQueryString(sql);
		if (CollectionUtils.isNotEmpty(list)) {
			return list;
		} else {
			return null;
		}
	}

	@Override
	public List<MemberInfor> validateMobileExist(String mobile, Integer memberId) {
		String sql = String
				.format("FROM MemberInfor WHERE personalInfor.mobile = '%s'", TextHelper.getSafeText(mobile));
		if (memberId != null) {
			sql += String.format(" AND memberId != %d", memberId);
		}
		List<MemberInfor> list = this.getResultByQueryString(sql);
		if (CollectionUtils.isNotEmpty(list)) {
			return list;
		} else {
			return null;
		}
	}

	@Override
	public List<MemberInfor> validateEmailExist(String email, Integer memberId) {
		String sql = String.format("FROM MemberInfor WHERE personalInfor.email = '%s'", TextHelper.getSafeText(email));
		if (memberId != null) {
			sql += String.format(" AND memberId != %d", memberId);
		}
		List<MemberInfor> list = this.getResultByQueryString(sql);
		if (CollectionUtils.isNotEmpty(list)) {
			return list;
		} else {
			return null;
		}
	}

	@Override
	public List<MemberInfor> validateInvitation(String code, Integer memberId) {
		String sql = String
				.format("FROM MemberInfor WHERE personalInfor.memberNo = '%s'", TextHelper.getSafeText(code));
		if (memberId != null) {
			sql += String.format(" AND memberId != %d", memberId);
		}
		List<MemberInfor> list = this.getResultByQueryString(sql);
		if (CollectionUtils.isNotEmpty(list)) {
			return list;
		} else {
			return null;
		}
	}

	@Override
	public MemberInfor validateLogin(String loginName, String loginPassword) {
		if (loginName != null && loginPassword != null && loginName.length() > 0 && loginPassword.length() > 0) {
			String safeLoginName = TextHelper.getSafeText(loginName);
			String safeLoginPassword = TextHelper.getSafeText(loginPassword);
			String sql = String.format("FROM MemberInfor WHERE"
					+ " (loginName = '%s' OR personalInfor.mobile = '%s' OR personalInfor.email = '%s')"
					+ " AND loginPassword='%s' AND isDeleted = 0", safeLoginName,safeLoginName, safeLoginName,
					safeLoginPassword );
			List<MemberInfor> list = this.getResultByQueryString(sql);
			if (CollectionUtils.isNotEmpty(list)) {
				return list.get(0);
			}
		}
		return null;
	}

	@Override
	public MemberInfor getInforByLoginName(String loginName) {
		if (StringHelper.isEmpty(loginName)) {
			return null;
		}

		List<MemberInfor> list = getResultByQueryString(String.format("FROM MemberInfor WHERE loginName = '%s'",
				TextHelper.getSafeText(loginName)));
		if (CollectionUtils.isNotEmpty(list)) {
			return list.get(0);
		}
		return null;
	}

	@Override
	public MemberInfor getLoginMember(HttpServletRequest request) {
		HttpSession session = request.getSession();
		String loginName = (String) session.getAttribute(CoreConstant.SESSION_ATTR_LOGIN_NAME);
		String loginPassword = (String) session.getAttribute( CoreConstant.SESSION_ATTR_LOGIN_PASSWORD);
		if (validateLogin(loginName, loginPassword) != null) {
			return getInforByLoginName(loginName);
		}
		return null;
	}

	@Override
	public MemberInfor createMember(MemberInforVo memberInforVo) {

		MemberInfor member = new MemberInfor();
		member.setLoginName( memberInforVo.getLoginName() );
		member.setLoginPassword(DigestUtils.md5Hex(memberInforVo.getLoginPassword()));
		member.setLastTime(new Date(System.currentTimeMillis()));
		member.setRegisterTime(new Date(System.currentTimeMillis()));
		member.setIsDeleted(0);
		this.save(member);

		MemberPersonalInfor personal = new MemberPersonalInfor();
		personal.setMember(member);

		try {
			personal.setAnniversary(memberInforVo.getAnniversary());
		} catch (Exception e) {
			personal.setAnniversary(null);
		}

		try {
			if (StringHelper.isNotEmpty(memberInforVo.getBirthday())) {
				personal.setBirthday(DateConverter.getDateFromString(memberInforVo.getBirthday()));
			}
		} catch (Exception e) {
		}

		if (memberInforVo.getSexe() != null)
			personal.setSexe(memberInforVo.getSexe());
		if (memberInforVo.getNickName() != null)
			personal.setNickName(memberInforVo.getNickName());
		else
			personal.setNickName(memberInforVo.getSexe() == 0 ? "先生" : "小姐");
		if (memberInforVo.getMobile() != null)
			personal.setMobile(memberInforVo.getMobile());

		personal.setEmail(memberInforVo.getEmail());
		personal.setCredit(new CalculateImpl().get().intValue());
		if(memberInforVo.getInvitationCode()!=null && "qiaolian|tcfa".contains( memberInforVo.getInvitationCode() )){
			personal.setCredit( 500 );
		}

		if (memberInforVo.getMemberType() != null)
			member.setMemberType(memberInforVo.getMemberType());

		String memberNo = countMemberNo(member.getMemberId(), memberInforVo.getInvitationCode());
		personal.setMemberNo(memberNo);

		memberPersonalInforDao.save(personal);

		MemberContactInfor contact = new MemberContactInfor();
		contact.setMember(member);
		contact.setName(personal.getNickName());
		contact.setTelphone(personal.getMobile());
		contact.setEmail(personal.getEmail());
		contact.setSexe(personal.getSexe());
		if(StringHelper.isNotEmpty(contact.getTelphone())){
			memberContactInforDao.save(contact);
		}

		save(new MemberCreditInfor(personal.getCredit(), "成功注册", member, new Date(), 1));

		return member;
	}

	private String countMemberNo(Integer memberId, String referer) {
		if (memberId == null)
			return null;
		try {
			MemberReferenceInfor reference = memberReferenceInforDao.get(memberId);
			reference.setRefnumber(referer);
			memberReferenceInforDao.update(reference);
			return reference.getNumber();
		} catch (Exception e) {
			logger.error("Cound not compute memberNo for new member " + memberId + ". Use memberId as memberNo.");
			return memberId.toString();
		}
	}

	@Override
	public MemberInfor ModifyMember(MemberInforVo memberInforVo, Integer memberId) {

		MemberInfor member = this.get(memberId);
		MemberPersonalInfor personal = member.getPersonalInfor();

		if (memberInforVo.getAnniversary() != null)
			personal.setAnniversary(memberInforVo.getAnniversary());
		if (memberInforVo.getBirthday() != null)
			personal.setBirthday(DateConverter.getDateFromString(memberInforVo.getBirthday()));
		if (memberInforVo.getSexe() != null)
			personal.setSexe(memberInforVo.getSexe());
		if (memberInforVo.getNickName() != null)
			personal.setNickName(memberInforVo.getNickName());
//		if (memberInforVo.getMobile() != null)
//			personal.setMobile(memberInforVo.getMobile());
//		if (memberInforVo.getEmail() != null)
//			personal.setEmail(memberInforVo.getEmail());
		if (memberInforVo.getLoginName() != null) {
			member.setLoginName(memberInforVo.getLoginName());
			this.update(member);
		}
		this.update(personal);
		return member;
	}
}
