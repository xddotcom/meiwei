package com.kwchina.ir.dao.impl;

import java.util.Date;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kwchina.core.common.BasicDaoImpl;
import com.kwchina.ir.dao.MemberCreditInforDao;
import com.kwchina.ir.entity.MemberCreditInfor;
import com.kwchina.ir.entity.MemberInfor;
import com.kwchina.ir.entity.MemberPersonalInfor;
import com.kwchina.ir.vo.MemberCreditInforVo;

@SuppressWarnings("unchecked")
@Repository
@Transactional
public class MemberCreditInforDaoImpl extends BasicDaoImpl<MemberCreditInfor> implements MemberCreditInforDao {

	@Override
	public void calculateTotalCredits(MemberPersonalInfor memberPersonal) {
		Integer credit = this.getResultNumBySQLQuery("SELECT SUM(T.amount) FROM member_credit_infor T WHERE T.member="
				+ memberPersonal.getMember().getMemberId());
		memberPersonal.setCredit(credit);
		this.update(memberPersonal);
	}

	@Override
	public void saveEntityByIntegral(MemberInfor member, MemberCreditInforVo memberCreditInforVo) {

		MemberCreditInfor credit = new MemberCreditInfor();
		credit.setAmount(-Math.abs(memberCreditInforVo.getAmount()));
		credit.setCreditType(6);
		credit.setHappendTime(new Date());
		credit.setMember(member);
		credit.setReason(memberCreditInforVo.getReason());
		this.save(credit);

	}

}
