package com.kwchina.ir.dao.impl;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kwchina.core.common.BasicDaoImpl;
import com.kwchina.ir.dao.MemberContactInforDao;
import com.kwchina.ir.entity.MemberContactInfor;
import com.kwchina.ir.entity.MemberInfor;
import com.kwchina.ir.vo.MemberContactInforVo;

@SuppressWarnings("unchecked")
@Repository
@Transactional
public class MemberContactInforDaoImpl extends BasicDaoImpl<MemberContactInfor> implements MemberContactInforDao {

	@Override
	public List<MemberContactInfor> getMemberContacts(Integer memberId) {
		List<MemberContactInfor> list = getResultByQueryString("FROM MemberContactInfor T "
				+ " WHERE T.name is not null AND T.name !='' AND T.member.memberId = " + memberId + " order by T.name desc");
		return list;
	}

	@Override
	public void deleteMemberContacts(Integer memberId, Integer contactId) {
		MemberContactInfor contact = this.get(contactId);
		if (contact != null && contact.getMember().getMemberId() == memberId) {
			contact.delete();
			this.update(contact);
		}
	}

	@Override
	public void saveOrUpdateByMember(MemberInfor member, MemberContactInforVo memberContactInforVo) {
		if (member != null) {
			MemberContactInfor memberContactInfor = null;
			if( memberContactInforVo.getContactId() != null && memberContactInforVo.getContactId() > 0 ){
				memberContactInfor = this.get(memberContactInforVo.getContactId());
			}else{
				memberContactInfor =  new MemberContactInfor();
				memberContactInfor.setMember( member );
			}
			
			if (memberContactInfor.getMember() !=null && 
					memberContactInfor.getMember().getMemberId() == member.getMemberId()) {
				memberContactInfor.setEmail(memberContactInforVo.getEmail());
				memberContactInfor.setName(memberContactInforVo.getName());
				memberContactInfor.setTelphone(memberContactInforVo.getTelphone());
				memberContactInfor.setSexe(memberContactInforVo.getSexe());
				memberContactInfor.setCreateTime( new Date());
				saveOrUpdate(memberContactInfor, memberContactInfor.getContactId());
			}
		}

	}

	@Override
	public void saveEntityByIntegral(MemberInfor member, MemberContactInforVo memberContactInfor) {
		
		MemberContactInfor contact = new MemberContactInfor();
		contact.setCreateTime( new Date());
		contact.setMember(member);
		contact.setName( memberContactInfor.getName()  );
		contact.setSexe( 1);
		contact.setTelphone( memberContactInfor.getTelphone()  );
		this.save( contact );
	}
}
