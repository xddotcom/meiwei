package com.kwchina.ir.dao.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kwchina.core.common.BasicDaoImpl;
import com.kwchina.core.sys.CoreConstant;
import com.kwchina.core.util.DateConverter;
import com.kwchina.ir.dao.MemberContactInforDao;
import com.kwchina.ir.dao.MemberCreditInforDao;
import com.kwchina.ir.dao.MemberInforDao;
import com.kwchina.ir.dao.OrderInforDao;
import com.kwchina.ir.dao.RestaurantInforDao;
import com.kwchina.ir.entity.MemberContactInfor;
import com.kwchina.ir.entity.MemberCreditInfor;
import com.kwchina.ir.entity.MemberInfor;
import com.kwchina.ir.entity.OrderInfor;
import com.kwchina.ir.entity.RestaurantInfor;
import com.kwchina.ir.entity.SortMessagesEntity;
import com.kwchina.ir.front.message.MessageHelper;
import com.kwchina.ir.util.CalculateImpl;
import com.kwchina.ir.util.StringHelper;
import com.kwchina.ir.util.TextHelper;
import com.kwchina.ir.util.TextHelper.Templete;
import com.kwchina.ir.util.mail.MailHelper;
import com.kwchina.ir.vo.OrderInforVo;

@SuppressWarnings("unchecked")
@Repository
@Transactional
public class OrderInforDaoImpl extends BasicDaoImpl<OrderInfor> implements OrderInforDao {

	MailHelper mailHelper = new MailHelper();

	@Resource
	private MemberInforDao memberInforDao;

	@Resource
	private MemberContactInforDao memberContactInforDao;

	@Resource
	private RestaurantInforDao restaurantInforDao;
	
	@Resource
	protected MemberCreditInforDao memberCreditInforDao;

	@Override
	public OrderInfor saveOrder(OrderInforVo orderInforVo, Integer memberId) {

		OrderInfor orderInfor = new OrderInfor();

		if (orderInforVo.getOrderId() != null) {
			orderInfor.setOrderId(orderInforVo.getOrderId());
		}

		orderInfor.setOrderNo(initialOrderNo(orderInforVo));
		orderInfor.setOrderSubmitTime(new Date(System.currentTimeMillis()));

		orderInfor.setStatus(0);
		orderInfor.setConsumeAmount(0.0);
		orderInfor.setCommission(0.0);

		try {
			orderInfor.setOrderDate(DateConverter.getDateFromString(orderInforVo.getOrderDate()));
		} catch (Exception e) {
			orderInfor.setOrderDate(new Date(System.currentTimeMillis()));
		}
		try {
			orderInfor.setOrderTime(DateConverter.getTimeFromString(orderInforVo.getOrderTime()));
		} catch (Exception e) {
			orderInfor.setOrderTime(new Date(System.currentTimeMillis()));
		}

		if (orderInforVo.getPersonNum() != null)
			orderInfor.setPersonNum(orderInforVo.getPersonNum());
		if (orderInforVo.getOther() != null)
			orderInfor.setOther(orderInforVo.getOther());
		if (orderInforVo.getAdminother() != null)
			orderInfor.setAdminother(orderInforVo.getAdminother());
		
		orderInfor.setContactName(StringHelper.getName(orderInforVo.getContactName(), orderInforVo.getContactSexe()));
		orderInfor.setContactTelphone(orderInforVo.getContactTelphone());
		

		if (orderInforVo.getTables() != null)
			orderInfor.setTables(orderInforVo.getTables());

		if (orderInforVo.getInviteFriends() != null)
			orderInfor.setInviteFriends(orderInforVo.getInviteFriends());

		if(orderInforVo.getProductIds() !=null)
			orderInfor.setProductIds( orderInforVo.getProductIds());
		
		MemberInfor member = memberInforDao.get(memberId);
		orderInfor.setMember(member);

		int restaurantId = orderInforVo.getRestaurantId();
		RestaurantInfor restaurant = restaurantInforDao.get(restaurantId);
		orderInfor.setRestaurant(restaurant);

		if (orderInforVo.getOrderId() != null) {
			this.update(orderInfor);
		} else {
			this.save(orderInfor);
		}
		resetOrderNo(orderInfor);
		return orderInfor;
	}

	private String initialOrderNo(OrderInforVo orderInforVo) {
		return orderInforVo.getRestaurantId() + "#" + String.valueOf(System.currentTimeMillis());
	}

	private void resetOrderNo(OrderInfor order) {
		String today = DateConverter.getDateString(new Date());
		List<OrderInfor> list = this.getResultByQueryString("FROM OrderInfor WHERE"
				+ " DATE(orderSubmitTime) = DATE(NOW()) ORDER BY orderSubmitTime ASC");
		int offset = 0;
		if (CollectionUtils.isNotEmpty(list)) {
			OrderInfor todayFirst = list.get(0);
			offset = order.getOrderId() - todayFirst.getOrderId() + 1;
		}
		order.setOrderNo(today.replaceAll("-", "") + String.format("%06d", offset));
		this.update(order);
	}

	@Override
	public OrderInfor getOrderByNo(String searchName) {
		List<OrderInfor> results = getResultByQueryString(String.format("FROM OrderInfor T WHERE T.orderNo = '%s'",
				TextHelper.getSafeText(searchName)));
		if (CollectionUtils.isNotEmpty(results)) {
			return results.get(0);
		}
		return null;
	}

	@Override
	public List<OrderInfor> getOrderInforsByMemberId(int memberId, int top) {
		String hql = "FROM OrderInfor T WHERE T.member.memberId = " + memberId + " ORDER BY T.orderSubmitTime DESC ";
		if (top > 0) {
			hql += " LIMIT " + top;
		}
		return getResultByQueryString(hql);
	}

	public void calculateCreditFromOrder(OrderInfor order) {

		String name = order.getRestaurant().getFullName().getText();
		
		if (order != null && order.getMember() != null) {
			Integer creditGain = 0;
			CalculateImpl cal = new CalculateImpl(order, memberCreditInforDao);
			if (DateConverter.getDiffDay(order.getOrderSubmitTime(), order.getMember().getRegisterTime()) == 0) {
				Integer credit = cal.get(CalculateImpl.RULE_DOUBLE).intValue();
				if (credit > 0) {
					memberCreditInforDao.save(new MemberCreditInfor(credit, name , order.getMember(), new Date(), 3));
					creditGain += credit;
				}
			} else {
				Integer credit = cal.get(CalculateImpl.RULE_NORMAL).intValue();
				if (credit > 0) {
					memberCreditInforDao.save(new MemberCreditInfor(credit,name, order.getMember(), new Date(), 2));
					creditGain += credit;
				}
			}

			if (CollectionUtils.isEmpty(memberCreditInforDao
					.getResultByQueryString(" FROM MemberCreditInfor T where T.creditType=4 AND T.member.memberId="
							+ order.getMember().getMemberId()))) {
				Integer credit = cal.get(CalculateImpl.RULE_AWEEK).intValue();
				if (credit > 0) {
					memberCreditInforDao.save(new MemberCreditInfor(credit, name , order.getMember(), new Date(), 4));
					creditGain += credit;
				}
			}

			if (CollectionUtils.isEmpty(memberCreditInforDao
					.getResultByQueryString(" FROM MemberCreditInfor T where T.creditType=5 AND T.member.memberId="
							+ order.getMember().getMemberId()))) {
				Integer credit = cal.get(CalculateImpl.RULE_AMONTH).intValue();
				if (credit > 0) {
					memberCreditInforDao.save(new MemberCreditInfor(credit, name, order.getMember(), new Date(), 5));
					creditGain += credit;
				}
			}
			order.setCreditGain(creditGain);
			update(order);
		}

	}
	
	/**
	 * @param order
	 * @param messageType
	 *            1:订单初步确认时(商户确认)2:订单消费金额确认时(商户确认)3:发送新订单邮件 4:就餐邀请5:订单取消6:发送邮件到商户邮箱
	 */
	public void sendMessage(String name, String telphone, OrderInfor order, int messageType) {

		try {
			String date = new SimpleDateFormat("MM月dd日(E)").format(order.getOrderDate());
			String time = new SimpleDateFormat("HH:mm").format(order.getOrderTime());
			String fullName = order.getRestaurant().getFullName().getChinese();
			String address = order.getRestaurant().getAddress().getChinese();
			if (messageType == 1) {
				String template = Templete.BEFORE_HAVING_MESSAGE.getTemplete();
				String personNum = order.getPersonNum().toString();
				sendMessage(name, telphone, String.format(template, name, date, time, fullName, address, personNum));
			} else if (messageType == 2) {
				String templete = Templete.AFTER_HAVING_MESSAGE.getTemplete();
				String amount = order.getConsumeAmount().toString();
				String credit = String.valueOf(order.getConsumeAmount().intValue());
				sendMessage(name, telphone, String.format(templete, name, date, time, fullName, amount, credit));
			} else if (messageType == 3 || messageType == 6) {
				String email = null;
				if(messageType == 3){
					email = CoreConstant.SYSTEM_EMAIL;
				}else if(messageType == 6 && order.getRestaurant().getMemberadmin() != null){
					email = order.getRestaurant().getMemberadmin().getPersonalInfor().getEmail();
				}
				if(StringHelper.isEmpty( email ))
					return;
				String loginName = "";
				if (order.getMember() != null) {
					loginName = order.getMember().getLoginName();
				}
				mailHelper
						.setTo(email)
						.setSubject(
								String.format(Templete.NEW_ORDER_TITLE.getTemplete(), fullName, date, time, order
										.getPersonNum().toString(), name, telphone))
						.setText(
								String.format(Templete.NEW_ORDER_CONTENT.getTemplete(), new SimpleDateFormat(
										"yyyy-MM-dd HH:mm:ss").format(order.getOrderSubmitTime()), order.getOrderNo(),
										loginName, fullName, order.getRestaurant().getTelphone(), order.getOther() ,order.getAdminother()));
				if (CoreConstant.JM_NEWORDER_POSITION) {
					mailHelper.send();
				}
			} else if (messageType == 4) {
				String template = Templete.INVITE_MESSAGE.getTemplete();
				sendMessage(name, telphone,
						String.format(template, order.getContactName(), date, time, fullName, address));
			} else if (messageType == 5) {
				String loginName = "";
				if (order.getMember() != null) {
					loginName = order.getMember().getLoginName();
				}
				mailHelper
						.setTo(CoreConstant.SYSTEM_EMAIL)
						.setSubject(
								String.format(Templete.ORDER_CANCEL_TITLE.getTemplete(), fullName, date, time, order
										.getPersonNum().toString(), name, telphone))
						.setText(
								String.format(Templete.ORDER_CANCEL_CONTENT.getTemplete(), new SimpleDateFormat(
										"yyyy-MM-dd HH:mm:ss").format(order.getOrderSubmitTime()), order.getOrderNo(),
										loginName, fullName, order.getRestaurant().getTelphone(), order.getOther()));
				if (CoreConstant.JM_NEWORDER_POSITION) {
					mailHelper.send();
				}
			}
		} catch (Exception e) {
			logger.error("Send message failed." + " Message TYPE is "+messageType, e);
		}
	}

	private void sendMessage(String name, String telphone, String content) {
		if (MessageHelper.sendMessage(telphone, content)) {
			memberContactInforDao.save(new SortMessagesEntity(name, telphone, content, new Date(System
					.currentTimeMillis()), 1));
		} else {
			memberContactInforDao.save(new SortMessagesEntity(name, telphone, content, new Date(System
					.currentTimeMillis()), 0));
		}

	}

	public void sendInviteMessage(OrderInfor order ) {
		try {

			if (order != null && StringHelper.isNotEmpty(order.getInviteFriends())) {
				List<MemberContactInfor> contacts = memberContactInforDao
						.getResultByQueryString("FROM MemberContactInfor T where T.member.memberId ="
								+ order.getMember().getMemberId() + " AND T.contactId in ("
								+ TextHelper.getSafeText(order.getInviteFriends().replace("|", ",")) + ") ");
				Map<String, MemberContactInfor> m = new HashMap<String, MemberContactInfor>();
				if (CollectionUtils.isNotEmpty(contacts)) {
					for (int i = 0; i < contacts.size(); i++) {
						MemberContactInfor contact = contacts.get(i);
						if (contact != null) {
							m.put(contact.getTelphone().trim(), contact);
						}
					}
					if (m != null && m.size() > 0 && m.size() < 12) {
						for (Iterator<String> iterator = m.keySet().iterator(); iterator.hasNext();) {
							MemberContactInfor contact = m.get(iterator.next());
							sendMessage(StringHelper.getName(contact.getName(), contact.getSexe()),
									contact.getTelphone(), order, 4);
						}
					}
				}
			}

		} catch (Exception e) {
			logger.error("send invite message failed.", e);
		}
	}

}
