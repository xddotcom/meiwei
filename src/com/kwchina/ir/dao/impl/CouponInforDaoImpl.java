package com.kwchina.ir.dao.impl;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kwchina.core.common.BasicDaoImpl;
import com.kwchina.ir.dao.CouponInforDao;
import com.kwchina.ir.entity.CouponInfor;

@SuppressWarnings("unchecked")
@Repository
@Transactional
public class CouponInforDaoImpl extends BasicDaoImpl<CouponInfor> implements
		CouponInforDao {

}
