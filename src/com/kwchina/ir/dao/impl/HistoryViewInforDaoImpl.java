package com.kwchina.ir.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kwchina.core.common.BasicDaoImpl;
import com.kwchina.ir.dao.HistoryViewInforDao;
import com.kwchina.ir.entity.HistoryViewInfor;

@SuppressWarnings("unchecked")
@Repository
@Transactional
public class HistoryViewInforDaoImpl extends BasicDaoImpl<HistoryViewInfor> implements
		HistoryViewInforDao {

	@Override
	public List<HistoryViewInfor> getRecentViews(int memberId) {
		return getResultByQueryString("FROM HistoryViewInfor T WHERE T.member.memberId = "
				+ memberId, true, 0, 6);
	}
}
