package com.kwchina.ir.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kwchina.core.common.BasicDaoImpl;
import com.kwchina.ir.dao.RestaurantPicInforDao;
import com.kwchina.ir.entity.RestaurantPicInfor;

@SuppressWarnings("unchecked")
@Repository
@Transactional
public class RestaurantPicInforDaoImpl extends BasicDaoImpl<RestaurantPicInfor> implements
		RestaurantPicInforDao {

	@Override
	public List<RestaurantPicInfor> getRestaurantPics(Integer restaurantId) {
		return getResultByQueryString("FROM RestaurantPicInfor T WHERE T.restaurant.restaurantId="
				+ restaurantId);
	}

}
