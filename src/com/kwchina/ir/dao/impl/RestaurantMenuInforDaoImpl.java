package com.kwchina.ir.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kwchina.core.common.BasicDaoImpl;
import com.kwchina.ir.dao.RestaurantMenuInforDao;
import com.kwchina.ir.entity.RestaurantMenuInfor;

@SuppressWarnings("unchecked")
@Repository
@Transactional
public class RestaurantMenuInforDaoImpl extends BasicDaoImpl<RestaurantMenuInfor> implements
		RestaurantMenuInforDao {

	@Override
	public List<RestaurantMenuInfor> getRestaurantMealMenus(int restaurantId) {
		return getResultByQueryString("FROM RestaurantMenuInfor T "
				+ " WHERE T.menuCategory = 0 AND T.restaurant.restaurantId = " + restaurantId);
	}

	@Override
	public List<RestaurantMenuInfor> getRecommendedMealMenus(int restaurantId) {
		return getResultByQueryString("FROM RestaurantMenuInfor T "
				+ "WHERE T.menuCategory = 0 AND T.isRecommended = 1 AND T.restaurant.restaurantId="
				+ restaurantId);
	}
}
