package com.kwchina.ir.dao.impl;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kwchina.core.common.BasicDaoImpl;
import com.kwchina.ir.dao.BaseContactInforDao;
import com.kwchina.ir.entity.BaseContactInfor;

@SuppressWarnings("unchecked")
@Repository
@Transactional
public class BaseContactInforDaoImpl extends BasicDaoImpl<BaseContactInfor>
		implements BaseContactInforDao {

}
