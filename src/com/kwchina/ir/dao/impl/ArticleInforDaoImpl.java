package com.kwchina.ir.dao.impl;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kwchina.core.common.BasicDaoImpl;
import com.kwchina.ir.dao.ArticleInforDao;
import com.kwchina.ir.entity.ArticleInfor;

@SuppressWarnings("unchecked")
@Repository
@Transactional
public class ArticleInforDaoImpl extends BasicDaoImpl<ArticleInfor> implements
		ArticleInforDao {

}
