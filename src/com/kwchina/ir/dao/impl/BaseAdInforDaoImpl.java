package com.kwchina.ir.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kwchina.core.common.BasicDaoImpl;
import com.kwchina.ir.dao.BaseAdInforDao;
import com.kwchina.ir.entity.BaseAdInfor;

@SuppressWarnings("unchecked")
@Repository
@Transactional
public class BaseAdInforDaoImpl extends BasicDaoImpl<BaseAdInfor> implements
		BaseAdInforDao {

	@Override
	public List<BaseAdInfor> getAds() {
		return this.getResultByQueryString("from BaseAdInfor where 1=1", true, 0, 10);
	}

}
