package com.kwchina.ir.dao.impl;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kwchina.core.common.BasicDaoImpl;
import com.kwchina.ir.dao.TextInforDao;
import com.kwchina.ir.entity.TextInfor;

@SuppressWarnings("unchecked")
@Repository
@Transactional
public class TextInforDaoImpl extends BasicDaoImpl<TextInfor> implements
		TextInforDao {

}
