package com.kwchina.ir.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kwchina.core.common.BasicDaoImpl;
import com.kwchina.ir.dao.ProductInforDao;
import com.kwchina.ir.entity.ProductInfor;

@SuppressWarnings("unchecked")
@Repository
@Transactional
public class ProductInforDaoImpl extends BasicDaoImpl<ProductInfor> implements ProductInforDao {

	@Override
	public List<ProductInfor> getProductsByType(int productType) {
		String sql = "FROM ProductInfor WHERE 1=1 AND productType=" + productType;
		return getResultByQueryString(sql);
	}

}
