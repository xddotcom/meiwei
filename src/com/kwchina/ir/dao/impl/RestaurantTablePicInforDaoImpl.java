package com.kwchina.ir.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kwchina.core.common.BasicDaoImpl;
import com.kwchina.ir.dao.RestaurantTablePicInforDao;
import com.kwchina.ir.entity.RestaurantTablePicInfor;

@SuppressWarnings("unchecked")
@Repository
@Transactional
public class RestaurantTablePicInforDaoImpl extends BasicDaoImpl<RestaurantTablePicInfor> implements
		RestaurantTablePicInforDao {

	@Override
	public List<RestaurantTablePicInfor> getRestaurantTablePics(Integer restaurantId) {
		return getResultByQueryString("FROM RestaurantTablePicInfor T WHERE T.restaurant.restaurantId="
				+ restaurantId);
	}

}
