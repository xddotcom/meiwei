package com.kwchina.ir.dao.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kwchina.core.common.BasicDaoImpl;
import com.kwchina.ir.dao.RestaurantTimeInforDao;
import com.kwchina.ir.entity.RestaurantTimeInfor;

@SuppressWarnings({ "unused", "unchecked" })
@Repository
@Transactional
public class RestaurantTimeInforDaoImpl extends
		BasicDaoImpl<RestaurantTimeInfor> implements RestaurantTimeInforDao {

	@Override
	public List<RestaurantTimeInfor> getRestaurantHours(Integer restaurantId) {
		return getResultByQueryString("FROM RestaurantTimeInfor T WHERE T.restaurant.restaurantId = "
				+ restaurantId + "ORDER BY startTime ASC");
	}
}
