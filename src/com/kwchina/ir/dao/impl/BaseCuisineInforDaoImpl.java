package com.kwchina.ir.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kwchina.core.common.BasicDaoImpl;
import com.kwchina.ir.dao.BaseCuisineInforDao;
import com.kwchina.ir.entity.BaseCuisineInfor;

@SuppressWarnings("unchecked")
@Repository
@Transactional
public class BaseCuisineInforDaoImpl extends BasicDaoImpl<BaseCuisineInfor>
		implements BaseCuisineInforDao {

	@Override
	public List<BaseCuisineInfor> getSortedCuisines() {
		return getResultByQueryString("FROM BaseCuisineInfor "
				+ "WHERE cuisineType = 1 ORDER BY isRecommended DESC, rank");
	}

}
