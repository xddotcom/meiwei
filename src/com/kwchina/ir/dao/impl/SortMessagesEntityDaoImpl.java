package com.kwchina.ir.dao.impl;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kwchina.core.common.BasicDaoImpl;
import com.kwchina.ir.dao.SortMessagesEntityDao;
import com.kwchina.ir.entity.SortMessagesEntity;

@SuppressWarnings("unchecked")
@Repository
@Transactional
public class SortMessagesEntityDaoImpl extends BasicDaoImpl<SortMessagesEntity>
		implements SortMessagesEntityDao {

}
