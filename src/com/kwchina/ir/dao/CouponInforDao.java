package com.kwchina.ir.dao;

import com.kwchina.core.common.BasicDao;
import com.kwchina.ir.entity.CouponInfor;

public interface CouponInforDao extends BasicDao<CouponInfor> {

}
