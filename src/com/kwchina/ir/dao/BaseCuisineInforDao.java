package com.kwchina.ir.dao;

import java.util.List;

import com.kwchina.core.common.BasicDao;
import com.kwchina.ir.entity.BaseCuisineInfor;

public interface BaseCuisineInforDao extends BasicDao<BaseCuisineInfor> {

	List<BaseCuisineInfor> getSortedCuisines();

}
