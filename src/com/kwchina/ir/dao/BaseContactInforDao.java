package com.kwchina.ir.dao;

import com.kwchina.core.common.BasicDao;
import com.kwchina.ir.entity.BaseContactInfor;

public interface BaseContactInforDao extends BasicDao<BaseContactInfor> {

}
