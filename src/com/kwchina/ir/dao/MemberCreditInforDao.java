package com.kwchina.ir.dao;

import com.kwchina.core.common.BasicDao;
import com.kwchina.ir.entity.MemberCreditInfor;
import com.kwchina.ir.entity.MemberInfor;
import com.kwchina.ir.entity.MemberPersonalInfor;
import com.kwchina.ir.vo.MemberCreditInforVo;

public interface MemberCreditInforDao  extends BasicDao<MemberCreditInfor> {

	void calculateTotalCredits(MemberPersonalInfor memberPersonal);

	void saveEntityByIntegral(MemberInfor member, MemberCreditInforVo memberCreditInforVo);

}
