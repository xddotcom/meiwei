package com.kwchina.ir.dao;

import java.util.List;

import com.kwchina.core.common.BasicDao;
import com.kwchina.ir.entity.MemberReviewInfor;
import com.kwchina.ir.vo.MemberReviewInforVo;

public interface MemberReviewInforDao extends BasicDao<MemberReviewInfor> {

	public void saveReview(MemberReviewInforVo reviewInforVo);

	public List<MemberReviewInfor> getReviewInforsByMemberId(int memberId, int top);

	public List<MemberReviewInfor> getReviewInforsByRestaurantId(int restaurantId, int top);

	public void deleteReview(Integer memberId, Integer reviewId);
}
