package com.kwchina.ir.dao;

import java.util.List;

import com.kwchina.core.common.BasicDao;
import com.kwchina.ir.entity.MemberInfor;
import com.kwchina.ir.entity.RestaurantInfor;
import com.kwchina.ir.vo.RestaurantInforVo;
import com.kwchina.ir.vo.SearchInforVo;

public interface RestaurantInforDao extends BasicDao<RestaurantInfor> {

	public RestaurantInfor getRestaurantByNo(String searchName);

	public List<RestaurantInfor> searchRestaurants(SearchInforVo searchInforVo);

	public boolean isRestaurantMember(MemberInfor member, Integer restaurantId);

	public void updateRestaurant(RestaurantInfor restaurantInfor, RestaurantInforVo restaurantInforVo);

}
