package com.kwchina.ir.dao;

import java.util.List;

import com.kwchina.core.common.BasicDao;
import com.kwchina.ir.entity.RestaurantMenuInfor;

public interface RestaurantMenuInforDao extends BasicDao<RestaurantMenuInfor> {

	List<RestaurantMenuInfor> getRestaurantMealMenus(int restaurantId);
	
	List<RestaurantMenuInfor> getRecommendedMealMenus(int restaurantId);

}
