package com.kwchina.ir.dao;

import java.util.List;

import com.kwchina.core.common.BasicDao;
import com.kwchina.ir.entity.MemberFavorite;
import com.kwchina.ir.vo.MemberFavoriteVo;

public interface MemberFavoriteDao extends BasicDao<MemberFavorite> {

	public void saveFavorite(MemberFavoriteVo memberFavoriteVo);

	public List<MemberFavorite> getFavoritesByMemberId(int memberId, int top);

	public MemberFavorite getFavoriteByMAR(Integer mid , Integer rid);

	public void deleteFavorite(Integer memberId, Integer favoriteId);
	
}
