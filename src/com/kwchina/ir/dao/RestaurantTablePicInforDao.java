package com.kwchina.ir.dao;

import java.util.List;

import com.kwchina.core.common.BasicDao;
import com.kwchina.ir.entity.RestaurantTablePicInfor;

public interface RestaurantTablePicInforDao extends BasicDao<RestaurantTablePicInfor> {

	List<RestaurantTablePicInfor> getRestaurantTablePics(Integer restaurantId);

}
