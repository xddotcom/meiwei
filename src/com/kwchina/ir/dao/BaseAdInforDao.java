package com.kwchina.ir.dao;

import java.util.List;

import com.kwchina.core.common.BasicDao;
import com.kwchina.ir.entity.BaseAdInfor;

public interface BaseAdInforDao extends BasicDao<BaseAdInfor> {

	List<BaseAdInfor> getAds();

}
