package com.kwchina.ir.dao;

import com.kwchina.core.common.BasicDao;
import com.kwchina.ir.entity.TextInfor;

public interface TextInforDao extends BasicDao<TextInfor> {

}
