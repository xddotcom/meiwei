package com.kwchina.ir.dao;

import com.kwchina.core.common.BasicDao;
import com.kwchina.ir.entity.SortMessagesEntity;

public interface SortMessagesEntityDao extends BasicDao<SortMessagesEntity> {

}
