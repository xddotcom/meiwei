package com.kwchina.ir.dao;

import com.kwchina.core.common.BasicDao;
import com.kwchina.ir.entity.CouponMemberRel;

public interface CouponMemberRelDao extends BasicDao<CouponMemberRel> {

}
