package com.kwchina.ir.dao;

import java.util.List;

import com.kwchina.core.common.BasicDao;
import com.kwchina.ir.entity.RestaurantPicInfor;

public interface RestaurantPicInforDao extends BasicDao<RestaurantPicInfor> {

	List<RestaurantPicInfor> getRestaurantPics(Integer restaurantId);

}
