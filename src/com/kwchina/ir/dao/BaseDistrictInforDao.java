package com.kwchina.ir.dao;

import java.util.List;

import com.kwchina.core.common.BasicDao;
import com.kwchina.ir.entity.BaseDistrictInfor;

public interface BaseDistrictInforDao extends BasicDao<BaseDistrictInfor> {

	List<BaseDistrictInfor> getDistricts();

}
