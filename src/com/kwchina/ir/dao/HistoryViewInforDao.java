package com.kwchina.ir.dao;

import java.util.List;

import com.kwchina.core.common.BasicDao;
import com.kwchina.ir.entity.HistoryViewInfor;

public interface HistoryViewInforDao extends BasicDao<HistoryViewInfor> {

	List<HistoryViewInfor> getRecentViews(int memberId);

}
