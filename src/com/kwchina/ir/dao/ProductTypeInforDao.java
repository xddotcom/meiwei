package com.kwchina.ir.dao;

import java.util.List;

import com.kwchina.core.common.BasicDao;
import com.kwchina.ir.entity.ProductTypeInfor;

public interface ProductTypeInforDao  extends BasicDao<ProductTypeInfor> {

	List<ProductTypeInfor> getValueAddedProducts();

}
