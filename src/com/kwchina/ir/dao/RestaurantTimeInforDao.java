package com.kwchina.ir.dao;

import java.util.List;

import com.kwchina.core.common.BasicDao;
import com.kwchina.ir.entity.RestaurantTimeInfor;

public interface RestaurantTimeInforDao extends BasicDao<RestaurantTimeInfor> {

	List<RestaurantTimeInfor> getRestaurantHours(Integer restaurantId);

}
