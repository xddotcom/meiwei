package com.kwchina.ir.dao;

import java.util.List;

import com.kwchina.core.common.BasicDao;
import com.kwchina.ir.entity.MemberContactInfor;
import com.kwchina.ir.entity.MemberInfor;
import com.kwchina.ir.vo.MemberContactInforVo;

public interface MemberContactInforDao extends BasicDao<MemberContactInfor> {

	List<MemberContactInfor> getMemberContacts(Integer memberId);

	void deleteMemberContacts(Integer memberId, Integer contactId);

	void saveOrUpdateByMember(MemberInfor memberInfor, MemberContactInforVo memberContactInforVo);

	void saveEntityByIntegral(MemberInfor member, MemberContactInforVo memberContactInfor);

}
