package com.kwchina.ir.dao;

import java.util.List;

import com.kwchina.core.common.BasicDao;
import com.kwchina.ir.entity.RecommendRule;

public interface RecommendRuleDao extends BasicDao<RecommendRule> {

	List<RecommendRule> getRecommendedRules();

	List<RecommendRule> getRestaurantTags(Integer restaurantId);

}
