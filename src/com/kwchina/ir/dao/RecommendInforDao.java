package com.kwchina.ir.dao;

import java.util.List;

import com.kwchina.core.common.BasicDao;
import com.kwchina.ir.entity.RecommendInfor;

public interface RecommendInforDao extends BasicDao<RecommendInfor> {

	List<RecommendInfor> getRecommendedRestaurants(int ruleId);

}
