package com.kwchina.ir.service.security.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by IntelliJ IDEA.
 * User: Administrator
 * Date: 2009-8-20
 * Time: 17:52:07
 * To change this template use File | Settings | File Templates.
 */
public class AjaxProcessingFilter implements Filter {

    public void init(FilterConfig filterConfig) throws ServletException {
    }

    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        if (!(servletRequest instanceof HttpServletRequest)) {
            throw new ServletException("Can only process HttpServletRequest");
        }

        if (!(servletResponse instanceof HttpServletResponse)) {
            throw new ServletException("Can only process HttpServletResponse");
        }

        if(isAjaxRequest(servletRequest)){
            ((HttpServletResponse)servletResponse).sendError(404);
        }else{
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }

    public void destroy() {
    }

    private boolean isAjaxRequest(final ServletRequest request) {
        return null != request.getParameterMap().get("AJAXREQUEST");
    }
}
