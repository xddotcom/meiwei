package com.kwchina.core.sys;

import java.util.HashMap;
import java.util.Map;

public class CoreConstant {

	public static final String SPLIT_SIGN = ";";

	public static final int Authenticate_Type_USER = 0;
	public static final int Authenticate_Type_SYSTEMUSERINFOR = 1;

	public static String Context_Name = "";
	public static String Context_Real_Path = "";
	public static final String FILE_BASE_PATH = "D:\\";
	public static final String DATA_FILE = "E:\\Business\\CompanyBusiness\\数据资料\\";

	public static final String SYSTEM_EMAIL = "neworder@clubmeiwei.com";
	
	public static String ROLE_ADMIN = "ROLE_ADMIN";
	public static String ROLE_CUSTOMER_SERVICE = "ROLE_CUSTOMER_SERVICE";
	public static String ROLE_FINANCES = "ROLE_FINANCES";
	public static String ROLE_DATAMANAGER = "ROLE_DATAMANAGER";

	public static String SESSION_ATTR_LOGIN_NAME="_FRONT_LOGIN_MEMBER_NAME";
	public static String SESSION_ATTR_LOGIN_PASSWORD="_FORN_LOGIN_MEMBER_PASSWORD";
	public static String SESSION_ATTR_VERIFYCODE = "VERIFYCODE";
	
	public static Map<String, String> cacheMap = new HashMap<String, String>();

	public static boolean SORT_MESSAGE_POSITION= false;
	public static boolean JAVA_MAIL_POSITION= false;
	public static boolean JM_PASSWORD_POSITION= true;
	public static boolean JM_NEWORDER_POSITION= true;
	
	public static int CUISINETYPE_COOKING = 1;
	public static int CUISINETYPE_DISHES = 2;

	public static int MEMBERTYPE_PERSONAL = 0;
	public static int MEMBERTYPE_STORE = 1;
	public static int MEMBERTYPE_ADMIN = 2;
	
	public static int SEX_MALE = 0;
	public static int SEX_FEMELA = 1;

	public static String YES = "y";
	public static String NO = "n";

	public static int FIELD_FALSE = 0;
	public static int FIELD_TRUE = 1;

	public static int INVALID = 0;
	public static int VALID = 1;

	public static int ADTYPE_PICTUER = 0;
	public static int ADTYPE_FLASH = 1;
	public static int ADTYPE_TEXT = 2;

	public static int MENUCATEGORY_MEAL = 0;
	public static int MENUCATEGORY_WINE = 1;

	public static int TARGET_FORMER = 0;
	public static int TARGET_BLANK = 1;

	public static int COMMENT_UNCHECKED = 2;
	public static int COMMENT_CHECKED = 1;
	public static int COMMENT_FAIL = 0;

	public static int LANGUAGETYPE_CHINESE = 1;
	public static int LANGUAGETYPE_ENGLISH = 2;

	public static int ORDER_STATUS_NEW = 0;
	public static int ORDER_STATUS_RESTAU_CONFIRM = 10;
	public static int ORDER_STATUS_MEMBER_PRESENT = 20;
	public static int ORDER_STATUS_RESTAU_PAYABLE = 30;
	public static int ORDER_STATUS_RESTAU_PAID = 40;
	public static int ORDER_STATUS_RESTAU_UNPAID = 41;
	public static int ORDER_STATUS_COMPLETE = 50;
	public static int ORDER_STATUS_MEMBER_CANCEL = 99;
	public static int ORDER_STATUS_RESTAU_CANCEL = 98;
	public static int ORDER_STATUS_MEIWEI_CANCEL = 97;
	public static int ORDER_STATUS_MEMBER_ABSENT = 96;//old 21

	public static int COMMISSION_UNPAID = 0;
	public static int COMMISSION_PAID = 1;
	public static int COMMISSION_PARTIALPAID = 2;

	public static int CONSUME_TYPE_COUPON = 1;
	public static int CONSUME_TYPE_GIFT = 2;
	public static int CONSUME_TYPE_SERVICE = 3;

	public static int ACQUIRE_TYPE_CONSUME = 1;
	public static int ACQUIRE_TYPE_COMMENT = 2;
	public static int ACQUIRE_TYPE_ACTIVITY = 3;

	public static int CONTACT_TYPE_BUSINESS = 0; // 商家
	public static int CONTACT_TYPE_MEMBER = 1; // 会员
	public static int CONTACT_TYPE_MEDIUM = 2; // 媒体
	public static int CONTACT_TYPE_INVESTOR = 3; // 投资人

}
