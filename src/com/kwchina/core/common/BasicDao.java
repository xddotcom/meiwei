package com.kwchina.core.common;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.hibernate.Session;
import org.springframework.dao.DataAccessException;

import com.kwchina.core.util.PageList;
import com.kwchina.core.util.Pages;
import com.kwchina.core.util.PropertyFilter;

public interface BasicDao<T> {
	
	public void clean(Object o);

	public void excuteBySQL(String sql);

	public PageForMesa<T> find(PageForMesa<T> page, List<PropertyFilter> filters, Map<String, String> alias);
	
	public T get(Serializable id) throws DataAccessException;

	public List<T> getAll();
	
	public T getInforByColumn(String columnName, Object value);
	
	public T getReference(int id);
	
	public List<T> getResultByQueryString(String queryString);

	public List<T> getResultByQueryString(String queryString, boolean isPageAble, int firstResult, int maxResults);

	public PageList getResultByQueryString(String querySQL, String countSQL, boolean isPageAble, Pages pages);

	public List<T> getResultBySQLQuery(String sql, boolean isPageAble,int firstResult, int maxResults);

	public int getResultNumByQueryString(String queryString);
	
	public int getResultNumBySQLQuery(String sql);
	
	public Session getSession();
	
	public Object merge(Object o);

	public void remove(Integer id);	
	
	public void remove(Object o);

	public void save(Object o);
	
	public void saveOrUpdate(Object o,Serializable id);

	public void update(Object o);
}
