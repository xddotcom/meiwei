package com.kwchina.core.util;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;

// TODO Avoid hard coding. Use @Responsebody instead.

public class HttpHelper {

	public static final HttpHelper DEFAULT = new HttpHelper();
	private String content = "<script language=\"javascript\">ReturnToLogin();</script>";

	public HttpHelper start() {
		this.content = "<script language=\"javascript\">";
		return this;
	}

	public HttpHelper append(String s) {
		this.content += s;
		return this;
	}

	public HttpHelper end() {
		this.content += "</script>";
		return this;
	}

	public void output(HttpServletResponse response, String result) {
		try {
			response.setContentType("text/html;charset=utf-8");
			response.getWriter().print(result);
			response.flushBuffer();
		} catch (IOException e) {
		}
	}

	public void output(HttpServletResponse response) {
		try {
			response.setCharacterEncoding("gbk");
			PrintWriter out = response.getWriter();
			out.print("<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>");
			out.print(getContent());
		} catch (IOException e) {
		}
	}

	public void json(HttpServletResponse response , String content){
		response.setContentType("application/json;charset=UTF-8");
		try {
			response.getWriter().print( content );
		} catch (IOException e) {
 		}
	}
	
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

}
