package com.kwchina.core.util;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * Title: HR Management System
 * </p>
 * <p>
 * Description: SPCWT-HR-SYSTEM
 * </p>
 * <p>
 * Copyright: Copyright (c) 2006
 * </p>
 * <p>
 * Company: kwchina.com
 * </p>
 * 
 * @author zhou lb
 * @version 1.0 Date: 2006-1-20 Time: 11:06:05 Class Name: Pages
 */

public class Pages {

	HttpServletRequest request = null;
	String filename = ""; // 文件数
	int page = 1; // 页号
	int totals = -1; // 记录总数
	int perpagenum = 20; // 每页显示记录数
	int style = 0; // 分页字串样式
	int allpage = 1; // 总页数
	int cpage = 1; // 当前页数
	int spage = 1; // 起始记录数
	String listPageBreak = "";
	String[] pagesign = null;

	String[] sqls = null;
	
	public int getCurrPage() {
		return this.page;
	}

	public int getTotalPage() {
		return this.allpage;
	}

	public Pages() {
	}

	public Pages(HttpServletRequest request) {
		this.request = request;
		this.pagesign = SysGeneralMethod.getPagesign(request);
	}

	public Pages(HttpServletRequest request, int page, int totals,
			int perpagenum, int style) {
		this.request = request;
		this.page = page;
		this.totals = totals;
		this.perpagenum = perpagenum;
		this.style = style;
		this.pagesign = SysGeneralMethod.getPagesign(request);
	}

	public Pages(HttpServletRequest request, int page, int totals,
			int perpagenum) {
		this.request = request;
		this.page = page;
		this.totals = totals;
		this.perpagenum = perpagenum;
		this.pagesign = SysGeneralMethod.getPagesign(request);
	}

	public Pages(HttpServletRequest request, int page, int perpagenum) {
		this.request = request;
		this.page = page;
		this.perpagenum = perpagenum;
		this.pagesign = SysGeneralMethod.getPagesign(request);
	}

	public String getFileName() {
		return this.filename;
	}

	public void setFileName(String aFileName) {
		this.filename = aFileName;
	}

	public int getPage() {
		return this.page;
	}

	public void setPage(int aPage) {
		this.page = aPage;
	}

	public int getTotals() {
		return this.totals;
	}

	public void setTotals(int aTotals) {
		this.totals = aTotals;
	}

	public int getPerPageNum() {
		return this.perpagenum;
	}

	public void setPerPageNum(int aperpagenum) {
		this.perpagenum = aperpagenum;
	}

	public int getStyle() {
		return this.style;
	}

	public void setStyle(int aStyle) {
		this.style = aStyle;
	}

	public void setPagesign(String[] apagesign) {
		this.pagesign = apagesign;
	}

	public int getSpage() {
		return this.spage;
	}

	public void doPageBreak() {
		this.allpage = (int) Math.ceil((this.totals + this.perpagenum - 1)
				/ this.perpagenum);
		int intPage = this.page;
		if (intPage > this.allpage) { // pages == 0
			this.cpage = 1;
		} else {
			this.cpage = intPage;
		}
		this.spage = (this.cpage - 1) * this.perpagenum;
		getPageBreakStr();
	}

	public String getListPageBreak() {
		return this.listPageBreak;
	}

	private void getPageBreakStr() {
 
	}
	
	public String[] getSqls() {
		return sqls;
	}

	public void setSqls(String[] sqls) {
		this.sqls = sqls;
	}

	public HttpServletRequest getRequest() {
		return request;
	}

	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}

}
