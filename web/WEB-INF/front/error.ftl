<#import "layouts/common_standard.ftl" as standard>
 

<#escape x as x?html>
<@standard.html >

<@standard.page_header "错误页面">
	<@standard.css>
		<meta http-equiv="refresh" content="3; URL=<@spring.url '/'/>">
	</@standard.css>
	<@standard.javascript>
	</@standard.javascript>
</@standard.page_header>

<body class="notfound" style="background-color: #fff;">
<div  class="container-fluid" style="text-align: center;margin: 100px auto;">
 		<img src="<@spring.url '/'/>front/images/404/logo.png" alt="logo" />
		<div class="notfound-chinese-text" style="margin: 30px auto;" >
			<p>对不起，您所访问的页面被删除或不存在，三秒后返回首页</p>
			<p>点击<a href="javascript:window.history.go(-1);" style="color:#77513d;">这里</a>返回上一个页面。</p>
		</div>
		<!--
		<div class="notfound-english-text">
			<p>SORRY!</p>
			<p>THERE WAS NO CLUBMEIWEI.COM WEB PAGE MATCHING YOUR REQUEST. PLEASE CLICK HERE TO CONTINUE YOUR SEARCH ON CLUBMEIWEI.COM</p>
		</div>
		-->
 </div>
</body>
 
</@standard.html>
</#escape>