<#macro encodeText2Url textInfor>
        ${encodeUrl(textInfor.text?html)}
</#macro>

<#macro text textInfor maxlength = 9999>
        <#if (LANGUAGE!"1")="1">
                <#assign output=textInfor.chinese?html />
        <#else>
                <#assign output=textInfor.english?html />
        </#if>
        <#if (output?length <= maxlength)>
                ${output}
        <#else>
                ${output[0..maxlength-1]}
        </#if>
</#macro>

<#macro imageSize path>${imageSize(path)}</#macro>
<#macro imageUri uri="/"><@spring.url "/upload" />${uri}</#macro>
