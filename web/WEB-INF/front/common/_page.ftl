<#macro page_old>
	<div style="width: 250px; margin: 30px auto;">
		<div class="gigantic pagination">
			<a href="pageNo=1" class="first" data-action="first">&laquo;</a>
			<a href="#" class="previous" data-action="previous">&lsaquo;</a>
			<input type="text" readonly="readonly" data-max-page="${_Page.totalPage!"40"}" data-current-page="${_Page.page}" />
			<a href="#" class="next" data-action="next">&rsaquo;</a>
			<a href="#" class="last" data-action="last">&raquo;</a>
		</div>
	</div>
</#macro>

<#macro page>
<div class="pagination pagination-centered">
	<ul id="page-ul">
        <script type="text/template" id="template-page">
		<% if(page.page > 1){ %>
			<li><a href="?pageNo=<%=(page.page-1) %>">&laquo;</a></li>
		<%}else{%>
			<li><a href="#">&laquo;</a></li>
		<%}%>
		<% if(page.page > 2){ %><li><a href="?pageNo=<%=(page.page-2) %>"><%=(page.page-2) %></a></li><%}%>
		<% if(page.page > 1){ %><li><a href="?pageNo=<%=(page.page-1) %>"><%=(page.page-1) %></a></li><%}%>
		<li class="active"><a href="#"><%=page.page %></a></li>
		<% if( (page.page + 1) <= page.totalPage ){ %><li><a href="?pageNo=<%=(page.page+1) %>"><%=(page.page+1) %></a></li><%}%>
		<% if( (page.page + 2) <= page.totalPage ){ %><li><a href="?pageNo=<%=(page.page+2) %>"><%=(page.page+2) %></a></li><%}%>
		<% if( (page.page + 1) <= page.totalPage ){ %>
			<li><a href="?pageNo=<%=(page.page+1) %>">&raquo;</a></li>
		<%}else{%>
			<li><a href="#">&raquo;</a></li>
		<%}%>
		</script>
	</ul>
</div>
</#macro>
