<#import "statics.ftl" as statics>

<@statics.content "企业服务">

<#if (LANGUAGE!"1")="1">

<h1 class="text-center">企业服务</h1>
<p>美位网作为一个全方位，多层次的餐饮服务平台，拥有资深的服务经验，致力于为企业用户提供专业，有品质的一站式餐饮服务。美位网立足于上海，即将实现对中国一线城市乃至亚洲，全球重要商务城市的全面开拓，为企业用户差旅出行的餐饮安排提供优质和信心的保证。</p>
<p class="lead">选择美位的理由: </p>
<ol>
	<li>
		<p>百余家高端餐饮合作商户</p>
		<p>百余家合作商户为您的企业订餐提供丰富的选择；精选的高端合作商户为企业商务宴请，企业年会活动提供品质保障；灵活的服务形式为满足企业多样式的需求提供了可能。</p>
	</li>
	<li>
		<p>贴心的私人管家服务</p>
		<p>考虑到企业用户大型会务及年会活动等的需求，美位为企业会员设置“私人管家”服务，为您记录会议，推荐场地，并协助您完成会务组织与活动策划。</p>
	</li>
</ol>

<#else>

<h1 class="text-center">Corporate Customer Service</h1>
<p>Meiwei focuses on creating a full-range and multi-level fine dining service platform.
	Equipped with abundant experience and comprehensive knowledge of catering industry,
	Meiwei is committed to provide high quality one-stop professional service to enterprise members.
	Meiwei is based in Shanghai, and is going to fully expand in first-tier cities in China,
	and other financial centers in Asia and even worldwide, in order to provide our members with
	better fine dining arrangement.</p>
<p class="lead">Meiwei's Advantage:</p>
<ol>
	<li>
		<p>Hundreds of High-End Partner Restaurants</p>
		<p>Meiwei offers a great selection of fine dining restaurants to meet your different requirements.
		These strictly selected partners will be able to provide high level business banquets and
		corporate annual conferences. Meiwei also provides other concierge services to satisfy your
		multiple needs.</p>
	</li>
	<li>
		<p>Personalized Butler Service</p>
		<p>Meiwei always takes into account members' needs of large-scale business and annual conference
		activities. We provide enterprise members with particular butler services, including venue
		recommendation, event planning. We have all it takes to help our corporate members to start
		from step one to a successfully held event.</p>
	</li>
</ol>

</#if>

</@statics.content>
