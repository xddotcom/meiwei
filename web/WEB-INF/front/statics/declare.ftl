<#import "statics.ftl" as statics>

<@statics.content "免责声明">

<#if (LANGUAGE!"1")="1">

<h1 class="text-center">免责声明</h1>
<p class="lead">用户在接受美位网（以下简称"本网站"）服务之前，请务必仔细阅读以下免责声明：</p>

<ol>
	<li>本网站秉持遵纪守法、维护互联网良好创业环境的原则，不支持互联网上一切侵犯任何个人、单位及组织合法利益的剽窃、抄袭、盗用行为。</li>
	<li>若任何单位或个人认为本网站上登载的任何内容侵犯了其合法权利，可与本网站联系，明确指出侵权内容。若本网站核定举报情况属实，
		将马上撤除相关侵权内容。</li>
	<li>用户在美位网发表的内容仅表明其个人的立场和观点，并不代表美位网的立场或观点。作为内容的发表者，需自行对所发表内容负责，
		因所发表内容引发的一切纠纷，由该内容的发表者承担全部责任。美位网不承担任何责任。</li>
</ol>

<#else>

<h1 class="text-center">Disclaimer </h1>
<p class="lead">Before accepting Clubmeiwei’s services,users must read the following disclaimer carefully: </p>

<ol>
	<li>Upholding laws and maintaining the good principles of Internet entrepreneurial environment, Clubmeiwei does not support any plagiarism, copying, and piracy that violate any of the elegitimate interests of any individual, group, or organization.</li>
	<li>Any group or individual could contact Clubmeiwei and point out any content posted on the website that violates their legal rights. Once Clubmeiwei approves the truthfulness of the report, the corresponding contents would be removed immediately.</li>
	<li>Contents posted by users only represent users’personal stances and opinions, but not represent the stances or opinions of Clubmeiwei. The publisher is fully responsible for the content he or she posted on Clubmeiwei, and any dispute causing by the published content. Clubmeiweidoes not take any responsibility of the disputes. </li>
</ol>






</#if>

</@statics.content>
