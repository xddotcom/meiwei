<#import "statics.ftl" as statics>

<@statics.content "会员积分">

<#if (LANGUAGE!"1")="1">

<h1 class="text-center">美位会员积分细则</h1>
<ol>
	<li>消费1元人民币（实际消费金额），获得1点美位积分；</li>
	<li>累积美位积分满50,000点，可获得价值500元美位消费券一张。（消费券可在美位合作商户折扣后抵用。）</li>
	<li>美位积分还可兑换美位提供的精美礼品，具体兑换办法详见礼品兑换描述。</li>
</ol>

<#else>

<h1 class="text-center">Point rules for Meiwei members</h1>
<ol>
	<li>1 RMB = 1 Point</li>
	<li>You can redeem Meiwei Voucher (worth 500RMB) with 50,000 points (The voucher can be used to pay the discounted price in Meiwei’s partner restaurants)</li>
	<li>The points can also be used to redeem various Meiwei gifts</li>
</ol>

</#if>

</@statics.content>
