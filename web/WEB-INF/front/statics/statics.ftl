<#import "../layouts/common_standard.ftl" as standard>

<#macro content title = "">

<#escape x as x?html>
<@standard.html>
<@standard.page_header title>
	<@standard.css>
	</@standard.css>
  	<@standard.javascript>
  	</@standard.javascript>
</@standard.page_header>	
<@standard.page_body body_class="information" bodySlider=false rightbox="">
	<div class="well">
		<#nested />
	</div>
</@standard.page_body>
</@standard.html>
</#escape>

</#macro>
