<#import "../layouts/common_standard.ftl" as standard>

<#escape x as x?html>
<@standard.html >

<@standard.page_header "联系我们">
	<@standard.css></@standard.css>
  	<@standard.javascript>
		<script type="text/javascript">
			$(function () {
				 $("#contactForm").Validform({
					btnSubmit: "#buttonContactDiv",
					ignoreHidden: true,
					showAllError: true,
					ajaxPost:true,
					tiptype: 4 ,
					callback: function (data) {
						var result = data.status;
						if (result == "y") {
							resetForm();
						}
					}
				});
			});
		</script>
  	</@standard.javascript>
</@standard.page_header>

<@standard.page_body "contact" false >

<div class="container-fluid well">
<div class="row-fluid">

	<div class="span6">
		<form class="form-horizontal" action="<@spring.url '/article/saveConatct.htm' />" id="contactForm" name="contactForm">
			<legend>
				<h3><@spring.message "index.header.contactUs" /></h3>
				<i class="icon-envelope"></i> <@spring.message "index.header.contact_infor" />
			</legend>
			<div class="control-group">
				<label class="control-label" for="contact-name"><@spring.message "index.header.contact_name" />：</label>
				<div class="controls">
					<input id="contact-name" type="text" name="contactName" datatype="*" 
						nullmsg="<@spring.message 'usercenter.userinfor.infor_null' /><@spring.message 'index.header.contact_name' /><@spring.message 'usercenter.userinfor.infor_null_end' />"
						errormsg="<@spring.message 'index.header.contact_name' /><@spring.message 'usercenter.userinfor.infor_length' />" />
					<div class="info">
						<span class="Validform_checktip">
							<@spring.message 'index.header.contact_name' /><@spring.message 'usercenter.userinfor.infor_length' />
						</span>
						<span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span>
					</div>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="contact-identity"><@spring.message "index.header.contact_identity" />：</label>
				<div class="controls">
					<select name="contactType" id="contact-identity">
						<option value="0 "><@spring.message "index.header.contact_identity_0" /></option>
						<option value="1"> <@spring.message "index.header.contact_identity_1" /></option>
						<option value="2"><@spring.message "index.header.contact_identity_2" /></option>
						<option value="3"><@spring.message "index.header.contact_identity_3" /></option>
					</select>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="contact-email"><@spring.message "index.header.contact_email" />：</label>
				<div class="controls">
					<input type="text" id="contact-email" name="email"  datatype="e"
						nullmsg="<@spring.message 'usercenter.userinfor.infor_null' /><@spring.message 'index.header.contact_email' /><@spring.message 'usercenter.userinfor.infor_null_end' />"
						errormsg="<@spring.message 'usercenter.userinfor.infor_error' /><@spring.message 'usercenter.userinfor.infor_null' /><@spring.message 'index.header.contact_email' /><@spring.message 'usercenter.userinfor.infor_null_end' />" /> 
					<div class="info">
						<span class="Validform_checktip">
							<@spring.message 'index.header.contact_email' /><@spring.message 'usercenter.userinfor.infor_length' />
						</span>
						<span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span>
					</div>
				</div>
			</div>			
			<div class="control-group">
				<label class="control-label" for="contact-mobile"><@spring.message "index.header.contact_phone" />：</label>
				<div class="controls">
					<input id="contact-mobile" type="text" name="mobile" />
				</div>
			</div>			
			<div class="control-group">
				<label class="control-label" for="contact-message"><@spring.message "index.header.contact_message" />：</label>
				<div class="controls">
					<textarea name="remark" id="contact-message"  datatype="*" 
						nullmsg="<@spring.message 'usercenter.userinfor.infor_null' /><@spring.message 'index.header.contact_message' /><@spring.message 'usercenter.userinfor.infor_null_end' />"
						errormsg="<@spring.message 'index.header.contact_message' /><@spring.message 'usercenter.userinfor.infor_length' />" /></textarea>
					<div class="info">
						<span class="Validform_checktip">
							<@spring.message 'index.header.contact_message' /><@spring.message 'usercenter.userinfor.infor_length' />
						</span>
						<span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span>
					</div>
				</div>
			</div>		
			<button class="btn btn-primary" id="buttonContactDiv"><@spring.message "index.header.contactUs" /></button>
		</form>
	</div>
	<div class="span6">	
		<h3>
			<i class="icon-map-marker"></i>
			<@spring.message "index.header.contact_here" />
		</h3>
		<iframe width="370" height="240" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=%E5%9B%BD%E5%AE%9A%E4%B8%9C%E8%B7%AF333%E5%8F%B7303%E5%AE%A4,+shanghai&amp;aq=&amp;sll=38.410558,-95.712891&amp;sspn=41.091375,79.013672&amp;ie=UTF8&amp;hq=&amp;hnear=333%E5%8F%B7+Guo+Ding+Dong+Lu,+Yang+Pu+Qu,+Shanghai,+China&amp;ll=31.294824,121.520525&amp;spn=0.011038,0.01929&amp;t=m&amp;z=14&amp;output=embed"></iframe>
		<p><small><@spring.message "basic.title94" /></small></p>
		<p><small><@spring.message "index.header.contact_address" /></small></p>
		<p><small><@spring.message "index.header.contact_telephone" /></small></p>
		<p><small><@spring.message "index.header.contact_zipcode" /></small></p>
	</div>

</div> <!-- /container-fluid  -->
</div> <!-- /row-fluid -->

</@standard.page_body>

</@standard.html>
</#escape>
