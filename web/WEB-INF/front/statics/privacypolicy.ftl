<#import "statics.ftl" as statics>

<@statics.content "隐私说明">
<#if (LANGUAGE!"1")="1">
<h1 class="text-center">隐私说明</h1>
<p>美位网订餐网站（以下称&quot;本网站&quot;），隐私权保护声明是本网站保护用户个人隐私的承诺。鉴于网络的特性，本网站将无可避免地与您产生直接或间接的互动关系，故特此说明本网站对用户个人信息所采取的收集、使用和保护政策，请您务必仔细阅读：&nbsp;</p>

<ol>
	<li>
		<p>使用者非个人化信息</p>
		<p>我们将通过您的IP地址来收集非个人化的信息，例如您的浏览器性质、操作系统种类、给您提供接入服务的ISP的域名等，以优化在您计算机屏幕上显示的页面。通过收集上述信息，我们也进行客流量统计，从而改进网站的管理和服务。</p>
	</li>
	<li>
		<p>个人资料与信息安全</p>
		<p>在使用本网站提供的网上订餐服务过程中，您会被要求向我们提供一些个人资料，包括姓名、联系电话、手机、送餐地址、电子邮件等，以便我们及时准确地提供订餐服务，请予以理解和配合。美位网尊重并保护所有使用美位用户的个人隐私权，您注册的用户名、电子邮件地址等个人资料，非经您亲自许可或根据相关法律、法规的强制性规定，美位网不会主动地泄露给第三方。</p>
	</li>
	<li>
		<p>免责</p>
		<p>本网站毋需对下列情况承担任何责任：</p>
		<ul>
			<li>由于您将个人资料告知他人，而导致的任何个人资料泄露；</li>
			<li>任何由于计算机问题、黑客政击、计算机病毒侵入或发作、因政府管制而造成的暂时性关闭等影响网络正常经营之不可抗力而造成的个人资料泄露、丢失、被盗用或被窜改等；</li>
			<li>由于与本网站链接的其它网站所造成之个人资料泄露及由此而导致的任何法律争议和后果。</li>
		<ul>
	</li>
</ol>
<#else>

<h1 class="text-center">Privacy Policy</h1>
<p>This privacy policy statement is Clubmeiwei’s promise of protecting its user’s individual privacy. Given the characteristics of Internet network, Clubmeiwei will inevitably interact with you directly or indirectly. Therefore, we hereby explain our policies of collecting, using, and protecting user information. Please be sure to read carefully:</p>

<ol>
	<li>
		<p>User Impersonal information</p>
		<p>We will collect your impersonal information through you IP address, for example, nature of web browser, type of operating system, domain name of the ISP that provides you connection access, in order to optimize the display on your screen. By collecting this information, we also carry out web traffic statistics to improve the management of the websites and the services we provide to you. </p>
	</li>
	<li>
		<p>Safety of Personal information</p>
		<p>When using Clubmeiwei’s online reservation services, you would be asked to provide some personal information including your name, phone number, mobile phone number, address, email, etc. Please understand and corporate in order for us to proved you timely and accurate reservation service.  Clubmeiwei respects and protects all users’ individual privacy that, without your authorization or enforcement of corresponding laws, Clubmeiwei would never disclose your personal information to a third party.</p>
	</li>
	<li>
		<p>Disclaimer</p>
		<p>Clubmeiwei is not responsible for the following situation:</p>
		<ul>
			<li>Due to your sharing your personal information with others, any personal information gets disclosed;</li>
			<li>Due to force majeure that would affect normal operation of the network, such as computer problems, hackings, computer virus attack, and temporary government regulation closure, personal information gets disclosed, lost, stolen or tampered with;</li>
			<li>Because of other websites that link to Clubmeiwei, personal information gets disclosed, and results controversy and legal consequences.</li>
		<ul>
	</li>
</ol>

</#if>

</@statics.content>
