<#import "layouts/common_standard.ftl" as standard>
<#import "index_content.ftl" as content>
<@standard.html >

<@standard.page_header>
	<@standard.css></@standard.css>
  	<@standard.javascript>
  		<script type="text/javascript">
  			$(document).ready(function () {
    			var home = new HomeView();
  			});
  		</script>
  	</@standard.javascript>
</@standard.page_header>

<@standard.page_body body_class="home" bodySlider=true rightbox="simpleRight">
	<!-- 菜系 -->
	<div class="row white-box cuisines">
	</div>
	<!-- 商圈 -->
	<div class="row white-box districts">
	</div>
	<!-- 推荐 -->
	<div class="row recommends">
	</div>
	
	<@content.content />
</@standard.page_body>
</@standard.html >
