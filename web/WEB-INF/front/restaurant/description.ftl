<#macro description>
<script type="text/template" id="template-restaurant-view-1">
<div class="container-fluid restaurant-description">
	<div class="row-fluid">
		<div class="span8">
			<div id="restaurant-pictures" class="carousel slide" data-interval="1000">
				<div class="carousel-inner">
 					<% var total = 0 ;  _.each( restaurant.pictures , function( item , key , list ){ %>
						<% if(total <= 7){ %>
						<div class="item <% if(total==0){ %>active<%}%>">
							<img src="<@helper.imageUri '/' /><%= item.bigPath%>" alt="" />
						</div>
						<% total += 1;%>
						<%}%>
					<%});%>
					<% if(total < 5){ %>
 						<% _.each( restaurant.menus , function( item , key , list ){ %>
							<% if(total <= 5){ %>
							<div class="item <% if(total==0){ %>active<%}%>">
								<img src="<@helper.imageUri '/' /><%= item.menuPic %>" alt="" />
							</div>
 							<% total += 1; %>
							<%}%>
 						<%});%>
					<%}%>
					<% if(total == 0){ %>
						<div class="item active">
							<img src="<@spring.url '/front/images/nophoto_big.jpg' />" alt="" />
						</div>
					<%}%>
				</div>
				<a class="carousel-control left" href="#restaurant-pictures" data-slide="prev">&lsaquo;</a>
				<a class="carousel-control right" href="#restaurant-pictures" data-slide="next">&rsaquo;</a>
			</div>
		</div>
		<div class="span4">
			<h3>
				<%= restaurant.fullName.text%>
	    		<div class="rating clearfix" id="overall-rating" value="<%= restaurant.score%>">
					<div class="full-star"></div>
					<div class="full-star"></div>
					<div class="full-star"></div>
					<div class="full-star"></div>
					<div class="full-star"></div>
				</div>
			</h3>
        	<form class="form-horizontal group-quick-order" action="<@spring.url '/restaurant/book/'/><%= restaurant.restaurantId%>" method="post">
				<div class="control-group">
					<label class="control-label" for="datetimepicker_book">
						<i class="icon-calendar"></i>
						<@spring.message "restaurant.order.date" />:
					</label>
					<div class="controls">
	                  	<input class="date input-block-level" restaurant-id="<%= restaurant.restaurantId%>" type="text" style="cursor: pointer;background-color: rgb(238, 238, 238);" name="orderDate" data-date-format="yyyy-mm-dd" id="datetimepicker_book"  readonly>
        	 		 </div>
				</div>
				<div class="control-group">
					<label class="control-label" for="orderTime">
						<i class="icon-time"></i>
						<@spring.message "restaurant.order.time" />:
					</label>
					<div class="controls">
	                  	<select class="input-block-level" name="orderTime" id="orderTime" >
							<option value="11:00">11:00</option><option value="11:30">11:30</option>
							<option value="12:00">12:00</option><option value="12:30">12:30</option>
							<option value="13:00">13:00</option><option value="13:30">13:30</option>
							<option value="14:00">14:00</option><option value="14:30">14:30</option>
							<option value="17:00">17:00</option><option value="17:30">17:30</option>
							<option value="18:00">18:00</option><option value="18:30">18:30</option>
							<option value="19:00">19:00</option><option value="19:30">19:30</option>
							<option value="20:00">20:00</option><option value="20:30">20:30</option>
						</select>
        	 		 </div>
				</div>
				<div class="control-group">
					<label class="control-label" for="personNum">
						<i class="icon-group"></i>
						<@spring.message "restaurant.order.number" />:
					</label>
					<div class="controls">
	                  	<select class="input-block-level" name="personNum" id="personNum">
							<% for(i=1;i<26;i++){ %>
								<option value="<%= i%>"><%= i%></option>			
							<%}%>
						</select>
        	 		 </div>
				</div>
				<button class="btn btn-primary btn-block" style="display:block;" onclick="$('#search-box').submit();">
					<@spring.message "index.rightSearch.button" />
				</button>
			</form>
		</div>
	</div>
</div>
<div class="container-fluid restaurant-description">
	<div class="row-fluid">
		<div class="span11">
			<p><%= restaurant.description.text%></p>
			<p>
				<i class="icon-map-marker"> </i>
				<span><%= restaurant.address.text%></span>
				<a href="#showMap" role="button" data-toggle="modal">(<@spring.message 'basic.title287' />)</a>
			</p>
			<p><i class="icon-map-marker"></i> <span><%= restaurant.parking.text%></span></p>
			<p><i class="icon-time"></i> <span><%= restaurant.workinghour.text%></span></p>
			<!--
            <p><i class="icon-list-alt"></i> <a href="javascript:void(0);" id="a-wine">查看酒单</a> 
            <i class="icon-list-alt"></i> <a href="javascript:void(0);"  id="a-meal">查看菜单</a></p>
            -->
			<!--<h4><@spring.message 'basic.title95' />:</h4>-->
			<% 
                var address= restaurant.address.chinese;
                var good_addr = "上海市" + address;
                if(address.indexOf("(") != -1){
                    good_addr ="上海市" + address.substring(0, address.indexOf("("));
                }
				var restaurantSearch = restaurant.restaurantSearch;
                var longitude = ((restaurantSearch ? restaurantSearch.longitude : 0)/100000).toFixed(5);
                var latitude = ((restaurantSearch ? restaurantSearch.latitude : 0)/100000).toFixed(5);
            %>
			<div id="showMap" class="modal hide fade" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-body">
                    <% if(restaurant.mapPic){ %>
                        <img src="<@helper.imageUri '/' /><%= restaurant.mapPic%>" alt=""/>
                    <%}else{%>
                        <img src="http://api.map.baidu.com/staticimage?width=560&height=400&center=<%= longitude%>,<%= latitude%>&markers=<%= longitude%>,<%= latitude%>&markerStyles=l, ,0x998877&zoom=17" />
                    <%}%>
                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true"><@spring.message "basic.title20" /></button>
                </div>
            </div>
		</div>
		
		<div class="span1">
			<div class="time pull-right well well-small hidden">
				<%
					var arrayHours = [[],[],[],[],[],[],[]];
					var arrayText = ["<@spring.message 'basic.week.0' />", 
						"<@spring.message 'basic.week.1' />","<@spring.message 'basic.week.2' />","<@spring.message 'basic.week.3' />",
						"<@spring.message 'basic.week.4' />","<@spring.message 'basic.week.5' />","<@spring.message 'basic.week.6' />"]
					_.each( restaurant.hours , function( item , key , list ) {
						for(var i=1; i<arrayHours.length+1; i++){
							if ( ((1 << (7 - i)) & item.days) != 0 ){
								arrayHours[i-1].push( item.startTime.substring(11,16) + "-" + item.endTime.substring(11,16) );
							}
						}
					});
				%>
				<% var day = new Date().getDay(); _.each( arrayHours , function( item , key , list ) { %>
					<p <% if(key==day){ %> style="color:red;" <%}%> >
						<i class="icon-time"></i>
						<%= arrayText[key]%>: <%= arrayHours[key].join(", ")%>
					</p>
				<%}); %>
			</div>
		</div>
	</div>
</div>
<div class="container-fluid restaurant-promotion">
	<div class="row-fluid">
		<div class="span5">
	  		<p class="lead">
	  			<i class="icon-thumbs-up"></i>
	  			<@spring.message 'basic.title93' />
	  		</p>
			<p class="description">
				<% if(restaurant.discount.text!=""){ %>
					<%= restaurant.discount.text%>
				<%}else{%>
					<@spring.message 'basic.title44' />
				<%}%>
			</p>
		</div>
		<div class="span7">
  			<% if(restaurant.discountPic!=""){ %>
	  			<a target="_blank" class="pop-image" href="<@helper.imageUri '/' /><%=  restaurant.discountPic %>" >
					<img class="small-picture" src="<@helper.imageUri '/'  /><%= restaurant.discountPic%>" alt="" />
				</a>
   			<%}%>
	  	</div>
	</div>
</div>
</script>

</#macro>
