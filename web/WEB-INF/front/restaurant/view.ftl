<#import "../layouts/common_standard.ftl" as standard>
<#import "reviews.ftl" as asse>
<#import "description.ftl" as desc>

<@standard.html >
<@standard.page_header "">
	<@standard.css></@standard.css>
	<@standard.javascript>
        <script type="text/javascript"  src="<@spring.url '/front/js/jquery/popImage/jquery.popImage.mini.js' />"></script>
		<script type="text/javascript">
			$(document).ready(function () {
 				var restaurant = new Restaurant({ id : '${id}' });
				var restaurantView = new RestaurantView({ model : restaurant});
				var spinner = spinnerLoader();
				$("#template-content-span").append( spinner );
				restaurant.fetch({ cache: false , type : "get" ,success : function( collection ,response ){
					spinner.remove();
					restaurantView.render();
					LoginVaildate.loginPanel.success=function(){ };
					$("#a-meal").click(function(){
						$('div.meal').modal(  );
					});
					$("#a-wine").click(function(){
						$('div.wine').modal(  );
					});
					
					$("a.span-a").popImage();
					$(".pop-image").popImage();
				}});
			});
		</script>
	</@standard.javascript>
</@standard.page_header>
			
<@standard.page_body body_class="restaurant-profile" bodySlider=false rightbox="restRight">
 
<@desc.description />
<@asse.review />


<script type="text/template" id="template-restaurant-view-9">
<!-- 菜单 Modal -->
<div id="myModal" class="modal hide fade meal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel">菜单</h3>
  </div>
  <div class="modal-body">
    <table class="table">
      <tr>
      <th></th>
        <th>菜名</th>
        <th>价格</th>
      </tr>
      <% _.each( restaurant.menus , function( item , key , list ){ %>
	  <% if(item.menuCategory==0){ %>
	 <tr>
    	<td>
   		<% if(item.menuPic!=""){ %>
    	<a href="<@helper.imageUri '/' /><%= item.menuPic%>"   class="span-a">
           <img  style="width: 50px;"  src="<@helper.imageUri '/' /><%= item.menuPic%>" alt="">
        </a>
        <%}%>
        </td>
        <td><%= item.menuName.text%></td>
        <td>￥<%= item.price%></td>
      </tr>
    <%}%>
	<%});%>
    </table>
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">关闭</button>
   </div>
</div>
<!-- 酒单 Modal -->
<div id="myModal" class="modal hide fade wine" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel">酒单</h3>
  </div>
  <div class="modal-body">
    <table class="table">
      <tr>
      <th></th>
        <th>菜名</th>
        <th>价格</th>
      </tr>
      <% _.each( restaurant.menus , function( item , key , list ){ %>
	  <% if(item.menuCategory==1){ %>
	 <tr>
    	<td>
   		<% if(item.menuPic!=""){ %>
    	<a href="<@helper.imageUri '/' /><%= item.menuPic%>"   class="span-a">
           <img  style="width: 50px;"  src="<@helper.imageUri '/' /><%= item.menuPic%>" alt="">
        </a>
        <%}%>
        </td>
        <td><%= item.menuName.text%></td>
        <td>￥<%= item.price%></td>
      </tr>
    <%}%>
	<%});%>
    </table>

  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">关闭</button>
   </div>
</div>
</script>
</@standard.page_body>
</@standard.html>
