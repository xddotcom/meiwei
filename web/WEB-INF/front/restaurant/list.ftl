<#import "../layouts/common_standard.ftl" as standard>
<@standard.html>
<@standard.page_header>
	<@standard.css></@standard.css>
	<@standard.javascript>
		<script type="text/javascript">
			$(document).ready(function () {
				 var restaurantList = new RestaurantList( );
				 restaurantList.model = Restaurant;
				 restaurantList.url = "/api/restaurant/search/${key}";
				 var restaurantListView = new RestaurantListView( {model : restaurantList } );
				 
				 var jsonData = { searchType :"order" , orderBy : "score" , searchKey : '${(searchInforVo.searchKey)!""}'
				 , cuisineId : '${(searchInforVo.cuisineId)!""}', circleId : '${(searchInforVo.circleId)!""}'
				 , date : '${(searchInforVo.date)!""}', time : '${(searchInforVo.time)!""}' };
				 var spinner = spinnerLoader();
				 $("#template-content-span").append( spinner );
				 restaurantList.fetch({ cache: false , type : "post" , data : jsonData ,success : function( collection ,response ){
					 spinner.remove();
					 restaurantListView.render();
				 }}); 
			 });
		</script>
	</@standard.javascript>
</@standard.page_header>
<@standard.page_body "restaurant-list" false "searchRight">
<script type="text/template" id="template-restaurant-list-01">
<div class="restaurant-list-box">
	<div class="row-fluid">
		<div class="span9">
			<h4 class="title" id="C3"><@spring.message "restaurants.list.title" /></h4>
		</div>
		<div class="span3">
			<div class="sort">
				<span for="restaurant-list-sort">
					<@spring.message "restaurants.sort.title" />：
				</span>
				<select class="input-small" name="orderBy" id="restaurant-list-sort" onchange="PageLoad.restaurantList.orderBy(this.value);">
					<option value="score">
						<@spring.message "restaurants.sort.method1" />
					</option>
					<option value="fullName">
						<@spring.message "restaurants.sort.method2" />
					</option>
					<option value="perBegin">
						<@spring.message "restaurants.sort.method3" />
					</option>
				</select>
			</div>
		</div>
	</div>

	<% _.each( restaurants , function( item , key , list ){ %>
		<div class="single-restaurant disable well well-small" <% if(key > 4){ %>style="display: none;"<%}%> >
			<div class="row-fluid">
				<div class="span3">
					<a href="<@spring.url '/'  />restaurant/view/<%= item.restaurantId%>"  target="_blank">
						<% if(item.frontPic!=""){ %>
							<img src="<@helper.imageUri '/'  /><%= item.frontPic%>" alt=""  />
						<%}else{%>
							<img src="<@spring.url '/front/images/nophoto_small.jpg' />" alt=""  />
						<%}%>
					</a>
					 
				</div>
				<div class="span9">
					<a href="<@spring.url '/' />restaurant/view/<%= item.restaurantId%>" target="_blank" >
						<h4 class="title"><%= item.fullName.text%></h4>
						<div class="description">
							<p>
								<span><%= item.description.text%></span>
								<!--<small>
									<a href="javascript:;" onclick="PageLoad.restaurantList.moreVisible(this);" id="moreAtag" 
										style="color:#921416;"><@spring.message "index.recom.more" />&gt;&gt;</a>
									<a href="javascript:;" onclick="PageLoad.restaurantList.moreVisible(this);" id="lessAtag" 
										style="color:#921416;display:none;"><@spring.message "index.recom.less" />&lt;&lt;</a>
								</small> -->
							</p>
						</div>
						<p>
							<small class="price label label-info"><@spring.message "restaurants.peopleavg" />:￥<%= item.perBegin%></small>
							<small><%= item.address.text%></small>
							&nbsp;&nbsp;&nbsp;
							<h5><%= item.discount.text%></h5>
						</p>
						<div class="rating clearfix" value="<%= item.score%>">
							<div class="full-star"></div>
							<div class="full-star"></div>
							<div class="full-star"></div>
							<div class="full-star"></div>
							<div class="full-star"></div>
							<div class="clear"></div>
						</div>
					</a>
				</div> <!-- END span9 -->
			</div> <!-- END row-fluid -->
		</div> <!-- END container-fluid -->
 	<%});%>
	<p class="text-right"><a href="#C3" class="return-top label label-info"><@spring.message "basic.title219" />&nbsp;↑</a>
	<a href="javascript:;" id="view-more" class="view-more label label-info" onclick="PageLoad.restaurantList.showOther()"><@spring.message "restaurants.show.other" /></a></p>
</div> <!-- END restaurant-list-box -->
</script>
	
</@standard.page_body>

</@standard.html>
