<#macro review>
<script type="text/template" id="template-restaurant-view-2">

<% if (_.isEmpty(restaurant.reviews) == false) { %>
	
	<div class="container-fluid restaurant-review-list">
		<div class="row-fluid">
			<div class="span9">
				<h4 class="title"><@spring.message "restaurant.review.title1" /></h4>
			</div>
			<div class="span3">
				<h5>
					<i class="icon-file"></i>
					<a href="#C12"><@spring.message "restaurant.review.title2" /></a>
				</h5>
			</div>
		</div>
		
		<% _.each( restaurant.reviews , function( item , key , list ){ %>
		<div class="media single-review">
			<div class="pull-left">
	            <% if(item.member.personalInfor.sexe==0 ){  %>
	              <img src="<@spring.url '/front/images/man.png'/>" alt="icon" />
	            <%}else{%>
	              <img src="<@spring.url '/front/images/woman.png'/>" alt="icon" />
	            <%}%>
	       	</div>
			<div class="media-body">
				<ul class="inline">
					<li class="rating" value="<%= item.score%>">
						<div class="full-star"></div>
						<div class="full-star"></div>
						<div class="full-star"></div>
						<div class="full-star"></div>
						<div class="full-star"></div>
					</li>
					<li class="environment">
						<@spring.message "restaurant.review.title3" />:
						<%= item.environmentScore%>
					</li>
					<li class="taste">
						<@spring.message "restaurant.review.title4" />:
						<%= item.tasteScore%>
					</li>
					<li class="service">
						<@spring.message "restaurant.review.title5" />:
						<%= item.serviceScore%>
					</li>
				</ul>
				<p class="name-date">
					<strong><%= _.escape(item.member.personalInfor.nickName)%></strong>
					<small><%= item.reviewTime%></small>
				</p>
				<div class="text"><p><%= _.escape(item.comments)%></p></div>
			</div>
		</div>
		<%});%>
		<#-- <a href="#" class="show-all"><@spring.message "restaurant.review.title7" /></a> -->
	</div>

<% } %>

<div class="container-fluid restaurant-review">
	<div class="row-fluid" id="C12">
		<h4><@spring.message "basic.title26" /></h4>
	</div>
	
	<form method="post" id="reviewForm" name="reviewForm">
		<input type="hidden" name="restaurantId" id="restaurantId" value="<%= restaurant.restaurantId%>">
		<input id="score" type="hidden" name="score" value="3"/>
		<input id="environment" type="hidden" name="environment" value="3" />
		<input id="service" type="hidden" name="service"  value="3"/>
		<input id="taste" type="hidden" name="taste"  value="3" />
		<div class="row-fluid overall">
			<div class="span2"><p><@spring.message 'basic.title27' />:</p></div>
			<div class="span10">
				<ul class="inline rating-click" hiddenInput="score">
					<li class="item">1</li><li class="item">2</li>
					<li class="item clearItem">3</li>
					<li class="item">4</li><li class="item">5</li>
				</ul>
			</div>
		</div>
		<div class="row-fluid environment">
			<div class="span2"><p><@spring.message 'basic.title30' />:</p></div>
			<div class="span10">
				<ul class="inline rating-click" hiddenInput="environment">
					<li class="item">1</li><li class="item">2</li>
					<li class="item clearItem">3</li>
					<li class="item">4</li><li class="item">5</li>
				</ul>
			</div>
		</div>
		<div class="row-fluid service">
			<div class="span2"><p><@spring.message 'basic.title33' />:</p></div>
			<div class="span10">
				<ul class="inline rating-click" hiddenInput="service">
					<li class="item">1</li><li class="item">2</li>
					<li class="item clearItem">3</li>
					<li class="item">4</li><li class="item">5</li>
				</ul>
			</div>
		</div>
		<div class="row-fluid taste">
			<div class="span2"><p><@spring.message 'basic.title36' />:</p></div>
			<div class="span10">
				<ul class="inline rating-click" hiddenInput="taste">
					<li class="item">1</li><li class="item">2</li>
					<li class="item clearItem">3</li>
					<li class="item">4</li><li class="item">5</li>
				</ul>
			</div>
		</div>
		<div class="row-fluid comment">
			<div class="span2"><p><@spring.message 'basic.title41' /></p></div>
			<div class="span10">
				<textarea class="input-xxlarge" name="comments" id="comments" cols="30" rows="3" datatype="*1-990"></textarea>
			</div>
		</div>
		<button class="btn btn-primary" id="reviewButton">
			<@spring.message 'basic.title43' />
		</button>
	</form>
</div>
</script>
</#macro>
