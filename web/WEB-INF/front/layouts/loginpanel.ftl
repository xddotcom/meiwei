<#macro login_panel >
<div id="loginpanel">
    <iframe name='hidden_frame' id="loginpanel-hidden-frame" style='display: none'></iframe>
    <form class="navbar-form pull-right" method="post" action="<@spring.url '/api/member/login'/>" id="navbar-loginform" target="hidden_frame">
        <div class="info Validform_checktip Validform_wrong" id="navbar-loginfail"><@spring.message 'basic.title83' /></div>
        <input class="input-medium" id="navbar-username" type="text" name="loginName" datatype="*" placeholder="<@spring.message 'usercenter.userinfor.loginName.placeholder' />" />
        <input class="input-medium" id="navbar-password" type="password" name="loginPassword" datatype="*" placeholder="<@spring.message 'usercenter.userinfor.password' />" />
        <button class="btn btn-primary" id="navbar-loginsubmit"><@spring.message 'index.header.login_submit' /></button>
        <button class="btn btn-primary" id="navbar-goregister" onclick='javascript:window.location.href="<@spring.url ' /member/register.htm'/>"'> <@spring.message "index.sign.up" />
        </button >
</form>
</div>
<ul class="nav pull-right">
    <li></li>
    <li>
        <a href="<@spring.url '/member/findpassword.htm' />"><@spring.message 'usercenter.userinfor.forgetPassword' /></a>
    </li>
</ul>
</#macro>
