<#macro right >

<!-- 微信 -->
<div class="container-fluid well well-small hidden-phone right-1" style="display: none;">
	<div class="row-fluid">
		<div class="span5">
			<img src="<@spring.url '/front/images/weixin.png' />" />
		</div>
		<div class="span7">
			<a href="http://weibo.com/u/3058840707">
				<!-- <wb:follow-button uid="3058840707" type="red_1" width="67" height="24" ></wb:follow-button>  -->
				<p>@美位clubmeiwei 更多动态敬请关注美位公共平台</p>
			</a>
		</div>
	</div>
</div>

<!-- 公告 -->
<div class="well well-small right-2"  style="display: none;">
	<h4 message="0001"><@spring.message "index.rightSearch.public" /></h4>
	<div>
		<!--<marquee scrollamount="1" direction="up" onmouseover=this.stop() onmouseout=this.start()>-->
			<p><a href="http://blog.clubmeiwei.com/?p=50" target="_blank">5月18日 美位·上海站正式开启</a></p>
			<p><a href="http://blog.clubmeiwei.com/?p=24" target="_blank">5月8日 La Finca庄源开业晚宴</a></p>
			<p><a href="http://blog.clubmeiwei.com/?p=15" target="_blank">4月12日 美位·上海站预约注册活动</a></p>
			<p><a href="http://blog.clubmeiwei.com/?p=35" target="_blank">3月14日 晚"白色情人节之夜"完美落幕</a></p>
			<p><a href="http://blog.clubmeiwei.com/?p=32" target="_blank">1月19日 晚"相遇美位尊享菲红"红酒品鉴晚宴</a></p>
		<!--</marquee>-->
	</div>
</div>

<!-- 快速订餐 -->
<div class="well well-small right-3"  style="display: none;">
	<div id="quick-order">
		<h4><@spring.message "index.rightSearch.quickSearch" /></h4>
		<form id="queryForm" name="queryForm" action="<@spring.url '/restaurant/search/quick#C3'/>" method="post">
			<input type="hidden" name="searchType" value="order" />
			<input type="hidden" name="orderBy" value="score" id="orderBy" />
			<input class="input-block-level" type="text" id="quickSearch" name="searchKey" autocomplete="off"  value='${(searchInforVo.searchKey)!""}'
				placeholder="<@spring.message "index.rightSearch.placeholder" />" 
				data-provide="typeahead" data-items="9">
			<select  class="input-block-level" name="cuisineId" id="cuisineId" value='${(searchInforVo.cuisineId)!""}'>
				<option value=""><@spring.message "index.rightSearch.chooseCook" /></option>
			</select>
			<select  class="input-block-level" name="circleId" id="circleId"  value='${(searchInforVo.circleId)!""}'>
				<option value="" message="2341"><@spring.message "index.rightSearch.chooseCircle" /></option>
				
			</select>
            <input  id="datetimepicker"  data-date-format="yyyy-mm-dd" class="date input-block-level" type="text"  name="date"  value='${(searchInforVo.date)!""}'
                 style="cursor: pointer;background-color: rgb(238, 238, 238);"  readonly />
			<select name="time" id="time"  class="input-block-level" >
				<option value=""><@spring.message "index.rightSearch.haveTime" /></option>
				
			</select>
			<button class="btn btn-primary btn-block" id="quicksearch-submit" 
				onclick="PageLoad.home.submitQuery();" >
				<@spring.message "index.rightSearch.button" />
			</button>
		</form>
	</div>
</div>

<!-- 商圈 菜系 -->
<div class="accordion well well-small right-4" id="accordion2"  style="display: none;">
	<div class="accordion-group">
	  	<div class="accordion-heading">
	    	<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
	        	<h4><@spring.message "index.hot.cook" /></h4>
	    	</a>
	  	</div>
	  	<div id="collapseOne" class="accordion-body collapse">
	    	<div class="accordion-inner">
	    		<ul class="inline" id="cuisine-ul">
					
				</ul>
	    	</div>
	  	</div>
	</div>
	<div class="accordion-group">
	  	<div class="accordion-heading">
	    	<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
	        	<h4><@spring.message "index.hot.circle" /></h4>
	    	</a>
	  	</div>
	  	<div id="collapseTwo" class="accordion-body collapse">
	    	<div class="accordion-inner">
	    		<ul class="inline" id="circles-ul">
	    		
				</ul>
	    	</div>
	  	</div>
	</div>
</div>

<!-- 标签 -->
<div class="well well-small right-5"  style="display: none;">
</div>

 

<!-- 餐厅浏览历史 -->
<div class="well well-small right-7"  style="display: none;">
	<h4><@spring.message "restaurant.right.isaw" /></h4>
	<ul class="my-restaurants" id="my_restaurantsUL"></ul>
</div>

<!-- 广告栏 -->
<div class="well well-small right-9"  style="display: none;">
	<a href="/front/html/blucetin.html" target="_blank">
		<img src="<@spring.url '/front/images/blucetin/blucetin.jpg' />" alt="" style="width: 260px" />
	</a>
</div>
<script type="text/template" id="template-right-box-1">
<% _.each( cuisines , function( item , key , list ){ %>
	<option value="<%=  item.cuisineId %>" <% if(item.cuisineId=='${(searchInforVo.cuisineId)!""}'){%>selected<%}%> >
		<%= item.cuisineName.text %>
	</option>
<%});%>
</script>
<script type="text/template" id="template-right-box-2">
<% _.each( circles , function( item , key , list ){ %>
	<option value="<%=  item.circleId %>"  <% if(item.circleId=='${(searchInforVo.circleId)!""}'){%>selected<%}%>  >
		<%= item.circleName.text %>
	</option>
<%});%>
</script>
<script type="text/template" id="template-right-box-3">
<% var timeArray = [ "11:00","11:30","12:00","12:30","13:00","13:30","14:00","14:30","17:00","17:30","18:00","18:30",
"19:00","19:30","20:00","20:30","21:00","21:30","22:00","22:30","23:00","23:30" ]; 
_.each( timeArray , function( item , key , list ){ %>
    <option value="<%=  item %>" <% if(item=='${(searchInforVo.time)!""}'){%>selected<%}%>  >
        <%= item %>
    </option>
<%});%>
</script>
<script type="text/template" id="template-right-box-4">
<% _.each( cuisines , function( item , key , list ){ %>
    <li <% if(item.isRecommended==0){  %> class="stylesDisable" style="display:none" <%} %> >
        <a href="<@spring.url '/' />restaurant/search/0-0-<%=  item.cuisineId %>-0#C3">
            <%= item.cuisineName.text %>
        </a>
    </li>
<%});%>
</script>
<script type="text/template" id="template-right-box-5">
<% _.each( districts , function( district , key , list ){ %>
    <li><span class="lead"><%= district.districtName.text %></span></li>
    <% _.each( district.circles , function( item , key , list ){ %>
        <li>
            <a href="<@spring.url '/'  />restaurant/search/0-<%=  item.circleId %>-0-0#C3">
                <small><%=  item.circleName.text %></small>
            </a>
        </li>
<%})});%>
</script>
<script type="text/template" id="template-right-box-6">
<% if(typeof restaurant != 'undefined'){%>
<!-- 标签 -->
     <h4>
        <i class="icon-bookmark"></i>
        <@spring.message "restaurant.label" />
        <% if(restaurant._FavoriteStatus &&  restaurant._FavoriteStatus=="1" ){ %>
            <a id="favoriteButton" href="javascript:void(0);"
                title="<@spring.message 'basic.title218' />" 
                onclick="PageLoad.restaurantProfile.inFavorite('<%= restaurant.restaurantId%>')"
                class="add-favorite"><@spring.message 'basic.title231' /></a>
        <%}%>
    </h4>
    <ul class="inline">
        <% _.each( restaurant.tags , function( item , key , list ){ %>
            <li>
                <a href="<@spring.url '/' />restaurant/search/0-0-0-<%= item.ruleId%>#C3">
                    <%= item.ruleName.text%>
                </a>
            </li>
        <%});%>
    </ul>
<%}%>
</script>
</#macro>