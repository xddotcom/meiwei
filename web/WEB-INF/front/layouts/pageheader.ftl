<#macro page_header title>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	    <meta http-equiv="keywords" content="美位网, 订餐, 餐饮">
		<meta http-equiv="description" content="美位网, 网上订餐">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="" >
	    <title>${title?html}</title>
	    <#nested />
	</head>
</#macro>

<#macro css>
	
	<link rel="stylesheet" href="<@spring.url '/front/css/bootstrap.css?version=${CACHE_TIMES}' />">
	<style> body { padding-top: 30px; padding-bottom: 20px; } </style>
    <link rel="stylesheet" href="<@spring.url '/front/css/bootstrap-responsive.css?version=${CACHE_TIMES}' />">
    <link rel="stylesheet" href="<@spring.url '/front/css/main.css?version=${CACHE_TIMES}' />">
	
	<#nested />
	
	<script src="<@spring.url '/front/js/vendor/modernizr-2.6.2-respond-1.1.0.min.js?version=${CACHE_TIMES}' />"></script>
</#macro>

<#macro javascript>
	
	<script src="http://tjs.sjs.sinajs.cn/open/api/js/wb.js?version=${CACHE_TIMES}" type="text/javascript" charset="utf-8"></script>
	
	<script type="text/javascript" src="<@spring.url '/front/js/vendor/jquery-1.8.3.min.js?version=${CACHE_TIMES}'/>"></script>
	<!--<script type="text/javascript" src="<@spring.url '/front/js/vendor/jquery-1.9.1.min.js'/>"></script>-->
	
	<script type="text/javascript" src="<@spring.url '/front/js/plugin/datetimepicker/bootstrap-datetimepicker.min.js?version=${CACHE_TIMES}'/>"></script>
	<script type="text/javascript" src="<@spring.url '/front/js/plugin/jquery.FillOptions.js?version=${CACHE_TIMES}'/>"></script>
	<script type="text/javascript" src="<@spring.url '/front/js/plugin/jquery.placeholder.js?version=${CACHE_TIMES}'/>"></script>
	<script type="text/javascript" src="<@spring.url '/front/js/plugin/Validform_v5.3.1.js?version=${CACHE_TIMES}'/>"></script>
	<script type="text/javascript" src="<@spring.url '/front/js/plugin/cnToLetter.js?version=${CACHE_TIMES}'/>"></script>
	
	<script type="text/javascript" src="<@spring.url '/front/js/vendor/bootstrap.js?version=${CACHE_TIMES}'/>"></script>
	<script type="text/javascript" src="<@spring.url '/front/js/vendor/underscore-min.js?version=${CACHE_TIMES}'/>"></script>
	<script type="text/javascript" src="<@spring.url '/front/js/vendor/backbone-min.js?version=${CACHE_TIMES}'/>"></script>
	<script type="text/javascript" src="<@spring.url '/front/js/plugin/responsive-nav.min.js?version=${CACHE_TIMES}'/>"></script>
	
	<script type="text/javascript" src="<@spring.url '/front/js/model.js?version=${CACHE_TIMES}'/>"></script>
	<script type="text/javascript" src="<@spring.url '/front/js/common.js?version=${CACHE_TIMES}'/>"></script>
	<script type="text/javascript">GlobalVariable.loginstatus = <#if (_LoginMember?exists)>1<#else>0</#if>;
	GlobalVariable.language = ${LANGUAGE!"1"};
	GlobalVariable.data = new Object();
	if(GlobalVariable.loginstatus==0){
		if(location.href.indexOf("/member/register.htm") < 0){
			setCookie("QUERY_URL",  location.href , 30*60*1000);
		}
	}
	</script>
	<script type="text/javascript" src="<@spring.url '/front/js/content.js?version=${CACHE_TIMES}'/>"></script>
	<script type="text/javascript" src="<@spring.url '/front/js/page/orderview.js?version=${CACHE_TIMES}'/>"></script>
	<script type="text/javascript" src="<@spring.url '/front/js/page/memberview.js?version=${CACHE_TIMES}'/>"></script>
	
	<#nested />
</#macro>
