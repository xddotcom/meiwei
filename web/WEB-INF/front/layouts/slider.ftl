<#macro body_slider >
<div class="hero-unit" style="z-index: -10">
	<div id="slider" class="carousel slide" data-interval="1000">
		<div class="carousel-inner">
			<div class="active item">
				<img src="<@spring.url '/front/images/1000width.jpg'/>" alt="" />
			</div>
			<div class="item">
				<img src="<@spring.url '/front/images/homepage-slide-3.jpg'/>" alt="" />
			</div>
		</div>
	</div>
</div>
</#macro>
