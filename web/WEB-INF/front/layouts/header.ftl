<#import "loginpanel.ftl" as loginpanel> <#macro masthead >
<div class="container masthead" id="container-masthead">
</div>
<script type="text/template" id="template-masthead">
    <div class="headbar row  hidden-phone">
        <div class="span7">
            <a href="<@spring.url '/'/>">
                <img class="logo" src="<@spring.url '/front/images/meiwei-logo.png'/>" alt="" />
            </a>
        </div>
        <div class="span5">
            <ul class="inline language-list">
                <li class="phone-number">
                    <p>
                        <i class="icon-phone icon-large"></i>
                        021 6051 5617
                    </p>
                </li>
                <li class="chinese">
                    <p>
                        <a href="javascript:void(0)" onclick="setLanguge('zh_CN')"><@spring.message "index.lang.cn" /></a>
                    </p>
                </li>
                <li class="english">
                    <p>
                        <a href="javascript:void(0)" onclick="setLanguge('en')"><@spring.message "index.lang.en" /></a>
                    </p>
                </li>
            </ul>
            <form action="<@spring.url '/changelanguage.htm' />" id="lanForm">
                <input type="hidden" name="locale" id="locale" />
            </form>
            <form class="form-search" id="search-box" action="<@spring.url '/restaurant/search/quick#C3'/>" method="post">
                <input type="text" id="pageSearch" name="searchKey" class="input-xlarge search-query" autocomplete="off" placeholder="<@spring.message 'index.homeSearch.placeholder' />"
                    value='${(searchInforVo.searchKey)!""}' data-provide="typeahead" data-items="9">
                <button class="btn btn-primary" onclick="$('#search-box').submit();"><@spring.message "index.homeSearch.button" /></button>
            </form>
        </div>
    </div>
    <div class="navbar">
        <div class="navbar-inner">
            <div class="container">
                <a class="btn btn-navbar" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <a class="brand hidden-phone" href="/"><@spring.message "index.menu.home" /></a>
                <a class="brand hidden-desktop" href="/" style="padding: 2px 0px 0px 0px;">
                    <img class="logo" src="<@spring.url '/front/images/logo-small.png'/>" alt="" style="height: 38px;" />
                </a>
                <div class="nav-collapse navbar-responsive-collapse in collapse" style="height: auto;">
                    <ul class="nav hidden-phone">
                        <li>
                            <a href="<@spring.url '/article/business.htm'/>"><@spring.message "index.menu.parter" /></a>
                        </li>
                        <li>
                            <a href="<@spring.url '/article/services.htm'/>"><@spring.message "index.menu.services" /></a>
                        </li>
                        <li>
                            <a href="<@spring.url '/article/points.htm'/>"><@spring.message "index.menu.points" /></a>
                        </li>
                    </ul>
                    <% if(member){ %>
                    <ul class="nav pull-right">
                        <li class="opt-user hidden-phone">
                            <p class="navbar-text"><@spring.message "basic.title13" />, <%=member.personalInfor.nickName %></p>
                        </li>
                        <li class="opt-user hidden-phone">
                            <%if(member.memberType==1 || member.memberType==2){ %>
                            <a href="<@spring.url '/myaccount/restaurant/newOrders.htm'/>"><@spring.message "basic.title12" /></a>
                            <%}else{%>
                            <a href="<@spring.url '/myaccount/user/usercenter.htm'/>"><@spring.message "usercenter.headbox.usercenter" /></a>
                            <%}%>
                        </li>
                        <% if(member.memberType==0){ %>
                        <li class="li-member opt-user">
                            <a href="<@spring.url '/myaccount/user/usercenter.htm'/>"><@spring.message 'usercenter.userinfor.individua' /></a>
                        </li>
                        <li class="li-member opt-user">
                            <a href="<@spring.url '/myaccount/user/editprofile.htm'/>"><@spring.message 'usercenter.userinfor.information' /></a>
                        </li>
                        <li class="li-member opt-user">
                            <a href="<@spring.url '/myaccount/user/handlepassword.htm'/>"><@spring.message 'basic.title233' /></a>
                        </li>
                        <li class="li-member opt-user">
                            <a href="<@spring.url '/myaccount/user/integral.htm'/>">礼品兑换</a>
                        </li>
                        <li class="li-member opt-user">
                            <a href="<@spring.url '/myaccount/user/credits.htm'/>">我的积分</a>
                        </li>
                        <li class="li-member opt-user">
                            <a href="<@spring.url '/myaccount/user/favorites.htm'/>"><@spring.message 'usercenter.userinfor.favorite' /></a>
                        </li>
                        <li class="li-member opt-user">
                            <a href="<@spring.url '/myaccount/user/orders.htm'/>"><@spring.message 'usercenter.userinfor.order' /></a>
                        </li>
                        <li class="li-member opt-user">
                            <a href="<@spring.url '/myaccount/user/reviews.htm'/>"><@spring.message 'usercenter.userinfor.review' /></a>
                        </li>
                        <li class="li-member opt-user">
                            <a href="<@spring.url '/myaccount/user/contacts.htm'/>"><@spring.message 'usercenter.userinfor.mycontact' /></a>
                        </li>
                        <%}else{%>
                        <li class="li-member opt-user">
                            <a href="<@spring.url '/myaccount/restaurant/newOrders.htm'/>"><@spring.message 'basic.title54' /></a>
                        </li>
                        <li class="li-member opt-user">
                            <a href="<@spring.url '/myaccount/restaurant/doOrders.htm'/>"><@spring.message 'basic.title62' /></a>
                        </li>
                        <li class="li-member opt-user">
                            <a href="<@spring.url '/myaccount/restaurant/detail.htm'/>">餐厅信息</a>
                        </li>
                        <li class="li-member opt-user">
                            <a href="<@spring.url '/myaccount/restaurant/reviews.htm'/>">评论列表</a>
                        </li>
                        <li class="li-member opt-user">
                            <a href="<@spring.url '/myaccount/restaurant/favorites.htm'/>">收藏列表</a>
                        </li>
                        <%}%>
                        <li class="opt-user">
                            <a href="<@spring.url '/member/loginout.htm'/>"><@spring.message "index.header.login_out" /></a>
                        </li>
                    </ul>
                    <%}else{%> <@loginpanel.login_panel /><%}%>
                </div>
            </div>
        </div>
    </div>
</script>
</#macro>