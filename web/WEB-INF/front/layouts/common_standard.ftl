<#import "pageheader.ftl" as pageheader>
<#import "header.ftl" as header>
<#import "footer.ftl" as footer>
<#import "slider.ftl" as slider>
<#import "leftbox.ftl" as leftbox>
<#import "rightbox.ftl" as right>
<#import "../common/_page.ftl" as page>

<#macro ClickTale_Top>
	<script type="text/javascript">
		var WRInitTime=(new Date()).getTime();
	</script>
</#macro>

<#macro ClickTale_Bottom>
	<div id="ClickTaleDiv" style="display: none;"></div>
	<script type="text/javascript">
		if(document.location.protocol!='https:')
			document.write(unescape("%3Cscript%20src='http://s.clicktale.net/WRe0.js'%20type='text/javascript'%3E%3C/script%3E"));
	</script>
	<script type="text/javascript">
		if(typeof ClickTale=='function') ClickTale(5422,1,"www08");
	</script>
</#macro>

<#macro css>
	<@pageheader.css>
		<#nested />
	</@pageheader.css>
</#macro>

<#macro javascript>
	<@pageheader.javascript>
		<#nested />
	</@pageheader.javascript>
</#macro>

<#macro page_header title="美位网订餐平台">
	<@pageheader.page_header title>
		<#nested  />
	</@pageheader.page_header>
</#macro>

<#macro html>
	<!DOCTYPE html>
	<#escape x as x?html>
		<!--[if lt IE 7]>      <html xmlns:wb="http://open.weibo.com/wb" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
		<!--[if IE 7]>         <html xmlns:wb="http://open.weibo.com/wb" class="no-js lt-ie9 lt-ie8"> <![endif]-->
		<!--[if IE 8]>         <html xmlns:wb="http://open.weibo.com/wb" class="no-js lt-ie9"> <![endif]-->
		<!--[if gt IE 8]><!--> <html xmlns:wb="http://open.weibo.com/wb" class="no-js"> <!--<![endif]-->
			<#nested />
		</html>
	</#escape>
</#macro>

<#macro page_body body_class="home" bodySlider=true rightbox="">
	<body class="${body_class}" id="body">
		<!--[if lte IE 7]>
            <p class="chromeframe text-center" style="margin-top: -30px;"><small><@spring.message 'basic.title224' /></small></p>
        <![endif]-->
        <@header.masthead />
		<!--<@ClickTale_Top />-->
		<div class="container">
			<#if bodySlider>
				<@slider.body_slider />
			</#if>
			<div class="row">
				<#if rightbox!="">
					<div class="span9" id="template-content-span"><#nested  /></div>
					<div class="span3" id="template-right-box-span">
						<@right.right />
					</div>
				<#else>
					<div class="span12"><#nested /></div>
				</#if>
			</div>
		</div>
		<@footer.mastfoot />
		<!--<@ClickTale_Bottom />-->
		</body>
	</body>
</#macro>

<#macro member_body active="default" paged=false>
	<body class="usercenter" leftbox="${active}">
		<!--<@ClickTale_Top />-->
		<@header.masthead />
		<div class="container">
			<div class="row">
				<div class="span12 tabbable tabs-left">
                     <ul class="nav nav-tabs hidden-phone" id="nav">
                         <@leftbox.leftbox active />
                     </ul>
 					<div class="tab-content">
						<#nested>
						<#if paged>
							<@page.page />
						</#if>
  					</div>
				</div>
			</div>
		</div>
		<@footer.mastfoot />
		<!--<@ClickTale_Bottom />-->
	</body>
</#macro>

<#macro encodeText2Url textInfor>
	${encodeUrl(textInfor.text?html)}
</#macro>

<#macro text textInfor maxlength = 9999>
	<#if (LANGUAGE!"1")="1">
		<#assign output=textInfor.chinese?html />
	<#else>
		<#assign output=textInfor.english?html />
	</#if>
	<#if (output?length <= maxlength)>
		${output}
	<#else>
		${output[0..maxlength-1]}
	</#if>
</#macro>

<#macro imageSize path>${imageSize(path)}</#macro>
