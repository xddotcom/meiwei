<#macro mastfoot>

<div class="container mastfoot hidden-phone" >
	<div class="well">
		<ul class="inline text-center">
			<li><a href="<@spring.url '/article/aboutus.htm'/>"><@spring.message "index.header.aboutUs" /></a></li>
			<li><a href="<@spring.url '/article/career.htm'/>"><@spring.message "basic.title5" /></a></li>
			<li><a href="javascript:;"><@spring.message "basic.title6" /></a></li>
			<li><a href="javascript:;"><@spring.message "basic.title7" /></a></li>
			<li><a href="<@spring.url '/article/terms.htm'/>"><@spring.message "basic.title8" /></a></li>
			<li><a href="<@spring.url '/article/privacypolicy.htm'/>"><@spring.message "basic.title9" /></a></li>
			<li><a href="<@spring.url '/article/faq.htm'/>">FAQ</a></li>
			<li><a href="<@spring.url '/article/declare.htm'/>"><@spring.message "basic.title10" /></a></li>
			<li><a href="<@spring.url '/article/contactus.htm'/>"><@spring.message "index.header.contactUs" /></a></li>
		</ul>
		<p class="text-center">Copyright&copy2013 ClubMeiWei.com. All Rights Reserved.</p>
		<p class="text-center"><a href="http://www.miibeian.gov.cn/" target="_blank">沪ICP备13004528号</a></p>
	</div>
	
	<script>
	    function S4() {return (((1+Math.random())*0x10000)|0).toString(16).substring(1);}
        function guid() {return (S4()+S4()+"-"+S4()+"-"+S4()+"-"+S4()+"-"+S4()+S4()+S4());}
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-38281340-1', 'clubmeiwei.com');
        ga('send', 'pageview');
        /*var clientId = guid();
        ga('create', 'UA-38281340-1', {'storage':'none','clientId':clientId});
        ga('send', 'pageview');
        $(function() { setTimeout(function() { window.location.reload(); }, 1000); });*/
    </script>
</div>

</#macro>
