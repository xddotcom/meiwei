<#macro leftbox active="default">
<script type="text/template" id="template-leftbox">
    <li class="nav-header">&nbsp;</li>
	<% if(member.memberType == 0){ %>
        <li active="usercenter">
			<a href="<@spring.url '/myaccount/user/usercenter.htm'/>"><@spring.message 'usercenter.userinfor.individua' /></a>
		</li>
		<li active="userinfor">
			<a href="<@spring.url '/myaccount/user/editprofile.htm'/>"><@spring.message 'usercenter.userinfor.information' /></a>
		</li>
         <li active="handle-password">
          <a href="<@spring.url '/myaccount/user/handlepassword.htm'/>"><@spring.message 'basic.title233' /></a>
        </li>
        <li active="integral">
          <a href="<@spring.url '/myaccount/user/integral.htm'/>"><@spring.message 'basic.title268' /></a>
        </li>
         <li active="credits">
          <a href="<@spring.url '/myaccount/user/credits.htm'/>"><@spring.message 'basic.title278' /></a>
        </li>
		<li active="favorites">
			<a href="<@spring.url '/myaccount/user/favorites.htm'/>"><@spring.message 'usercenter.userinfor.favorite' /></a>
		</li>
		<li active="orders">
			<a href="<@spring.url '/myaccount/user/orders.htm'/>"><@spring.message 'usercenter.userinfor.order' /></a>
		</li>
		<li active="reviews">
			<a href="<@spring.url '/myaccount/user/reviews.htm'/>"><@spring.message 'usercenter.userinfor.review' /></a>
		</li>
		<li active="contacts">
			<a href="<@spring.url '/myaccount/user/contacts.htm'/>"><@spring.message 'usercenter.userinfor.mycontact' /></a>
		</li>          
	<%}else{%>
		<li active="newOrders">
			<a href="<@spring.url '/myaccount/restaurant/newOrders.htm'/>"><@spring.message 'basic.title54' /> <span id="new-order-num"></span></a>
		</li>
		<li active="doOrders">
			<a href="<@spring.url '/myaccount/restaurant/doOrders.htm'/>"><@spring.message 'basic.title62' /></a>
		</li>
         <li active="rest-detail">
          <a href="<@spring.url '/myaccount/restaurant/detail.htm'/>">餐厅信息</a>
        </li>
        <li active="rest-reviews">
          <a href="<@spring.url '/myaccount/restaurant/reviews.htm'/>">评论列表</a>
        </li>
        <li active="rest-favorites">
          <a href="<@spring.url '/myaccount/restaurant/favorites.htm'/>">收藏列表</a>
        </li>
	<%}%>
    <li class="nav-header">&nbsp;</li>
</script>
</#macro>
