<#import "../layouts/common_standard.ftl" as standard>
<#import "table.ftl" as table>
<#import "detail.ftl" as details>
<#import "contact.ftl" as contact>
<#import "extra.ftl" as extra>

<@standard.html >

<@standard.page_header "订餐">
	<@standard.css></@standard.css>
  	<@standard.javascript>
  		<script type="text/javascript" src="<@spring.url '/front/js/plugin/jquery.svgView.js'/>"></script>
        <script type="text/javascript" src="<@spring.url '/front/js/jquery/popImage/jquery.popImage.mini.js' />"></script>
		<script type="text/javascript">
			afterRender = function( restaurant ){
				var personNum = '${(orderInforVo.personNum)!""}';
				var orderDate = '${(orderInforVo.orderDate)!""}' || '${(searchInforVo.date)!""}';
				var orderTime = '${(orderInforVo.orderTime)!""}' || '${(searchInforVo.time)!""}';
 	 			initOrderView(restaurant.restaurantId, personNum, orderDate, orderTime, "");
				PageLoad.restaurantProfile.initCookie(restaurant.restaurantNo ,restaurant.fullName.text , restaurant.restaurantId);
				PageLoad.init();
				console.log("successfully!");
			}
			$(document).ready(function () {
				var restaurant = new Restaurant({ id : '${id}' });
				var orderView = new OrderView({ model : restaurant});
				var spinner = spinnerLoader();
				$("#template-content-span").append( spinner );
				restaurant.fetch({ cache: false , type : "get" ,success : function( collection ,response ){
					spinner.remove();
 					orderView.render();
				}});
			});
		</script>
  	</@standard.javascript>
</@standard.page_header>

<@standard.page_body "restaurant-profile" false "restRight">

	<@details.infor />
	<@table.table />
	<@contact.contact />
    <@extra.extra />
    <div class="container-fluid well well-small" style="display:none;" id="div-order-btn">
		<button class="btn btn-primary pull-right" onclick="validOrderForm();" >
			<@spring.message "restaurant.order.button" />
		</button>
	</div>
	
	<div id="book-confirm" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="book-confirm-title" aria-hidden="true">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			<h4 id="book-confirm-title"><@spring.message "basic.title241" /></h4>
		</div>
		<div class="modal-body">
			<p id="p-text-restaurant-name"><@spring.message "basic.title49" />：</p>
			<p><@spring.message "restaurant.order.postion" />：<@spring.message "basic.title232" /></p>
			<p><@spring.message "restaurant.order.date" />：<span class="date"></span></p>
			<p><@spring.message "restaurant.order.time" />：<span class="time"></span></p>
			<p><@spring.message "restaurant.order.number" />：<span class="number"></span><@spring.message "basic.title23" /></p>
			<p><@spring.message "index.header.contact_name" />：<span class="contact_name"></span></p>
			<p><@spring.message "restaurant.order.otherask" />：<span class="other"></span></p>
			<p><@spring.message "restaurant.order.otherask" />：<span class="adminother"></span></p>
			<p class="extra"><@spring.message "basic.title240" /></p>
			<p class="confirm"><@spring.message "basic.title239" />。</p>
		</div>
		<div class="modal-footer">
			<button class="btn" data-dismiss="modal" aria-hidden="true"><@spring.message "basic.title20" /></button>
			<button id="book-submit-button" class="btn btn-primary"><@spring.message "basic.title58" /></button>
		</div>
	</div>

</@standard.page_body>
</@standard.html>
