<#macro table>
<script type="text/template" id="template-order-view-02">
<div class="container-fluid well well-small restaurant-floorplan" id="seatMap">
<% if(restaurant.tablePics.length>0){ %>
	<h4 class="headline"><@spring.message "restaurant.position.title" /></h4>
	<div class="clearfix">
		<ul class="nav inline pull-left">
			<li>
				<div class="input-prepend">
					<span class="add-on"><@spring.message "basic.title215" /></span>
					<div class="selectableDiv"></div>
				</div>
			</li>
			<li>
				<div class="input-prepend">
					<span class="add-on"><@spring.message "basic.title216" /></span>
					<div class="selectedDiv"></div>
				</div>
			</li>
			<li>
				<div class="input-prepend">
					<span class="add-on"><@spring.message "basic.title217" /></span>
					<div class="noselectDiv"></div>
				</div>
			</li>
		</ul>
		<ul class="nav inline pull-right">
			<div class="input-prepend">
				<span class="add-on" ><@spring.message "restaurant.order.floor" /></span>
				<select id="selectImg" onchange="changefloorImg()">
				<% _.each( restaurant.tablePics , function( item , key , list ){ %>
					<option imgPath="<%= item.tablePicPath%>" picId="<%= item.tablePicId%>">
						<% if (item.picName.text==""){ %>
							<%= restaurant.fullName.text%>
						<%}else{%>
							<%= item.picName.text%>
						<%}%>
					</option>
				<%});%>
				</select>
			</div>
		</ul>
	</div>
	<div id="svgDiv"></div>
<%}%>
</div>
</script>
</#macro>
