<#macro contact>
<script type="text/template" id="template-order-view-03">
<div class="container-fluid well well-small member-contact" id="liaisonDiv">

	<div class="row-fluid">
		<div class="input-prepend">
			<span class="add-on fix-mama"><@spring.message "liaison.title1" /></span>
			<select class="input-xlarge" id="select-contact">
				<!-- Backbone Fills Me -->
			</select>
		</div>
	</div>
	<div class="row-fluid">
		<form method="post" class="form-horizontal" id="new-contact-form">
			<input type="hidden" name="contactId" />
			<input class="input-small" id="nickName" type="text" name="name" datatype="*1-20" placeholder="<@spring.message 'index.header.contact_name' />" />
			<select class="input-small" name="sexe" id="sexSelect">
					<option value="0"><@spring.message 'basic.title121' /></option>
					<option value="1"><@spring.message 'basic.title122' /></option>
				</select>
				<input class="input-small" id="telphone" type="text" name="telphone" datatype="m" placeholder="<@spring.message 'usercenter.userinfor.mobile' />" />
	 
			<div class="input-prepend" style="display: none;">
				<span class="add-on"><@spring.message 'usercenter.userinfor.email' />:</span>
				<input class="input-small" id="email" type="text" name="email" datatype="e" ignore="ignore" />
			</div>
			<div class="btn btn-primary" onclick="if(PageLoad.requireLogin() && contactValid()){addContact();}">
				<@spring.message "liaison.title7" />
			</div>
			<a href="#invite-modal" role="button" class="btn btn-primary"  onclick="if(PageLoad.requireLogin()){$('#invite-modal').modal('show');}">
				<@spring.message 'basic.title242' />
			</a>
		</form>
	</div>
</div>
<div id="invite-modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="invite-friends-title" aria-hidden="true">
	<div class="modal-body">
		<div class="row-fluid">
			<div class="span7">
				<form class="form-horizontal">
					<legend><@spring.message 'basic.title243' /></legend>
					<div class="well well-small" id = "select-invite">
						<!-- Backbone Fills Me -->
					</div>
					<div class="help-block"></div>
					<button class="btn" data-dismiss="modal" aria-hidden="true"><@spring.message "basic.title20" /></button>
					<div class="btn btn-primary" onclick="inviteFriends();"><@spring.message "basic.title58" /></div>
				</form>
			</div>
			<div class="span5">
				<form method="post" id="new-invite-form" name="new-invite-form">
					<input type="hidden" name="contactId" />
					<label><@spring.message 'index.header.contact_name' />:</label>
					<input class="input-small" id="nickName" type="text" name="name" datatype="*1-20" />
					<select class="input-small" name="sexe" id="sexSelect">
						<option value="0"><@spring.message 'basic.title121' /></option>
						<option value="1"><@spring.message 'basic.title122' /></option>
					</select>
					<label class="add-on">*<@spring.message 'usercenter.userinfor.mobile' />:</label>
					<input id="telphone" type="text" name="telphone" datatype="m" />
					<label><@spring.message 'usercenter.userinfor.email' />:</label>
					<input id="email" type="text" name="email" datatype="e" ignore="ignore" />
					<div class="btn btn-primary" onclick="addInvite();" >
						<@spring.message "liaison.title7" />
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
</script>
</#macro>
