<#macro infor>
<script type="text/template" id="template-order-view-01">
<div class="container-fluid well well-small booking-detail">
	<div class="row-fluid">
		<div class="span4">
			<h4 id="restaurant-name-h4"><%= restaurant.fullName.text%></h4>
			<div class="picture">
				<% if(restaurant.frontPic!=""){%>
				<img src="<@helper.imageUri '/' /><%= restaurant.frontPic%>" alt="" />
				<%}else{%>
				<img src="<@spring.url '/front/images/nophoto_small.jpg' />" alt="" />
				<%}%>
			</div>
			<p><!--<small ><@spring.message "restaurant.order.title1" /></small>  --></p>
		</div>
		<div class="span4 booking-info">
			<h4><@spring.message 'basic.title244' /></h4>
			<form class="form-horizontal mirro-margin" 
				method="post" id="order-form" name="formInfor">
                <div id="order-form-input-hidden">
                  <input type="hidden" name="tables" id="tables">
                  <input type="hidden" name="restaurantId" id="restaurantId" value="<%= restaurant.restaurantId%>">
                  <input type="hidden" name="contactId" id="contactId">
                  <input type="hidden" name="contactName" id="contactName">
                  <input type="hidden" name="contactTelphone" id="contactTelphone">
                  <input type="hidden" name="contactSexe" id="contactSexe">
                  <input type="hidden" name="adminother" id="adminother" />
                  <input type="hidden" name="inviteFriends" id="inviteFriends" />
                </div>
				<div class="control-group">
					<label class="control-label" for="datetimepicker_book">
						<i class="icon-calendar"></i>
						<@spring.message "restaurant.order.date" />:
					</label>
					<div class="controls">
	                  	<input class="date input-block-level" restaurant-id="<%= restaurant.restaurantId%>"  data-date-format="yyyy-mm-dd" type="text" style="cursor: pointer;background-color: rgb(238, 238, 238);" name="orderDate" id="datetimepicker_book"  readonly>
        	 		 </div>
				</div>
				<div class="control-group">
					<label class="control-label" for="orderTime">
						<i class="icon-time"></i>
						<@spring.message "restaurant.order.time" />:
					</label>
					<div class="controls">
						<select name="orderTime" id="orderTime" class="input-block-level">
							<option value="11:00">11:00</option><option value="11:30">11:30</option>
							<option value="12:00">12:00</option><option value="12:30">12:30</option>
							<option value="13:00">13:00</option><option value="13:30">13:30</option>
							<option value="14:00">14:00</option><option value="14:30">14:30</option>
							<option value="17:00">17:00</option><option value="17:30">17:30</option>
							<option value="18:00">18:00</option><option value="18:30">18:30</option>
							<option value="19:00">19:00</option><option value="19:30">19:30</option>
							<option value="20:00">20:00</option><option value="20:30">20:30</option>
						</select>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="personNum">
						<i class="icon-group"></i>
						<@spring.message "restaurant.order.number" />:
					</label>
					<div class="controls">
						<select name="personNum" id="personNum"  class="input-block-level">
							<% for(i=1;i<26;i++){ %>
								<option value="<%= i%>"><%= i%></option>			
							<%}%>
						</select>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="reservation-additional-info">
						<span><@spring.message "restaurant.order.remark" />:</span>
					</label>
					<div class="controls">
						<textarea id="reservation-additional-info" type="text"  class="input-block-level" rows="3"
							name="other" style="background-color: #ebebeb;"></textarea>
					</div>
				</div>
			</form>
		</div>
        <div class="span3 add-contact">
            <h4><@spring.message 'basic.title245' /></h4>
            <select  class="input-block-level" id="select-contact">
                <option><@spring.message 'basic.title246' /></option>
            </select>
            <form method="post" class="form-horizontal" id="new-contact-form">
                <input type="hidden" name="contactId" />
                <input  class="input-block-level"  id="nickName" type="text" name="name" datatype="*1-20" placeholder="<@spring.message 'index.header.contact_name' />" />
                <select  class="input-block-level"  name="sexe" id="sexSelect">
                    <option value="0"><@spring.message 'basic.title121' /></option>
                    <option value="1"><@spring.message 'basic.title122' /></option>
                </select>
                <input class="input-block-level" id="telphone" type="text" name="telphone" datatype="m" placeholder="<@spring.message 'usercenter.userinfor.mobile' />" />
                <div class="input-prepend" style="display: none;">
                    <span class="add-on"><@spring.message 'usercenter.userinfor.email' />:</span>
                    <input class="input-small" id="email" type="text" name="email" datatype="e" ignore="ignore" />
                </div>
                <div class="btn btn-primary btn-block hidden" onclick="if(PageLoad.requireLogin() && contactValid()){addContact();}">
                    添加预订人
                </div>
                <a href="#invite-modal" role="button" class="btn btn-primary btn-block"  onclick="if(PageLoad.requireLogin()){$('#invite-modal').modal('show');}">
                   <@spring.message 'basic.title249' />
                </a>
            </form>
        </div>
	</div>
</div>
<div id="invite-modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="invite-friends-title" aria-hidden="true">
	<div class="modal-body">
		<div class="row-fluid">
			<div class="span7">
				<form class="form-horizontal">
					<legend><@spring.message 'basic.title243' /></legend>
					<div class="well well-small" id = "select-invite">
						<!-- Backbone Fills Me -->
					</div>
					<div class="help-block"></div>
					<button class="btn" data-dismiss="modal" aria-hidden="true"><@spring.message "basic.title20" /></button>
					<div class="btn btn-primary" onclick="inviteFriends();"><@spring.message "basic.title58" /></div>
				</form>
			</div>
			<div class="span5">
				<form method="post" id="new-invite-form" name="new-invite-form">
					<input type="hidden" name="contactId" />
					<label><@spring.message 'index.header.contact_name' />:</label>
					<input class="input-small" id="nickName" type="text" name="name" datatype="*1-20" />
					<select class="input-small" name="sexe" id="sexSelect">
						<option value="0"><@spring.message 'basic.title121' /></option>
						<option value="1"><@spring.message 'basic.title122' /></option>
					</select>
					<label class="add-on">*<@spring.message 'usercenter.userinfor.mobile' />:</label>
					<input id="telphone" type="text" name="telphone" datatype="m" />
					<label><@spring.message 'usercenter.userinfor.email' />:</label>
					<input id="email" type="text" name="email" datatype="e" ignore="ignore" />
					<div class="btn btn-primary" onclick="addInvite();" >
						<@spring.message 'basic.title291' />
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
</script>
</#macro>
