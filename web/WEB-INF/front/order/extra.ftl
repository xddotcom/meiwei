<#macro extra>
<script type="text/template" id="template-order-view-04">
<div class="container-fluid well well-small value-added">
	<h4 class="headline hl-click" style="cursor: pointer;"><@spring.message 'basic.title247' /></h4>
	
<% _.each( restaurant.valueAddedProducts , function( procuctType , key , list ){ %>
	<div class="row-fluid">
		<p>
			<span class="lead"><%= procuctType.typeName.text%></span>
			&nbsp;&nbsp;&nbsp;
			<small><%= procuctType.typeRemark.text%></small>
		</p>
		<ul class="thumbnails product-category">
			
			<% var span = "span3"; if(procuctType.products.length>4){ span = "span2" } %>
			<% _.each( procuctType.products , function( item , key , list ){ %>
				<li class="<%= span%>">
					<div class="thumbnail product">
						<img src="<@helper.imageUri '/'  /><%= item.picturePath%>" alt="" class="picture" />
						<div class="selected-hover"></div>
						<p class="product-desc text-center">
                            <small class="hidden"><%= procuctType.typeName.text%>:</small>
							<%= item.productName.text%>
							<% if(item.price!="" && item.price!="0"){%>￥<%= item.price%><%}%>
							<small><%= item.model.text%></small>
                        </p>
						<p >
                           <input type="hidden" name="productIds"  class="product-ids-temp"  value="<%= item.productId%>" />
                        </p>
					</div>
				</li>
			<%});%>
		</ul>
	</div>
<%});%>

<div class="row-fluid">
	<div class="span2"><span><@spring.message 'basic.title254' />:</span></div>
	<div class="span10">
		<textarea class="input-xxlarge" id="areaother" type="text" cols="70" rows="4"></textarea>
		<p class="description"><@spring.message 'basic.title255' /></p>
	</div>
</div
</div>
</script>
</#macro>
