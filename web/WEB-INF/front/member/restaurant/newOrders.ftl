<#import "../../layouts/common_standard.ftl" as standard> <@standard.html > <@standard.page_header "新订单"> <@standard.css> </@standard.css> <@standard.javascript> </@standard.javascript>
</@standard.page_header> <@standard.member_body "newOrders" true >
<div class="container-fluid">
    <p class="lead"><@spring.message 'basic.title54' /></p>
    <div>
        <div class="input-prepend"  style="display: none;">
            <span class="add-on">订单日期</span>
            <select id="date-search-select"></select>
        </div>
    </div>
    <div style="display: none;">
        <p id="basic-title59"><@spring.message 'basic.title59' /></p>
        <p id="basic-title70"><@spring.message 'basic.title70' /></p>
    </div>
    <table class="table table-bordered newOrders">
        <script type="text/template" id="template-newOrders">
        <tr>
            <td colspan="6" class="usercenter-table-bg"><@spring.message 'basic.title48' />: <%=item.orderNo%>&nbsp;&nbsp;&nbsp; <%=item.orderSubmitTime%>&nbsp;&nbsp;&nbsp;
                <%=item.restaurant.fullName.text%></td>
        </tr>
        <tr>
            <th><@spring.message 'restaurant.order.date' /></th>
            <th><@spring.message 'usercenter.userinfor.orderPerson' /></th>
            <th><@spring.message 'index.header.contact_name' /></th>
            <th><@spring.message 'liaison.title4' /></th>
            <th><@spring.message 'basic.title51' /></th>
            <th><@spring.message 'basic.title65' /></th>
        </tr>
        <tr>
            <td><%=item.orderDate.substring(0,11)%><%=item.orderTime.substring(11,16)%></td>
            <td><%=item.personNum%>人</td>
            <td><%=item.contactName%></td>
            <td><%=item.contactTelphone%></td>
            <td>
                <a href="<@spring.url '/restaurant/viewtable/<%=item.orderNo%>' />
                " target="_blank">查看桌位图
                </a>
            </td>
            <td>
               	<% if( item.status==0 ){ %>
                <a href="javascript:void(0);" onclick="doConfirm('<%=item.orderId%>');"> <@spring.message 'basic.title69' /> </a>
                <a href="javascript:void(0);" onclick="doDelete('<%=item.orderId%>');"> <@spring.message 'basic.title68' /> </a>
                <%}%>
            </td>
        </tr>
        <% if(item.other != ""){ %>
        <tr>
            <td colspan="1"><@spring.message "restaurant.order.remark" /></td>
            <td colspan="5" style="overflow: hidden; text-align: left; color: #FF0000"><%=_.escape(item.other)%></td>
        </tr>
         <%}%>
		<% if(member.memberType==2 && item.adminother!= ""){ %>
        <tr>
            <td colspan="1"><@spring.message "restaurant.order.remark" />2</td>
            <td colspan="5" style="overflow: hidden; text-align: left; color: #FF0000"><%=_.escape(item.adminother)%></td>
        </tr>
       <%}%>
		</script>
    </table>
    <p class="noresults"><@spring.message 'form.member.list.4' /></p>
</div>
</@standard.member_body> </@standard.html >
