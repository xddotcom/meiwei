<#import "../../layouts/common_standard.ftl" as standard> <@standard.html > <@standard.page_header "评论列表"> <@standard.css></@standard.css> <@standard.javascript> </@standard.javascript>
</@standard.page_header> <@standard.member_body "rest-reviews" true >
<div class="container-fluid rest-reviews">
    <p class="lead">评论列表</p>
    <script type="text/template" id="template-reviews">
    <div class="media single-review well well-small">
        <div class="pull-left">
            <img style="width: 64px; height: 64px;" src="<@helper.imageUri '/' /><%=item.restaurant.frontPic%>" alt="icon" />
            <p class="text-center"></p>
        </div>
        <div class="media-body">
            <ul class="inline">
                <li class="rating" value="<%=item.score%>">
                    <div class="full-star"></div>
                    <div class="full-star"></div>
                    <div class="full-star"></div>
                    <div class="full-star"></div>
                    <div class="full-star"></div>
                </li>
                <li class="environment"><@spring.message "restaurant.review.title3" />: <%=item.environmentScore%></li>
                <li class="taste"><@spring.message "restaurant.review.title4" />: <%=item.tasteScore%></li>
                <li class="service"><@spring.message "restaurant.review.title5" />: <%=item.serviceScore%></li>
            </ul>
            <p class="name-date">
                <strong>(<%=item.restaurant.fullName.text%>)</strong>
                <strong>${(review.member.personalInfor.nickName)!""}<%=item.member.personalInfor.nickName%></strong>
                <small><%=item.reviewTime%></small>
            </p>
            <div class="text">
                <p><%=_.escape(item.comments)%></p>
            </div>
        </div>
    </div>
	</script>
    <p class="noresults"><@spring.message 'form.member.list.5' /></p>
</div>
</@standard.member_body> </@standard.html >
