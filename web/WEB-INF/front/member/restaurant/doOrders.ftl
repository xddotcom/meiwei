<#import "../../layouts/common_standard.ftl" as standard> <@standard.html> <@standard.page_header "订单列表"> <@standard.css> </@standard.css> <@standard.javascript> </@standard.javascript>
</@standard.page_header> <@standard.member_body "doOrders" true>
<div class="container-fluid">
    <p class="lead"><@spring.message 'basic.title62' /></p>
    <div>
        <div class="input-prepend">
            <span class="add-on"><@spring.message 'basic.title288' /></span>
            <select id="date-search-select" search-key="month"></select>
        </div>
    </div>
    <div style="display: none;">
        <p id="basic-title59"><@spring.message 'basic.title59' /></p>
        <p id="basic-title70"><@spring.message 'basic.title70' /></p>
    </div>
    <table class="table table-bordered doOrders">
    	<script type="text/template" id="template-doOrders">
        <tr>
            <td colspan="7" class="usercenter-table-bg"><@spring.message 'basic.title48' />: <%=item.orderNo%>&nbsp;&nbsp;&nbsp; <%=item.orderSubmitTime%>&nbsp;&nbsp;&nbsp;
                <%=item.restaurant.fullName.text%></td>
        </tr>
        <tr>
            <th><@spring.message 'restaurant.order.date' /></th>
            <th><@spring.message 'usercenter.userinfor.orderPerson' /></th>
            <th><@spring.message 'index.header.contact_name' /></th>
            <th><@spring.message 'liaison.title4' /></th>
            <th><@spring.message 'basic.title51' /></th>
            <th><@spring.message 'basic.title52' /></th>
            <th><@spring.message 'basic.title53' /></th>
        </tr>
        <tr>
            <td><%=item.orderDate.substring(0,11)%><%=item.orderTime.substring(11,16)%></td>
            <td><%=item.personNum%><@spring.message 'basic.title23' /></td>
            <td><%=item.contactName%></td>
            <td><%=item.contactTelphone%></td>
            <td>
                <a href="<@spring.url '/restaurant/viewtable/<%=item.orderNo%>' />
                " target="_blank"><@spring.message 'basic.title220' />
                </a></td>
            <td>
            	 <% var _message = {"0":"<@spring.message 'order.status.0' />","10":"<@spring.message 'order.status.10' />","20":"<@spring.message 'order.status.20' />",
                    "30":"<@spring.message 'order.status.30' />","40":"<@spring.message 'order.status.40' />","41":"<@spring.message 'order.status.41' />",
                    "50":"<@spring.message 'order.status.50' />" ,"96":"<@spring.message 'order.status.96' />","97":"<@spring.message 'order.status.97' />",
                    "98":"<@spring.message 'order.status.98' />","99":"<@spring.message 'order.status.99' />"};%>
                <%=_message[item.status]%>
            </td>
            <td>
                <% if( item.status==0 || item.status==10){ %>
                <form class="form-inline" method="post" onsubmit="return validateCheck(this);" style="margin: 0; padding: 0;">
                    <input type="hidden" name="orderId" value="<%=item.orderId%>" />
                    <input type="text" name="consumeAmount" value="<%=item.consumeAmount%>" size="3" class="input-mini" onkeyup="javascript:CheckInputIntFloat(this);" />
                    <input type="submit" class="btn btn-primary" value="<@spring.message 'basic.title58' />" />
                </form>
                <%}else{%>
                <span><%=item.consumeAmount%>(佣金:<%=item.commission%>)</span>
                <%}%>
            </td>
        </tr>
        <% if(item.other != ""){ %>
        <tr class="usercenter-table-content">
            <td colspan="1"><@spring.message "restaurant.order.remark" /></td>
            <td colspan="6" style="overflow: hidden; text-align: left; color: #FF0000"><%=_.escape(item.other)%></td>
        </tr>
        <%}%>
         <% if(member.memberType==2 && item.adminother!= ""){ %>
        <tr class="usercenter-table-content">
            <td colspan="1"><@spring.message "restaurant.order.remark" />2</td>
            <td colspan="6" style="overflow: hidden; text-align: left; color: #FF0000"><%=_.escape(item.adminother)%></td>
        </tr>
        <%}%>
        </script>
    </table>
    <p class="noresults"><@spring.message 'form.member.list.4' /></p>
</div>
</@standard.member_body> </@standard.html>
