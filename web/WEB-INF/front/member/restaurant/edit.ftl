<#import "../../layouts/common_standard.ftl" as standard><@standard.html > <@standard.page_header "餐厅资料"> <@standard.css></@standard.css> <@standard.javascript>
<script type="text/javascript" src="<@spring.url '/front/js/jquery/jquery.jSelectDate.js'/>"></script>
</@standard.javascript> </@standard.page_header> <@standard.member_body "rest-detail" false>
<div class="container-fluid rest-detail">
    <iframe name="restedit_hidden_frame" style="display: none;"></iframe>
    <form class="form-horizontal" method="post" id="resteditForm" name="resteditForm" target="restedit_hidden_frame">
        <legend>餐厅资料</legend>
        <div>
            <input type="hidden" name="restaurantId" id="restaurantId" />
        </div>
        <div class="control-group">
            <label class="control-label"></label>
            <div class="controls">
                <select id="deep-select"></select>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">餐厅名称(中文):</label>
            <div class="controls">
                <input class="input-xlarge" type="text" name="fullName" id="fullName" />
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">餐厅名称(英文):</label>
            <div class="controls">
                <input class="input-xlarge" type="text" name="fullNameEN" id="fullNameEN" />
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">餐厅地址(中文):</label>
            <div class="controls">
                <input class="input-xxlarge" type="text" name="address" id="address" />
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">餐厅地址(英文):</label>
            <div class="controls">
                <input class="input-xxlarge" type="text" name="addressEN" id="addressEN" />
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">餐厅工作时间(中文):</label>
            <div class="controls">
                <input class="input-xxlarge" type="text" name="workinghour" id="workinghour" />
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">餐厅工作时间(英文):</label>
            <div class="controls">
                <input class="input-xxlarge" type="text" name="workinghourEN" id="workinghourEN" />
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">餐厅描述(中文):</label>
            <div class="controls">
                <textarea class="input-xxlarge" name="description" id="description" cols="30" rows="5"></textarea>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">餐厅描述(英文):</label>
            <div class="controls">
                <textarea class="input-xxlarge" name="descriptionEN" id="descriptionEN" cols="30" rows="5"></textarea>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">餐厅折扣(中文):</label>
            <div class="controls">
                <textarea class="input-xxlarge" name="discount" id="discount" cols="30" rows="3"></textarea>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">餐厅折扣(英文):</label>
            <div class="controls">
                <textarea class="input-xxlarge" name="discountEN" id="discountEN" cols="30" rows="3"></textarea>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">公交信息(中文):</label>
            <div class="controls">
                <textarea class="input-xxlarge" name="parking" id="parking" cols="30" rows="3"></textarea>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">公交信息(英文):</label>
            <div class="controls">
                <textarea class="input-xxlarge" name="parkingEN" id="parkingEN" cols="30" rows="3"></textarea>
            </div>
        </div>
        <div class="control-group">
            <div class="controls">
                <button class="btn btn-primary" id="btnRestsubmit">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <@spring.message 'usercenter.userinfor.save' /> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</button>
            </div>
        </div>
    </form>
</div>
</@standard.member_body> </@standard.html >
