<#import "../layouts/common_standard.ftl" as standard><@standard.html ><@standard.page_header "登录"> <@standard.css> </@standard.css> <@standard.javascript>
<script type="text/javascript">
	$(document).ready(function() {
		LoginVaildate.loginPanel.success = function() {
			window.location.href = "/myaccount/user/usercenter.htm";
		}
		$("#loginName").placeholder();
		$("#loginPassword,#loginName").keydown(function(e) {
			if (e.keyCode == 13) {
				$("#buttonLoginBtn").click();
			}
		});
		$("#loginForm").Validform({
			btnSubmit : "#buttonLoginBtn",
			ajaxPost : true,
			ignoreHidden : true,
			showAllError : true,
			tiptype : 4,
			callback : function(data) {
				if (data && data.status == "y") {
					location.href = "/";
				} else {
					$("#loginfirst-wrong-tip").css("display","block");
				}
			}
		});
	});
</script>
</@standard.javascript> </@standard.page_header> <@standard.page_body body_class="loginfirst" bodySlider=false rightbox="">
<div class="container-fluid">
    <iframe name='hidden_frame' id="hidden_frame" style='display: none'></iframe>
    <form class="form-horizontal form-register" method="post" action="<@spring.url '/api/member/login'/>" id="loginForm" target="hidden_frame">
        <legend><@spring.message 'usercenter.userinfor.login_formeiwei' /></legend>
        <div class="control-group" id="loginfirst-wrong-tip" style="display: none;">
            <label class="control-label"></label>
            <div class="controls">
                <div class="help-inline">
                    <span class="Validform_checktip Validform_wrong">用户名或密码错误</span>
                </div>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">*<@spring.message 'usercenter.userinfor.loginName' />:</label>
            <div class="controls">
                <input id="loginName" type="text" name="loginName" datatype="*" placeholder="<@spring.message 'usercenter.userinfor.loginName.placeholder' />"
                    nullmsg="<@spring.message 'usercenter.userinfor.infor_null' /><@spring.message 'usercenter.userinfor.loginName' /><@spring.message 'usercenter.userinfor.infor_null_end' />"
                     />
                <div class="help-inline">
                    <span class="Validform_checktip"></span>
                </div>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">*<@spring.message 'usercenter.userinfor.password' />:</label>
            <div class="controls">
                <input id="loginPassword" type="password" name="loginPassword" datatype="*"
                    nullmsg="<@spring.message 'usercenter.userinfor.infor_null' /><@spring.message 'usercenter.userinfor.password' /><@spring.message 'usercenter.userinfor.infor_null_end' />" />
                <div class="help-inline">
                    <span class="Validform_checktip"></span>
                </div>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label"></label>
            <div class="controls">
                <label class="checkbox inline">
                    <input type="checkbox" name="rememberme" />
                    <span><@spring.message 'basic.title238' /></span>
                    |
                    <a href="<@spring.url '/member/findpassword.htm' />">
                        <span class="register-grey-text"> <@spring.message 'usercenter.userinfor.forgetPassword' /> </span>
                    </a>
                </label>
                <div class="help-inline">
                    <span class="Validform_checktip"></span>
                </div>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label"> </label>
            <div class="controls">
                <button class="btn btn-primary" id="buttonLoginBtn"><@spring.message 'basic.title84' /></button>
            </div>
        </div>
    </form>
</div>
</@standard.page_body> </@standard.html>
