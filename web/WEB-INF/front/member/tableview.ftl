<#import "../layouts/common_standard.ftl" as standard>

<#escape x as x?html>

<@standard.html >

<@standard.page_header "桌位图">
	<@standard.css>		
	</@standard.css>
  	<@standard.javascript>
  		<script src="<@spring.url '/front/js/plugin/jquery.svgView.js'/>" type="text/javascript"></script>
  	</@standard.javascript>
</@standard.page_header>

<@standard.page_body "floorplan" false>

<div class="seat-map" id="seatMap">
	<div id="svgDiv" ></div>
</div>

<script type="text/javascript" >
	$(document).ready(function() {
		changefloorImg();
	});
	
	function changefloorImg(){
		$.ajax({url:'/api/reservation/viewtable/${key}' , success:function(data){
			if(data.imgPath && data.tables){
				var imgPath ="<@helper.imageUri "/" />"+data.imgPath;
				var tables = data.tables;
				if(tables){
					tables = tables.substring(tables.indexOf("|")+1);
					if(tables.indexOf(",")>-1){
						tables= tables.split(",");
					}else{
						tables = [tables];
					}
				}
				$("#svgDiv").floorplan({imgPath:imgPath ,selected:tables ,disabled:true});
			}
		}});
	}
</script>

</@standard.page_body>
	 
</@standard.html>

</#escape>
 