<#import "../layouts/common_standard.ftl" as standard> <#escape x as x?html> <@standard.html > <@standard.page_header "修改密码"> <@standard.css></@standard.css> 
<@standard.javascript>
<script type="text/javascript">
$(document).ready(function(){
	 $("#resetpasswordForm").Validform({
         btnSubmit : "#resetPasswordBtn",ignoreHidden : true,showAllError : true,tiptype : 4,
         beforeSubmit : function(){
        	 $.post("/api/member/resetpassword" , $("#resetpasswordForm").serialize() , function(data){
        		 alert("密码修改成功");
        		 location.href  = "/";
        	 });
        	 return false;
         }
     });
});
</script>
</@standard.javascript>
</@standard.page_header> <@standard.page_body body_class="handle-password" bodySlider=false rightbox="">
<div class="container-fluid reset-password">
    <form class="form-horizontal" method="post"  id="resetpasswordForm">
        <legend><@spring.message 'basic.title200' /></legend>
        <div class="control-group">
            <label class="control-label">*<@spring.message 'basic.title201' />:</label>
            <div class="controls">
                <input id="regloginPassword" type="password" name="loginPassword" datatype="*6-18"
                    nullmsg="<@spring.message 'usercenter.userinfor.infor_null' /><@spring.message 'usercenter.userinfor.password' /><@spring.message 'usercenter.userinfor.infor_null_end' />"
                    errormsg="<@spring.message 'usercenter.userinfor.password' /><@spring.message 'usercenter.userinfor.infor_length' />" />
                <span class="Validform_checktip"> <@spring.message 'usercenter.userinfor.password' /> <@spring.message 'usercenter.userinfor.infor_length' /> </span>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">*<@spring.message 'basic.title202' />:</label>
            <div class="controls">
                <input id="regloginPassword2" type="password" name="regloginPassword2" recheck="loginPassword" datatype="*6-18"
                    nullmsg="<@spring.message 'usercenter.userinfor.infor_ensure' /><@spring.message 'usercenter.userinfor.password' /><@spring.message 'usercenter.userinfor.infor_null_end' />"
                    errormsg="<@spring.message 'usercenter.userinfor.password_re_error' />" />
                <span class="Validform_checktip"> </span>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label"></label>
            <div class="controls">
                <button class="btn btn-primary" id="resetPasswordBtn"><@spring.message 'basic.title58' /></button>
            </div>
        </div>
    </form>
</div>
</@standard.page_body> </@standard.html > </#escape>
