<#import "../layouts/common_standard.ftl" as standard> <@standard.html > <@standard.page_header "忘记密码"> <@standard.css> </@standard.css> <@standard.javascript>
<script type="text/javascript">

function change(s) {
	s.attr("src", "<@spring.url '/api/public/base/code.htm' />");
}

$(document).ready(function() {
	$("#loginName").placeholder();
	$("#vcimg").click(function() {
		change($(this));
	});
	$("#verifyCode,#loginName").keydown(function(e) {
		if (e.keyCode == 13) {
			$("#verifyCode,#loginName").blur();
			$("#findpasswordBtn").click();
		}
	});
	$("#findpasswordForm").Validform({ btnSubmit : "#findpasswordBtn", ignoreHidden : true, showAllError : true, tiptype : 4 ,
		beforeSubmit : function() {
			$.post("/api/member/findpassword" ,$("#findpasswordForm").serialize(), function(data){
 				if(data.status=="y")
					$("#fp-div-message").text("一封邮件已经发送到您的注册邮箱");
				else
					$("#fp-div-message").text("请输入正确的邮箱地址或手机号");
				resetForm();
				change($('#vcimg'));
			});
			return false;
		}
	});
});

	
</script>
</@standard.javascript> </@standard.page_header> <@standard.page_body body_class="forget" bodySlider=false rightbox="">
<div class="container-fluid">
    <form class="form-horizontal form-findpassword" method="post"  id="findpasswordForm">
        <legend><@spring.message 'basic.title203' /></legend>
        <div class="control-group" >
            <label class="control-label"></label>
            <div class="controls" style="color: red;" id="fp-div-message">
                    
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">*<@spring.message 'usercenter.userinfor.loginName' />:</label>
            <div class="controls">
                <input id="loginName" type="text" name="loginName" datatype="*" placeholder="<@spring.message 'usercenter.userinfor.loginName.placeholder' />"
                    nullmsg="<@spring.message 'usercenter.userinfor.infor_null' /><@spring.message 'usercenter.userinfor.loginName' /><@spring.message 'usercenter.userinfor.infor_null_end' />"
                    errormsg="<@spring.message 'usercenter.userinfor.infor_error_had' /><@spring.message 'usercenter.userinfor.loginName' /><@spring.message 'usercenter.userinfor.infor_error_had_used' />" />
                <div class="help-inline">
                    <span class="Validform_checktip"><@spring.message 'usercenter.userinfor.loginName' /><@spring.message 'usercenter.userinfor.infor_length' /></span>
                </div>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">*<@spring.message 'basic.title205' />:</label>
            <div class="controls">
                <input id="verifyCode" type="text" name="verifyCode" class="input-small" datatype="s4-4"
                    ajaxurl="<@spring.url '/api/member/validate/judgexist/code'/>" nullmsg="<@spring.message 'basic.title206' />"
                    errormsg="<@spring.message 'basic.title207' />" />
                <img src="<@spring.url '/api/public/base/code.htm' />" id="vcimg" />
                <@spring.message 'basic.title208' />
                <a style="width: 40px;" href="javascript:change($('#vcimg'))"><@spring.message 'basic.title209' /></a>
                <div class="help-inline">
                    <span class="Validform_checktip"><@spring.message 'basic.title206' /></span>
                </div>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label"> </label>
            <div class="controls">
                <button class="btn btn-primary" id="findpasswordBtn"><@spring.message 'basic.title89' /></button>
            </div>
        </div>
    </form>
    <div id="find-password" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="fp-confirm-title" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 id="fp-confirm-title"><@spring.message 'basic.title210' /></h4>
        </div>
        <div class="modal-body">
            <p>
                <@spring.message 'usercenter.userinfor.nickName' />：
                <span id="fp-nickname"></span>
            </p>
            <p>
                <@spring.message 'basic.title213' />：
                <span id="fp-email"></span>
            </p>
        </div>
        <div class="modal-footer">
            <span style="padding-right: 30px;display: none;" id="fp-span-message">一封邮件已经发送到您的注册邮箱</span>
            <button class="btn" data-dismiss="modal" aria-hidden="true"><@spring.message "basic.title20" /></button>
            <button id="inte-submit-button" class="btn btn-primary"><@spring.message 'basic.title214' /></button>
        </div>
    </div>
</div>
</@standard.page_body> </@standard.html >
