<#import "../../layouts/common_standard.ftl" as standard><@standard.html > <@standard.page_header "我的联系人"> <@standard.css> </@standard.css> <@standard.javascript>
</@standard.javascript> </@standard.page_header> <@standard.member_body "contacts" true >
<div class="container-fluid">
    <p class="lead">
        <@spring.message 'usercenter.userinfor.mycontact' />
        <a class="btn btn-mini btn-primary" href="<@spring.url '/myaccount/user/contactedit.htm'/>"> <@spring.message 'usercenter.userinfor.contact_add' /> </a>
    </p>
    <div class="row-fluid contacts">
    </div>
    <p class="text-center noresults"><@spring.message 'form.member.list.1' /></p>
</div>
<script type="text/template" id="template-contacts">
<div class="span3">
	<div class="well well-small">
		<p>
			<@spring.message 'index.header.contact_name' />:
			<% if(GlobalVariable.language=="1"){ %><%=item.name %><%}%>
			<% if(item.sexe==0){ %>
				<@spring.message 'basic.title121' />
			<%}else{%>
				<@spring.message 'basic.title122' />
			<%}%>
			<% if(GlobalVariable.language=="2"){ %><%=item.name %><%}%>
		</p>
		<p>
			<@spring.message 'index.header.contact_phone' />:
			<%=item.telphone %>
		</p>
		<p>
			<@spring.message 'index.header.contact_email' />:
			<%=item.email %>
		</p>
		<div class="option">
			<a class="btn btn-mini" href="<@spring.url '/' />myaccount/user/contactedit.htm?contactId=<%=item.contactId %>">
				<@spring.message 'usercenter.userinfor.contact_edit' />
			</a>
			<button class="btn btn-mini" onclick="deleteContact('<%=item.contactId %>')">
				<@spring.message 'usercenter.userinfor.contact_del' />
			</button>
		</div>
	</div>
</div>
</script>
</@standard.member_body> </@standard.html >
