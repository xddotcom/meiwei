<#import "../../layouts/common_standard.ftl" as standard> <@standard.html > <@standard.page_header "我的订单"> <@standard.css></@standard.css> <@standard.javascript> </@standard.javascript>
</@standard.page_header> <@standard.member_body "orders" true >
<div class="container-fluid">
    <p class="lead"><@spring.message 'usercenter.userinfor.order' /></p>
    <div>
        <div class="input-prepend">
            <span class="add-on"><@spring.message 'basic.title288' /></span>
            <select id="date-search-select" search-key="month"></select>
        </div>
    </div>
       <div style="display: none;">
        <p id="basic-title59"><@spring.message 'basic.title59' /></p>
       </div>
    <div class="row-fluid orders">
        
    </div>
    <script type="text/template" id="template-orders">
        <div class="span4">
            <div class="well well-small">
                <p><@spring.message 'basic.title48' />： <%=item.orderNo%></p>
                <p>
                    <@spring.message 'basic.title292' />：
                    <a href="<@spring.url '/'  />restaurant/view/<%=item.restaurant.restaurantId%>"  target="_blank"><%=item.restaurant.fullName.text%></a>
                </p>
                <p><@spring.message 'basic.title293' />：<%=item.orderDate.substring(0,11)%><%=item.orderTime.substring(11,16)%> <%=item.personNum%><@spring.message 'basic.title23' /></p>
                <p>
                    <@spring.message 'basic.title51' />：
                    <a href="<@spring.url '/restaurant/viewtable/' /><%=item.orderNo%>" target="_blank"> <@spring.message 'basic.title220' />
                    </a>
                 </p>
                <p><@spring.message 'basic.title52' />：
                <% var _message = {"0":"<@spring.message 'order.status.0' />","10":"<@spring.message 'order.status.10' />","20":"<@spring.message 'order.status.20' />",
                    "50":"<@spring.message 'order.status.50' />" ,"96":"<@spring.message 'order.status.96' />","97":"<@spring.message 'order.status.97' />",
                    "98":"<@spring.message 'order.status.98' />","99":"<@spring.message 'order.status.99' />"};%>
                <% if(item.status < 20  || item.status >= 50){ %><%=_message[item.status]%><%}else{%><@spring.message 'order.status.50' /><%}%></p>
                <p>
                    <% if( item.status==0 || item.status==10){ %><@spring.message 'basic.title65' />： <%}else{%> <@spring.message 'basic.title53' />： <%}%> 
                    <% if( item.status==0 || item.status==10){ %>
                        <a href="javascript:void(0);" onclick="doDelete('<%=item.orderId%>');"> <@spring.message 'basic.title68' /> </a>
                        <% if( item.status==0 && item.inviteFriends=="" ){ %>
                        <a href="javascript:void(0);" onclick="show(<%=item.orderId%>)"> <@spring.message 'basic.title242' /> </a>
                        <%}%>
                    <%}else{%>
                        <span><%=item.consumeAmount%></span>
                    <%}%>
                </p>
                <p>
                    <@spring.message 'basic.title96' />：
                    <% if(item.creditGain < 0){ %>
                    <span class="label label-important">X <%=(-item.creditGain)%></span>
                    <%}else{%>
                    <span><%=item.creditGain%></span>
                    <%}%>
                </p>
                <div class="accordion" id="accordion2_<%=item.orderId%>">
                    <% if(item.other != ""){ %>
                    <div class="accordion-group">
                        <div class="accordion-heading">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2_<%=item.orderId%>" href="#collapseOne_<%=item.orderId%>">
                                <h4><@spring.message "restaurant.order.remark" /><small>(点击查看)</small></h4>
                            </a>
                        </div>
                        <div id="collapseOne_<%=item.orderId%>" class="accordion-body in collapse" style="height: auto;">
                            <div class="accordion-inner" style="overflow: hidden; text-align: left; color: #FF0000"><%=_.escape(item.other)%></div>
                        </div>
                    </div>
                   <%}%>
                    <% if(member.memberType==0 && item.adminother!= ""){ %>
                    <div class="accordion-group">
                        <div class="accordion-heading">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2_<%=item.orderId%>" href="#collapseTwo_<%=item.orderId%>">
                                <h4><@spring.message "restaurant.order.remark"/><small>(点击查看)</small></h4>
                            </a>
                        </div>
                        <div id="collapseTwo_<%=item.orderId%>" class="accordion-body collapse" style="height: 0px;">
                            <div class="accordion-inner" style="overflow: hidden; text-align: left; color: #FF0000"><%=_.escape(item.adminother)%></div>
                        </div>
                    </div>
                    <%}%>
                </div>
            </div>
        </div>       
      </script>
    <p class="noresults"><@spring.message 'form.member.list.4' /></p>
</div>
<div id="invite-modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="invite-friends-title" aria-hidden="true">
    <div class="modal-body">
        <div class="row-fluid">
            <div class="span7">
                <form class="form-horizontal">
                    <legend><@spring.message 'basic.title243' /></legend>
                    <input type="hidden" name="orderId" id="orderId" />
                    <div class="well well-small" id="select-invite">
                        <!-- Backbone Fills Me -->
                    </div>
                    <div class="help-block"></div>
                    <button class="btn" data-dismiss="modal" aria-hidden="true"><@spring.message "basic.title20" /></button>
                    <div class="btn btn-primary" onclick="inviteFriendsPost();"><@spring.message "basic.title58" /></div>
                </form>
            </div>
            <div class="span5">
                <form method="post" id="new-invite-form" name="new-invite-form">
                    <input type="hidden" name="contactId" />
                    <label><@spring.message 'index.header.contact_name' />:</label>
                    <input class="input-small" id="nickName" type="text" name="name" datatype="*1-20" />
                    <select class="input-small" name="sexe" id="sexSelect">
                        <option value="0"><@spring.message 'basic.title121' /></option>
                        <option value="1"><@spring.message 'basic.title122' /></option>
                    </select>
                    <label class="add-on">*<@spring.message 'usercenter.userinfor.mobile' />:</label>
                    <input id="telphone" type="text" name="telphone" datatype="m" />
                    <label><@spring.message 'usercenter.userinfor.email' />:</label>
                    <input id="email" type="text" name="email" datatype="e" ignore="ignore" />
                    <div class="btn btn-primary" onclick="addInvite();"><@spring.message "liaison.title7" /></div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="roll-modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="rollModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="rollModalLabel">5月18日至6月1日下单赢取多倍积分</h3>
    </div>
    <div class="modal-body">
        <div style="height: 200px;">
            <p id="roll" class="text-center" style="font-size: 150px; line-height: 150px;"></p>
        </div>
    </div>
    <div class="modal-footer">
        <button class="btn btn-primary btn-block" data-dismiss="modal" aria-hidden="true" id="close-roll" style="display: none;">关闭</button>
        <button class="btn btn-primary btn-block" id="stop-roll">试试手气，点击停止!</button>
    </div>
</div>
</@standard.member_body> </@standard.html >
