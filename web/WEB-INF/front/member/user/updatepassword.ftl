 <#import "../../layouts/common_standard.ftl" as standard> <#escape x as x?html> <@standard.html > <@standard.page_header "修改密码"> <@standard.css> </@standard.css> <@standard.javascript>
</@standard.javascript> </@standard.page_header> <@standard.member_body "handle-password" false>
<div class="container-fluid handle-password">
    <form class="form-horizontal" method="post" id="handlepasswordForm" name="handlepasswordForm">
        <legend><@spring.message 'basic.title233' /></legend>
         <div class="control-group" style="display: none;color: red;" id="fp-div-message">
            <label class="control-label"></label>
            <div class="controls">
                    密码修改成功
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">*<@spring.message 'basic.title234' />:</label>
            <div class="controls">
                <input type="password" name="loginPassword" datatype="*"
                    nullmsg="<@spring.message 'usercenter.userinfor.infor_null' /><@spring.message 'basic.title234' /><@spring.message 'usercenter.userinfor.infor_null_end' />" errormsg="" />
                <span class="Validform_checktip"> </span>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">*<@spring.message 'basic.title201' />:</label>
            <div class="controls">
                <input id="newPassword" type="password" name="newPassword" datatype="*6-18"
                    nullmsg="<@spring.message 'usercenter.userinfor.infor_null' /><@spring.message 'usercenter.userinfor.password' /><@spring.message 'usercenter.userinfor.infor_null_end' />"
                    errormsg="<@spring.message 'usercenter.userinfor.password' /><@spring.message 'usercenter.userinfor.infor_length' />" />
                <span class="Validform_checktip"><@spring.message 'usercenter.userinfor.password' /><@spring.message 'usercenter.userinfor.infor_length' /></span>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">*<@spring.message 'basic.title202' />:</label>
            <div class="controls">
                <input id="regloginPassword2" type="password" name="regloginPassword2" recheck="newPassword" datatype="*6-18"
                    nullmsg="<@spring.message 'usercenter.userinfor.infor_ensure' /><@spring.message 'usercenter.userinfor.password' /><@spring.message 'usercenter.userinfor.infor_null_end' />"
                    errormsg="<@spring.message 'usercenter.userinfor.password_re_error' />" />
                <span class="Validform_checktip"> </span>
            </div>
        </div>
        <div class="control-group">
            <div class="controls">
                <button class="btn btn-primary" id="updatePasswordBtn">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <@spring.message 'basic.title58' /> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</button>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">
	$(function() {
		$("#handlepasswordForm").Validform({
			btnSubmit : "#updatePasswordBtn", ignoreHidden : true, showAllError : true, tiptype : 4 ,
			beforeSubmit : function() {
				$.post("/api/myaccount/user/updatepassword.htm" ,$("#handlepasswordForm").serialize(), function(data){
					$("#fp-div-message").css("display","block");
					resetForm();
				});
				return false;
			}
		});
	});
</script>
</@standard.member_body> </@standard.html > </#escape>
