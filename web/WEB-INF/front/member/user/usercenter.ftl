<#import "../../layouts/common_standard.ftl" as standard>

<@standard.html >

<@standard.page_header "会员中心">
	<@standard.css>
	</@standard.css>
  	<@standard.javascript>
   		<script type="text/javascript">
			$(document).ready(function() {
				shareWeibo(
					"#share-invitation",
					"/member/register.htm?code=${_LoginMember.personalInfor.memberNo}", 
					"#分享邀请码#独乐乐不如众乐乐，快来分享我的邀请码，和我一起成为美位会员吧！愿与你齐聚美位，成为幸福的美食达人〜");
				
			});
			
		</script>
  	</@standard.javascript>
</@standard.page_header>

<@standard.member_body "usercenter" false>
 
<div class="container-fluid getstarted">
	<p class="lead">
		<@spring.message 'basic.title256' />
		(<@spring.message 'basic.title257' />:<strong class="credit"></strong>)：
	</p>
	
	<table class="table">
		<tbody>
			<tr>
				<td><i class="icon-chevron-right icon-large"></i></td>
				<td>1</td>
				<td><@spring.message 'basic.title258' /></td>
 				<td id="td-completed"></td>
			</tr>
			
			<tr>
				<td rowspan="4"><i class="icon-chevron-right icon-large"></i></td>
				<td rowspan="4">2</td>
				<td><@spring.message 'basic.title260' /></td>
				<td rowspan="4"><a href="<@spring.url '/restaurant/search/quick.htm' />"><@spring.message 'basic.title264' />&gt;&gt;</a></td>
			</tr>
			<tr><td><@spring.message 'basic.title261' /></td></tr>
			<tr><td><@spring.message 'basic.title262' /></td></tr>
			<tr><td><@spring.message 'basic.title263' /></td></tr>
			
			<tr>
				<td><i class="icon-chevron-right icon-large"></i></td>
				<td>3</td>
				<td>
					<@spring.message 'basic.title265' />
					<span style="color: #FF0000;" class="member-no"></span>
					<@spring.message 'basic.title266' />
				</td>
				<td>
					<p><@spring.message 'basic.title267' /></p>
					<div id="share-invitation" style="height:25px;margin-top:5px;"></div>
				</td>
			</tr>
			
		</tbody>
	</table>
</div>

</@standard.member_body>

</@standard.html >
