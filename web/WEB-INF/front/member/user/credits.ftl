<#import "../../layouts/common_standard.ftl" as standard><@standard.html > <@standard.page_header "我的积分"> <@standard.css></@standard.css> <@standard.javascript></@standard.javascript>
</@standard.page_header> <@standard.member_body "credits" true >
<div class="container-fluid">
    <p class="lead"><@spring.message 'basic.title278' /></p>
    <div class="well well-small ">
        <div class="row-fluid">
            <h4><@spring.message 'basic.title279' /></h4>
            <div class="control-group" style="float: left;">
                <span><@spring.message 'basic.title269' /> : </span>
                <span>
                    <strong id="strong-credit"></strong>
                </span>
            </div>
            <div class="control-group" style="float: right;">
                <span>
                    <a class="btn" href="<@spring.url '/'  />myaccount/user/integral.htm"><@spring.message 'basic.title280' /></a>
                </span>
                <span>
                    <a href="<@spring.url '/'  />myaccount/user/usercenter.htm"><@spring.message 'basic.title281' />>></a>
                </span>
            </div>
        </div>
    </div>
    <table class="table table-bordered credits">
        <tr>
            <th><@spring.message 'basic.title282' /></th>
            <th><@spring.message 'basic.title283' /></th>
            <th><@spring.message 'basic.title284' /></th>
            <th><@spring.message 'basic.title285' /></th>
        </tr>
    </table>
    <script type="text/template" id="template-credits">
     <tr>
        <td class="happend-time"><%=item.happendTime.substring(0,10)%></td>
		<% 
			var creditType = "";
			if(item.creditType==1){
				creditType="成功注册";
			}else if(item.creditType==2){
				creditType="订单完成";
			}else if(item.creditType==3){
				creditType="双倍积分";
			}else if(item.creditType==4){
				creditType="+100积分";
			}else if(item.creditType==5){
				creditType="+100积分";
			}else if(item.creditType==6){
				creditType="礼品兑换";
			}
		%>
        <td class="credit-type">
            <%=creditType%>
        </td>
        <td >
		<%=item.reason%>
        </td> 
        <td class="amount"><%if(item.amount>0){%>+<%=Math.abs(item.amount)%><%}else{%><%=Math.abs(item.amount)%><%}%></td>
    </tr>
	</script>
    <p class="text-center noresults">您还未兑换礼品哦！</p>
</div>
<div class="usercenter-content-item-bottom"></div>
</@standard.member_body> </@standard.html >
