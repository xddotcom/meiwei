<#import "../../layouts/common_standard.ftl" as standard> <@standard.html > <@standard.page_header "个人资料"> <@standard.css></@standard.css> <@standard.javascript>
<script type="text/javascript" src="<@spring.url '/front/js/jquery/jquery.jSelectDate.js'/>"></script>
</@standard.javascript> </@standard.page_header> <@standard.member_body "userinfor" false>
<div class="container-fluid useredit">
    <form class="form-horizontal" action="<@spring.url '/api/myaccount/user/profile/save.htm'/>" method="post" id="registerForm" name="registerForm">
        <legend><@spring.message 'usercenter.userinfor.information' /></legend>
        <div class="control-group" style="display: none;">
            <label class="control-label">*<@spring.message 'usercenter.userinfor.loginName' />:</label>
            <div class="controls">
                <input id="loginName" type="text" name="loginName" datatype="/^[a-zA-Z0-9]{4,12}$/" shoudDecode="true"
                    nullmsg="<@spring.message 'usercenter.userinfor.infor_null' /><@spring.message 'usercenter.userinfor.loginName' /><@spring.message 'usercenter.userinfor.infor_null_end' />"
                    errormsg="<@spring.message 'form.register.label.2' />" />
                <span class="Validform_checktip"> <@spring.message 'form.register.label.2' /> </span>
            </div>
        </div>
        <div class="control-group" style="display: none;">
            <label class="control-label">*<@spring.message 'usercenter.userinfor.email' />:</label>
            <div class="controls">
                <input id="email" type="text" name="email" datatype="eom" shoudDecode="true" nullmsg="手机与邮箱至少填写一项" errormsg="<@spring.message 'form.register.label.3' />" />
                <span class="Validform_checktip"> </span>
            </div>
        </div>
        <div class="control-group" style="display: none;">
            <label class="control-label">*<@spring.message 'usercenter.userinfor.mobile' />:</label>
            <div class="controls">
                <input id="mobile" type="text" name="mobile" datatype="m" ignore="ignore"
                    errormsg="<@spring.message 'usercenter.userinfor.infor_error' /><@spring.message 'usercenter.userinfor.infor_null' /><@spring.message 'usercenter.userinfor.mobile' /><@spring.message 'usercenter.userinfor.infor_null_end' />" />
                <span class="Validform_checktip"> </span>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label"><@spring.message 'usercenter.userinfor.nickName' />:</label>
            <div class="controls">
                <input id="nickName" type="text" name="nickName" datatype="*1-20" ignore="ignore"
                    nullmsg="<@spring.message 'usercenter.userinfor.infor_null' /><@spring.message 'usercenter.userinfor.nickName' /><@spring.message 'usercenter.userinfor.infor_null_end' />"
                    errormsg="<@spring.message 'usercenter.userinfor.infor_error' />" />
                <span class="Validform_checktip"> </span>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label"><@spring.message 'basic.title120' />:</label>
            <div class="controls">
                <label class="radio">
                    <input type="radio" name="sexe" id="sex1" checked="checked" value="0">
                    <@spring.message 'basic.title121' />
                </label>
                <label class="radio">
                    <input type="radio" name="sexe" id="sex2" value="1">
                    <@spring.message 'basic.title122' />
                </label>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label"><@spring.message 'usercenter.userinfor.birthday' />:</label>
            <div class="controls">
                <input style="display: none;" type="text" id="birthday" name="birthday" class="date" />
            </div>
        </div>
        <input id="anniversary" type="hidden" name="anniversary" />
        <div class="field">
            <label class="control-label"><@spring.message 'usercenter.userinfor.anniversary' />1:</label>
            <div class="controls">
                <input type="text" class="date" id="date1" style="display: none;" />
                <select name="Input_Sary1" id="Input_Sary1" style="width: 138px;" onchange="setReValue()">
                    <option value=""><@spring.message 'basic.title75' /></option>
                    <option value="1"><@spring.message 'basic.title237' /></option>
                    <option value="2"><@spring.message 'basic.title76' /></option>
                    <option value="3"><@spring.message 'basic.title77' /></option>
                    <option value="4"><@spring.message 'basic.title78' /></option>
                    <option value="5"><@spring.message 'basic.title79' /></option>
                    <option value="6"><@spring.message 'basic.title80' /></option>
                    <option value="7"><@spring.message 'basic.title81' /></option>
                </select>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label"><@spring.message 'usercenter.userinfor.anniversary' />2:</label>
            <div class="controls">
                <input type="text" class="date" id="date2" style="display: none;" />
                <select name="Input_Sary2" id="Input_Sary2" style="width: 138px;" onchange="setReValue()">
                    <option value=""><@spring.message 'basic.title75' /></option>
                    <option value="1"><@spring.message 'basic.title237' /></option>
                    <option value="2"><@spring.message 'basic.title76' /></option>
                    <option value="3"><@spring.message 'basic.title77' /></option>
                    <option value="4"><@spring.message 'basic.title78' /></option>
                    <option value="5"><@spring.message 'basic.title79' /></option>
                    <option value="6"><@spring.message 'basic.title80' /></option>
                    <option value="7"><@spring.message 'basic.title81' /></option>
                </select>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">
                <a href="<@spring.url '/myaccount/user/handlepassword.htm'/>"><@spring.message 'basic.title233' /></a>
            </label>
            <div class="controls">
                <button class="btn btn-primary" id="buttonRegDiv">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <@spring.message 'usercenter.userinfor.save' /> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</button>
            </div>
        </div>
    </form>
</div>
</@standard.member_body> </@standard.html >
