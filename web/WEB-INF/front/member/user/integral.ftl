<#import "../../layouts/common_standard.ftl" as standard> <@standard.html> <@standard.page_header "礼品兑换"> <@standard.css></@standard.css> <@standard.javascript>
<script type="text/javascript" src="<@spring.url '/front/js/jquery/popImage/jquery.popImage.mini.js' />"></script>
</@standard.javascript> </@standard.page_header> <@standard.member_body "integral" false >
<div class="container-fluid">
    <p class="lead"><@spring.message 'basic.title268' /></p>
    <div class="row-fluid">
        <div class="span9 integral">
            <div class="row-fluid "></div>
        </div>
        <div class="span3">
            <div class="well well-small">
                <p>
                    <span><@spring.message 'basic.title269' /></span>
                    <span class="label label-success"></span>
                </p>
                <small>
                    <a href="<@spring.url '/' />myaccount/user/usercenter.htm"> <@spring.message 'basic.title270' /> </a>
                </small>
                |
                <small>
                    <a href="<@spring.url '/' />myaccount/user/credits.htm"> <@spring.message 'basic.title271' /></a>
                </small>
            </div>
            <div class="well well-small">
                <div id="inte-table-right">
                    <table class="table">
                        <thead>
                            <tr>
                                <th><@spring.message 'basic.title272' /></th>
                                <th></th>
                                <th><@spring.message 'basic.title273' /></th>
                            </tr>
                        </thead>
                        <tbody id="integral-tbody">
                        </tbody>
                        <tbody>
                            <tr>
                                <td></td>
                                <td><@spring.message 'basic.title289' /></td>
                                <td>
                                    <span class="integral-total">0</span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="well well-small">
                <form method="post" name="integral-form" id="integral-form"  >
                    <legend><@spring.message 'basic.title274' /></legend>
                    <div class="input-prepend">
                        <div id="div-integral-hidden"></div>
                        <span class="add-on"><@spring.message 'basic.title275' /></span>
                        <input class="input-medium" type="text" datatype="*1-20" name="name" id="input-name"  />
                    </div>
                    <div class="input-prepend">
                        <span class="add-on"><@spring.message 'basic.title276' /></span>
                        <input class="input-medium" type="text" datatype="m" name="telphone" id="input-telphone"  />
                    </div>
                    <div class="input-prepend">
                        <span class="add-on" style="height: 40px;"><@spring.message 'basic.title277' /></span>
                        <textarea class="input-medium" rows="2" cols="20" datatype="*1-200" name="address" id="input-address">上海</textarea>
                    </div>
                    <div class="control-group">
                        <div class="controls">
                            <button class="btn btn-primary" id="integral-submit">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <@spring.message 'basic.title58' /> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
<script type="text/template" id="template-integral">
<div class="span4">
	<div class="well well-small integraluse clearfix"
		product-id="<%=item.productId%>" product-cost="<%=item.credit%>" 
		product-name="<%=item.productName.text%>">
		<div class="product-picture" style="max-width:100%;max-height:250px;">
			<a href="<@helper.imageUri '/' /><%=item.picturePath%>" class="pop-image">
				<img class="small-pictrue" src="<@helper.imageUri '/' /><%=item.picturePath%>" alt="">
			</a>
		</div>
		<div class="product-text">
			<p><strong class="name"><%=item.productName.text%></strong> <small><%=item.model.text%></small></p>
			<p><span><@spring.message 'basic.title290' />: </span><span class="cost"><%=item.credit%></span></p>
			<div class="input-prepend input-append input-pend-small">
				<a class="add-on btn minus-a-tag" href="javascript:void(0);"><i class="icon-minus"></i></a>
				<input class="input-mini text-center uneditable-input" type="text" value="0"/>
				<a class="add-on btn plus-a-tag" href="javascript:void(0);"><i class="icon-plus"></i></a>
			</div>
		</div>
	</div>
</div>
</script>
    <div class="hidden" id="hidden-text"><@spring.message 'basic.title246' /></div>
    <div class="usercenter-content-item-bottom"><@spring.message 'basic.title241' /></div>
    <div id="integral-confirm" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="inte-confirm-title" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 id="inte-confirm-title"><@spring.message 'basic.title241' /></h4>
        </div>
        <div class="modal-body">
            <h4><@spring.message 'basic.title274' /></h4>
            <div id="inte-box-detail"></div>
            <h4><@spring.message 'basic.title274' /></h4>
            <p>
                <@spring.message 'basic.title275' />：
                <span id="inte-name"></span>
            </p>
            <p>
                <@spring.message 'basic.title276' />：
                <span id="inte-telphone"></span>
            </p>
            <p>
                <@spring.message 'basic.title277' />：
                <span id="inte-address"></span>
            </p>
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true"><@spring.message "basic.title20" /></button>
            <button id="inte-submit-button" class="btn btn-primary" ><@spring.message "basic.title58" /></button>
        </div>
    </div>
    </@standard.member_body> </@standard.html >