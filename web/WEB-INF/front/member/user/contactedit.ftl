<#import "../../layouts/common_standard.ftl" as standard> <@standard.html > <@standard.page_header "修改联系人"> <@standard.css></@standard.css> <@standard.javascript></@standard.javascript>
</@standard.page_header> <@standard.member_body "contacts" >
<div class="container-fluid contact-edit">
    <form class="form-horizontal" action="<@spring.url '/api/myaccount/user/contact/save.htm'/>" method="post" id="contactEditForm" name="contactEditForm">
        <legend><@spring.message 'usercenter.userinfor.edit' /></legend>
        <input type="hidden" name="contactId" id="contactId" />
        <div class="control-group">
            <label class="control-label">*<@spring.message 'usercenter.userinfor.mobile' />:</label>
            <div class="controls">
                <input id="telphone" type="text" name="telphone" datatype="m"
                    nullmsg="<@spring.message 'usercenter.userinfor.input_null'/><@spring.message 'usercenter.userinfor.mobile'/><@spring.message 'usercenter.userinfor.infor_null_end' />"
                    errormsg="<@spring.message 'usercenter.userinfor.infor_error' /> " />
                <div class="help-inline">
                    <span class="Validform_checktip"></span>
                </div>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">*<@spring.message 'index.header.contact_name' />:</label>
            <div class="controls">
                <input id="nickName" type="text" name="name" datatype="*1-20"
                    nullmsg="<@spring.message 'usercenter.userinfor.input_null'/><@spring.message 'index.header.contact_name' /><@spring.message 'usercenter.userinfor.infor_null_end' />"
                    errormsg="<@spring.message 'usercenter.userinfor.infor_error' />" />
                <div class="help-inline">
                    <span class="Validform_checktip"></span>
                </div>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label"><@spring.message 'basic.title120' />:</label>
            <div class="controls">
                <label class="radio inline">
                    <input type="radio" name="sexe" id="sex1" checked="checked" value="0">
                    <@spring.message 'basic.title121' />
                </label>
                <label class="radio inline">
                    <input type="radio" name="sexe" id="sex2" value="1">
                    <@spring.message 'basic.title122' />
                </label>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label"> <@spring.message 'usercenter.userinfor.email' />: </label>
            <div class="controls">
                <input id="email" type="text" name="email" datatype="e" ignore="ignore"
                    nullmsg="<@spring.message 'usercenter.userinfor.input_null'/><@spring.message 'usercenter.userinfor.email'/><@spring.message 'usercenter.userinfor.infor_null_end' />"
                    errormsg="<@spring.message 'form.register.label.3' />" />
                <div class="help-inline">
                    <span class="Validform_checktip"></span>
                </div>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label"></label>
            <div class="controls">
                <button class="btn btn-primary" id="btnContacteditSubmit"><@spring.message 'usercenter.userinfor.save' /></button>
            </div>
        </div>
    </form>
</div>
</@standard.member_body> </@standard.html >
