<#import "../../layouts/common_standard.ftl" as standard><@standard.html > <@standard.page_header "我的收藏"> <@standard.css> </@standard.css> <@standard.javascript>
</@standard.javascript> </@standard.page_header> <@standard.member_body "favorites" true >
<div class="usercenter-content-item-top">
    <p class="lead"><@spring.message 'usercenter.userinfor.favorite' /></p>
</div>
<div style="height: 88%;">
    <table style="width: 100%" class="favorites">
        <tr class="usercenter-table-title1" style="border-bottom: 1px solid #c9c9c9; height: 20px; text-align: left;">
            <th><@spring.message 'basic.title63' /></th>
            <th><@spring.message 'basic.title64' /></th>
            <th><@spring.message 'basic.title65' /></th>
        </tr>
    </table>
    <p  class="noresults"><@spring.message  'form.member.list.3' /></p>
</div>
<script type="text/template" id="template-favorites">
    <tr class="usercenter-table-content">
      <td style="padding-bottom: 5px;padding-top: 5px;">
        <a
          href="<@spring.url '/'  />restaurant/view/<%=item.restaurant.restaurantId%>"
          target="_blank"><%=item.restaurant.fullName.text%></a>
      </td>
      <td><%=item.favoriteTime%></td>
      <td>
        <a href="javascript:void(0);"  onclick="deleteFavorite('<%=item.favoriteId%>');"><@spring.message  'basic.title66' /></a>
      </td>
    </tr>
</script>
<div class="usercenter-content-item-bottom"></div>
<div class="clear"></div>
</@standard.member_body> </@standard.html >
