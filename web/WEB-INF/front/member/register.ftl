<#import "../layouts/common_standard.ftl" as standard><@standard.html > <@standard.page_header "注册"> <@standard.css></@standard.css> <@standard.javascript>
<script type="text/javascript" src="<@spring.url '/front/js/jquery/jquery.jSelectDate.js'/>"></script>
<script type="text/javascript">
$(document).ready(function () {
 	var qs = new QS;
	if(qs.qs["code"] && qs.qs["code"]=="qiaolian"){
		$("#small-text").text(" (侨联独家邀请，注册成功后即可获得500积分)");
	}else if(qs.qs["code"] && qs.qs["code"]=="tcfa"){
		$("#small-text").text(" (TCFA独家邀请，注册成功后即可获得500积分)");
	}
	
    $("#registerForm").Validform({ btnSubmit: "#buttonRegDiv", ignoreHidden: true, showAllError: true, tiptype: 4, ajaxPost: true,
        callback: function (data) {
            if (data.status && data.status == "y") {
                window.location.href = "<@spring.url '/myaccount/user/usercenter.htm'/>";
                var go = getCookie("QUERY_URL");
                setCookie("QUERY_URL", "", -1);
                if(go && go.indexOf("restaurant/book")>-1){
                    window.location.href = go;
                }
            }
        }
    });

    $("#birthday, #date1, #date2").jSelectDate({
        css: "jselectdate input-small",
        yearBeign: 1955,
        disabled: false,
        isShowLabel: false
    });

    $("<option value='0'><@spring.message 'form.register.label.1' /></option>").prependTo($(".jselectdate[id*=selYear]")).attr("selected", true);
    $("<option value='0'><@spring.message 'basic.title73' /></option>").prependTo($(".jselectdate[id*=selMonth]")).attr("selected", true);
    $("<option value='0'><@spring.message 'basic.title74' /></option>").prependTo($(".jselectdate[id*=selDay]")).attr("selected", true);
    $(".jselectdate").val(0);

    //shareWeibo("#require-invitation", "", "#跪求邀请码#还木有邀请码？！听说杜甫和元芳都有了！跪求哪位大神分享邀请码给我吧，愿以美食作为交换！");
});	
</script>
</@standard.javascript> </@standard.page_header> <@standard.page_body body_class="register" bodySlider=false rightbox="">
<div class="container-fluid register">
    <iframe name='hidden_frame' id="hidden_frame" style='display: none'></iframe>
    <form class="form-horizontal form-register" method="post" action="<@spring.url '/api/member/register'/>" id="registerForm" name="registerForm" target="hidden_frame">
        <legend>
            <@spring.message 'usercenter.userinfor.register_formeiwei' />
            <small id="small-text"></small>
        </legend>
        <div class="control-group" style="display: none">
            <label class="control-label">*<@spring.message 'usercenter.userinfor.invitationCode' />:</label>
            <div class="controls">
                <input id="invitationCode" type="text" name="invitationCode" datatype="*" ajaxurl="<@spring.url '/api/member/validate/judgexist/invitation'/>"
                    nullmsg="<@spring.message 'usercenter.userinfor.infor_null' /><@spring.message 'usercenter.userinfor.invitationCode' />"
                    errormsg="<@spring.message 'usercenter.userinfor.infor_error_had' /><@spring.message 'usercenter.userinfor.invitationCode' /><@spring.message 'usercenter.userinfor.infor_error_had_used' />"
                <#if _INVITATION_CODE??>value="${_INVITATION_CODE!""}" readonly="true"</#if> placeholder="<@spring.message 'basic.title223' />" value="999999999"/>
                <div class="help-inline">
                    <span class="Validform_checktip"><@spring.message 'usercenter.userinfor.infor_length' /></span>
                </div>
            </div>
        </div>
        <div class="control-group" style="display: none">
            <label class="control-label"><@spring.message 'usercenter.userinfor.requireinvitation' />:</label>
            <div class="controls">
                <div id="require-invitation" style="height: 25px;"></div>
            </div>
        </div>
        <div class="control-group" style="display: none;">
            <label class="control-label">*<@spring.message 'usercenter.userinfor.loginName' />:</label>
            <div class="controls">
                <input id="regloginName" type="text" name="loginName" datatype="/^[a-zA-Z0-9]{4,12}$/" shoudDecode="true" ajaxurl="<@spring.url '/api/member/validate/judgexist/member'/>"
                    nullmsg="<@spring.message 'usercenter.userinfor.infor_null' /><@spring.message 'usercenter.userinfor.loginName' />" errormsg="<@spring.message 'form.register.label.2' />" />
                <div class="help-inline">
                    <span class="Validform_checktip"><@spring.message 'form.register.label.2' /></span>
                </div>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">*<@spring.message 'usercenter.userinfor.email' />:</label>
            <div class="controls">
                <input id="email" type="text" name="email" datatype="eom" shoudDecode="true" nullmsg="手机与邮箱至少填写一项" ajaxurl="<@spring.url '/api/member/validate/judgexist/email'/>"
                    errormsg="<@spring.message 'form.register.label.3' />" />
                <div class="help-inline">
                    <span class="Validform_checktip"></span>
                </div>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">*<@spring.message 'usercenter.userinfor.mobile' />:</label>
            <div class="controls">
                <input id="mobile" type="text" name="mobile" datatype="m" ignore="ignore" ajaxurl="<@spring.url '/api/member/validate/judgexist/mobile'/>"
                    errormsg="<@spring.message 'usercenter.userinfor.infor_error' /> , <@spring.message 'usercenter.userinfor.infor_null' /><@spring.message 'usercenter.userinfor.mobile' />" />
                <div class="help-inline">
                    <span class="Validform_checktip"></span>
                </div>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">*<@spring.message 'usercenter.userinfor.password' />:</label>
            <div class="controls">
                <input id="regloginPassword" type="password" name="loginPassword" datatype="*6-18"
                    nullmsg="<@spring.message 'usercenter.userinfor.infor_null' /><@spring.message 'usercenter.userinfor.password' />"
                    errormsg="<@spring.message 'usercenter.userinfor.password' /><@spring.message 'usercenter.userinfor.infor_length' />" />
                <div class="help-inline">
                    <span class="Validform_checktip"><@spring.message 'usercenter.userinfor.infor_length' /></span>
                </div>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">*<@spring.message 'usercenter.userinfor.password_re' />:</label>
            <div class="controls">
                <input id="regloginPassword2" type="password" name="regloginPassword2" recheck="loginPassword" datatype="*6-18"
                    nullmsg="<@spring.message 'usercenter.userinfor.infor_ensure' /><@spring.message 'usercenter.userinfor.password' />"
                    errormsg="<@spring.message 'usercenter.userinfor.password_re_error' />" />
                <div class="help-inline">
                    <span class="Validform_checktip"></span>
                </div>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label"><@spring.message 'usercenter.userinfor.nickName' />:</label>
            <div class="controls">
                <input id="nickName" type="text" name="nickName" datatype="*1-20" ignore="ignore"
                    nullmsg="<@spring.message 'usercenter.userinfor.infor_null' /><@spring.message 'usercenter.userinfor.nickName' />" errormsg="<@spring.message 'usercenter.userinfor.infor_error' />" />
                <div class="help-inline">
                    <span class="Validform_checktip"></span>
                </div>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label"><@spring.message 'basic.title120' />:</label>
            <div class="controls">
                <label class="radio inline">
                    <input type="radio" name="sexe" id="sex1" checked="checked" value="0">
                    <span><@spring.message 'basic.title121' /></span>
                </label>
                <label class="radio inline">
                    <input type="radio" name="sexe" id="sex2" value="1">
                    <span><@spring.message 'basic.title122' /></span>
                </label>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label"></label>
            <div class="controls">
                <p>*<@spring.message 'usercenter.userinfor.Leave' /></p>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label"><@spring.message 'usercenter.userinfor.birthday' />:</label>
            <div class="controls">
                <input type="text" id="birthday" name="birthday" class="date" style="display: none;" />
            </div>
        </div>
        <input id="anniversary" type="hidden" name="anniversary" />
        <div class="control-group anniversary">
            <label class="control-label"><@spring.message 'usercenter.userinfor.anniversary' />1:</label>
            <div class="controls">
                <input type="text" class="date" id="date1" style="display: none;" />
                <select name="Input_Sary1" id="Input_Sary1" onchange="setReValue()">
                    <option value=""><@spring.message 'basic.title75' /></option>
                    <option value="1"><@spring.message 'basic.title237' /></option>
                    <option value="2"><@spring.message 'basic.title76' /></option>
                    <option value="3"><@spring.message 'basic.title77' /></option>
                    <option value="4"><@spring.message 'basic.title78' /></option>
                    <option value="5"><@spring.message 'basic.title79' /></option>
                    <option value="6"><@spring.message 'basic.title80' /></option>
                    <option value="7"><@spring.message 'basic.title81' /></option>
                </select>
            </div>
        </div>
        <div class="control-group anniversary">
            <label class="control-label"><@spring.message 'usercenter.userinfor.anniversary' />2:</label>
            <div class="controls">
                <input type="text" class="date" id="date2" style="display: none;" />
                <select name="Input_Sary2" id="Input_Sary2" onchange="setReValue()">
                    <option value=""><@spring.message 'basic.title75' /></option>
                    <option value="1"><@spring.message 'basic.title237' /></option>
                    <option value="2"><@spring.message 'basic.title76' /></option>
                    <option value="3"><@spring.message 'basic.title77' /></option>
                    <option value="4"><@spring.message 'basic.title78' /></option>
                    <option value="5"><@spring.message 'basic.title79' /></option>
                    <option value="6"><@spring.message 'basic.title80' /></option>
                    <option value="7"><@spring.message 'basic.title81' /></option>
                </select>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label"></label>
            <div class="controls">
                <button class="btn btn-primary" id="buttonRegDiv"><@spring.message 'usercenter.userinfor.register' /></button>
            </div>
        </div>
    </form>
</div>
<!-- .form-wrapper -->
</@standard.page_body> </@standard.html >
