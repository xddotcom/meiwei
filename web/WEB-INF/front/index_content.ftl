<#macro content>

<script type="text/template" id="template-cuisines">
<!-- 菜系 -->
<div class="span1">
	<h4 class="text-right"><@spring.message "index.hot.cook" /></h4>
</div>
<div class="span8">
	<div class="well well-small">
		<ul class="inline">
			<% _.each( cuisines , function( item , key , list ){ %>
				<li <% if(item.isRecommended==0){  %> class="stylesDisable" style="display:none" <%} %> >
					<a href="<@spring.url '/' />restaurant/search/0-0-<%=  item.cuisineId %>-0#C3">
						<%= item.cuisineName.text %>
					</a>
				</li>
			<%});%>
		</ul>
	</div>
</div>
</script>

<script type="text/template" id="template-districts">
<!-- 商圈 -->
<div class="span1">
	<h4 class="text-right"><@spring.message "index.hot.circle" /></h4>
</div>
<div class="span8">
	<div class="well well-small">
		<ul class="inline">
			<% _.each( districts , function( district , key , list ){ %>
				<li><span class="lead"><%= district.districtName.text %></span></li>
				<% _.each( district.circles , function( item , key , list ){ %>
					<li>
						<a href="<@spring.url '/'  />restaurant/search/0-<%=  item.circleId %>-0-0#C3">
							<small><%=  item.circleName.text %></small>
						</a>
					</li>
			<%})});%>
		</ul>
	</div>
</div>
</script>

<script type="text/template" id="template-recommends">
<!-- 推荐 -->
<div class="row">
<% _.each( recommends , function( recommend , key , list ){ %>
	<div class="span3 <% if( key > 0){ %>hidden-phone <% } %>">
		<div class="well well-small">
			<h4 class="text-center">
				<#if (LANGUAGE!"1")="1">美位</#if><%= recommend.ruleName%><#if (LANGUAGE!"1")="1">推荐</#if>&nbsp;&nbsp;&nbsp;
				<small><a href="<@spring.url '/' />restaurant/search/0-0-0-<%= recommend.ruleId%>"><@spring.message "index.recom.more"/></a></small>
			</h4>
 
			<div class="carousel-box" id='carousel-box-<%= recommend.ruleId%>'>
				<% var l = recommend.restaurants.length;%>

				<div class="carousel large slide" data-interval="1000" id="carousel<%= recommend.ruleId%>">
					<div class="carousel-inner">
						<% _.each( recommend.restaurants , function( item , k , list ){ %>
							<div class="item <% if(k + 1 == 1){ %>active<%}%>">
								<a href="<@spring.url '/' />restaurant/view/<%= item.restaurantId%>" target="_blank">
									<img src="<@helper.imageUri '/' /><%= item.frontPic%>" alt="" />
								</a>
								<div class="carousel-caption">
									<small><%= item.shortName.text%></small>
								</div>
							</div>
						<%});%>
					</div>
					<a class="carousel-control left" href="#carousel<%= recommend.ruleId%>" data-slide="prev">&lsaquo;</a>
					<a class="carousel-control right" href="#carousel<%= recommend.ruleId%>" data-slide="next">&rsaquo;</a>
				</div>

				<div class="carousel small slide" data-interval="1000" id="carousel<%= recommend.ruleId%>">
					<div class="carousel-inner">
						<% _.each( recommend.restaurants.slice(0,(l/2-1)).reverse() , function( item , k , list ){ %>
							<div class="item <% if(k + 1 == 1){ %>active<%}%>">
								<a href="<@spring.url '/' />restaurant/view/<%= item.restaurantId%>" target="_blank">
									<img src="<@helper.imageUri '/' /><%= item.frontPic%>" alt="" />
								</a>
								<div class="carousel-caption">
									<small><%= item.shortName.text%></small>
								</div>
							</div>
						<%});%>
					</div>
				</div>
				<div class="carousel small slide" data-interval="1000" id="carousel<%= recommend.ruleId%>">
					<div class="carousel-inner">
						<% _.each( recommend.restaurants.slice(l/2,l-1) , function( item , k , list ){ %>
							<div class="item <% if(k + 1 == 1){ %>active<%}%>">
								<a href="<@spring.url '/' />restaurant/view/<%= item.restaurantId%>" target="_blank">
									<img src="<@helper.imageUri '/' /><%= item.frontPic%>" alt="" />
								</a>
								<div class="carousel-caption">
									<small><%= item.shortName.text%></small>
								</div>
							</div>
						<%});%>
					</div>
				</div>
			</div>
		</div>
	</div>
	<% if(key % 3 == 2){ %>
		</div><div class="row">
	<%}%>
<%});%>
</div>
</script>

</#macro>
