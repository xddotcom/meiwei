var Restaurant = Backbone.Model.extend({
	url: function() { return '/api/restaurant/view/' + this.get('id') + '.htm'; }
});

var Member = Backbone.Model.extend({
	url: function() { return '/api/member/user/detail.htm'; }
});

var RestaurantList = Backbone.Collection.extend({
});

var ContactList = Backbone.Collection.extend({
});

var Cuisine = Backbone.Model.extend({
});

var CuisineList = Backbone.Collection.extend({
	model : Cuisine,
	url : function(){ return "/api/public/base/cuisines.htm?catch=true" }
});

var Circle = Backbone.Model.extend({
});

var CircleList = Backbone.Collection.extend({
	model : Circle,
	url : function(){ return "/api/public/base/circles.htm?catch=true" }
});

var District = Backbone.Model.extend({
});

var DistrictList = Backbone.Collection.extend({
	model : District,
	url : function(){ return "/api/public/base/districts.htm?catch=true" }
});

var Recommend = Backbone.Model.extend({
});

var RecommendList = Backbone.Collection.extend({
	model : Recommend,
	url : function(){ return "/api/public/base/recommends.htm?catch=true" }
});

var BaseData = Backbone.Model.extend({
 });

(function() {
	
}());
