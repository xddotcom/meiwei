/*!
 * jQuery SvgView
 *
 *
 *@version 1.0
 *
 *
 *$('#tag').svgView(); || $('#tag').floorplan();
 *
 *<div id="tag"></div>
 *
 */

(function($) {

	var viewer = {
		colorAvailable : "#669933",
		colorSelected : "#77513D",
		colorOccupied : "#E6E6E6",
		identifiers : [],
		options : {
			width : 890,
			height : 800,
			imgPath : "",
			selected : [],
			disabled : false,
			clickCallback : null
		},

		init : function(options) {
			var ele = this;
			viewer.identifiers = []
			viewer.options = $.extend(viewer.options, options);
			viewer.fetchSVG(ele);
			return viewer;
		},

		setSize : function(ele) {
			ele.find("svg").each(function() {
				wrapw = viewer.options.width;
				wraph = viewer.options.height;
				svgw = valueOf($(this).attr("width"));
				svgh = valueOf($(this).attr("height"));
				if (svgh / svgw * wrapw <= wraph) {
					svgh = svgh / svgw * wrapw;
					wraph = svgh; // avoid unecessary margins on the bottom
					svgw = wrapw;
				} else {
					svgw = svgw / svgh * wraph;
					wrapw = svgw; // avoid unecessary margins on the right
					svgh = wraph;
				}
				ele.css({
					"width" : wrapw,
					"height" : wraph,
					"margin" : "0 auto"
				});
				$(this).attr("width", svgw + "px");
				$(this).attr("height", svgh + "px");
			});
		},

		message : function() {
		},

		initRgb : function(ele) {
			if (viewer.options.selected == null) {
				viewer.options.selected = []
			}
			ele.find("[class=table]").each(function() {
				var id = $(this).attr("tableid") | "";
				var status = $(this).attr("status");
				if ($.inArray(id + "", viewer.options.selected) > -1) {
					$(this).attr("fill", (color = viewer.colorSelected));
					$(this).attr("status", "selected");
					viewer.setIdentifiers(true, id);
				} else if (status == "available") {
					$(this).attr("fill", (color = viewer.colorAvailable));
				} else if (status == "occupied") {
					$(this).attr("fill", (color = viewer.colorOccupied));
				}
			});
		},

		bindClick : function(ele) {
			ele.find("[class=table]").each(function() {
				$(this).click(function() {
					var id = $(this).attr("tableid") | "";
					var status = $(this).attr("status");
					if (status == "selected") {
						$(this).attr("status", "available");
						$(this).attr("fill", viewer.colorAvailable);
						viewer.setIdentifiers(false, id);
					} else if (status == "available") {
						$(this).attr("status", "selected");
						$(this).attr("fill", viewer.colorSelected);
						viewer.setIdentifiers(true, id);
					} else if (status == "occupied") {
					}
					if (viewer.options.clickCallback) {
						viewer.options.clickCallback(viewer.identifiers);
					}
				});
			});
		},

		setIdentifiers : function(flag, value) {
			if (value) {
				var as = viewer.identifiers;
				if (flag) {
					as.push(value);
				} else {
					as = $.grep(as, function(cur, i) {
						return cur != value;
					});
				}
				$.unique(as);
				viewer.identifiers = as;
			}

		},

		fetchSVG : function(ele) {
			if (viewer.options.imgPath) {
				if ($.browser.msie && $.browser.version < 9) {
					var uri = viewer.options.imgPath;
					uri = uri.substring(0, uri.length-3) + 'png';
					content = "<img src='" + uri + "' width = '" + viewer.options.width + "'>";
					content = '<h2>由于您的浏览器版本问题，导致无法选定桌位，建议使用' + 
								'<a href="http://www.google.cn/intl/zh-CN/chrome/browser/"><span>Chrome<strong>(点击下载)</strong></span></a>,' +  
								'<a href="http://firefox.com.cn"><span>Firefox<strong>(点击下载)</strong></span></a>, ' + 
								'<a href="http://www.apple.com.cn/safari/"><span>Safari<strong>(点击下载)</strong></span></a>, ' + 
								'<a href="http://windows.microsoft.com/zh-cn/internet-explorer/downloads/ie-9/"><span>IE9<strong>(点击下载)</strong></span></a>' + 
								'以上版本的浏览器</h2>'
					ele.html(content);
				} else {
					$.ajax({
						url : viewer.options.imgPath,
						type : "GET",
						contentType : "text/plain; charset=utf-8",
						dataType : "text",
						success : function(data) {
							if (data.indexOf("svg") > 0) {
								ele.html(data);
								if (!(viewer.options.disabled)) {
									viewer.bindClick(ele);
								}
								viewer.setSize(ele);
								viewer.initRgb(ele);
							} else {
								if (viewer.options.callback) {
									viewer.options.callback();
								}
							}
						}
					});
				}
			}
		}
	}

	$.fn.floorplan = $.fn.svgView = function() {
		return viewer.init.apply(this, arguments);
	};

	//$.fn.svgView.defaults = {};

	var valueOf = function(number) {
		if (number) {
			number = number.replace("px", "");
		}
		var n = parseInt(new Number(number | 0).toString().replace("px", ""));
		return isNaN(n) ? 0 : n;
	};

	var reg = /^#([0-9a-fA-f]{3}|[0-9a-fA-f]{6})$/;

	String.prototype.colorHex = function() {
		var that = this;
		if (/^(rgb|RGB)/.test(that)) {
			var aColor = that.replace(/(?:\(|\)|rgb|RGB)*/g, "").split(",");
			var strHex = "#";
			for ( var i = 0; i < aColor.length; i++) {
				var hex = Number(aColor[i]).toString(16);
				if (hex === "0") {
					hex += hex;
				}
				strHex += hex;
			}
			if (strHex.length !== 7) {
				strHex = that;
			}
			return strHex;
		} else if (reg.test(that)) {
			var aNum = that.replace(/#/, "").split("");
			if (aNum.length === 6) {
				return that;
			} else if (aNum.length === 3) {
				var numHex = "#";
				for ( var i = 0; i < aNum.length; i += 1) {
					numHex += (aNum[i] + aNum[i]);
				}
				return numHex;
			}
		} else {
			return that;
		}
	};

	String.prototype.colorRgb = function() {
		var sColor = this.toLowerCase();
		if (sColor && reg.test(sColor)) {
			if (sColor.length === 4) {
				var sColorNew = "#";
				for ( var i = 1; i < 4; i += 1) {
					sColorNew += sColor.slice(i, i + 1).concat(
							sColor.slice(i, i + 1));
				}
				sColor = sColorNew;
			}
			var sColorChange = [];
			for ( var i = 1; i < 7; i += 2) {
				sColorChange.push(parseInt("0x" + sColor.slice(i, i + 2)));
			}
			// return "RGB(" + sColorChange.join(",") + ")";
			return sColorChange;
		} else {
			return sColor;
		}
	};

})(jQuery)
