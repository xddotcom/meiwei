var GlobalVariable = {
		
}
//删除左右两端的空格
function trim(str) {
	return str.replace(/(^\s*)|(\s*$)/g, "");
}

// 删除左边的空格
function ltrim(str) {
	return str.replace(/(^\s*)/g, "");
}

//删除右边的空格
function rtrim(str) {
	return str.replace(/(\s*$)/g, "");
}
/**
 * 只能输浮点数,且小数点后面只能有两位
 * 
 * @param oInput
 */
function CheckInputIntFloat(oInput) {
	if ("" != oInput.value.replace(/\d{1,}\.{0,1}\d{0,2}/, "")) {
		oInput.value = oInput.value.match(/\d{1,}\.{0,1}\d{0,2}/) == null ? "" : oInput.value.match(/\d{1,}\.{0,1}\d{0,2}/);
	}
}
/**
 * 重置表彰
 */
function resetForm() {
	$("form").each(function () {
		this.reset();
	});
}
/**
 * 将validate的提示框
 */
function closeTips() {
	$(".formError").each(function () {
		$(this).fadeOut(150, function () {
			$(this).remove();
		});
	});
}
/**
 * 清空查询
 * @param className
 */
function setNullInfor(className) {
	$("." + className).each(function () {
		if (this.type == "checkbox") {
			$(this).attr("checked", false);
		} else {
			document.getElementById(this.id).value = "";
		}
	});
}

Date.prototype.format = function(format) {
  var o = {
    "M+" : this.getMonth()+1, //month
    "d+" : this.getDate(),    //day
    "h+" : this.getHours(),   //hour
    "m+" : this.getMinutes(), //minute
    "s+" : this.getSeconds(), //second
    "q+" : Math.floor((this.getMonth()+3)/3),  //quarter
    "S" : this.getMilliseconds() //millisecond
  }

  if(/(y+)/.test(format)) format=format.replace(RegExp.$1,
    (this.getFullYear()+"").substr(4 - RegExp.$1.length));
  for(var k in o)if(new RegExp("("+ k +")").test(format))
    format = format.replace(RegExp.$1,
      RegExp.$1.length==1 ? o[k] :
        ("00"+ o[k]).substr((""+ o[k]).length));
  return format;
}


function QS(){
    this.qs = {};
    var s = location.search.replace( /^\?|#.*$/g, '' );
    if( s ) {
        var qsParts = s.split('&');
        var i, nv;
        for (i = 0; i < qsParts.length; i++) {
            nv = qsParts[i].split('=');
            this.qs[nv[0]] = nv[1];
        }
    }
}

QS.prototype.add = function( name, value ) {
    if( arguments.length == 1 && arguments[0].constructor == Object ) {
        this.addMany( arguments[0] );
        return;
    }
    this.qs[name] = value;
}

QS.prototype.addMany = function( newValues ) {
    for( nv in newValues ) {
        this.qs[nv] = newValues[nv];
    }
}

QS.prototype.remove = function( name ) {
    if( arguments.length == 1 && arguments[0].constructor == Array ) {
        this.removeMany( arguments[0] );
        return;
    }
    delete this.qs[name];
}

QS.prototype.removeMany = function( deleteNames ) {
    var i;
    for( i = 0; i < deleteNames.length; i++ ) {
        delete this.qs[deleteNames[i]];
    }
}

QS.prototype.getQueryString = function() {
    var nv, q = [];
    for( nv in this.qs ) {
        q[q.length] = nv+'='+this.qs[nv];
    }
    return q.join( '&' );
}

QS.prototype.toString = QS.prototype.getQueryString;


function autoCompleteByR(url, tag, widthPx) {
	$.ajax( {
		url : url,
		type : 'GET',
		async : false,
		complete : function(str) {
			var result = str.responseText;
			var json = eval("(" + result + ")").json;
			//var json =JSON.parse(result);
			$(tag).autocomplete(json, {
				max : 10,
				minChars : 0,
				width : widthPx,
				scrollHeight : 300,
				matchContains : true,
				selectFirst : false,
				autoFill : false,
				formatItem : function(row, i, max) {
					return row.fullName;
				},
				formatMatch : function(row, i, max) {
					return row.fullName + makePy(row.fullName);
				},
				formatResult : function(row) {
					return row.fullName;
				}
			}).result(function(event, row, formatted) {
			});
		}
	});
}

function ReturnToLogin() {
	window.location.href = "/member/loginfirst.htm";
	return false;
}

var LoginVaildate={
	loginPanel:{
		success:function(){
			//window.location.href = "/myaccount/user/usercenter.htm";
			return false;
		},sorry:function(){
			resetForm();
			$("#loginpanel").show();
			$("#loginFail").css("display", "block");
			return false;
		}
	}
}

function setLanguge(loca) {
	$.ajax({
		url: "/changelanguage.htm",
		data: "locale=" + loca,
		type: 'POST',
		async: false,
		complete: function (req, textStatus) {
			window.location.reload(true);
		}
	});
}

function shareWeibo(container, link, message) {
	var host = "http://" + location.host;
	var _w = 90, _h = 24;
	var param = {
		url : host + link,
		type : '2',
		count : '1', /** 是否显示分享数，1显示(可选) */
		appkey : '', /** 您申请的应用appkey,显示分享来源(可选) */
		title : message, /** 分享的文字内容(可选，默认为所在页面的title) */
		pic : host + "/front/images/sharemeiwei.jpg", /** 分享图片的路径(可选) */
		ralateUid : '3058840707', /** 关联用户的UID，分享微博会@该用户(可选) */
		language : 'zh_cn', /** 设置语言，zh_cn|zh_tw(可选) */
		rnd : new Date().valueOf()
	}
	var temp = [];
	for ( var p in param) {
		temp.push(p + '=' + encodeURIComponent(param[p] || ''))
	}
	$(container).html(
			'<iframe allowTransparency="true" frameborder="0" scrolling="no" '
					+ 'src="http://hits.sinajs.cn/A1/weiboshare.html?'
					+ temp.join('&') + '" width="' + _w + '" height="' + _h
					+ '"></iframe>')
}


function getCookie( name ) {
	if (document.cookie.length > 0) {
		start = document.cookie.indexOf(name + "=");
		if (start != -1) {
			start = start + name.length + 1;
			end = document.cookie.indexOf(";", start);
			if (end == -1){
				end = document.cookie.length;
			}
			return unescape(document.cookie.substring(start, end));
		}
	}
	return ""
}

function setCookie(name, value, expiredays) {
	var exdate = new Date()
	exdate.setDate(exdate.getDate() + expiredays);
	document.cookie = name + "=" + escape(value)
			+ ((expiredays == null) ? "" : ";expires=" + exdate.toGMTString())+";path=/";
}


/**
 * 
 * @param url
 * @param data
 * @param redirect {success:"url",error:"url"}
 * @param msg {success:"message",error:"message"}
 */
function deleteEntity(url, data, redirect , msg ) {
	$.ajax({
		url : url,
		type : 'POST',
		data : data ? data : "",
		async : false,
		complete : function(str) {
			var result = str.responseText;
			if (result == "y") {
				window.location.href = redirect.success;
			} else {
			}
		}
	});
}


$.Datatype.eom=function( val , ele , form , regxp ){
	var m=regxp["m"],
	e = regxp["e"],
	mv= $("#mobile").val();
	if( m.test( mv )  ){ 
		if(val &&  !e.test( val )){
			return false;
		}
		return true;
	}
	if( e.test( val )){ return true; }
	return false;
}


function spinnerLoader(){
	return $('<div style="text-align:center;background-color: rgb(222, 222, 222);display:none;"><p id="spinner-loading"><a class="btn btn-large btn-success has-spinner"><span class="spinner"><i class="icon-spin icon-refresh"></i></span>Loading....</a></p></div>');
}


//@Deprecated
function pictureReplace(){
	var localImages = ["/front/images/temp/picture1.jpg",
   "/front/images/temp/picture2.jpg",
   "/front/images/temp/picture3.jpg",
   "/front/images/temp/picture4.jpg",
   "/front/images/temp/picture5.jpg",
   "/front/images/temp/picture6.jpg",
   "/front/images/temp/picture7.jpg",
   "/front/images/temp/picture8.jpg",
   "/front/images/temp/picture9.jpg",
   "/front/images/temp/picture10.jpg",
   "/front/images/temp/picture11.jpg",
   "/front/images/temp/picture13.jpg",
   "/front/images/temp/picture14.jpg",
   "/front/images/temp/picture15.jpg",
   "/front/images/temp/picture16.jpg",
   "/front/images/temp/picture17.jpg"];
   var romoteImages = ["/upload/restaurant/fdf806deebef7dea3f0a65c491fc7f4e.jpg"
   ,"/upload/restaurant/330782df1ac35ced9adced0742aa1426.jpg"
   ,"/upload/restaurant/8ec72ab260c7c8fdf59ddb58812e7036.jpg"
   ,"/upload/restaurant/efe5ce8b196f389958e420069a393baf.jpg"
   ,"/upload/restaurant/5876bf95bf7794f36bffb1e3a94ca046_1.jpg"
   ,"/upload/restaurant/6f62a2057e005bed5d0479e04554796a.jpg"
   ,"/upload/restaurant/ea303af3ef45b246c947e229e06d3fc0_1.jpg"
   ,"/upload/restaurant/4a46680d82f1e68a4e117c0ad396e195.jpg"
   ,"/upload/restaurant/f676de79903cd93cf77befd09d4b56de.jpg"
   ,"/upload/restaurant/e115baab21d7fa8f3c674b85ce6a0a78.jpg"
   ,"/upload/restaurant/f0a28ec4e9858f69fef9e26c58195afb.jpg"
   ,"/upload/restaurant/07e8f61ab800a8a430f4ad270f78150d.jpg"
   ,"/upload/restaurant/8ec72ab260c7c8fdf59ddb58812e7036.jpg"
   ,"/upload/restaurant/efe5ce8b196f389958e420069a393baf.jpg"
   ,"/upload/restaurant/4a4ff4015e2e887c4eefabeeb7337508.jpg"
   ,"/upload/restaurant/4a46680d82f1e68a4e117c0ad396e195.jpg"];
   for(i=0;i<16;i++){
   	$(" #carousel-box-18 , #carousel-box-19, #carousel-box-1, #carousel-box-2, #carousel-box-3, #carousel-box-4").find("[src='"+romoteImages[i]+"']").attr("src",localImages[i]);
   }
   $("#carousel-box-5").find("[src='/upload/restaurant/bb58a0553af311196637a749d5b275cc.jpg']").attr("src","/front/images/temp/picture12.jpg");
   $("#carousel-box-19").find("[src='/upload/restaurant/ce77a6191f7111b19dd69edef590d6d3.jpg']").attr("src","/front/images/temp/picture18.jpg");
}
