var selector ="";

RestaurantView = Backbone.View.extend({
	initialize: function() {
	},
	render: function() {
		var restaurant = this.model.attributes.restaurant;
 		
		$("#template-content-span").append(_.template( $("#template-restaurant-view-1").html(), {restaurant : restaurant}))
		.append(_.template( $("#template-restaurant-view-2").html(), {restaurant : restaurant}));
		
		$("#template-right-box-span .right-5").append( _.template( $("#template-right-box-6").html() , {restaurant : restaurant} ));
 
		selector = ".right-5,.right-7,.right-3,.right-9";
		var rightView = new RightView();
		
		resetTime(restaurant.restaurantId);
		
		PageLoad.restaurantProfile.init();
		try{ $("title").first().text(restaurant.fullName.text); } catch(e){  }
		PageLoad.restaurantProfile.initCookie(restaurant.restaurantNo ,restaurant.fullName.text , restaurant.restaurantId);
	}
});

ContactListView = Backbone.View.extend({
	template_contact: _.template('<option value="<%= item.contactId %>" <% if(selected==item.contactId){%> selected="selected" <%}%> ><%= item.name %> <%= item.telphone %> <%= item.email %></option>'),
	template_invite: _.template('<label class="checkbox">' + 
		'<input type="checkbox" value="<%= contactId %>"> <%= name %> <%= telphone %> <%= email %></label>'),
	_is_change : "default",
	selected : "",
	initialize: function() {
		this.model.on('change', this.render, this);
	},
	render: function() {
		var member = this.model.get('member');
		if (member) {
			contacts = member.contacts;
			var thisView = this;
			
			if(thisView._is_change!="still-contact"){
				$("#select-contact").empty();
				var str = '请选择预订人信息';
				if(GlobalVariable.language=="2"){
					str = 'please choose from the list';
				}
				$("#select-contact").append("<option>"+str+"</option>");
				var selected = "";
				$.each(contacts, function(i, item) {
					$("#select-contact").append(thisView.template_contact({item : item , selected : thisView.selected}));
 				});
				$("#select-contact").change(function(e){
					$.each(contacts, function(i, item) {
						if(item.contactId==$(e.target).attr('value')){
							$("#nickName").val(item.name);
							$("#telphone").val(item.telphone);
							$("#sexSelect option[value="+item.sexe+"]").attr("selected",true);
						}
	 				});
				});
			}
			if(thisView._is_change!="still-invite"){
				$("#select-invite").empty();
 				$.each(contacts, function(i, item) {
 					$("#select-invite").append(thisView.template_invite(item));
 				});
			}
			
		}
	}
});

var member = new Member();
var contactListView = new ContactListView({model: member});

function initOrderView(restaurantid, personNum, orderDate, orderTime, liaisonId) {
	
 	member.fetch({ cache: false , type : "get" ,success : function( collection ,response ){
 		contactListView.render();
	}});
	$(".product-category .product").on("click", function() {
		if ($(this).hasClass("active")) {
			$(this).removeClass("active");
		} else {
			$(this).addClass("active");
		}
	});
	
	if (orderDate) $("#datetimepicker_book").val(orderDate);
	resetTime(restaurantid);
	if (orderTime) $("#orderTime").val(orderTime);
	if (!$("#orderTime").val()) $("#orderTime").val($("#orderTime option:first-child").val());
	if (personNum) $("#personNum").val(personNum);
	if (liaisonId) $("#select-contact").val(liaisonId);
	var tables = $("#tables").val();
	if (tables) $("#selectImg [picId='" + tables.split("|")[0] + "']").attr("selected", true);
	$("a.a_picture").popImage();
	changefloorImg();

	LoginVaildate.loginPanel.success = function() {
		member.fetch({cache: false});
	}
}

function addContact() {
	var id = $("#select-contact").val();
	$.post("/api/myaccount/user/contact/save.htm", $("#new-contact-form").serialize(), function() {
		resetForm();
		contactListView._is_change="still-invite";
		contactListView.selected = id;
		member.fetch({cache: false});
		$("#select-contact").click();
		$("#telphone").tooltip("hide");
		alert("添加联系人成功");
	});
}

function addInvite() {
	var id = $("#select-contact").val();
	$.post("/api/myaccount/user/contact/save.htm", $("#new-invite-form").serialize(), function() {
		contactListView._is_change="still-contact";
		contactListView.selected = id;
		member.fetch({cache: false});
	});
}

function resetTime(rid) {
	var value = $("select#orderTime").children("option:selected").val();
	fillUserOption("#orderTime", "/api/public/order/openingHours.htm?time=" + $("#datetimepicker_book").val() + "&restaurantId=" + rid, "json", "optionTitle", "optionValue", "POST", "", "times", function(json) {
		$("#datetimepicker_book").val(json.day);
	});
}

function changefloorImg() {
	if (!$("#selectImg").length) {
		return false;
	}
	var path = $("#selectImg").find("option:selected").attr("imgPath");
	var picId = $("#selectImg").find("option:selected").attr("picId");
	if (!path) {
		path = '/front/images/nophoto_big.jpg';
	}
	var imgPath = "/upload/" + path;
	var tables = $("#tables").val();
	if (tables) {
		split = tables.split("|");
		if (picId != split[0]) {
			tables = [];
			$("#tables").val('');
		} else {
			tables = split[1].split(",");
		}
	} else {
		tables = []
	}
	var _table_width = 800;
	if($("body").innerWidth() < 600  ){
		_table_width = 230;
	}
	$("#svgDiv").floorplan({
		width : _table_width,
		height : 800,
		imgPath : imgPath,
		selected : tables,
		clickCallback : function(identifiers) {
			if (identifiers && identifiers.length > 0) {
				$("#tables").val(picId + "|" + identifiers);
			} else {
				$("#tables").val("");
			}
		}
	});
}

function validOrderForm() {
	if (GlobalVariable.loginstatus == "1") {
		var remark = "" , productIds;
		var els = $.trim($("#areaother").val());
		$(".product.active .product-desc").each(function() {
			if ($(this).text()) {
				remark += $(this).text() + "; ";
			}
		});
		remark += els;
		if (!remark) {
			$("p.extra").css("display", "none");
		} else {
			$("#adminother").val($.trim(remark).replace(/[\s]/g,""));
			$("span.adminother").text(remark);
		}
		
		$("#contactName").val( $("#nickName").val() );
		$("#contactTelphone").val( $("#telphone").val() );
		$("#contactSexe").val( $('#sexSelect').val() );
		
		if(!$("#contactName").val()){
			$("#nickName").focus();
			return;
		}
		if(!$("#contactTelphone").val()){
			$("#telphone").focus();
			return;
		}
		
		$("#p-text-restaurant-name").append($("#restaurant-name-h4").text());
		$("span.date").text($("#datetimepicker_book").val());
		$("span.time").text($("#orderTime").val());
		$("span.number").text($("#personNum").val());
		$("span.contact_name").text($("#select-contact option:selected").html());
		$("span.other").text($("#reservation-additional-info").val());
		$('#book-submit-button').on('click', function() {
			$("#order-form-input-hidden").append($(".product.active .product-ids-temp"));
			$.post("/api/reservation/do/confirmation.htm" ,$("#order-form").serialize() , function(data){
				location.href = "/myaccount/user/orders.htm";
			});
		});
		$("#book-confirm").modal();
	} else {
		//$("#navbar-username").focus();
		PageLoad.requireLogin();
	}
}

function inviteFriends() {
	var array = new Array();
	$("#select-invite input[type='checkbox']:checked").each(function() {
		array.push($(this).val());
	});
	if (array.length >= 12) {
		$("#select-invite").parent().find(".help-block").text("单个订单邀请上限为12个人").css("display", "inline");
	} else {
		$("#order-form #inviteFriends").val(array.join("|"));
		$("#invite-modal").modal('hide');
	}
}

function _count(){
	if($('#new-order-num').length>0)
	$.ajax({
		url : "/api/myaccount/restaurant/neworder/count.htm"
		,type : "POST"
		,success  : function(data){
			if(data){
				$('#new-order-num').text("("+data+")");
			}
		}
	});
}

HeaderView = Backbone.View.extend({
	initialize: function() {
 	},
	render: function() {	
		var member = this.model.get('member');
		GlobalVariable.data.member = member;
  		$("#container-masthead").empty();
 		$("#container-masthead").append( _.template( $("#template-masthead").html() , { member : member } ));
		PageLoad.initLoginpanel();
		
		nameList("#pageSearch");
		
		if($("#template-leftbox").length>0){
			$("#nav").append( _.template( $("#template-leftbox").html() , { member : member } ) );
			$("[active="+$("body").attr("leftbox")+"]").attr("class","active");
			_count();
		}
 	}
});

CuisineListView = Backbone.View.extend({
	initialize: function() {
		this.render();
 	},
	render: function() {	
		this.collection = new CuisineList();
		this.collection.fetch({success:function( results , response){
			if($(".row .cuisines") && $(".row .cuisines").length>0) 
			$(".row .cuisines").html( _.template( $("#template-cuisines").html() , { cuisines : response}));
			$("#cuisineId").append( _.template( $("#template-right-box-1").html() , { cuisines : response}));
			$("#cuisine-ul").append( _.template( $("#template-right-box-4").html() , { cuisines : response}));
		}});
 	}
});

CircleListView = Backbone.View.extend({
	initialize: function() {
		this.render();
 	},
	render: function() {	
		this.collection = new CircleList();
		this.collection.fetch({success:function( results , response){
			GlobalVariable.data.circles = response;
			nameList("#quickSearch");
			$("#circleId").append( _.template( $("#template-right-box-2").html() , { circles : response}));
			$("#time").append( _.template( $("#template-right-box-3").html()));
		}});
 	}
});


DistrictListView = Backbone.View.extend({
	initialize: function() {
		this.render();
 	},
	render: function() {	
		this.collection = new DistrictList();
		this.collection.fetch({success:function( results , response){
			if($(".row .districts") && $(".row .districts").length>0)
			$(".row .districts").html( _.template( $("#template-districts").html() , { districts : response}));
			$("#circles-ul").append( _.template( $("#template-right-box-5").html(), { districts : response} ));
		}});
 	}
});


RecommendListView = Backbone.View.extend({
	initialize: function() {
		this.render();
 	},
	render: function() {	
		this.collection = new RecommendList();
		var spinner = spinnerLoader();
		$(".recommends").append( spinner );
		this.collection.fetch({success:function( results , response){
	 		spinner.remove();
			$(".row .recommends").after( _.template( $("#template-recommends").html() , { recommends : response}) );
			pictureReplace();		
			$(".carousel").each(function() {
		 		var time = 5000 * Math.random() + 3000;
		 		$(this).carousel({interval: time});
		 	});
		}});
 	}
});

RightView = Backbone.View.extend({
	initialize: function() {
		this.render();
	},
	render: function() {	
  		var div =$("<div style='display:none'></div>");
		var sa = selector.split(",");
		var array = new Array();
		for ( var i = 0; i < sa.length; i++) {
			array[i] = $("#template-right-box-span").find( sa[i] ).css("display","block");
		}
		div.data("data",array);
		$(".right-1,.right-2,.right-3,.right-4,.right-5,.right-7,.right-9").remove();
		$("#template-right-box-span").append(div.data("data"));
 	}
});

HomeView = Backbone.View.extend({
	initialize: function() {
		this.render();
 	},
	render: function() {
		var recommendListView = new RecommendListView();
		selector = ".right-1,.right-2,.right-3,.right-9";
		var rightView = new RightView();
		PageLoad.home.init();
 	}
});

RestaurantListView = Backbone.View.extend({
	initialize: function() {
 	},
	render: function() {	
		$("#template-content-span")
		.append(_.template( $("#template-restaurant-list-01").html() , {restaurants : this.model.toJSON()}));

		selector = ".right-4,.right-1,.right-2,.right-3,.right-9";
		var rightView = new RightView();
		PageLoad.restaurantList.init();
 	}
});

OrderView = Backbone.View.extend({
	initialize: function() {
 	},
	render: function() {	
		var restaurant = this.model.attributes.restaurant;
 		$("#template-content-span")
		.prepend(_.template( $("#template-order-view-02").html(), {restaurant : restaurant}))
		//.prepend(_.template( $("#template-order-view-03").html(), {restaurant : restaurant}))
		.prepend(_.template( $("#template-order-view-04").html(), {restaurant : restaurant}))
		.prepend(_.template( $("#template-order-view-01").html(), {restaurant : restaurant}));
		
 		$("#div-order-btn").css("display","block");
 		
		$("#template-right-box-span .right-5").append( _.template( $("#template-right-box-6").html() , {restaurant : restaurant} ));
		
		selector = ".right-5,.right-7,.right-3,.right-9";
		var rightView = new RightView();
		
		try{ $("title").first().text(restaurant.fullName.text); } catch(e){  }
		
		if(typeof(afterRender) == "function"){
			afterRender( restaurant );
		}
  	}
});
