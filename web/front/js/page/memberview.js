
function deleteReview(id){
	deleteEntity('/api/myaccount/user/review/delete.htm','reviewId=' + id,{success:"/myaccount/user/reviews.htm",error:""});
}

function deleteContact(id){
	deleteEntity('/api/myaccount/user/contact/delete.htm','contactId='+id,{success: "/myaccount/user/contacts.htm", error: ""});
}

function deleteFavorite(id){
	deleteEntity('/api/myaccount/user/favorite/delete.htm','favoriteId='+id,{success:"/myaccount/user/favorites.htm",error:""});
}

function doConfirm(orderId){
	var yes = window.confirm($("#basic-title70").text()+"？");
	if (yes) {
		$.ajax({
			url: "/api/reservation/restaurant/confirm.htm",
			data: "orderId=" + orderId,
			cache: false,
			type: "POST",
			complete : function (req) {
				if(req.responseText == "1"){
					window.document.location.reload();
		      	}
			}
		});
	}
}

function doDelete(orderId) {
    var yes = window.confirm($("#basic-title59").text()+"？");
    if (yes) {
        $.ajax({
            url: "/api/reservation/do/delete.htm",
            data: "orderId=" + orderId,
            cache: false,
            type: "POST",
            complete: function (req) {
                 if (req.responseText == "1") {
                    window.document.location.reload();
                }
            }
        });
    }
}

function show( orderId ){
	$("#orderId").val(orderId);
	$("#invite-modal").modal('show');
}

function inviteFriendsPost(){
	var array = new Array();
	$("#select-invite input[type='checkbox']:checked").each(function() {
		array.push($(this).val());
	});
	if (array.length >= 12) {
		$("#select-invite").parent().find(".help-block").text("单个订单邀请上限为12个人").css("display", "inline");
	} else if($("#orderId").val()) {
		$.ajax({
	        url: "/api/reservation/do/inviteFriends.htm",
        	type:'POST',
        	data : "orderId="+$("#orderId").val()+"&inviteFriends="+array.join("|") ,
           	async: false,
           	complete : function(str) {
           		window.location.reload();
	        }
		});
	}
}

function setDateText(){
	if($("#date-search-select").length>0 && $("#date-search-select").attr("search-key")){
		var month = new Date().getMonth();
		for( var i=-3 ; i<3;i++){
			var date =  new Date(new Date().setMonth(i+month));
			var optionValue = date.format("yyyy-MM") ;
			var optionLabel = date.format("yyyy年MM月")
			$("#date-search-select").append("<option value='"+optionValue+"'>"+optionLabel+"</option>");
		}
	}else{
		for( var i=-30 ; i<30;i++){
			var date =  new Date(new Date() - i * 24*60*60*1000).format("yyyy-MM-dd") ;
			$("#date-search-select").append("<option value='"+date+"'>"+date+"</option>");
		}
		
	}
	
}

function _query(){
	setDateText();
    
    $(".pagination-centered a").click(function(){ 
    	var qs = new QS;
	    qs.add('pageNo' ,$(this).attr("href").substring(8));
    	$(this).attr("href","#");
    	location.search = qs.toString();
    });
    
    $("#date-search-select").change(function(){
    	var qs = new QS;
    	qs.add('orderSubmitTime' ,$("#date-search-select").val() );
    	qs.add('pageNo' , "1" );
    	location.search = qs.toString();
    });
    
    var qs = new QS;
    if(qs.qs["orderSubmitTime"]){
    	$("#date-search-select").val( qs.qs["orderSubmitTime"] );
    }else{
    	$("#date-search-select").val( new Date().format("yyyy-MM-dd") );
    	if($("#date-search-select").length>0 && $("#date-search-select").attr("search-key")){
    		$("#date-search-select").val( new Date().format("yyyy-MM") );
    	}
    }
}

function validateCheck(form){
	var consumeValue = $(form).children("[type=text]").val();
	if(consumeValue<=0){
		alert("请输入正确的消费金额！");
		return false;
	}
	if(window.confirm("确认提交该消费金额？")){
		$.post("/api/reservation/save/amount.htm" , $(form).serialize() , function(data){
			location.reload();
		});
	}
	return false;
}

var integralValue = 0;

function recalculate() {
	var total = 0;
	$(".integraluse").each(function () {
		var count = parseInt($(this).find("input").val());
		var credit = parseInt($(this).attr('product-cost'));
		var name = parseInt($(this).attr("product-name"));
		var productId = parseInt($(this).attr("product-id"));
		total += count * credit;
	});
	if (total > integralValue) { alert("很抱歉，您的积分不足，无法继续兑换礼品"); return false; } 
	else { return true; }
}

function resetTable() {
	$("#integral-tbody").empty();
	$("#div-integral-hidden").empty();
	var total = 0;
	$(".integraluse").each(function () {
		var count = parseInt($(this).find("input").val());
		if (count > 0) {
			var credit = parseInt($(this).attr('product-cost'));
			var name = $(this).attr("product-name");
			var productId = parseInt($(this).attr("product-id"));
			var cost = count * credit;
			total += cost;
			$("#integral-tbody").append('<tr><td>' + name + '</td><td><span>' + count + '</span>份</td><td><span>' + cost + '</span></td></tr>');
			$("#div-integral-hidden").append('<input name="productIdArray" type="hidden" value="' + productId + '" /><input name="countArray" type="hidden" value="' + count + '" />');
		}
	});
	$("span.integral-total").text(total);
}

function pack(array) {
	if(array) {
		try {
			if (array) {
				select([[$("#Input_Sary1") , array[0]], [$("#selMonth5"), "2"] ,[$("#selMonth5"),array[1].split("-")[1]] ]);
				if(array.length>2){
					try {
						select([ [$("#Input_Sary2") , array[2]], [$("#selMonth6"),"2"],[$("#selMonth6"),array[3].split("-")[1]] ]);
					} catch (e) { }
				}
			}
		} catch (e) { }
 	}
}

function select(array){
	if (array) {
		for ( var i = 0; i < array.length; i++) {
			if(array[i][1]){
 				array[i][0].val(array[i][1]);
			}
		}
	}
}

function setReValue() {
	var content = "",input1="",input2="";
	if ($("#Input_Sary1").val()) { 
		content =input1= $("#Input_Sary1").val() + "|" + $("#date1").val();
	}
	if ($("#Input_Sary2").val()) {
		content =input2= $("#Input_Sary2").val() + "|" + $("#date2").val();
	}
	if(input1 && input2){
		content = input1+"|"+input2;
	}
	$("#anniversary").val(content);
}


function roll(){
	$("#roll-modal").modal("show");
    var int = setInterval(roll, 100);
    function roll() {
        var x = parseInt(2 + Math.random() * 5);
        $('#roll').html(x);
    }
    $('#stop-roll').on("click", function() {
        $.get('/api/reservation/roll/'+item.orderId+'.htm', function(str) {
            $('#roll').html('恭喜您，新订单成功消费后将获得<span style="font-size:70px;">' + str + '</span>倍积分');
            $('#roll').css({'font-size': '50px', 'line-height': '90px'});
            clearInterval(int);
            $("#close-roll").css('display', 'block');
            $("#stop-roll").css('display', 'none');
        });
    });
    $('#roll-modal').on('hidden', function () {
        window.location.href = "/myaccount/user/orders.htm";
    })
}

 MemberView = {
	_in : function(_model){
		$("#restaurantId").val(_model.get('restaurantId'));
		
		$("#fullName").val(_model.get('fullName').chinese);
		$("#fullNameEN").val(_model.get('fullName').english);
		$("#address").val(_model.get('address').chinese);
		$("#addressEN").val(_model.get('address').english);
		$("#description").val(_model.get('description').chinese);
		$("#descriptionEN").val(_model.get('description').english);
		$("#discount").val(_model.get('discount').chinese);
		$("#discountEN").val(_model.get('discount').english);
		$("#parking").val(_model.get('parking').chinese);
		$("#parkingEN").val(_model.get('parking').english);
		$("#workinghour").val(_model.get('workinghour').chinese);
		$("#workinghourEN").val(_model.get('workinghour').english);
	},
	RestaurantList : Backbone.View.extend({
 		el : '#resteditForm' ,
		initialize: function() {
			this.render();
		},
 		events : {
			"change #deep-select" : "handleChange"
			,"click #btnRestsubmit" : "saveOrUpdate"
		},
		render: function() {
			$("#deep-select").empty();
 			$.each(this.model.models, function(key , _model ){
				var content = '<option value="'+_model.get('restaurantId')+'">'+_model.get('fullName').chinese+'</option>';
				$("#deep-select").append(content);
				if(key==0){
					MemberView._in(_model);
				}
 			});
  			return this;
		},
		handleChange : function(e){
			var id = $(e.srcElement ? e.srcElement: e.target).val();
			
			$.each(this.model.models, function(key , _model ){
				var _id = _model.get('restaurantId');
				if(id==_id){
					MemberView._in(_model);
				}
 			});		
 		},
 		saveOrUpdate : function(){
 			$.post("/api/myaccount/restaurant/save.htm", $("#resteditForm").serialize(), function() {
 				alert("Successfully!");
			});
 		}
	})
 }
 
$(document).ready(function(){
	
	if($( "div.getstarted").length>0){
		$.get("/api/member/user/detail.htm"+location.search,function(response){
			var member = response.member;
			$( ".credit" ).append(member.personalInfor.credit );
			if(member.personalInfor.memberNo != member.loginName && false){
				$("#td-completed").attr("class","completed").text("已完成");
			}else{
				var _modify = '立即修改';
				if(GlobalVariable.language=="2"){
					_modify = 'Modify now';
				}
				$("#td-completed").attr("class","todo").html("<a href='/myaccount/user/editprofile.htm'>"+_modify+"&gt;&gt;</a>");
			}
			$(".member-no").append("（"+member.personalInfor.memberNo+"）");
		});
	}else if($("div.useredit").length>0){
		$("#registerForm").Validform({
			btnSubmit:"#buttonRegDiv", ignoreHidden:true, showAllError:true, ajaxPost:true, tiptype:  4 ,
			beforeSubmit:function(form){ setReValue(); return true; },
	        callback:function(data){
				var result = data.status;
				if (result == "y") {
		      		window.location.href="/myaccount/user/usercenter.htm";
				}
			}
		});
		
		$.get("/api/member/user/detail.htm"+location.search,function(response){
			var member = response.member;
			$("#loginName").attr("ajaxurl", "/api/member/validate/judgexist/member?repeat="+member.loginName)
			.val(member.loginName);
			$("#email").attr("ajaxurl", "/api/member/validate/judgexist/email?repeat="+member.personalInfor.email)
			.val(member.personalInfor.email);
			$("#mobile").attr("ajaxurl", "/api/member/validate/judgexist/mobile?repeat="+member.personalInfor.mobile)
			.val(member.personalInfor.mobile);
			$("#nickName").val(member.personalInfor.nickName);
 			$("#birthday").val(member.personalInfor.birthday);
			$("#anniversary").val(member.personalInfor.anniversary);
			
			$("input[name=sexe][value="+member.personalInfor.sexe+"]").attr("checked","checked");
			
			data=$.trim($("#anniversary").val()).split("|");
			if(data && data.length>0){
				$("#date1").val(data[1]);
				if( data.length>2){
					$("#date2").val(data[3]);
				}
			}
			$("#birthday, #date1, #date2").jSelectDate({
				css: "jselectdate input-small",
				yearBeign: 1955,
				disabled: false,
				isShowLabel: false
			});
		 	pack(data);
		});
	}else if($("div.integral").length>0){
		$.get("/api/myaccount/user/integral/view.htm",function(response){
			
			var member = response.member;
			$(".label-success").append( member.personalInfor.credit );
			$("#input-name").val( member.personalInfor.nickName );
			$("#input-telphone").val( member.personalInfor.mobile );
			integralValue = member.personalInfor.credit;
			
			if($("#template-integral").length>0){
				if(response && response.results.length>0){
					var total = 0;
					var row = $("<div class=\"row-fluid\">");
  					$.each( response.results, function(i, item) {
  						if( item.picturePath!="" && item.credit>0 ){
  							total += 1;
	  						row.append( _.template( $("#template-integral").html(), {item : item } ) );
 							if(total % 3 ==0 ){
 								$("div.integral").append( row );
 								row = $("<div class=\"row-fluid\">");
 							}
  						}
 					});
  					
  					$("a.plus-a-tag").click(function () {
  						var num = parseInt($(this).parent().find("input").val());
  						$(this).parent().find("input").val(num + 1);
  						if (recalculate()) resetTable();
  					});
  					$("a.minus-a-tag").click(function () {
  						var num = parseInt($(this).parent().find("input").val());
  						$(this).parent().find("input").val(Math.max(num - 1, 0));
  						if (recalculate()) resetTable();
  					});
  					
  					$(".pop-image").popImage();
				}
			}
		});

		$("#integral-form").Validform({
			btnSubmit: "#integral-submit",
			tiptype: function () {},
			beforeSubmit: function () {
				var total = parseInt($.trim($("span.integral-total").first().text()));
				if (total <= 0) { alert($('#hidden-text').text()); return false; }
				$("#inte-box-detail").html($("#inte-table-right").html());
				$("#inte-name").text($("#input-name").val());
				$("#inte-telphone").text($("#input-telphone").val());
				$("#inte-address").text($("#input-address").val());
				$("#integral-confirm").modal();
				$('#inte-submit-button').on('click', function () {
  					$.post("/api/myaccount/user/integral/save.htm" , $("#integral-form").serialize() ,function(data){
						location.href = "/myaccount/user/credits.htm";
					});
 				});
				return false;
			}
		});
		
	}else if($("div.reviews").length>0){
		$.get("/api/myaccount/user/reviews.htm"+location.search,function(response){
			if($("#template-reviews").length>0){
				$("#page-ul").append( _.template( $("#template-page").html(), {page : response.page } ) );
 				if(response && response.results.length>0){
					$.each( response.results, function(i, item) {
						$("div.reviews").append( _.template( $("#template-reviews").html(), {item : item } ) );
					});
				}else{
					$("div.reviews").append( $("p.noresults").css("display","block"));
				}
			}
		});
	}else if($("div.contacts").length>0){
		$.get("/api/myaccount/user/contacts.htm"+location.search,function(response){
			if($("#template-contacts").length>0){
				$("#page-ul").append( _.template( $("#template-page").html(), {page : response.page } ) );
				if(response && response.results.length>0){
					$.each( response.results, function(i, item) {
						$("div.contacts").append( _.template( $("#template-contacts").html(), {item : item } ) );
					});
				}else{
					$("div.contacts").append( $("p.noresults").css("display","block") );
				}
			}
		});
	}else if($("div.contact-edit").length>0){
		$("#contactEditForm").Validform({btnSubmit: "#btnContacteditSubmit",ignoreHidden: true,showAllError: true,ajaxPost: true,tiptype: 4,
			callback: function(data){
				var result = data.responseText;
				if (result == "y") {
	           		window.location.href="/myaccount/user/contacts.htm";
				}
			}
		});
		$.get("/api/myaccount/user/contact/view.htm"+location.search , function(response){
			if(response && response.contact){
				var contact = response.contact;
				$("#contactId").val(contact.contactId);
				$("#telphone").val(contact.telphone);
				$("#nickName").val(contact.name);
				$("#email").val(contact.email);
				$("input[name=sexe][value="+contact.sexe+"]").attr("checked","checked");
			}
		});
	}else if($("table.favorites").length>0){
		$.get("/api/myaccount/user/favorites.htm"+location.search,function(response){
			if($("#template-favorites").length>0){
				$("#page-ul").append( _.template( $("#template-page").html(), {page : response.page } ) );
				if(response && response.results.length>0){
					$.each( response.results, function(i, item) {
						$("table.favorites").append( _.template( $("#template-favorites").html(), {item : item } ) );
					});
				}else{
					$("table.favorites").append( $(".noresults").css("display","block") );
				}
			}
		});
	}else if($("table.credits").length>0){
		$.get("/api/myaccount/user/credits.htm"+location.search,function(response){
			if($("#template-credits").length>0){
				$("#page-ul").append( _.template( $("#template-page").html(), {page : response.page } ) );
				$("#strong-credit").append( "(" + response.member.personalInfor.credit + ")" );
				if(response && response.results.length>0){
  					$.each( response.results, function(i, item) {
						$("table.credits").append( _.template( $("#template-credits").html(), {item : item } ) );
 					});
				}else{
					$("table.credits").append( $("p.noresults").css("display","block"));
				}
				$('a.apopover').tooltip({ html : true });
			}
		});
	}else if($("div.orders").length>0){
		member.fetch({cache: false});
		$.get("/api/myaccount/user/orders.htm"+location.search,function(response){
			if($("#template-orders").length>0){
				$("#page-ul").append( _.template( $("#template-page").html(), {page : response.page } ) );
				_query();	 
 				if(response && response.results.length>0){
  					$.each( response.results, function(i, item) {
						$("div.orders").append( _.template( $("#template-orders").html(), {item : item , member : response.member  }));
						
						if(false && i==0 && item.status == 0 && item.creditGain == 0){
							roll();
						}
 					});
				}else{
					$("div.orders").append( $("p.noresults").css("display","block"));
				}
 			}
		});
	}else if($("table.newOrders").length>0){
		$.get("/api/myaccount/restaurant/newOrders.htm"+location.search,function(response){
			if($("#template-newOrders").length>0){
				$("#page-ul").append( _.template( $("#template-page").html(), {page : response.page } ) );
				//_query();
 				if(response && response.results.length>0){
  					$.each( response.results, function(i, item) {
						$("table.newOrders").append( _.template( $("#template-newOrders").html(), {item : item , member : response.member  }));
 					});
				}else{
					$("table.newOrders").append( $("p.noresults").css("display","block"));
				}
 			}
		});
	}else if($("table.doOrders").length>0){

		$.get("/api/myaccount/restaurant/doOrders.htm"+location.search,function(response){
			if($("#template-doOrders").length>0){
				$("#page-ul").append( _.template( $("#template-page").html(), {page : response.page } ) );
				_query();
 				if(response && response.results.length>0){
  					$.each( response.results, function(i, item) {
						$("table.doOrders").append( _.template( $("#template-doOrders").html(), {item : item , member : response.member  }));
 					});
				}else{
					$("table.doOrders").append( $("p.noresults").css("display","block"));
				}
 			}
		});
	}else if($("div.rest-detail").length>0){
		 var restaurantList = new RestaurantList( );
		 restaurantList.model = Restaurant;
		 restaurantList.url = "/api/restaurant/list/detail.htm";
		 var memberView = new MemberView.RestaurantList( {model : restaurantList } );
		 
		 restaurantList.fetch({ cache: false , type : "get" ,success : function( collection ,response ){
			 memberView.render();
		 }}); 
	}else if($("div.rest-reviews").length>0){
		$.get("/api/myaccount/restaurant/reviews.htm"+location.search,function(response){
			if($("#template-reviews").length>0){
				$("#page-ul").append( _.template( $("#template-page").html(), {page : response.page } ) );
 				if(response && response.results.length>0){
					$.each( response.results, function(i, item) {
						$("div.rest-reviews").append( _.template( $("#template-reviews").html(), {item : item } ) );
					});
				}else{
					$("div.rest-reviews").append( $("p.noresults").css("display","block"));
				}
			}
		});
	}else if($("table.rest-favorites").length>0){
		$.get("/api/myaccount/restaurant/favorites.htm"+location.search,function(response){
			if($("#template-favorites").length>0){
				$("#page-ul").append( _.template( $("#template-page").html(), {page : response.page } ) );
				if(response && response.results.length>0){
					$.each( response.results, function(i, item) {
						$("table.rest-favorites").append( _.template( $("#template-favorites").html(), {item : item } ) );
					});
				}else{
					$("table.rest-favorites").append( $(".noresults").css("display","block") );
				}
			}
		});
	}
});
 