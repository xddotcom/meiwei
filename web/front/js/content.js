//start==remove auto-select
$.fn.typeahead.Constructor.prototype.render = function(items) {
     var that = this
     
     items = $(items).map(function (i, item) {
       i = $(that.options.item).attr('data-value', item)
       i.find('a').html(that.highlighter(item))
       return i[0]
     })
 
     this.$menu.html(items)
     return this
};
$.fn.typeahead.Constructor.prototype.select = function() {
    var val = this.$menu.find('.active').attr('data-value');
    if (val) {
      this.$element
        .val(this.updater(val))
        .change();
    }
    return this.hide()
};
//==end


//#pageSearch, #quickSearch
function nameList(name){
	$.get('/api/public/restaurant/nameList.htm', function( json ) {
		var names = [];
		$.each(json, function(i, item) {
		if($.inArray(item.fullName.chinese , names ,0 )<0){
			names.push(item.fullName.chinese);
		}
		if($.inArray(item.fullName.english , names ,0 )<0){
			names.push(item.fullName.english);
		}
	});
		$(name).typeahead({source: names });
	});
}

function contactValid(){
	 if( !$.trim($('#nickName').val()) ){
 		 $('#nickName').focus(); 
 		 $("#nickName").tooltip( {title : "请输入联系人名称" ,trigger : "manual"}) .tooltip("show");
		 return false; 
	 }else if( !$.trim($('#telphone').val())  ){
		 $("#nickName").tooltip("hide");
		 $('#telphone').focus(); 
		 $("#telphone").tooltip( {title : "手机号码格式错误" ,trigger : "manual"}) .tooltip("show");
		 return false;
	 }else if(!$('#telphone').val().match(/^13[0-9]{9}$|14[0-9]{9}|15[0-9]{9}$|18[0-9]{9}$/)){
		 $('#telphone').focus(); 
		 $("#telphone").tooltip( {title : "手机号码格式错误" ,trigger : "manual"}) .tooltip("show");
		 return false;
	 } else{
		 return true;
	 }
};

var PageLoad = {
	init : function(){
	 	PageLoad.initPlugins();
	},
	requireLogin: function() {
		if(GlobalVariable.loginstatus=="1"){
			return true;
		}else{
			$('#navbar-username').tooltip({
				toggle:'tooltip', 
				html: true, 
				title:'请先登录以后再操作', 
				placement: 'bottom'}
			);
			$('#navbar-username').focus();
			return false;
		}
	},
	form : {
		validationTipType : function( ) {}
	}, 
	home : {
		init : function() {
			PageLoad.init();
		},
		submitQuery: function () {
			$("#queryForm").submit();
		}
	},
	restaurantList: {
		init: function () {
			PageLoad.init();
		},
		orderBy: function (value) {
			var sort = {
			  	fullName:function(elea,eleb){
					if(elea && eleb){
						return $.trim($(elea).text()).localeCompare($.trim($(eleb).text()));
					}
					return -1;
			  	},
				score:function(elea,eleb){
					if(elea && eleb){
						return -( $.trim( $(elea).attr("value") ).localeCompare( $.trim( $(eleb).attr("value") ) ) );
					}
					return -1;
				},
				perBegin:function(elea,eleb){					
					if(elea && eleb){
						var a = $.trim( $(elea).text().replace(/[\D]/g,"") );
						var b = $.trim( $(eleb).text().replace(/[\D]/g,"") );
						a = $.isNumeric(a) ? new Number(a) : 0;
						b = $.isNumeric(b) ? new Number(b) : 0;
 						return b-a;
					}
					return -1;
				}
			}
			
			var data;
			if(value=="fullName") {
				data =$(".title");
				data.sort(sort.fullName);
			} else if(value=="perBegin") {
				data = $(".price");
				data.sort(sort.perBegin);
			} else if(value=="score") {
				data = $(".rating");
				data.sort(sort.score);
			}
			var array = new Array();
			data.each(function(index){
				array.push($(this).parents("div.single-restaurant").html());
			});
			var div =$("<div style='display:none'></div>");
			div.data("data",array);
			$("div.single-restaurant").each(function(index){
				$(this).html(array[index]);
			});
		},
		showOther: function () {
			var once = 5;
			$("div.disable").each(function (i) {
				var display = $(this).css("display");
				if (display == "none" && once >= 0) {
					$(this).css({
						display: "block"
					});
					once = once - 1;
				}
			});
			if (once == 5) {
				$("#view-more").hide();
			}
		},
		moreVisible: function (tag) {
			var descriptionPtag = $(tag).parent().parent().find("[id=descriptionPtag]");
			var moreAtag = $(tag).parent().find("[id=moreAtag]");
			var lessAtag = $(tag).parent().find("[id=lessAtag]");
			if ($(tag).attr("id") == "moreAtag") {
				$(descriptionPtag).css({
					'height': 'auto',
					'overflow': 'hidden'
				});
				$(moreAtag).css("display", "none");
				$(lessAtag).css("display", "");
			}
			else {
				$(descriptionPtag).css({
					'height': '40px',
					'overflow': 'hidden'
				});
				$(lessAtag).css("display", "none");
				$(moreAtag).css("display", "");
			}
			return false;
		}
	},
	restaurantProfile: {
		init: function () {
			$("#reviewForm").Validform({
				btnSubmit: "#reviewButton",
				ignoreHidden: false,
				showAllError: true,
				tiptype:function(){},
				beforeSubmit:function(){
					if(GlobalVariable.loginstatus=="1"){
						$.post("/api/restaurant/review/add.htm", $("#reviewForm").serialize(), function(data) {
							location.reload();
						});
						return false;
					}else{
						$("#navbar-username").focus();
						return false;
					}
				}
			});
			$(".rating-click .item").click(function () {
				if ($(this).attr("class") == "item") {
					$(this).parent().children().each(function (i) {
						$(this).attr("class", "item");
					});
					$(this).attr("class", "item clearItem");
				}
				var hiddenId = $(this).parent().attr("hiddenInput");
				$("#" + hiddenId).val(trim($(this).text()));
			});
			PageLoad.init();
		},
		initCookie:function( number , name ,id ){
			var history = getCookie("RESTAURANT_HASREAD");
			var view = new Array();
			if(history && history.length>0){
				view = history.split("[]");
			}
			if(number && name && history.indexOf(number)==-1){
				view.push( number +"||"+name+"||"+id );
			}

			var k = view.length>8 ?  8 : view.length;
 			for(i=0;i<k;i++){
				if(view[i]){
 					var ia = view[i].split("||");
					var html="";
					if(ia && ia.length>1 && ia[2] && ia[1]){
						html+="<li><a style='color: #aa4d4f;' target='_blank' href='/restaurant/view/"+ia[2]+"'>"+ia[1]+"</a> </li> ";
					}
					$("#my_restaurantsUL").append(html);
				}
			}
			setCookie("RESTAURANT_HASREAD", view.join("[]"), 365);
			
			
		},
		inFavorite: function (id) {
			$.ajax({
				url: "/api/restaurant/favorite/add.htm",
				type: "GET",
				data: "restaurantId=" + id,
				async: false,
				complete: function (str) {
					var result = str.responseText;
					var json;  
					if (typeof(JSON) == 'undefined'){  
						json = eval("("+result+")");  
					}else{  
						json = JSON.parse(result);  
					}
					if (json && json.status == "y" ) {
						$("#favoriteButton").css("display","none");
					}
				}
			});
		}
	},
	initLoginpanel: function () {
		$("#navbar-username, #navbar-password").keydown(function (e) {
			if (e.keyCode == 13) {
				$("#navbar-loginsubmit").click();
			}
		});
		$("#navbar-loginform").Validform({
			btnSubmit : "#navbar-loginsubmit",
			ajaxPost: true,
			tiptype : function(msg, o, cssctl) {
				$("#navbar-loginfail").css("display", "none");
				$("#navbar-loginfail").addClass("Validform_wrong");
			},callback:function(data){
 				if(data && data.status == "y"){
					GlobalVariable.loginstatus="1";
					var header = new HeaderView( {model : member} );
					member.fetch({ cache: false , type : "get" ,success : function( data ,response ){
						header.render();
					}}); 
					LoginVaildate.loginPanel.success();
				}else{
					LoginVaildate.loginPanel.sorry();
					$("#navbar-username").tooltip( {title : "用户名或密码错误，请重新输入" ,trigger : "manual"}) .tooltip("show");
				}
			}
		});
	},
	initPlugins: function () {
		$(".rating").each(function (i) {
			var score = $(this).attr("value");
			$(".full-star", this).each(function (k) {
				if (score < k + 1) {
					$(this).attr("class", "empty-star");
				}
			});
		});

		$(".carousel").each(function() {
	 		var time = 5000 * Math.random() + 3000;
	 		$(this).carousel({interval: time});
	 	});
		
		$('#datetimepicker').datetimepicker( {  
	 		todayBtn: false ,
	 		autoclose : true,
	 		minView: 'month' 
	  	}).datetimepicker('setStartDate',new Date()).on('changeDate', function(ev){
	  		
	  	});
	
		$('#datetimepicker_book').datetimepicker( {  
	 		todayBtn: false ,
	 		autoclose : true,
	 		minView: 'month' 
	  	}).datetimepicker('setStartDate',new Date()).on('changeDate', function(ev){
	  		resetTime( $(this).attr("restaurant-id") );
	  	});
		
		$('input, textarea').placeholder();
		$(".collapse").collapse();
	}
}

URL = { _home_header_quicksearch_url : "/restaurant/search/quick#C3" }

///================ jquery ready ============
$(document).ready(function () {
	var header = new HeaderView( {model : member} );
	member.fetch({ cache: false , type : "get" ,success : function( data ,response ){
		header.render();
	}});
	
	if($("#template-right-box-1").length>0){
		var cuisineListView = new CuisineListView();
		var circleListView = new CircleListView();
		var districtListView = new DistrictListView();
	}
});
///=======================





